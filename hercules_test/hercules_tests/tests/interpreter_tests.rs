use std::env;

use hercules_interpreter::*;
use hercules_interpreter::*;
use hercules_ir::ID;
use juno_scheduler::ir::*;
use juno_scheduler::pass;

use juno_scheduler::{default_schedule, run_schedule_on_hercules};
use rand::Rng;

#[test]
fn twodeefork() {
    let module = parse_file("../test_inputs/2d_fork.hir");
    let d1 = 2;
    let d2 = 3;
    let dyn_consts = [d1, d2];
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    let sched = Some(default_schedule![
        Verify, ForkSplit, //Xdot,
        Unforkify, //Xdot,
        DCE, Verify,
    ]);

    let module = run_schedule_on_hercules(module, sched).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, 2);

    let res = (d1 as i32 * d2 as i32);
    let result_2: InterpreterWrapper = res.into();
    println!("result: {:?}", result_1); // Should be d1 * d2.
}

#[test]
fn threedee() {
    let module = parse_file("../test_inputs/3d_fork.hir");
    let d1 = 2;
    let d2 = 3;
    let d3 = 5;
    let dyn_consts = [d1, d2, 5];
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    let sched = Some(default_schedule![
        Verify, ForkSplit, //Xdot,
        Unforkify, //Xdot,
        DCE, Verify,
    ]);

    let module = run_schedule_on_hercules(module, sched).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, 2);

    let res = (d1 as i32 * d2 as i32 * d3 as i32);
    let result_2: InterpreterWrapper = res.into();
    println!("result: {:?}", result_1); // Should be d1 * d2.
}

#[test]
fn fivedeefork() {
    let module = parse_file("../test_inputs/5d_fork.hir");
    let dyn_consts = [1, 2, 3, 4, 5];
    let result_1 = interp_module!(module, 0, dyn_consts, 2);
    println!("result: {:?}", result_1); // Should be 1 * 2 * 3 * 4 * 5;
}
