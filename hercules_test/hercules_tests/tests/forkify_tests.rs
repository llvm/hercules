use std::{env, fs::File, io::Read, path::Path};

use hercules_interpreter::*;
use hercules_ir::ID;

use hercules_interpreter::*;
use juno_scheduler::ir::*;
use juno_scheduler::pass;

use juno_scheduler::{default_schedule, run_schedule_on_hercules};
use rand::Rng;

#[test]
#[ignore]
fn inner_fork_chain() {
    let module = parse_file("../test_inputs/forkify/inner_fork_chain.hir");
    let dyn_consts = [10];
    let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
                    // let result_1 = interp_module!(module, 0, dyn_consts, 2);

    let sched: Option<ScheduleStmt> = Some(default_schedule![Verify, Forkify, PhiElim, Verify,]);

    let module = run_schedule_on_hercules(module, sched).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    println!("result: {:?}", result_2);
    //assert_eq!(result_1, result_2)
}

#[test]
fn loop_simple_iv() {
    let module = parse_file("../test_inputs/forkify/loop_simple_iv.hir");
    let dyn_consts = [10];
    let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);

    let sched: Option<ScheduleStmt> = Some(default_schedule![Verify, Forkify, Verify,]);

    let module = run_schedule_on_hercules(module, sched).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    println!("result: {:?}", result_2);
    assert_eq!(result_1, result_2)
}

#[test]
#[ignore]
fn merged_phi_cycle() {
    let module = parse_file("../test_inputs/forkify/merged_phi_cycle.hir");
    let dyn_consts = [10];
    let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);

    let sched: Option<ScheduleStmt> = Some(default_schedule![Verify, Verify,]);

    let module = run_schedule_on_hercules(module, sched).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    println!("result: {:?}", result_2);
    assert_eq!(result_1, result_2)
}

#[test]
fn split_phi_cycle() {
    let module = parse_file("../test_inputs/forkify/split_phi_cycle.hir");
    let dyn_consts = [10];
    let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);

    let sched: Option<ScheduleStmt> = Some(default_schedule![Verify, Verify,]);

    let module = run_schedule_on_hercules(module, sched).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    println!("result: {:?}", result_2);
    assert_eq!(result_1, result_2)
}

#[test]
fn loop_sum() {
    let module = parse_file("../test_inputs/forkify/loop_sum.hir");
    let dyn_consts = [20];
    let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    assert_eq!(result_1, result_2);
    println!("{:?}, {:?}", result_1, result_2);
}

#[test]
fn loop_tid_sum() {
    let module = parse_file("../test_inputs/forkify/loop_tid_sum.hir");
    let dyn_consts = [20];
    let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    assert_eq!(result_1, result_2);
    println!("{:?}, {:?}", result_1, result_2);
}

#[test]
fn loop_array_sum() {
    let module = parse_file("../test_inputs/forkify/loop_array_sum.hir");
    let len = 5;
    let dyn_consts = [len];
    let params = vec![1, 2, 3, 4, 5]; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, params.clone());

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, params);
    assert_eq!(result_1, result_2);
    println!("{:?}, {:?}", result_1, result_2);
}

/** Nested loop 2 is 2 nested loops with different dyn var parameter dimensions.
 * It is a add of 1 for each iteration, so the result should be dim1 x dim2
 * The loop PHIs are structured such that on every outer iteration, inner loop increment is set to the running sum,
 * Notice how there is no outer_var_inc.
 *
 * The alternative, seen in nested_loop1, is to intiailize the inner loop to 0 every time, and track
 * the outer sum more separaetly.
 *
 * Idk what im yapping about.
*/
#[test]
fn nested_loop2() {
    let module = parse_file("../test_inputs/forkify/nested_loop2.hir");
    let len = 5;
    let dyn_consts = [5, 6];
    let params = vec![1, 2, 3, 4, 5]; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    assert_eq!(result_1, result_2);
}

#[test]
fn super_nested_loop() {
    let module = parse_file("../test_inputs/forkify/super_nested_loop.hir");
    let len = 5;
    let dyn_consts = [5, 10, 15];
    let params = vec![1, 2, 3, 4, 5]; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    assert_eq!(result_1, result_2);
}

/**
 * Tests forkify on a loop where there is control in between the continue projection
 * and the header. aka control *after* the `loop condition / guard`. This should forkify.
 */
#[test]
fn control_after_condition() {
    let module = parse_file("../test_inputs/forkify/control_after_condition.hir");

    let size = 10;
    let dyn_consts = [size];
    let mut vec = vec![0; size];
    let mut rng = rand::thread_rng();

    for x in vec.iter_mut() {
        *x = rng.gen::<i32>() / 100;
    }

    let result_1 = interp_module!(module, 0, dyn_consts, vec.clone());

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, vec);
    assert_eq!(result_1, result_2);
}

/**
 * Tests forkify on a loop where there is control before the loop condition, so in between the header
 * and the loop condition. This should not forkify.
 *
 * This example is bugged, it reads out of bounds even before forkify.
 */
#[ignore]
#[test]
fn control_before_condition() {
    let module = parse_file("../test_inputs/forkify/control_before_condition.hir");

    let size = 11;
    let dyn_consts = [size - 1];
    let mut vec = vec![0; size];
    let mut rng = rand::thread_rng();

    for x in vec.iter_mut() {
        *x = rng.gen::<i32>() / 100;
    }

    let result_1 = interp_module!(module, 0, dyn_consts, vec.clone());

    println!("result: {:?}", result_1);

    let sched: Option<ScheduleStmt> = Some(default_schedule![Verify, Forkify, DCE, Verify,]);

    let module = run_schedule_on_hercules(module, sched).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, vec);
    assert_eq!(result_1, result_2);
}

#[test]
fn nested_tid_sum() {
    let module = parse_file("../test_inputs/forkify/nested_tid_sum.hir");
    let len = 5;
    let dyn_consts = [5, 6];
    let params = vec![1, 2, 3, 4, 5]; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);

    let sched: Option<ScheduleStmt> = Some(default_schedule![Verify, Forkify, DCE, Verify,]);

    let module = run_schedule_on_hercules(module, sched).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    assert_eq!(result_1, result_2);

    let sched: Option<ScheduleStmt> = Some(default_schedule![Verify, Forkify, DCE, Verify,]);

    let module = run_schedule_on_hercules(module, sched).unwrap();
    let result_3 = interp_module!(module, 0, dyn_consts, 2);

    println!("{:?}, {:?}, {:?}", result_1, result_2, result_3);
}

#[test]
fn nested_tid_sum_2() {
    let module = parse_file("../test_inputs/forkify/nested_tid_sum_2.hir");
    let len = 5;
    let dyn_consts = [5, 6];
    let params = vec![1, 2, 3, 4, 5]; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);

    let sched: Option<ScheduleStmt> = Some(default_schedule![Verify, Forkify, DCE, Verify,]);

    let module = run_schedule_on_hercules(module, sched).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    assert_eq!(result_1, result_2);

    let sched: Option<ScheduleStmt> = Some(default_schedule![Verify, Forkify, DCE, Verify,]);

    let module = run_schedule_on_hercules(module, sched).unwrap();
    let result_3 = interp_module!(module, 0, dyn_consts, 2);

    println!("{:?}, {:?}, {:?}", result_1, result_2, result_3);
}

/** Tests weird control in outer loop for possible 2d fork-join pair. */
#[test]
fn inner_fork_complex() {
    let module = parse_file("../test_inputs/forkify/inner_fork_complex.hir");
    let dyn_consts = [5, 6];
    let params = vec![1, 2, 3, 4, 5]; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 10);

    println!("result: {:?}", result_1);

    let sched: Option<ScheduleStmt> = Some(default_schedule![Verify, Forkify, DCE, Verify,]);

    let module = run_schedule_on_hercules(module, sched).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, 10);
    assert_eq!(result_1, result_2);
    println!("{:?}, {:?}", result_1, result_2);
}
