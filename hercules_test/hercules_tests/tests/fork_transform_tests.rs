use std::{env, fs::File, io::Read, path::Path};

use hercules_interpreter::*;
use hercules_ir::ID;
use juno_scheduler::ir::*;

use juno_scheduler::pass;
use juno_scheduler::{default_schedule, run_schedule_on_hercules};
use rand::Rng;

#[test]
fn fission_simple1() {
    let module = parse_file("../test_inputs/fork_transforms/fork_fission/simple1.hir");
    let dyn_consts = [10];
    let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);

    let sched = Some(default_schedule![
        Verify,    //Xdot,
        Unforkify, //Xdot,
        DCE, Verify,
    ]);

    let module = run_schedule_on_hercules(module, sched).unwrap();
    let result_2 = interp_module!(module, 0, dyn_consts, 2);
    println!("result: {:?}", result_2);
    assert_eq!(result_1, result_2)
}

// #[test]
// fn fission_simple2() {
//     let module = parse_file("../test_inputs/fork_transforms/fork_fission/simple2.hir");
//     let dyn_consts = [10];
//     let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
//     let result_1 = interp_module!(module, 0,  dyn_consts, 2);

//     println!("result: {:?}", result_1);

//     let sched: Option<ScheduleStmt> = Some(default_schedule![
//         Verify,
//         ForkFission,
//         DCE,
//         Verify,
//     ]);

//     let module = run_schedule_on_hercules(module, sched).unwrap();
//     let result_2 = interp_module!(module, 0,  dyn_consts, 2);
//     println!("result: {:?}", result_2);
//     assert_eq!(result_1, result_2)
// }

// #[ignore] // Wait
// #[test]
// fn fission_tricky() {
//     // This either crashes or gives wrong result depending on the order which reduces are observed in.
//     let module = parse_file("../test_inputs/fork_transforms/fork_fission/tricky.hir");
//     let dyn_consts = [10];
//     let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
//     let result_1 = interp_module!(module, 0,  dyn_consts, 2);

//     println!("result: {:?}", result_1);

//     let sched: Option<ScheduleStmt> = Some(default_schedule![
//         Verify,
//         ForkFission,
//         DCE,
//         Verify,
//     ]);

//     let module = run_schedule_on_hercules(module, sched).unwrap();
//     let result_2 = interp_module!(module, 0,  dyn_consts, 2);
//     println!("result: {:?}", result_2);
//     assert_eq!(result_1, result_2)
// }

// #[ignore]
// #[test]
// fn inner_loop() {
//     let module = parse_file("../test_inputs/fork_transforms/fork_fission/inner_loop.hir");
//     let dyn_consts = [10, 20];
//     let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
//     let result_1 = interp_module!(module, 0,  dyn_consts, 2);

//     println!("result: {:?}", result_1);

//     let sched: Option<ScheduleStmt> = Some(default_schedule![
//         Verify,
//         ForkFission,
//         DCE,
//         Verify,
//     ]);

//     let module = run_schedule_on_hercules(module, sched).unwrap();
//     let result_2 = interp_module!(module, 0,  dyn_consts, 2);
//     println!("result: {:?}", result_2);
//     assert_eq!(result_1, result_2)
// }
