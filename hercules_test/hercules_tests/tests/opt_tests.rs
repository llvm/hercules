use std::env;

use rand::Rng;

use hercules_interpreter::*;
use hercules_ir::ID;
use juno_scheduler::*;

// #[test]
// fn matmul_int() {
//     let module = parse_file("../test_inputs/matmul_int.hir");
//     let dyn_consts = [2, 2, 2];
//     let m1 = vec![3, 4, 5, 6];
//     let m2 = vec![7, 8, 9, 10];
//     let result_1 = interp_module!(module, 0,  dyn_consts, m1.clone(), m2.clone());

//     let mut pm = hercules_opt::pass::PassManager::new(module.clone());

//     let passes = vec![
//         // Pass::Verify,
//         // Pass::CCP,
//         // Pass::DCE,
//         // Pass::GVN,
//         // Pass::DCE,
//         // Pass::Forkify,
//         // Pass::DCE,
//         // Pass::Predication,
//         // Pass::DCE,
//     ];

//     for pass in passes {
//         pm.add_pass(pass);
//     }
//     pm.run_passes();

//     let module = pm.get_module();
//     let result_2 = interp_module!(module, 0,  dyn_consts, m1, m2);
//     // println!("result: {:?}", result_1);
//     assert_eq!(result_1, result_2)
// }

// #[test]
// fn ccp_example() {
//     let module = parse_file("../test_inputs/ccp_example.hir");
//     let dyn_consts = [];
//     let x = 34;
//     let result_1 = interp_module!(module, 0,  dyn_consts, x);

//     let mut pm = hercules_opt::pass::PassManager::new(module.clone());

//     let passes = vec![
//         Pass::Verify,
//         Pass::CCP,
//         Pass::DCE,
//         Pass::GVN,
//         Pass::DCE,
//         Pass::Forkify,
//         Pass::DCE,
//         Pass::Predication,
//         Pass::DCE,
//     ];

//     for pass in passes {
//         pm.add_pass(pass);
//     }
//     pm.run_passes();

//     let module = pm.get_module();
//     let result_2 = interp_module!(module, 0,  dyn_consts, x);
//     assert_eq!(result_1, result_2)
// }

// #[test]
// fn gvn_example() {
//     let module = parse_file("../test_inputs/gvn_example.hir");

//     let dyn_consts = [];
//     let x: i32 = rand::random();
//     let x = x / 32;
//     let y: i32 = rand::random();
//     let y = y / 32; // prevent overflow,
//     let result_1 = interp_module!(module, 0,  dyn_consts, x, y);

//     let mut pm = hercules_opt::pass::PassManager::new(module.clone());

//     let passes = vec![
//         Pass::Verify,
//         Pass::CCP,
//         Pass::DCE,
//         Pass::GVN,
//         Pass::DCE,
//         Pass::Forkify,
//         Pass::DCE,
//         Pass::Predication,
//         Pass::DCE,
//     ];

//     for pass in passes {
//         pm.add_pass(pass);
//     }
//     pm.run_passes();

//     let module = pm.get_module();
//     let result_2 = interp_module!(module, 0,  dyn_consts, x, y);
//     assert_eq!(result_1, result_2)
// }

// #[test]
// fn sum_int() {
//     let module = parse_file("../test_inputs/sum_int1.hir");

//     let size = 2;
//     let dyn_consts = [size];
//     let mut vec = vec![0; size];
//     let mut rng = rand::thread_rng();

//     for x in vec.iter_mut() {
//         *x = rng.gen::<i32>() / 100;
//     }

//     println!("{:?}", vec);

//     let result_1 = interp_module!(module, 0,  dyn_consts, vec.clone());

//     println!("{:?}", result_1);

//     let mut pm = hercules_opt::pass::PassManager::new(module.clone());

//     let passes = vec![
//         Pass::Verify,
//         Pass::CCP,
//         Pass::DCE,
//         Pass::GVN,
//         Pass::DCE,
//         Pass::Forkify,
//         Pass::DCE,
//         Pass::Predication,
//         Pass::DCE,
//     ];

//     for pass in passes {
//         pm.add_pass(pass);
//     }
//     pm.run_passes();

//     let module = pm.get_module();
//     let result_2 = interp_module!(module, 0,  dyn_consts, vec);

//     assert_eq!(result_1, result_2)
// }

// #[test]
// fn sum_int2() {
//     let module = parse_file("../test_inputs/sum_int2.hir");

//     let size = 10;
//     let dyn_consts = [size];
//     let mut vec = vec![0; size];
//     let mut rng = rand::thread_rng();

//     for x in vec.iter_mut() {
//         *x = rng.gen::<i32>() / 100;
//     }

//     let result_1 = interp_module!(module, 0,  dyn_consts, vec.clone());

//     let mut pm = hercules_opt::pass::PassManager::new(module.clone());

//     let passes = vec![
//         Pass::Verify,
//         Pass::CCP,
//         Pass::DCE,
//         Pass::GVN,
//         Pass::DCE,
//         Pass::Forkify,
//         Pass::DCE,
//         Pass::Predication,
//         Pass::DCE,
//     ];

//     for pass in passes {
//         pm.add_pass(pass);
//     }
//     pm.run_passes();

//     let module = pm.get_module();
//     let result_2 = interp_module!(module, 0,  dyn_consts, vec);
//     assert_eq!(result_1, result_2)
// }

// #[test]
// fn sum_int2_smaller() {
//     interp_file_with_passes!("../test_inputs/sum_int2.hir",
//     [100],
//     vec![
//         Pass::Verify,
//         Pass::CCP,
//         Pass::DCE,
//         Pass::GVN,
//         Pass::DCE,
//         Pass::Forkify,
//         Pass::DCE,
//         Pass::Predication,
//         Pass::DCE,
//     ],
//     vec![1; 100]);
// }
