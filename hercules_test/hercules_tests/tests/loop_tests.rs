use std::{env, fs::File, io::Read, path::Path};

use hercules_interpreter::*;
use hercules_ir::ID;
use juno_scheduler::ir::*;
use juno_scheduler::pass;

use juno_scheduler::{default_schedule, run_schedule_on_hercules};
use rand::random;
use rand::Rng;

// Tests canonicalization

#[ignore]
#[test]
fn loop_trip_count() {
    let module = parse_file("../test_inputs/loop_analysis/loop_trip_count.hir");
    let dyn_consts = [10];
    let params = 2; // TODO: (@xrouth) fix macro to take no params as an option.
    let result_1 = interp_module!(module, 0, dyn_consts, 2);

    println!("result: {:?}", result_1);
}

// Test canonicalization
#[test]
#[ignore]
fn alternate_bounds_use_after_loop_no_tid() {
    let len = 1;
    let dyn_consts = [len];

    let module =
        parse_file("../test_inputs/loop_analysis/alternate_bounds_use_after_loop_no_tid.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, 3);

    println!("result: {:?}", result_1);

    let schedule = default_schedule![Forkify,];

    let module = run_schedule_on_hercules(module, Some(schedule)).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 3);
    println!("{:?}", result_1);
    println!("{:?}", result_2);

    assert_eq!(result_1, result_2);
}

// Test canonicalization
#[test]
#[ignore]
fn alternate_bounds_use_after_loop() {
    let len = 4;
    let dyn_consts = [len];

    let a = vec![3, 4, 5, 6];
    let module = parse_file("../test_inputs/loop_analysis/alternate_bounds_use_after_loop.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, a.clone());

    println!("result: {:?}", result_1);

    let schedule = Some(default_schedule![Forkify,]);

    let module = run_schedule_on_hercules(module, schedule).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, a.clone());
    println!("{:?}", result_2);

    assert_eq!(result_1, result_2);
}

// Test canonicalization
#[test]
#[ignore]
fn alternate_bounds_use_after_loop2() {
    let len = 4;
    let dyn_consts = [len];

    let a = vec![3, 4, 5, 6];
    let module = parse_file("../test_inputs/loop_analysis/alternate_bounds_use_after_loop2.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, a.clone());

    println!("result: {:?}", result_1);

    let schedule = Some(default_schedule![]);

    let module = run_schedule_on_hercules(module, schedule).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, a.clone());
    println!("{:?}", result_2);

    assert_eq!(result_1, result_2);
}

// Test canonicalization
#[test]
fn do_while_separate_body() {
    let len = 2;
    let dyn_consts = [len];

    let a = vec![3, 4, 5, 6];
    let module = parse_file("../test_inputs/loop_analysis/do_while_separate_body2.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, 2i32);

    println!("result: {:?}", result_1);

    let schedule = Some(default_schedule![PhiElim, Forkify,]);

    let module = run_schedule_on_hercules(module, schedule).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 2i32);
    println!("{:?}", result_2);

    assert_eq!(result_1, result_2);
}

#[test]
fn alternate_bounds_internal_control() {
    let len = 4;
    let dyn_consts = [len];

    let module = parse_file("../test_inputs/loop_analysis/alternate_bounds_internal_control.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, 3);

    println!("result: {:?}", result_1);

    let schedule = Some(default_schedule![PhiElim, Forkify,]);

    let module = run_schedule_on_hercules(module, schedule).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 3);
    println!("{:?}", result_1);
    println!("{:?}", result_2);

    assert_eq!(result_1, result_2);
}

#[test]
fn alternate_bounds_internal_control2() {
    let len = 2;
    let dyn_consts = [len];

    let module = parse_file("../test_inputs/loop_analysis/alternate_bounds_internal_control2.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, 3);

    println!("result: {:?}", result_1);

    let schedule = Some(default_schedule![PhiElim, Forkify,]);

    let module = run_schedule_on_hercules(module, schedule).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 3);
    println!("{:?}", result_1);
    println!("{:?}", result_2);

    assert_eq!(result_1, result_2);
}

#[test]
fn alternate_bounds_nested_do_loop() {
    let len = 1;
    let dyn_consts = [10, 5];

    let module = parse_file("../test_inputs/loop_analysis/alternate_bounds_nested_do_loop.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, 3);

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 3);
    println!("{:?}", result_1);
    println!("{:?}", result_2);

    assert_eq!(result_1, result_2);
}

#[test]
fn alternate_bounds_nested_do_loop_array() {
    let len = 1;
    let dyn_consts = [10, 5];

    let a = vec![4u64, 4, 4, 4, 4];
    let module =
        parse_file("../test_inputs/loop_analysis/alternate_bounds_nested_do_loop_array.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, a.clone());

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, a);
    println!("{:?}", result_1);
    println!("{:?}", result_2);

    assert_eq!(result_1, result_2);
}

#[test]
fn alternate_bounds_nested_do_loop_guarded() {
    let len = 1;
    let dyn_consts = [3, 2];

    let module =
        parse_file("../test_inputs/loop_analysis/alternate_bounds_nested_do_loop_guarded.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, 3);

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 3);
    println!("{:?}", result_1);
    println!("{:?}", result_2);

    assert_eq!(result_1, result_2);

    let mut pm = PassManager::new(module.clone());

    let module = run_schedule_on_hercules(module, None).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 3);
    println!("{:?}", result_1);
    println!("{:?}", result_2);

    assert_eq!(result_1, result_2);
}

// Tests a do while loop that only iterates once,
// canonicalization *should not* transform this to a while loop, as there is no
// guard that replicates the loop condition.
#[ignore]
#[test]
fn do_loop_not_continued() {
    // let len = 1;
    // let dyn_consts = [len];
    // let params = vec![1, 2, 3, 4, 5];

    // let module = parse_file("../test_inputs/loop_analysis/alternate_bounds_use_after_loop.hir");
    // let result_1 = interp_module!(module, 0,dyn_consts, params);

    // println!("result: {:?}", result_1);
}

// Tests a do while loop that is guarded, so should be canonicalized
// It also has
#[test]
fn do_loop_complex_immediate_guarded() {
    let len = 1;
    let dyn_consts = [len];

    let module = parse_file("../test_inputs/loop_analysis/do_loop_immediate_guard.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, 3);

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 3);
    assert_eq!(result_1, result_2);
}

#[ignore]
#[test]
fn loop_canonical_sum() {
    let len = 1;
    let dyn_consts = [len];
    let params = vec![1, 2, 3, 4, 5];

    let module = parse_file("../test_inputs/loop_analysis/loop_array_sum.hir");
    let result_1 = interp_module!(module, 0, dyn_consts, params);

    println!("result: {:?}", result_1);
}

#[test]
#[ignore]
fn antideps_pipeline() {
    let len = 1;
    let dyn_consts = [2, 2, 2];

    // FIXME: This path should not leave the crate
    let module = parse_module_from_hbin("../../juno_samples/antideps/antideps.hbin");
    let result_1 = interp_module!(module, 0, dyn_consts, 9i32);

    println!("result: {:?}", result_1);

    let module = run_schedule_on_hercules(module, None).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 9i32);
    assert_eq!(result_1, result_2);
}

#[test]
#[ignore]
fn implicit_clone_pipeline() {
    let len = 1;
    let dyn_consts = [2, 2, 2];

    // FIXME: This path should not leave the crate
    let module = parse_module_from_hbin("../../juno_samples/implicit_clone/out.hbin");
    let result_1 = interp_module!(module, 0, dyn_consts, 2u64, 2u64);

    println!("result: {:?}", result_1);
    let schedule = default_schedule![
        Forkify,
        ForkGuardElim,
        Forkify,
        ForkGuardElim,
        Forkify,
        ForkGuardElim,
        DCE,
        ForkSplit,
        Unforkify,
        GVN,
        DCE,
        DCE,
        AutoOutline,
        InterproceduralSROA,
        SROA,
        InferSchedules,
        DCE,
        GCM,
        DCE,
        FloatCollections,
        GCM,
    ];
    let module = run_schedule_on_hercules(module, Some(schedule)).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, 2u64, 2u64);
    assert_eq!(result_1, result_2);
}

#[test]
#[ignore]
fn look_at_local() {
    const I: usize = 4;
    const J: usize = 4;
    const K: usize = 4;
    let a: Vec<i32> = (0..I * J).map(|_| random::<i32>() % 100).collect();
    let b: Vec<i32> = (0..J * K).map(|_| random::<i32>() % 100).collect();
    let dyn_consts = [I, J, K];
    let mut correct_c: Box<[i32]> = (0..I * K).map(|_| 0).collect();
    for i in 0..I {
        for k in 0..K {
            for j in 0..J {
                correct_c[i * K + k] += a[i * J + j] * b[j * K + k];
            }
        }
    }

    let module = parse_module_from_hbin(
        "/home/xavierrouth/dev/hercules/hercules_test/hercules_tests/save_me.hbin",
    );

    let schedule = Some(default_schedule![]);

    let result_1 = interp_module!(module, 0, dyn_consts, a.clone(), b.clone());

    let module = run_schedule_on_hercules(module.clone(), schedule).unwrap();

    let schedule = Some(default_schedule![Unforkify, Verify,]);

    let module = run_schedule_on_hercules(module.clone(), schedule).unwrap();

    let result_2 = interp_module!(module, 0, dyn_consts, a.clone(), b.clone());

    println!("golden: {:?}", correct_c);
    println!("result: {:?}", result_2);
}
#[test]
#[ignore]
fn matmul_pipeline() {
    let len = 1;

    const I: usize = 4;
    const J: usize = 4;
    const K: usize = 4;
    let a: Vec<i32> = (0i32..(I * J) as i32).map(|v| v + 1).collect();
    let b: Vec<i32> = ((I * J) as i32..(J * K) as i32 + (I * J) as i32)
        .map(|v| v + 1)
        .collect();
    let a: Vec<i32> = (0..I * J).map(|_| random::<i32>() % 100).collect();
    let b: Vec<i32> = (0..J * K).map(|_| random::<i32>() % 100).collect();
    let dyn_consts = [I, J, K];

    // FIXME: This path should not leave the crate
    let mut module = parse_module_from_hbin("../../juno_samples/matmul/out.hbin");
    //
    let mut correct_c: Box<[i32]> = (0..I * K).map(|_| 0).collect();
    for i in 0..I {
        for k in 0..K {
            for j in 0..J {
                correct_c[i * K + k] += a[i * J + j] * b[j * K + k];
            }
        }
    }

    let result_1 = interp_module!(module, 1, dyn_consts, a.clone(), b.clone());

    // println!("golden: {:?}", correct_c);
    // println!("result: {:?}", result_1);

    // let InterpreterVal::Array(_, d) = result_1.clone() else {
    //     panic!()
    // };
    // let InterpreterVal::Integer32(value) = d[0] else {
    //     panic!()
    // };
    // assert_eq!(correct_c[0], value);

    let schedule = Some(default_schedule![Xdot, Verify,]);

    module = run_schedule_on_hercules(module, schedule).unwrap();

    let result_2 = interp_module!(module, 1, dyn_consts, a.clone(), b.clone());

    println!("result: {:?}", result_2);
    assert_eq!(result_1, result_2);
}
