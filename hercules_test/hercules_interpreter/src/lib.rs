pub mod interpreter;
pub mod value;

use std::fs::File;
use std::io::Read;

use hercules_ir::Module;
use hercules_ir::TypeID;
use hercules_ir::ID;

pub use juno_scheduler::PassManager;

pub use crate::interpreter::*;
pub use crate::value::*;

// Get a vec of
pub fn into_interp_val(
    module: &Module,
    wrapper: InterpreterWrapper,
    target_ty_id: TypeID,
) -> InterpreterVal {
    match wrapper {
        InterpreterWrapper::Boolean(v) => InterpreterVal::Boolean(v),
        InterpreterWrapper::Integer8(v) => InterpreterVal::Integer8(v),
        InterpreterWrapper::Integer16(v) => InterpreterVal::Integer16(v),
        InterpreterWrapper::Integer32(v) => InterpreterVal::Integer32(v),
        InterpreterWrapper::Integer64(v) => InterpreterVal::Integer64(v),
        InterpreterWrapper::UnsignedInteger8(v) => InterpreterVal::UnsignedInteger8(v),
        InterpreterWrapper::UnsignedInteger16(v) => InterpreterVal::UnsignedInteger16(v),
        InterpreterWrapper::UnsignedInteger32(v) => InterpreterVal::UnsignedInteger32(v),
        InterpreterWrapper::UnsignedInteger64(v) => InterpreterVal::UnsignedInteger64(v),
        InterpreterWrapper::Float32(v) => InterpreterVal::Float32(v),
        InterpreterWrapper::Float64(v) => InterpreterVal::Float64(v),
        InterpreterWrapper::Product(_) => todo!(),
        InterpreterWrapper::Summation(_, _) => todo!(),

        InterpreterWrapper::Array(array) => {
            let ty = &module.types[target_ty_id.idx()];
            ty.try_element_type()
                .expect("PANIC: Invalid parameter type");

            let mut values = vec![];

            for i in 0..array.len() {
                values.push(into_interp_val(module, array[i].clone(), TypeID::new(0)));
            }

            InterpreterVal::Array(target_ty_id, values.into_boxed_slice())
        }
    }
}

// Recursively turns rt args into interpreter wrappers.
#[macro_export]
macro_rules! parse_rt_args {
    ($arg:expr) => {
        {

            let mut values: Vec<InterpreterWrapper> = vec![];

            values.push(($arg).into());

            values
        }
    };
    ( $arg:expr, $($tail_args:expr), +) => {
        {
            let mut values: Vec<InterpreterWrapper> = vec![];

            values.push($arg.into());

            let mut recurse = parse_rt_args!($($tail_args), +); // How do you pass the rest?
            values.append(&mut recurse);
            values
        }
    };
}

pub fn parse_file(path: &str) -> Module {
    let mut file = File::open(path).expect("PANIC: Unable to open input file.");
    let mut contents = String::new();
    std::io::Read::read_to_string(&mut file, &mut contents)
        .expect("PANIC: Unable to read input file contents.");
    let module =
        hercules_ir::parse::parse(&contents).expect("PANIC: Failed to parse Hercules IR file.");
    module
}

pub fn parse_module_from_hbin(path: &str) -> hercules_ir::ir::Module {
    let mut file = File::open(path).expect("PANIC: Unable to open input file.");
    let mut buffer = vec![];
    file.read_to_end(&mut buffer).unwrap();
    postcard::from_bytes(&buffer).unwrap()
}

#[macro_export]
macro_rules! interp_module {
    ($module:ident, $entry_func:expr, $dynamic_constants:expr, $($args:expr), *) => {
        {
            //let hir_file = String::from($path);

            let wrappers = parse_rt_args!( $($args), *);

            let dynamic_constants: Vec<usize> = $dynamic_constants.into();
            let module = $module.clone(); //parse_file(hir_file);

            let mut pm = PassManager::new(module);
            pm.make_typing();
            pm.make_reverse_postorders();
            pm.make_doms();
            pm.make_fork_join_maps();
            pm.make_fork_join_nests();
            pm.make_control_subgraphs();

            let reverse_postorders = pm.reverse_postorders.as_ref().unwrap().clone();
            let doms = pm.doms.as_ref().unwrap().clone();
            let fork_join_maps = pm.fork_join_maps.as_ref().unwrap().clone();
            let fork_join_nests = pm.fork_join_nests.as_ref().unwrap().clone();
            let control_subgraphs = pm.control_subgraphs.as_ref().unwrap().clone();
            let def_uses = pm.def_uses.as_ref().unwrap().clone();

            let module = pm.get_module();
            let mut function_contexts = vec![];

            for idx in 0..module.functions.len() {
                let context = FunctionContext::new(
                    &control_subgraphs[idx],
                    &def_uses[idx],
                    &fork_join_maps[idx],
                    &fork_join_nests[idx],
                );
                function_contexts.push(context);
            }

            let function_number = $entry_func;

            let parameter_types = &module.functions[function_number].param_types;

            let args = wrappers.into_iter().enumerate().map(|(idx, val)| {into_interp_val(&module, val, parameter_types[idx])}).collect();

            let mut state = FunctionExecutionState::new(
                args,
                &module,
                hercules_ir::FunctionID::new(function_number),
                &function_contexts,
                dynamic_constants,
            );

            state.run()
        }
    };
}

#[macro_export]
macro_rules! interp_file_with_passes {
    ($path:literal, $dynamic_constants:expr, $passes:expr, $($args:expr), *) => {
        {
            let module = parse_file($path);

            let result_before = interp_module!(module, $dynamic_constants, $($args), *);

            let module = run_schedule_on_hercules(module, None).unwrap();

            let result_after = interp_module!(module, $dynamic_constants, $($args), *);

            assert_eq!(result_after, result_before);
        }
    };
}
