use std::collections::hash_map::Entry::Occupied;
use std::collections::HashMap;
use std::panic;

use itertools::Itertools;
use std::cmp::{max, min};

use hercules_ir::*;

use crate::value;
use value::*;

const VERBOSE: bool = false;

/* High level design details / discussion for this:
 *
 * This crate includes tools for interpreting a hercules IR module. Execution model / flow is based on
 * "A Simple Graph-Based Intermediate Representation" Section 3 by Cliff Click.
 *
 * In the future we would an *interactive* interpreter. I.e to be able to step forward and backwards
 * throughout the IR (idealy visually) and analyze results at certain steps in the computation.
 */
pub struct FunctionExecutionState<'a> {
    args: Vec<InterpreterVal>, // parameters

    // Map a vec of (thread indicies, reduce_node) -> Value
    // Reduction values are shared across the fork / join pair that drives them, so they can't be local to a control token.
    reduce_values: HashMap<(Vec<usize>, NodeID), InterpreterVal>, // Map (thread indicies, reduction node) -> value
    join_counters: HashMap<(Vec<usize>, NodeID), usize>, // Map (thread indicies, join_node) -> join_counter. Counters until the joins are done!
    dynamic_constant_params: Vec<usize>,

    // Per module State.
    module: &'a Module,
    function_contexts: &'a Vec<FunctionContext<'a>>,

    function_id: FunctionID,
}

pub struct FunctionContext<'a> {
    control_subgraph: &'a Subgraph,
    def_use: &'a ImmutableDefUseMap,
    fork_join_map: &'a HashMap<NodeID, NodeID>, // Map forks -> joins
    fork_join_nest: &'a HashMap<NodeID, Vec<NodeID>>,
}

impl<'a> FunctionContext<'a> {
    pub fn new(
        control_subgraph: &'a Subgraph,
        def_use: &'a ImmutableDefUseMap,
        fork_join_map: &'a HashMap<NodeID, NodeID>, // Map forks -> joins
        fork_join_nest: &'a HashMap<NodeID, Vec<NodeID>>,
    ) -> FunctionContext<'a> {
        FunctionContext {
            control_subgraph,
            def_use,
            fork_join_map,
            fork_join_nest,
        }
    }
}

// TODO: (@xrouth) I feel like this funcitonality should be provided by the manager that holds and allocates dynamic constants & IDs.
pub fn dyn_const_value(
    dc: &DynamicConstantID,
    dyn_const_values: &[DynamicConstant],
    dyn_const_params: &[usize],
) -> usize {
    let dc = &dyn_const_values[dc.idx()];
    match dc {
        DynamicConstant::Constant(v) => *v,
        DynamicConstant::Parameter(v) => dyn_const_params[*v],
        DynamicConstant::Add(xs) => xs
            .iter()
            .map(|x| dyn_const_value(x, dyn_const_values, dyn_const_params))
            .fold(0, |s, v| s + v),
        DynamicConstant::Sub(a, b) => {
            dyn_const_value(a, dyn_const_values, dyn_const_params)
                - dyn_const_value(b, dyn_const_values, dyn_const_params)
        }
        DynamicConstant::Mul(xs) => xs
            .iter()
            .map(|x| dyn_const_value(x, dyn_const_values, dyn_const_params))
            .fold(1, |p, v| p * v),
        DynamicConstant::Div(a, b) => {
            dyn_const_value(a, dyn_const_values, dyn_const_params)
                / dyn_const_value(b, dyn_const_values, dyn_const_params)
        }
        DynamicConstant::Rem(a, b) => {
            dyn_const_value(a, dyn_const_values, dyn_const_params)
                % dyn_const_value(b, dyn_const_values, dyn_const_params)
        }
        DynamicConstant::Max(xs) => xs
            .iter()
            .map(|x| dyn_const_value(x, dyn_const_values, dyn_const_params))
            .fold(None, |m, v| {
                if let Some(m) = m {
                    Some(max(m, v))
                } else {
                    Some(v)
                }
            })
            .unwrap(),
        DynamicConstant::Min(xs) => xs
            .iter()
            .map(|x| dyn_const_value(x, dyn_const_values, dyn_const_params))
            .fold(None, |m, v| {
                if let Some(m) = m {
                    Some(min(m, v))
                } else {
                    Some(v)
                }
            })
            .unwrap(),
    }
}

// Each control token stores a current position, and also a mapping of fork nodes -> thread idx.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ControlToken {
    pub curr: NodeID,
    pub prev: NodeID, // Used for properly latching PHI nodes, as they depend on their control predecessor.
    pub thread_indicies: Vec<usize>, // Stack of thread_indicies, can use fork_join nests to recover thread indicies per fork.

    // Map phis -> values
    // Oddly, these are stored per control token, when some other per-token data is stored in the FunctionExecutionState
    // we could move this `phi_values` to be consistent, however that would make CoW behvaior optimization more difficult.
    pub phi_values: HashMap<NodeID, InterpreterVal>, // TODO: Optimize this to a Cow<HashMap>, or optimize this whole thing.
}

impl ControlToken {
    pub fn moved_to(&self, next: NodeID) -> ControlToken {
        ControlToken {
            curr: next,
            prev: self.curr,
            thread_indicies: self.thread_indicies.clone(),
            phi_values: self.phi_values.clone(),
        }
    }
}
impl<'a> FunctionExecutionState<'a> {
    pub fn new(
        args: Vec<InterpreterVal>,
        module: &'a Module,
        function_id: FunctionID,
        function_contexts: &'a Vec<FunctionContext>,
        dynamic_constant_params: Vec<usize>,
    ) -> Self {
        println!(
            "param  types: {:?}",
            module.functions[function_id.idx()].param_types
        );

        assert_eq!(
            args.len(),
            module.functions[function_id.idx()].param_types.len()
        );

        FunctionExecutionState {
            args,
            reduce_values: HashMap::new(),
            join_counters: HashMap::new(),
            module,
            function_contexts,
            function_id,
            dynamic_constant_params,
        }
    }

    pub fn get_control_subgraph(&self) -> &Subgraph {
        self.function_contexts[self.function_id.idx()].control_subgraph
    }

    pub fn get_fork_join_map(&self) -> &HashMap<NodeID, NodeID> {
        self.function_contexts[self.function_id.idx()].fork_join_map
    }

    pub fn get_fork_join_nest(&self) -> &HashMap<NodeID, Vec<NodeID>> {
        self.function_contexts[self.function_id.idx()].fork_join_nest
    }

    pub fn get_def_use(&self) -> &ImmutableDefUseMap {
        self.function_contexts[self.function_id.idx()].def_use
    }

    pub fn get_function(&self) -> &Function {
        &self.module.functions[self.function_id.idx()]
    }

    /* Drives PHI values of this region for a control token, returns the next control node. */
    pub fn handle_region(&mut self, token: &mut ControlToken, preds: &Box<[NodeID]>) -> NodeID {
        let prev = token.prev;
        let node = token.curr;

        // Gather PHI nodes for this region node.
        let phis: Vec<NodeID> = self
            .get_def_use()
            .get_users(node)
            .into_iter()
            .filter_map(|n| {
                if self.get_function().nodes[n.idx()].is_phi() {
                    Some(*n)
                } else {
                    None
                }
            })
            .collect();

        // Find incoming edge of this region.
        let (edge_num, _) = preds
            .into_iter()
            .find_position(|pred| **pred == prev)
            .expect("PANIC: invalid incoming edge.");

        let mut to_latch = vec![];

        // Get values to latch at this point.
        for phi in phis {
            to_latch.push(self.handle_phi(token, edge_num, phi));
        }

        // Write values into map.
        for (phi, value) in to_latch {
            token.phi_values.insert(phi, value);
        }

        // Return the (single) control successor of the region node
        self.get_control_subgraph().succs(node).next().unwrap()
    }

    pub fn handle_phi(
        &mut self,
        token: &ControlToken,
        edge: usize,
        phi: NodeID,
    ) -> (NodeID, InterpreterVal) {
        let (_control, data) = &self.get_function().nodes[phi.idx()]
            .try_phi()
            .expect("PANIC: handle_phi on non-phi node.");
        let value_node = data[edge];

        let value = self.handle_data(token, value_node);
        if VERBOSE {
            println!("Latching PHI {:?} to {:?}", phi.idx(), value);
        }

        (phi, value)
    }

    // Drive all reductions for this join.
    // TODO (@xrouth): Change all these to return vecs of ControlTokens, so this is consistent with other handle_* functions.
    pub fn handle_join(&mut self, mut token: ControlToken, join: NodeID) -> Option<ControlToken> {
        let reduces: Vec<NodeID> = self
            .get_def_use()
            .get_users(join)
            .into_iter()
            .filter_map(|n| {
                if self.get_function().nodes[n.idx()].is_reduce() {
                    Some(*n)
                } else {
                    None
                }
            })
            .collect();

        for reduction in &reduces {
            self.handle_reduction(&token, *reduction);
        }

        let thread_values = self.get_thread_factors(&token, join);
        // This and_modify doesn't do aynthing??
        self.join_counters
            .entry((thread_values.clone(), join))
            .and_modify(|v| *v -= 1);

        if VERBOSE {
            println!(
                "join, thread_values : {:?}, {:?}",
                join,
                thread_values.clone()
            );
        }
        if *self
            .join_counters
            .get(&(thread_values.clone(), join))
            .expect("PANIC: join counter not initialized")
            == 0
        {
            let curr = token.curr;
            token.prev = curr;
            token.thread_indicies.truncate(thread_values.len()); // Get rid of this thread index.
            token.curr = self.get_control_subgraph().succs(join).next().unwrap();
            Some(token)
        } else {
            None
        }
    }

    /* Get top N-1 thread factors, where N is the depth of the control token. */
    pub fn get_thread_factors(&self, token: &ControlToken, control_node: NodeID) -> Vec<usize> {
        let nested_forks = self
            .get_fork_join_nest()
            .get(&control_node)
            .expect("PANIC: invalid node for fork join nest.")
            .clone();
        // Take the top N entries such that it matches the length of the TRF in the control token.

        // Get the depth of the control token that is requesting this reduction node.

        // Sum over all thread dimensions in nested forks
        let fork_levels: usize = nested_forks
            .iter()
            .map(|ele| {
                self.get_function().nodes[ele.idx()]
                    .try_fork()
                    .unwrap()
                    .1
                    .len()
            })
            .sum();

        let len = if nested_forks.is_empty() {
            fork_levels - 1
        } else {
            fork_levels
                - (self.get_function().nodes[nested_forks.first().unwrap().idx()]
                    .try_fork()
                    .unwrap()
                    .1
                    .len())
        };

        let mut thread_values = token.thread_indicies.clone();
        thread_values.truncate(len);
        thread_values
    }

    pub fn initialize_reduction(&mut self, token_at_fork: &ControlToken, reduce: NodeID) {
        let token = token_at_fork;

        let (control, init, _) = &self.get_function().nodes[reduce.idx()]
            .try_reduce()
            .expect("PANIC: handle_reduction on non-reduce node.");

        let thread_values = self.get_thread_factors(token, *control);

        let init = self.handle_data(&token, *init);

        if VERBOSE {
            println!(
                "reduction {:?} initialized to: {:?} on thread {:?}",
                reduce, init, thread_values
            );
        }

        self.reduce_values
            .insert((thread_values.clone(), reduce), init);
    }

    // Drive the reduction, this will be invoked for each control token.
    pub fn handle_reduction(&mut self, token: &ControlToken, reduce: NodeID) {
        let (control, _, reduct) = &self.get_function().nodes[reduce.idx()]
            .try_reduce()
            .expect("PANIC: handle_reduction on non-reduce node.");

        let thread_values = self.get_thread_factors(token, *control);

        let data = self.handle_data(&token, *reduct);

        if VERBOSE {
            println!(
                "reduction {:?} write of {:?} on thread {:?}",
                reduce, data, thread_values
            );
        }

        self.reduce_values.insert((thread_values, reduce), data);
    }

    pub fn handle_data(&mut self, token: &ControlToken, node: NodeID) -> InterpreterVal {
        // Partial borrow complaint. :/
        match &self.module.functions[self.function_id.idx()].nodes[node.idx()] {
            Node::Phi {
                control: _,
                data: _,
            } => (*token
                .phi_values
                .get(&node)
                .expect(&format!("PANIC: Phi {:?} value not latched.", node)))
            .clone(),
            Node::ThreadID { control, dimension } => {
                // `control` is the fork that drives this node.

                let nested_forks = self
                    .get_fork_join_nest()
                    .get(control)
                    .expect("PANIC: No nesting information for thread index!")
                    .clone();

                // Skip forks until we get to this level.
                // How many forks are outer? idfk.
                let outer_forks: Vec<NodeID> = nested_forks
                    .iter()
                    .cloned()
                    .take_while(|fork| *fork != node)
                    .collect();

                let fork_levels: usize = outer_forks
                    .iter()
                    .skip(1)
                    .map(|ele| {
                        self.get_function().nodes[ele.idx()]
                            .try_fork()
                            .unwrap()
                            .1
                            .len()
                    })
                    .sum();

                // Dimension might need to instead be dimensions - dimension
                let v = token.thread_indicies[fork_levels + dimension]; // Might have to -1?
                if VERBOSE {
                    println!(
                        "node: {:?} gives tid: {:?} for thread: {:?}, dim: {:?}",
                        node, v, token.thread_indicies, dimension
                    );
                }
                InterpreterVal::DynamicConstant((v).into())
            }
            // If we read from a reduction that is the same depth as this thread, we need to write back to it before anyone else reads from it.
            // This probably isn't the exact condition, but somethign similar. Anyways, we achieve correctness by iterating control nodes recursively.
            Node::Reduce {
                control,
                init: _,
                reduct: _,
            } => {
                let thread_values = self.get_thread_factors(token, *control);

                let entry = self.reduce_values.entry((thread_values.clone(), node));

                let val = match entry {
                    Occupied(v) => v.get().clone(),
                    std::collections::hash_map::Entry::Vacant(_) => panic!(
                        "Ctrl token: {:?}, Reduce {:?} has not been initialized!, TV: {:?}",
                        token, node, thread_values
                    ),
                };
                val
            }
            Node::Parameter { index } => self.args[*index].clone(),
            Node::Constant { id } => {
                let con = &self.module.constants[id.idx()];
                InterpreterVal::from_constant(
                    con,
                    &self.module.constants,
                    &self.module.types,
                    &self.module.dynamic_constants,
                    &self.dynamic_constant_params,
                )
            }
            Node::DynamicConstant { id } => {
                let v = dyn_const_value(
                    id,
                    &self.module.dynamic_constants,
                    &self.dynamic_constant_params,
                );

                // TODO: Figure out what type / semantics are of thread ID and dynamic const.
                InterpreterVal::UnsignedInteger64(v.try_into().expect("too big dyn const!"))
            }
            Node::Unary { input, op } => {
                let val = self.handle_data(token, *input);
                InterpreterVal::unary_op(&self.module.types, *op, val)
            }
            Node::Binary { left, right, op } => {
                let left = self.handle_data(token, *left);
                let right = self.handle_data(token, *right);

                InterpreterVal::binary_op(*op, left, right)
            }
            Node::Ternary {
                first,
                second,
                third,
                op,
            } => {
                let first = self.handle_data(token, *first);
                let second = self.handle_data(token, *second);
                let third = self.handle_data(token, *third);

                match op {
                    TernaryOperator::Select => {
                        if first == InterpreterVal::Boolean(true) {
                            second
                        } else {
                            third
                        }
                    }
                }
            }
            Node::Call {
                control: _,
                function,
                dynamic_constants,
                args,
            } => {
                let args = args
                    .into_iter()
                    .map(|arg_node| self.handle_data(token, *arg_node))
                    .collect();

                let dynamic_constant_params = dynamic_constants
                    .into_iter()
                    .map(|id| {
                        dyn_const_value(
                            id,
                            &self.module.dynamic_constants,
                            &self.dynamic_constant_params,
                        )
                    })
                    .collect_vec();

                let mut state = FunctionExecutionState::new(
                    args,
                    self.module,
                    *function,
                    self.function_contexts,
                    dynamic_constant_params,
                );

                state.run()
            }
            Node::DataProjection { data, selection } => {
                let data = self.handle_data(token, *data);
                let InterpreterVal::MultiReturn(vs) = data else {
                    panic!();
                };
                vs[*selection].clone()
            }
            Node::Read { collect, indices } => {
                let collection = self.handle_data(token, *collect);
                if let InterpreterVal::Undef(_) = collection {
                    collection
                } else {
                    let result = self.handle_read(token, collection.clone(), indices);

                    if VERBOSE {
                        println!(
                            "{:?} read value : {:?} from {:?}, {:?} at index {:?}",
                            node, result, collect, collection, indices
                        );
                    }
                    result
                }
            }
            Node::Write {
                collect,
                data,
                indices,
            } => {
                let collection = self.handle_data(token, *collect);
                if let InterpreterVal::Undef(_) = collection {
                    collection
                } else {
                    let data = self.handle_data(token, *data);
                    self.handle_write(token, collection, data, indices)
                }
            }
            Node::Undef { ty } => InterpreterVal::Undef(*ty),
            _ => todo!(),
        }
    }

    pub fn handle_write(
        &mut self,
        token: &ControlToken,
        collection: InterpreterVal,
        data: InterpreterVal,
        indices: &[Index],
    ) -> InterpreterVal {
        // TODO (@xrouth): Recurse on writes correctly
        let val = match indices.first() {
            Some(Index::Field(idx)) => {
                if let InterpreterVal::Product(type_id, mut vals) = collection {
                    vals[*idx] = data;
                    InterpreterVal::Product(type_id, vals)
                } else {
                    panic!("PANIC: Field index on not a product type")
                }
            }
            None => collection,
            Some(Index::Variant(_)) => todo!(),
            Some(Index::Position(array_indices)) => {
                // Arrays also have inner indices...
                // Recover dimensional data from types.
                let array_indices: Vec<_> = array_indices
                    .into_iter()
                    .map(|idx| self.handle_data(token, *idx).as_usize())
                    .collect();
                // TODO: Implemenet . try_array() and other try_conversions on the InterpreterVal type
                if let InterpreterVal::Array(type_id, mut vals) = collection {
                    // TODO: Make this its own funciton to reuse w/ array_size
                    let extents: Vec<_> = self.module.types[type_id.idx()]
                        .try_extents()
                        .expect("PANIC: wrong type for array")
                        .into_iter()
                        .map(|extent| {
                            dyn_const_value(
                                extent,
                                &self.module.dynamic_constants,
                                &self.dynamic_constant_params,
                            )
                        })
                        .collect();
                    let idx = InterpreterVal::array_idx(&extents, &array_indices);
                    if idx >= vals.len() {
                        InterpreterVal::Undef(type_id)
                    } else {
                        vals[idx] = data;
                        InterpreterVal::Array(type_id, vals)
                    }
                } else {
                    panic!("PANIC: Position index on not an array")
                }
            }
        };
        val
    }

    pub fn handle_read(
        &mut self,
        token: &ControlToken,
        collection: InterpreterVal,
        indices: &[Index],
    ) -> InterpreterVal {
        let index = &indices[0];

        let val = match index {
            Index::Field(_) => todo!(),
            Index::Variant(_) => todo!(),
            Index::Position(array_indices) => {
                // Arrays also have inner indices...
                // Recover dimensional data from types.
                let array_indices: Vec<_> = array_indices
                    .into_iter()
                    .map(|idx| self.handle_data(token, *idx).as_usize())
                    .collect();

                if VERBOSE {
                    println!("read at rt indicies: {:?}", array_indices);
                }

                // TODO: Implemenet . try_array() and other try_conversions on the InterpreterVal type
                if let InterpreterVal::Array(type_id, vals) = collection {
                    // TODO: Make this its own funciton to reuse w/ array_size
                    let extents: Vec<_> = self.module.types[type_id.idx()]
                        .try_extents()
                        .expect("PANIC: wrong type for array")
                        .into_iter()
                        .map(|extent| {
                            dyn_const_value(
                                extent,
                                &self.module.dynamic_constants,
                                &self.dynamic_constant_params,
                            )
                        })
                        .collect();
                    // FIXME: This type may be wrong.
                    let ret = vals
                        .get(InterpreterVal::array_idx(&extents, &array_indices))
                        .unwrap_or(&InterpreterVal::Undef(type_id))
                        .clone();
                    ret
                } else {
                    panic!("PANIC: Position index on not an array")
                }
            }
        };

        // Recurse
        if indices.len() == 1 {
            val
        } else {
            self.handle_read(token, val, &indices[1..])
        }
    }

    pub fn run(&mut self) -> InterpreterVal {
        let start_node: NodeID = NodeID::new(0);

        let start_token = ControlToken {
            curr: start_node,
            prev: start_node,
            thread_indicies: Vec::new(),
            phi_values: HashMap::new(),
        };

        let mut live_tokens: Vec<ControlToken> = Vec::new();
        live_tokens.push(start_token);

        // To do reduction nodes correctly we have to traverse control tokens in a depth-first fashion (i.e immediately handle spawned threads).
        'outer: loop {
            let mut ctrl_token = live_tokens
                .pop()
                .expect("PANIC: Interpreter ran out of control tokens without returning.");

            // TODO: (@xrouth): Enable this + PHI latch logging  wi/  a simple debug flag.
            // Tracking PHI vals and control state is very useful for debugging.

            if VERBOSE {
                println!(
                    "control token {} {}",
                    ctrl_token.curr.idx(),
                    &self.get_function().nodes[ctrl_token.curr.idx()].lower_case_name()
                );
            }

            // TODO: Rust is annoying and can't recognize that this is a partial borrow.
            // Can't partial borrow, so need a clone.
            let node = &self.get_function().nodes[ctrl_token.curr.idx()].clone();
            let new_tokens = match node {
                Node::Start => {
                    let next: NodeID = self
                        .get_control_subgraph()
                        .succs(ctrl_token.curr)
                        .next()
                        .unwrap();

                    let ctrl_token = ctrl_token.moved_to(next);

                    vec![ctrl_token]
                }
                Node::Region { preds } => {
                    // Updates
                    let next = self.handle_region(&mut ctrl_token, &preds);
                    let ctrl_token = ctrl_token.moved_to(next);

                    vec![ctrl_token]
                }
                Node::If { control: _, cond } => {
                    let cond = self.handle_data(&ctrl_token, *cond);

                    // Convert condition to usize
                    let cond: usize = match cond {
                        InterpreterVal::Boolean(v) => v.into(),
                        InterpreterVal::Undef(_) => panic!("PANIC: Undef reached IF"),
                        _ => panic!("PANIC: Invalid condition for IF, please typecheck."),
                    };

                    // Find control read succesor that matches the condition.
                    let next = self
                        .get_control_subgraph()
                        .succs(ctrl_token.curr)
                        .find(|n| {
                            self.get_function().nodes[n.idx()]
                                .try_control_projection(cond)
                                .is_some()
                        })
                        .expect("PANIC: No outgoing valid outgoing edge.");

                    let ctrl_token = ctrl_token.moved_to(next);
                    vec![ctrl_token]
                }
                Node::ControlProjection { .. } => {
                    let next: NodeID = self
                        .get_control_subgraph()
                        .succs(ctrl_token.curr)
                        .next()
                        .unwrap();

                    let ctrl_token = ctrl_token.moved_to(next);

                    vec![ctrl_token]
                }

                Node::Match { control: _, sum: _ } => todo!(),
                Node::Fork {
                    control: _,
                    factors,
                } => {
                    let fork = ctrl_token.curr;
                    // if factors.len() > 1 {
                    //     panic!("multi-dimensional forks unimplemented")
                    // }

                    let factors = factors.iter().map(|f| {
                        dyn_const_value(
                            &f,
                            &self.module.dynamic_constants,
                            &self.dynamic_constant_params,
                        )
                    });

                    let n_tokens: usize = factors.clone().product();

                    // Update control token
                    let next = self
                        .get_control_subgraph()
                        .succs(ctrl_token.curr)
                        .nth(0)
                        .unwrap();
                    let ctrl_token = ctrl_token.moved_to(next);

                    let mut tokens_to_add = Vec::with_capacity(n_tokens);

                    assert_ne!(n_tokens, 0);

                    // Token is at its correct sontrol succesor already.

                    // Add the new thread index.
                    let num_outer_dims = ctrl_token.thread_indicies.len();
                    for i in 0..n_tokens {
                        let mut temp = i;
                        let mut new_token = ctrl_token.clone(); // Copy map, curr, prev, etc.

                        for (_, dim) in factors.clone().enumerate().rev() {
                            new_token.thread_indicies.insert(num_outer_dims, temp % dim); // Stack of thread indicies
                            temp /= dim;
                        }
                        tokens_to_add.push(new_token);
                    }

                    let thread_factors = self.get_thread_factors(&ctrl_token, ctrl_token.curr);

                    // Find join and initialize them, and set their reduction counters as well.
                    let join = self
                        .get_fork_join_map()
                        .get(&fork)
                        .expect("PANIC: fork missing a join.")
                        .clone();

                    let reduces: Vec<NodeID> = self
                        .get_def_use()
                        .get_users(join)
                        .into_iter()
                        .filter_map(|n| {
                            if self.get_function().nodes[n.idx()].is_reduce() {
                                Some(*n)
                            } else {
                                None
                            }
                        })
                        .collect();

                    for reduction in reduces {
                        // TODO: Is this the correct reduction?
                        self.initialize_reduction(&ctrl_token, reduction);
                    }

                    if VERBOSE {
                        println!(
                            "tf, fork, join, n_tokens: {:?}, {:?}, {:?}, {:?}",
                            thread_factors, fork, join, n_tokens
                        );
                    }
                    self.join_counters.insert((thread_factors, join), n_tokens);

                    tokens_to_add.reverse();
                    tokens_to_add
                }
                Node::Join { control: _ } => {
                    let join = ctrl_token.curr;

                    // Only make a control token if the join is finished, checked in handle_join.
                    if let Some(next) = self.handle_join(ctrl_token, join) {
                        vec![next]
                    } else {
                        vec![]
                    }
                }
                Node::Return { control: _, data } => {
                    let results = data
                        .iter()
                        .map(|data| self.handle_data(&ctrl_token, *data))
                        .collect();
                    break 'outer InterpreterVal::MultiReturn(results);
                }
                _ => {
                    panic!("PANIC: Unexpected node in control subgraph {:?}", node);
                }
            };

            for i in new_tokens {
                live_tokens.push(i);
            }
        }
    }
}
