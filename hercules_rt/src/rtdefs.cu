#include <stdint.h>
#include <cublas_v2.h>

static cublasHandle_t cublas_handle = 0;

extern "C" {
    void *___cuda_alloc(size_t size) {
	void *ptr = NULL;
	cudaError_t res = cudaMalloc(&ptr, size);
	if (res != cudaSuccess) {
	    ptr = NULL;
	}
	return ptr;
    }
    
    void ___cuda_dealloc(void *ptr, size_t size) {
	(void) size;
	cudaFree(ptr);
    }
    
    void ___cuda_zero_mem(void *ptr, size_t size) {
	cudaMemset(ptr, 0, size);
    }
    
    void ___copy_cpu_to_cuda(void *dst, void *src, size_t size) {
	cudaMemcpy(dst, src, size, cudaMemcpyHostToDevice);
    }
    
    void ___copy_cuda_to_cpu(void *dst, void *src, size_t size) {
	cudaMemcpy(dst, src, size, cudaMemcpyDeviceToHost);
    }
    
    void ___copy_cuda_to_cuda(void *dst, void *src, size_t size) {
	cudaMemcpy(dst, src, size, cudaMemcpyDeviceToDevice);
    }
    
    void ___cublas_sgemm(uint64_t i, uint64_t j, uint64_t k, float *c, float *a, float *b) {
	if (!cublas_handle) {
	    cublasCreate(&cublas_handle);
	}
	float alf = 1.0;
	float beta = 0.0;
	cublasSgemm(cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N,
		    k, i, j,
		    &alf, b, k, a, j,
		    &beta, c, k);
	cudaDeviceSynchronize();
    }
}
