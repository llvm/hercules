#![feature(exit_status_error)]

use std::env::var;
use std::path::Path;
use std::process::Command;

fn main() {
    if cfg!(feature = "cuda") {
        let out_dir = var("OUT_DIR").unwrap();
        Command::new("nvcc")
            .args(&["src/rtdefs.cu", "-c", "-o"])
            .arg(&format!("{}/rtdefs.o", out_dir))
            .status()
            .expect("PANIC: NVCC failed when building runtime. Is NVCC installed?")
            .exit_ok()
            .expect("NVCC did not succeed");
        Command::new("ar")
            .current_dir(&Path::new(&out_dir))
            .args(&["crus", "librtdefs.a", "rtdefs.o"])
            .status()
            .unwrap()
            .exit_ok()
            .expect("ar did not succeed");

        println!("cargo::rustc-link-search=native={}", out_dir);
        println!("cargo::rustc-link-search=native=/usr/lib/x86_64-linux-gnu/");
        println!("cargo::rustc-link-search=native=/usr/local/cuda/lib64");
        println!("cargo::rustc-link-search=native=/opt/cuda/lib/");
        println!("cargo::rustc-link-lib=static=rtdefs");
        println!("cargo::rustc-link-lib=cudart");
        println!("cargo::rustc-link-lib=cublas");
        println!("cargo::rerun-if-changed=src/rtdefs.cu");
    }
}
