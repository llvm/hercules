use std::collections::{HashMap, HashSet};

use crate::*;

/*
 * Construct a map from fork node to all control nodes (including itself)
 * satisfying:
 * 1. Dominated by the fork.
 * 2. Not dominated by the fork's join.
 * 3. Not dominated by any other fork that's also dominated by the fork, where
 *    we do count self-domination.
 * We include the non-fork start node as the key for all control nodes outside
 * any fork.
 */
pub fn fork_control_map(
    fork_join_nesting: &HashMap<NodeID, Vec<NodeID>>,
) -> HashMap<NodeID, HashSet<NodeID>> {
    let mut fork_control_map = HashMap::new();
    for (control, forks) in fork_join_nesting {
        let fork = forks.first().copied().unwrap_or(NodeID::new(0));
        fork_control_map
            .entry(fork)
            .or_insert_with(HashSet::new)
            .insert(*control);
    }
    fork_control_map
}

/*
 * Construct a map from fork node to all fork nodes (including itself)
 * satisfying:
 * 1. Dominated by the fork.
 * 2. Not dominated by the fork's join.
 * 3. Not dominated by any other fork that's also dominated by the fork, where
 *    we do count self-domination.
 * Note that the fork tree also includes the start node as the unique root node.
 */
pub fn fork_tree(
    function: &Function,
    fork_join_nesting: &HashMap<NodeID, Vec<NodeID>>,
) -> HashMap<NodeID, HashSet<NodeID>> {
    let mut fork_tree = HashMap::new();
    for (control, forks) in fork_join_nesting {
        if function.nodes[control.idx()].is_fork() {
            fork_tree.entry(*control).or_insert_with(HashSet::new);
            let nesting_fork = forks.get(1).copied().unwrap_or(NodeID::new(0));
            fork_tree
                .entry(nesting_fork)
                .or_insert_with(HashSet::new)
                .insert(*control);
        }
    }
    fork_tree.entry(NodeID::new(0)).or_default();
    fork_tree
}
