use std::fs::read_to_string;
use std::path::PathBuf;

use clap::Parser;

use juno_compiler::*;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    src_file: String,
    #[clap(short, long, value_name = "SCHEDULE")]
    schedule: Option<String>,
    #[arg(short, long = "output-dir", value_name = "OUTPUT DIR")]
    output_dir: Option<String>,
    #[arg(short, long, value_name = "TREAT AS HERCULES IR")]
    ir: bool,
}

fn main() {
    let args = Cli::parse();
    let output_dir = match args.output_dir {
        Some(dir) => dir,
        None => PathBuf::from(args.src_file.clone())
            .parent()
            .unwrap()
            .to_str()
            .unwrap()
            .to_string(),
    };
    if args.ir {
        let Ok(contents) = read_to_string(&args.src_file) else {
            panic!("Unable to open and read input file.");
        };
        let Ok(ir_mod) = hercules_ir::parse::parse(&contents) else {
            panic!("Unable to parse Hercules IR file.");
        };

        let module_name = "module".to_string();
        match compile_ir(ir_mod, args.schedule, output_dir, module_name) {
            Ok(()) => {}
            Err(errs) => {
                eprintln!("{}", errs);
            }
        }
    } else {
        match compile(args.src_file, args.schedule, output_dir) {
            Ok(()) => {}
            Err(errs) => {
                eprintln!("{}", errs);
            }
        }
    }
}
