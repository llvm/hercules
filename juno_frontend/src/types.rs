use std::collections::{HashMap, HashSet, VecDeque};

use crate::dynconst::DynConst;
use crate::locs::Location;
use crate::parser;

use hercules_ir::build::*;
use hercules_ir::ir::*;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Either<A, B> {
    Left(A),
    Right(B),
}

impl parser::Kind {
    fn is_a(&self, other: parser::Kind) -> bool {
        match other {
            parser::Kind::USize => false,
            parser::Kind::Type => true,
            parser::Kind::Number => match self {
                parser::Kind::Number | parser::Kind::Integer | parser::Kind::Float => true,
                _ => false,
            },
            parser::Kind::Integer => match self {
                parser::Kind::Integer => true,
                _ => false,
            },
            parser::Kind::Float => match self {
                parser::Kind::Float => true,
                _ => false,
            },
        }
    }

    fn unify(&self, other: parser::Kind) -> Option<parser::Kind> {
        match self {
            parser::Kind::USize => None,
            parser::Kind::Type => Some(other),
            parser::Kind::Number => match other {
                parser::Kind::USize => None,
                parser::Kind::Type | parser::Kind::Number => Some(parser::Kind::Number),
                parser::Kind::Integer => Some(parser::Kind::Integer),
                parser::Kind::Float => Some(parser::Kind::Float),
            },
            parser::Kind::Integer => match other {
                parser::Kind::USize => None,
                parser::Kind::Type | parser::Kind::Number | parser::Kind::Integer => {
                    Some(parser::Kind::Integer)
                }
                parser::Kind::Float => None,
            },
            parser::Kind::Float => match other {
                parser::Kind::USize => None,
                parser::Kind::Type | parser::Kind::Number | parser::Kind::Float => {
                    Some(parser::Kind::Float)
                }
                parser::Kind::Integer => None,
            },
        }
    }

    pub fn to_string(&self) -> String {
        match self {
            parser::Kind::USize => "usize".to_string(),
            parser::Kind::Type => "type".to_string(),
            parser::Kind::Number => "number".to_string(),
            parser::Kind::Integer => "integer".to_string(),
            parser::Kind::Float => "float".to_string(),
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Primitive {
    Bool,
    U8,
    I8,
    U16,
    I16,
    U32,
    I32,
    U64,
    I64,
    FP8,
    BF16,
    F32,
    F64,
    Unit,
}

impl Primitive {
    fn is_kind(&self, kind: parser::Kind) -> bool {
        match kind {
            parser::Kind::Type => true,
            parser::Kind::USize => false,

            parser::Kind::Number => match self {
                Primitive::U8
                | Primitive::I8
                | Primitive::U16
                | Primitive::I16
                | Primitive::U32
                | Primitive::I32
                | Primitive::U64
                | Primitive::I64
                | Primitive::FP8
                | Primitive::BF16
                | Primitive::F32
                | Primitive::F64 => true,
                _ => false,
            },
            parser::Kind::Integer => match self {
                Primitive::U8
                | Primitive::I8
                | Primitive::U16
                | Primitive::I16
                | Primitive::U32
                | Primitive::I32
                | Primitive::U64
                | Primitive::I64 => true,
                _ => false,
            },
            parser::Kind::Float => match self {
                Primitive::FP8 | Primitive::BF16 | Primitive::F32 | Primitive::F64 => true,
                _ => false,
            },
        }
    }

    fn to_string(&self) -> String {
        match self {
            Primitive::Bool => "bool".to_string(),
            Primitive::I8 => "i8".to_string(),
            Primitive::U8 => "u8".to_string(),
            Primitive::I16 => "i16".to_string(),
            Primitive::U16 => "u16".to_string(),
            Primitive::I32 => "i32".to_string(),
            Primitive::U32 => "u32".to_string(),
            Primitive::I64 => "i64".to_string(),
            Primitive::U64 => "u64".to_string(),
            Primitive::FP8 => "fp8".to_string(),
            Primitive::BF16 => "bf16".to_string(),
            Primitive::F32 => "f32".to_string(),
            Primitive::F64 => "f64".to_string(),
            Primitive::Unit => "()".to_string(),
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Type {
    val: usize,
}

// Type forms, which include both concrete types, as well as unsolved types that may have some
// constraints. Note that constrained types are just primitives (particularly the numeric types)
// because the type system ensures that we always know information about arrays, structs, unions,
// and tuples
#[derive(Clone, Debug)]
enum TypeForm {
    Primitive {
        prim: Primitive,
    },
    Tuple {
        fields: Vec<Type>,
    },
    Array {
        elem: Type,
        dims: Vec<DynConst>,
    },

    // This type is the same type as another type
    OtherType {
        other: Type,
    },

    // For type variables, we record its name, its index (in the list of type variables in this
    // context), and anything we know about it (is it a number, is it an integer)
    TypeVar {
        name: usize,
        index: usize,
        kind: parser::Kind,
    },

    // For structs and unions we record the name (via its interned representation), a UID, and the
    // types of its fields/constructors in a set order and a map from field/constructor names to
    // its index in the list
    Struct {
        name: usize,
        id: usize,
        fields: Vec<Type>,
        names: HashMap<usize, usize>,
    },
    Union {
        name: usize,
        id: usize,
        constr: Vec<Type>,
        names: HashMap<usize, usize>,
    },

    // Constrained types
    AnyOfKind {
        kind: parser::Kind,
        loc: Location,
    },

    // The types of call nodes are MultiReturns
    MultiReturn {
        types: Vec<Type>,
    },
}

#[derive(Debug)]
pub struct TypeSolver {
    types: Vec<TypeForm>,
    solved: usize, // which types have been "solved" (i.e. are known not to be AnyOfKinds)
}

#[derive(Debug)]
pub struct TypeSolverInst<'a> {
    solver: &'a TypeSolver,
    // A collection of current values for type variables, and variables that we've solved for in
    // that context
    type_vars: Vec<TypeID>,
    solved: Vec<Option<TypeID>>,
}

impl TypeSolver {
    pub fn new() -> TypeSolver {
        TypeSolver {
            types: vec![],
            solved: 0,
        }
    }

    pub fn new_of_kind(&mut self, kind: parser::Kind, loc: Location) -> Type {
        self.create_type(TypeForm::AnyOfKind { kind, loc })
    }

    pub fn new_primitive(&mut self, prim: Primitive) -> Type {
        self.create_type(TypeForm::Primitive { prim })
    }

    pub fn new_tuple(&mut self, fields: Vec<Type>) -> Type {
        self.create_type(TypeForm::Tuple { fields })
    }

    pub fn new_array(&mut self, elem: Type, dims: Vec<DynConst>) -> Type {
        self.create_type(TypeForm::Array { elem, dims })
    }

    pub fn new_type_var(&mut self, name: usize, index: usize, kind: parser::Kind) -> Type {
        self.create_type(TypeForm::TypeVar { name, index, kind })
    }

    pub fn new_struct(
        &mut self,
        name: usize,
        id: usize,
        fields: Vec<Type>,
        names: HashMap<usize, usize>,
    ) -> Type {
        self.create_type(TypeForm::Struct {
            name,
            id,
            fields,
            names,
        })
    }

    pub fn new_union(
        &mut self,
        name: usize,
        id: usize,
        constr: Vec<Type>,
        names: HashMap<usize, usize>,
    ) -> Type {
        self.create_type(TypeForm::Union {
            name,
            id,
            constr,
            names,
        })
    }

    pub fn new_multi_return(&mut self, types: Vec<Type>) -> Type {
        self.create_type(TypeForm::MultiReturn { types })
    }

    fn create_type(&mut self, typ: TypeForm) -> Type {
        let idx = self.types.len();
        self.types.push(typ);
        Type { val: idx }
    }

    pub fn unify_void(&mut self, Type { val }: Type) -> bool {
        match &self.types[val] {
            TypeForm::Primitive {
                prim: Primitive::Unit,
                ..
            } => true,
            TypeForm::OtherType { other, .. } => self.unify_void(*other),
            TypeForm::AnyOfKind {
                kind: parser::Kind::Type,
                ..
            } => {
                self.types[val] = TypeForm::Primitive {
                    prim: Primitive::Unit,
                };
                true
            }
            _ => false,
        }
    }

    pub fn is_array(&self, Type { val }: Type) -> bool {
        match &self.types[val] {
            TypeForm::Array { .. } => true,
            TypeForm::OtherType { other, .. } => self.is_array(*other),
            _ => false,
        }
    }

    pub fn get_element_type(&self, Type { val }: Type) -> Option<Type> {
        match &self.types[val] {
            TypeForm::Array { elem, .. } => Some(*elem),
            TypeForm::OtherType { other, .. } => self.get_element_type(*other),
            _ => None,
        }
    }

    pub fn get_dimensions(&self, Type { val }: Type) -> Option<Vec<DynConst>> {
        match &self.types[val] {
            TypeForm::Array { elem: _, dims, .. } => Some(dims.to_vec()),
            TypeForm::OtherType { other, .. } => self.get_dimensions(*other),
            _ => None,
        }
    }

    pub fn unify_bool(&mut self, Type { val }: Type) -> bool {
        match &self.types[val] {
            TypeForm::Primitive {
                prim: Primitive::Bool,
                ..
            } => true,
            TypeForm::OtherType { other, .. } => self.unify_bool(*other),
            TypeForm::AnyOfKind {
                kind: parser::Kind::Type,
                ..
            } => {
                self.types[val] = TypeForm::Primitive {
                    prim: Primitive::Bool,
                };
                true
            }
            _ => false,
        }
    }

    pub fn unify_u64(&mut self, Type { val }: Type) -> bool {
        match &self.types[val] {
            TypeForm::Primitive {
                prim: Primitive::U64,
                ..
            } => true,
            TypeForm::OtherType { other, .. } => self.unify_u64(*other),

            TypeForm::AnyOfKind { kind, .. } => match kind {
                parser::Kind::Type | parser::Kind::Number | parser::Kind::Integer => {
                    self.types[val] = TypeForm::Primitive {
                        prim: Primitive::U64,
                    };
                    true
                }
                _ => false,
            },

            _ => false,
        }
    }

    pub fn unify_kind(&mut self, Type { val }: Type, kind: parser::Kind) -> bool {
        match &self.types[val] {
            TypeForm::Primitive { prim, .. } => prim.is_kind(kind),
            TypeForm::OtherType { other, .. } => self.unify_kind(*other, kind),
            TypeForm::TypeVar {
                name: _,
                index: _,
                kind: var_kind,
                ..
            } => var_kind.is_a(kind),

            TypeForm::AnyOfKind { kind: ty_kind, loc } => match ty_kind.unify(kind) {
                None => false,
                Some(unified) => {
                    self.types[val] = TypeForm::AnyOfKind {
                        kind: unified,
                        loc: *loc,
                    };
                    true
                }
            },

            _ => false,
        }
    }

    pub fn unify(&mut self, Type { val: ty1 }: Type, Type { val: ty2 }: Type) -> bool {
        if let TypeForm::OtherType { other, .. } = self.types[ty1] {
            return self.unify(other, Type { val: ty2 });
        }
        if let TypeForm::OtherType { other, .. } = self.types[ty2] {
            return self.unify(Type { val: ty1 }, other);
        }

        match (&self.types[ty1], &self.types[ty2]) {
            (TypeForm::Primitive { prim: p1, .. }, TypeForm::Primitive { prim: p2, .. }) => {
                p1 == p2
            }

            (TypeForm::Primitive { prim, .. }, TypeForm::AnyOfKind { kind, .. })
                if prim.is_kind(*kind) =>
            {
                self.types[ty2] = TypeForm::OtherType {
                    other: Type { val: ty1 },
                };
                true
            }
            (
                TypeForm::TypeVar {
                    name: _,
                    index: _,
                    kind: var_kind,
                    ..
                },
                TypeForm::AnyOfKind { kind, .. },
            ) if var_kind.is_a(*kind) => {
                self.types[ty2] = TypeForm::OtherType {
                    other: Type { val: ty1 },
                };
                true
            }

            (TypeForm::AnyOfKind { kind, .. }, TypeForm::Primitive { prim, .. })
                if prim.is_kind(*kind) =>
            {
                self.types[ty1] = TypeForm::OtherType {
                    other: Type { val: ty2 },
                };
                true
            }
            (
                TypeForm::AnyOfKind { kind, .. },
                TypeForm::TypeVar {
                    name: _,
                    index: _,
                    kind: var_kind,
                    ..
                },
            ) if var_kind.is_a(*kind) => {
                self.types[ty1] = TypeForm::OtherType {
                    other: Type { val: ty2 },
                };
                true
            }

            (TypeForm::Tuple { fields: f1, .. }, TypeForm::Tuple { fields: f2, .. })
                if f1.len() == f2.len() =>
            {
                for (t1, t2) in f1.clone().iter().zip(f2.clone().iter()) {
                    if !self.unify(*t1, *t2) {
                        return false;
                    }
                }
                true
            }

            (
                TypeForm::Array {
                    elem: t1,
                    dims: dm1,
                    ..
                },
                TypeForm::Array {
                    elem: t2,
                    dims: dm2,
                    ..
                },
            ) => dm1 == dm2 && self.unify(*t1, *t2),

            (
                TypeForm::TypeVar {
                    name: _,
                    index: idx1,
                    ..
                },
                TypeForm::TypeVar {
                    name: _,
                    index: idx2,
                    ..
                },
            ) => idx1 == idx2,

            (
                TypeForm::Struct {
                    name: _,
                    id: id1,
                    fields: fs1,
                    ..
                },
                TypeForm::Struct {
                    name: _,
                    id: id2,
                    fields: fs2,
                    ..
                },
            )
            | (
                TypeForm::Union {
                    name: _,
                    id: id1,
                    constr: fs1,
                    ..
                },
                TypeForm::Union {
                    name: _,
                    id: id2,
                    constr: fs2,
                    ..
                },
            ) if id1 == id2 && fs1.len() == fs2.len() => {
                for (t1, t2) in fs1.clone().iter().zip(fs2.clone().iter()) {
                    if !self.unify(*t1, *t2) {
                        return false;
                    }
                }
                true
            }

            (TypeForm::AnyOfKind { kind: k1, loc: l1 }, TypeForm::AnyOfKind { kind: k2, .. }) => {
                match k1.unify(*k2) {
                    None => false,
                    Some(kind) => {
                        let loc = *l1;
                        self.types[ty1] = TypeForm::AnyOfKind { kind, loc };
                        self.types[ty2] = TypeForm::OtherType {
                            other: Type { val: ty1 },
                        };
                        true
                    }
                }
            }

            // Note that MultReturn types never unify with anything (even itself), this is
            // intentional and makes it so that the only way MultiReturns can be used is to
            // destruct them
            _ => false,
        }
    }

    // Returns the types of the fields of a tuple
    pub fn get_fields(&self, Type { val }: Type) -> Option<&Vec<Type>> {
        match &self.types[val] {
            TypeForm::Tuple { fields, .. } => Some(fields),
            TypeForm::OtherType { other, .. } => self.get_fields(*other),
            _ => None,
        }
    }

    // Return the type of the field (in a tuple) at a particular index
    pub fn get_index(&self, Type { val }: Type, idx: usize) -> Option<Type> {
        match &self.types[val] {
            TypeForm::Tuple { fields, .. } => fields.get(idx).copied(),
            TypeForm::OtherType { other, .. } => self.get_index(*other, idx),
            _ => None,
        }
    }

    pub fn is_struct(&self, Type { val }: Type) -> bool {
        match &self.types[val] {
            TypeForm::Struct { .. } => true,
            TypeForm::OtherType { other, .. } => self.is_struct(*other),
            _ => false,
        }
    }

    // Return the number of fields a struct has
    pub fn get_num_struct_fields(&self, Type { val }: Type) -> Option<usize> {
        match &self.types[val] {
            TypeForm::Struct {
                name: _,
                id: _,
                fields,
                ..
            } => Some(fields.len()),
            TypeForm::OtherType { other, .. } => self.get_num_struct_fields(*other),
            _ => None,
        }
    }

    // Returns the position and type of a field in a type (if it exists)
    pub fn get_field(&self, Type { val }: Type, name: usize) -> Option<(usize, Type)> {
        match &self.types[val] {
            TypeForm::Struct {
                name: _,
                id: _,
                fields,
                names,
                ..
            } => names.get(&name).map(|idx| (*idx, fields[*idx])),
            TypeForm::OtherType { other, .. } => self.get_field(*other, name),
            _ => None,
        }
    }

    // Returns the type of the field at a certain index in a struct
    pub fn get_struct_field_type(&self, Type { val }: Type, idx: usize) -> Option<Type> {
        match &self.types[val] {
            TypeForm::Struct {
                name: _,
                id: _,
                fields,
                ..
            } => fields.get(idx).copied(),
            TypeForm::OtherType { other, .. } => self.get_struct_field_type(*other, idx),
            _ => None,
        }
    }

    pub fn get_field_names(&self, Type { val }: Type) -> Option<Vec<usize>> {
        match &self.types[val] {
            TypeForm::Struct {
                name: _,
                id: _,
                fields: _,
                names,
            } => Some(names.keys().map(|i| *i).collect::<Vec<_>>()),
            TypeForm::OtherType { other, .. } => self.get_field_names(*other),
            _ => None,
        }
    }

    pub fn get_num_dimensions(&self, Type { val }: Type) -> Option<usize> {
        match &self.types[val] {
            TypeForm::Array { elem: _, dims, .. } => Some(dims.len()),
            TypeForm::OtherType { other, .. } => self.get_num_dimensions(*other),
            _ => None,
        }
    }

    pub fn is_union(&self, Type { val }: Type) -> bool {
        match &self.types[val] {
            TypeForm::Union { .. } => true,
            TypeForm::OtherType { other, .. } => self.is_union(*other),
            _ => false,
        }
    }

    pub fn get_constructor_info(&self, Type { val }: Type, name: usize) -> Option<(usize, Type)> {
        match &self.types[val] {
            TypeForm::Union {
                name: _,
                id: _,
                constr,
                names,
                ..
            } => names.get(&name).map(|idx| (*idx, constr[*idx])),
            TypeForm::OtherType { other, .. } => self.get_constructor_info(*other, name),
            _ => None,
        }
    }

    pub fn get_return_types(&self, Type { val }: Type) -> Option<&Vec<Type>> {
        match &self.types[val] {
            TypeForm::MultiReturn { types } => Some(types),
            TypeForm::OtherType { other, .. } => self.get_return_types(*other),
            _ => None,
        }
    }

    pub fn to_string(&self, Type { val }: Type, stringtab: &dyn Fn(usize) -> String) -> String {
        match &self.types[val] {
            TypeForm::Primitive { prim, .. } => prim.to_string(),
            TypeForm::Tuple { fields, .. } => {
                "(".to_string()
                    + &fields
                        .iter()
                        .map(|t| self.to_string(*t, stringtab))
                        .collect::<Vec<_>>()
                        .join(", ")
                    + ")"
            }
            TypeForm::Array { elem, dims, .. } => {
                self.to_string(*elem, stringtab)
                    + "["
                    + &dims
                        .iter()
                        .map(|d| d.to_string(stringtab))
                        .collect::<Vec<_>>()
                        .join(", ")
                    + "]"
            }
            TypeForm::OtherType { other, .. } => self.to_string(*other, stringtab),
            TypeForm::TypeVar { name, .. }
            | TypeForm::Struct { name, .. }
            | TypeForm::Union { name, .. } => stringtab(*name),
            TypeForm::AnyOfKind { kind, .. } => kind.to_string(),
            TypeForm::MultiReturn { types } => types
                .iter()
                .map(|t| self.to_string(*t, stringtab))
                .collect::<Vec<_>>()
                .join(", "),
        }
    }

    // Instantiate a type using the provided list of type variables and dynamic constants
    // This is useful for instantiating the return type of a function and parametric types
    pub fn instantiate(
        &mut self,
        Type { val }: Type,
        type_vars: &Vec<Type>,
        dynamic_constants: &Vec<DynConst>,
    ) -> Option<Type> {
        match self.types[val].clone() {
            TypeForm::Primitive { .. } => Some(Type { val }),
            TypeForm::AnyOfKind { kind, loc } => Some(self.new_of_kind(kind, loc)),
            TypeForm::OtherType { other, .. } => {
                self.instantiate(other, type_vars, dynamic_constants)
            }
            TypeForm::Tuple { fields } => {
                let mut types = vec![];
                let mut changed = false;
                for typ in fields {
                    let inst = self.instantiate(typ, type_vars, dynamic_constants)?;
                    changed = changed || typ.val != inst.val;
                    types.push(inst);
                }
                if changed {
                    Some(self.new_tuple(types))
                } else {
                    Some(Type { val })
                }
            }
            TypeForm::Array { elem, dims } => {
                let elem_typ = self.instantiate(elem, type_vars, dynamic_constants)?;
                let mut subst_dims = vec![];

                for c in dims.iter() {
                    subst_dims.push(c.subst(dynamic_constants)?);
                }

                Some(self.new_array(elem_typ, subst_dims))
            }
            TypeForm::TypeVar {
                name: _,
                index,
                kind,
                ..
            } => {
                let typ = type_vars[index];
                assert!(self.unify_kind(typ, kind));
                Some(typ)
            }
            TypeForm::Struct {
                name,
                id,
                fields,
                names,
            } => {
                let mut new_fields = vec![];
                let mut changed = false;
                for typ in fields {
                    let inst = self.instantiate(typ, type_vars, dynamic_constants)?;
                    changed = changed || typ.val != inst.val;
                    new_fields.push(inst);
                }

                if changed {
                    Some(self.create_type(TypeForm::Struct {
                        name: name,
                        id: id,
                        fields: new_fields,
                        names: names.clone(),
                    }))
                } else {
                    Some(Type { val })
                }
            }
            TypeForm::Union {
                name,
                id,
                constr,
                names,
            } => {
                let mut new_constr = vec![];
                let mut changed = false;
                for typ in constr {
                    let inst = self.instantiate(typ, type_vars, dynamic_constants)?;
                    changed = changed || typ.val != inst.val;
                    new_constr.push(inst);
                }

                if changed {
                    Some(self.create_type(TypeForm::Union {
                        name: name,
                        id: id,
                        constr: new_constr,
                        names: names.clone(),
                    }))
                } else {
                    Some(Type { val })
                }
            }
            TypeForm::MultiReturn { .. } => {
                panic!("Multi-Return types should never be instantiated")
            }
        }
    }

    // Used as type and function boundaries to ensure that no uncounstrained
    // types remain
    pub fn solve(&mut self) -> Result<(), (parser::Kind, Location)> {
        for i in self.solved..self.types.len() {
            match self.types[i] {
                TypeForm::AnyOfKind { kind, loc } => return Err((kind, loc)),
                _ => (),
            }
        }

        self.solved = self.types.len();
        Ok(())
    }

    pub fn create_instance(&self, type_vars: Vec<TypeID>) -> TypeSolverInst {
        let num_vars = self.types.len();
        assert!(
            self.solved == num_vars,
            "Cannot instantiate with unsolved variables"
        );

        TypeSolverInst {
            solver: self,
            type_vars: type_vars,
            solved: vec![None; num_vars],
        }
    }
}

impl TypeSolverInst<'_> {
    pub fn lower_type(&mut self, builder: &mut Builder, Type { val }: Type) -> TypeID {
        if self.solved[val].is_some() {
            return self.solved[val].unwrap();
        }

        let mut worklist = VecDeque::from([val]);
        let mut depends: HashMap<usize, HashSet<usize>> = HashMap::new();

        while !worklist.is_empty() {
            let typ = worklist.pop_front().unwrap();

            // If this type is already solved, just continue.
            // Since we don't depend on something unless its unsolved we only need to drain the set
            // of dependences once
            if self.solved[typ].is_some() {
                continue;
            }

            let solution: Either<TypeID, usize> = match &self.solver.types[typ] {
                TypeForm::Primitive { prim, .. } => {
                    Either::Left(Self::build_primitive(builder, *prim))
                }
                TypeForm::Tuple { fields, .. } => {
                    let mut needs = None;
                    let mut i_fields = vec![];

                    for Type { val } in fields {
                        match &self.solved[*val] {
                            Some(ty) => i_fields.push(*ty),
                            None => {
                                needs = Some(*val);
                                break;
                            }
                        }
                    }

                    if let Some(t) = needs {
                        Either::Right(t)
                    } else {
                        Either::Left(Self::build_product(builder, i_fields))
                    }
                }
                TypeForm::Array {
                    elem: Type { val },
                    dims,
                    ..
                } => match &self.solved[*val] {
                    Some(ty) => Either::Left(Self::build_array(builder, *ty, dims)),
                    None => Either::Right(*val),
                },
                TypeForm::OtherType {
                    other: Type { val },
                    ..
                } => match &self.solved[*val] {
                    Some(ty) => Either::Left(*ty),
                    None => Either::Right(*val),
                },
                TypeForm::TypeVar { name: _, index, .. } => Either::Left(self.type_vars[*index]),
                TypeForm::Struct {
                    name: _,
                    id: _,
                    fields,
                    ..
                } => {
                    let mut needs = None;
                    let mut i_fields = vec![];

                    for Type { val } in fields {
                        match &self.solved[*val] {
                            Some(ty) => i_fields.push(*ty),
                            None => {
                                needs = Some(*val);
                                break;
                            }
                        }
                    }

                    if let Some(t) = needs {
                        Either::Right(t)
                    } else {
                        Either::Left(Self::build_product(builder, i_fields))
                    }
                }
                TypeForm::Union {
                    name: _,
                    id: _,
                    constr,
                    ..
                } => {
                    let mut needs = None;
                    let mut i_constr = vec![];

                    for Type { val } in constr {
                        match &self.solved[*val] {
                            Some(ty) => i_constr.push(*ty),
                            None => {
                                needs = Some(*val);
                                break;
                            }
                        }
                    }

                    if let Some(t) = needs {
                        Either::Right(t)
                    } else {
                        Either::Left(Self::build_union(builder, i_constr))
                    }
                }
                TypeForm::AnyOfKind { .. } => {
                    panic!("TypeSolverInst only works on solved types which do not have AnyOfKinds")
                }
                TypeForm::MultiReturn { .. } => {
                    panic!("MultiReturn types should never be lowered")
                }
            };

            match solution {
                Either::Left(solution) => {
                    self.solved[typ] = Some(solution);
                    match depends.get_mut(&typ) {
                        None => {}
                        Some(set) => {
                            for idx in set.drain() {
                                worklist.push_back(idx);
                            }
                        }
                    }
                }
                Either::Right(needs) => {
                    depends.entry(needs).or_insert(HashSet::new()).insert(typ);
                    worklist.push_back(needs);
                }
            }
        }

        self.solved[val].expect("Failure to solve type constraints")
    }

    pub fn as_numeric_type(&mut self, builder: &mut Builder, ty: Type) -> Primitive {
        let type_id = self.lower_type(builder, ty);
        if type_id == builder.create_type_i8() {
            Primitive::I8
        } else if type_id == builder.create_type_i16() {
            Primitive::I16
        } else if type_id == builder.create_type_i32() {
            Primitive::I32
        } else if type_id == builder.create_type_i64() {
            Primitive::I64
        } else if type_id == builder.create_type_u8() {
            Primitive::U8
        } else if type_id == builder.create_type_u16() {
            Primitive::U16
        } else if type_id == builder.create_type_u32() {
            Primitive::U32
        } else if type_id == builder.create_type_u64() {
            Primitive::U64
        } else if type_id == builder.create_type_fp8() {
            Primitive::FP8
        } else if type_id == builder.create_type_bf16() {
            Primitive::BF16
        } else if type_id == builder.create_type_f32() {
            Primitive::F32
        } else if type_id == builder.create_type_f64() {
            Primitive::F64
        } else {
            panic!("as_numeric_type() called on non-numeric type")
        }
    }

    fn build_primitive(builder: &mut Builder, p: Primitive) -> TypeID {
        match p {
            Primitive::Bool => builder.create_type_bool(),
            Primitive::I8 => builder.create_type_i8(),
            Primitive::I16 => builder.create_type_i16(),
            Primitive::I32 => builder.create_type_i32(),
            Primitive::I64 => builder.create_type_i64(),
            Primitive::U8 => builder.create_type_u8(),
            Primitive::U16 => builder.create_type_u16(),
            Primitive::U32 => builder.create_type_u32(),
            Primitive::U64 => builder.create_type_u64(),
            Primitive::FP8 => builder.create_type_fp8(),
            Primitive::BF16 => builder.create_type_bf16(),
            Primitive::F32 => builder.create_type_f32(),
            Primitive::F64 => builder.create_type_f64(),
            Primitive::Unit => builder.create_type_prod(vec![].into()),
        }
    }

    fn build_product(builder: &mut Builder, tys: Vec<TypeID>) -> TypeID {
        builder.create_type_prod(tys.into())
    }

    fn build_union(builder: &mut Builder, tys: Vec<TypeID>) -> TypeID {
        builder.create_type_sum(tys.into())
    }

    fn build_array(builder: &mut Builder, elem: TypeID, dims: &Vec<DynConst>) -> TypeID {
        let extents = Self::build_dyn_consts(builder, dims);
        builder.create_type_array(elem, extents.into())
    }

    pub fn build_dyn_consts(builder: &mut Builder, vals: &Vec<DynConst>) -> Vec<DynamicConstantID> {
        let mut res = vec![];
        for val in vals {
            res.push(val.build(builder));
        }
        res
    }
}
