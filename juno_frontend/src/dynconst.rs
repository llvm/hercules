/* A data structure for normalizing and performing computation over dynamic constant expressions */
use std::collections::HashMap;
use std::{fmt, iter};

use hercules_ir::{Builder, DynamicConstantID};

use num_rational::Ratio;
use num_traits::identities::{One, Zero};

// A dynamic constant is represented as a map from a vector of the powers of the variables to the
// coefficient for that term
#[derive(Eq, Clone)]
pub struct DynConst {
    terms: HashMap<Vec<i64>, Ratio<i64>>,
    vars: usize,
}

// Two dynamic constants are equal if all terms in each polynomial with non-zero
// coefficients have an equivalent term in the other polynomial
impl PartialEq for DynConst {
    fn eq(&self, other: &Self) -> bool {
        if self.vars != other.vars {
            return false;
        }

        for (t, c) in self.terms.iter() {
            if !c.is_zero() {
                if let Some(q) = other.terms.get(t) {
                    if c != q {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }

        for (t, c) in other.terms.iter() {
            if !c.is_zero() {
                if let Some(q) = other.terms.get(t) {
                    if c != q {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }

        return true;
    }
}

impl fmt::Debug for DynConst {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string(&|i| format!("v{}", i)))
    }
}

impl DynConst {
    // Construct a dynamic constant whose value is a constant integer
    pub fn constant(val: i64, num_dyn: usize) -> DynConst {
        Self::constant_ratio(Ratio::from_integer(val), num_dyn)
    }

    // Construct a dynamic constant whose value is a constant rational value
    pub fn constant_ratio(val: Ratio<i64>, num_dyn: usize) -> DynConst {
        // Create the vector of powers for each variable, all these powers are
        // just 0
        let powers = vec![0; num_dyn];
        DynConst {
            terms: HashMap::from([(powers, val)]),
            vars: num_dyn,
        }
    }

    // Construct the zero dynamic constant
    pub fn zero(dc: &DynConst) -> DynConst {
        Self::constant(0, dc.vars)
    }

    // Construct the dynamic constant value for a single particular dynamic
    // constant
    pub fn dynamic_constant(idx: usize, num_dyn: usize) -> DynConst {
        let mut powers = vec![0; num_dyn];
        powers[idx] = 1;
        DynConst {
            terms: HashMap::from([(powers, Ratio::one())]),
            vars: num_dyn,
        }
    }

    // Adds a term (defined by the powers in t and coefficient c) to the
    // dynamic constant dc
    // This is used in implementing other operations
    fn add_term(dc: &mut DynConst, t: &Vec<i64>, c: Ratio<i64>) {
        if !c.is_zero() {
            if let Some(cur_coeff) = dc.terms.get_mut(t) {
                *cur_coeff += c;
            } else {
                dc.terms.insert(t.clone(), c);
            }
        }
    }

    // Same as add_term except the vector of powers is owned (which allows us
    // to avoid a clone that might otherwise be necessary)
    fn add_term_owned(dc: &mut DynConst, t: Vec<i64>, c: Ratio<i64>) {
        if !c.is_zero() {
            if let Some(cur_coeff) = dc.terms.get_mut(&t) {
                *cur_coeff += c;
            } else {
                dc.terms.insert(t, c);
            }
        }
    }

    // Adds two dynamic constants
    pub fn add(lhs: &DynConst, rhs: &DynConst) -> DynConst {
        let mut res = lhs.clone();
        assert!(lhs.vars == rhs.vars);

        for (term, coeff) in rhs.terms.iter() {
            Self::add_term(&mut res, term, *coeff);
        }

        res
    }

    // Subtracts a dynamic constant from another
    pub fn sub(lhs: &DynConst, rhs: &DynConst) -> DynConst {
        let mut res = lhs.clone();
        assert!(lhs.vars == rhs.vars);

        // Works like add except the coefficients we add are negated
        for (term, coeff) in rhs.terms.iter() {
            Self::add_term(&mut res, term, -*coeff);
        }

        res
    }

    // Multiplies two dynamic constants
    pub fn mul(lhs: &DynConst, rhs: &DynConst) -> DynConst {
        let mut res = DynConst::zero(lhs);
        assert!(lhs.vars == rhs.vars);

        // Loop through each pair of terms in the left and right-hand sides
        // We multiply each of pairs and add all of those results together
        for (ltrm, lcoeff) in lhs.terms.iter() {
            for (rtrm, rcoeff) in rhs.terms.iter() {
                // Add the powers from the two terms together
                let term = ltrm
                    .iter()
                    .zip(rtrm.iter())
                    .map(|(lp, rp)| lp + rp)
                    .collect::<Vec<_>>();
                // Multiply their coefficients
                let coeff = *lcoeff * *rcoeff;

                Self::add_term_owned(&mut res, term, coeff);
            }
        }

        res
    }

    // Our division at least currently only supports division by a single term
    // This also returns None if we try to divide by zero
    pub fn div(lhs: &DynConst, rhs: &DynConst) -> Option<DynConst> {
        assert!(lhs.vars == rhs.vars);

        if rhs.terms.len() != 1 {
            return None;
        }
        let (dterm, dcoeff) = rhs.terms.iter().nth(0).expect("From above");
        if dcoeff.is_zero() {
            return None;
        }

        let mut res = DynConst::zero(lhs);

        for (t, c) in lhs.terms.iter() {
            let term = t
                .iter()
                .zip(dterm.iter())
                .map(|(lp, rp)| lp - rp)
                .collect::<_>();
            let coeff = c / dcoeff;
            Self::add_term_owned(&mut res, term, coeff);
        }

        Some(res)
    }

    // Computes the negation of a dynamic constant
    pub fn negate(val: &DynConst) -> DynConst {
        DynConst {
            terms: val
                .terms
                .iter()
                .map(|(t, c)| (t.clone(), -c))
                .collect::<_>(),
            vars: val.vars,
        }
    }

    // Raises a dynamic constant to a particular power
    fn power(val: &DynConst, power: i64) -> Option<DynConst> {
        let mut res = DynConst::constant(1, val.vars);

        if power > 0 {
            for _ in 0..power {
                res = DynConst::mul(&res, val);
            }
        } else if power < 0 {
            let times = -power;
            for _ in 0..times {
                res = DynConst::div(&res, val)?;
            }
        }

        Some(res)
    }

    // Substitutes the variables in this dynamic constant for the dynamic
    // constant expressions vars
    pub fn subst(&self, vars: &Vec<DynConst>) -> Option<DynConst> {
        assert!(self.vars == vars.len());

        // Only a constant, so we can just clone
        if self.vars == 0 {
            return Some(self.clone());
        }

        // Otherwise, we have at least one variable and can use this to determine the number of
        // variables in the resulting polynomial
        let num_dyns = vars[0].vars;
        let mut res = DynConst::constant(0, num_dyns);

        for (term, coeff) in self.terms.iter() {
            let mut product = DynConst::constant_ratio(*coeff, num_dyns);
            for (var, power) in term.iter().enumerate() {
                product = DynConst::mul(&product, &DynConst::power(&vars[var], *power)?);
            }
            res = DynConst::add(&res, &product);
        }

        Some(res)
    }

    fn term_to_string(term: &Vec<i64>, stringtab: &dyn Fn(usize) -> String) -> String {
        term.iter()
            .enumerate()
            .map(|(i, p)| {
                if *p == 0 {
                    "".to_string()
                } else {
                    format!("{}^{}", stringtab(i), p)
                }
            })
            .filter(|s| !s.is_empty())
            .collect::<Vec<_>>()
            .join(" ")
    }

    // Converts the dynamic constant into a string using a function which maps
    // variable numbers into names
    // Useful for debugging and error messages
    pub fn to_string(&self, stringtab: &dyn Fn(usize) -> String) -> String {
        let mut vec = self.terms.iter().collect::<Vec<_>>();
        vec.sort();
        vec.iter()
            .map(|(t, c)| format!("{} {}", c.to_string(), Self::term_to_string(*t, stringtab)))
            .collect::<Vec<_>>()
            .join(" + ")
    }

    // Builds a dynamic constant in the IR
    pub fn build(&self, builder: &mut Builder) -> DynamicConstantID {
        // Identify the terms with non-zero coefficients, based on the powers
        let mut non_zero_coeff = self
            .terms
            .iter()
            .filter(|(_, c)| !c.is_zero())
            .collect::<Vec<_>>();
        non_zero_coeff.sort_by(|(d1, _), (d2, _)| d1.cmp(d2));

        let (pos, neg): (Vec<_>, Vec<_>) = non_zero_coeff
            .iter()
            .map(|(d, c)| self.build_mono(builder, d, c))
            .partition(|(_, neg)| !*neg);

        let pos_sum =
            builder.create_dynamic_constant_add_many(pos.into_iter().map(|(t, _)| t).collect());

        let neg_sum = if neg.is_empty() {
            None
        } else {
            Some(
                builder.create_dynamic_constant_add_many(neg.into_iter().map(|(t, _)| t).collect()),
            )
        };

        match neg_sum {
            None => pos_sum,
            Some(neg) => builder.create_dynamic_constant_sub(pos_sum, neg),
        }
    }

    // Build's a monomial, with a given list of powers (term) and coefficients
    // Returns the dynamic constant id of the positive value and a boolean
    // indicating whether the value should actually be negative
    fn build_mono(
        &self,
        builder: &mut Builder,
        term: &Vec<i64>,
        coeff: &Ratio<i64>,
    ) -> (DynamicConstantID, bool) {
        let (pos, neg): (Vec<_>, Vec<_>) = term
            .iter()
            .enumerate()
            .filter(|(_, p)| **p != 0)
            .map(|(v, p)| self.build_power(builder, v, *p))
            .partition(|(_, neg)| !*neg);
        let mut pos: Vec<_> = pos.into_iter().map(|(t, _)| t).collect();
        let mut neg: Vec<_> = neg.into_iter().map(|(t, _)| t).collect();

        let numerator = {
            let numer: i64 = coeff.numer().abs();
            let numer_dc = builder.create_dynamic_constant_constant(numer as usize);
            pos.push(numer_dc);
            builder.create_dynamic_constant_mul_many(pos)
        };

        let denominator = {
            let denom: i64 = *coeff.denom();
            assert!(denom > 0);

            if neg.is_empty() && denom == 1 {
                None
            } else {
                let denom_dc = builder.create_dynamic_constant_constant(denom as usize);
                neg.push(denom_dc);
                Some(builder.create_dynamic_constant_mul_many(neg))
            }
        };

        if let Some(denominator) = denominator {
            (
                builder.create_dynamic_constant_div(numerator, denominator),
                *coeff.numer() < 0,
            )
        } else {
            (numerator, *coeff.numer() < 0)
        }
    }

    // Build's a dynamic constant that is a certain power of a specific variable
    // Returns the dynamic constant id of variable raised to the absolute value of the power and a
    // boolean indicating whether the power is actually negative
    fn build_power(
        &self,
        builder: &mut Builder,
        v: usize,
        power: i64,
    ) -> (DynamicConstantID, bool) {
        assert!(power != 0);
        let power_pos = power.abs() as usize;

        let var_id = builder.create_dynamic_constant_parameter(v);
        let power_id =
            builder.create_dynamic_constant_mul_many((0..power_pos).map(|_| var_id).collect());

        (power_id, power < 0)
    }
}
