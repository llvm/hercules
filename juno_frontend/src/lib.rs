mod codegen;
mod dynconst;
mod intrinsics;
mod labeled_builder;
mod locs;
mod parser;
mod semant;
mod ssa;
mod types;

use juno_scheduler::{schedule_hercules, schedule_juno};

use std::fmt;
use std::path::Path;

pub enum JunoVerify {
    None,
    JunoOpts,
    AllPasses,
}
impl JunoVerify {
    pub fn verify(&self) -> bool {
        match self {
            JunoVerify::None => false,
            _ => true,
        }
    }
    pub fn verify_all(&self) -> bool {
        match self {
            JunoVerify::AllPasses => true,
            _ => false,
        }
    }
}

pub enum JunoSchedule {
    None,
    DefaultSchedule,
    Schedule(String),
}

macro_rules! add_verified_pass {
    ($pm:ident, $verify:ident, $pass:ident) => {
        $pm.add_pass(hercules_opt::pass::Pass::$pass);
        if $verify.verify() || $verify.verify_all() {
            $pm.add_pass(hercules_opt::pass::Pass::Verify);
        }
    };
}
macro_rules! add_pass {
    ($pm:ident, $verify:ident, $pass:ident) => {
        $pm.add_pass(hercules_opt::pass::Pass::$pass);
        if $verify.verify_all() {
            $pm.add_pass(hercules_opt::pass::Pass::Verify);
        }
    };
}

pub enum ErrorMessage {
    SemanticError(semant::ErrorMessages),
    SchedulingError(String),
}

impl fmt::Display for ErrorMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ErrorMessage::SemanticError(errs) => {
                for err in errs {
                    write!(f, "{}\n", err)?;
                }
            }
            ErrorMessage::SchedulingError(msg) => {
                write!(f, "{}\n", msg)?;
            }
        }
        Ok(())
    }
}

pub fn compile(
    src_file: String,
    schedule: Option<String>,
    output_dir: String,
) -> Result<(), ErrorMessage> {
    let src_file_path = Path::new(&src_file);
    let module_name = String::from(src_file_path.file_stem().unwrap().to_str().unwrap());

    let prg = match semant::parse_and_analyze(src_file) {
        Ok(prg) => prg,
        Err(msg) => {
            return Err(ErrorMessage::SemanticError(msg));
        }
    };
    let (module, juno_info) = codegen::codegen_program(prg);

    schedule_juno(module, juno_info, schedule, output_dir, module_name)
        .map_err(|s| ErrorMessage::SchedulingError(s))
}

pub fn compile_ir(
    module: hercules_ir::ir::Module,
    schedule: Option<String>,
    output_dir: String,
    module_name: String,
) -> Result<(), ErrorMessage> {
    schedule_hercules(module, schedule, output_dir, module_name)
        .map_err(|s| ErrorMessage::SchedulingError(s))
}
