%start Program

%token UNARY
%avoid_insert "FUNC_ATTR" "DOT_NUM" "ID" "INT" "HEX_INT" "BIN_INT" "OCT_INT" "FLOAT_LIT" "LABEL"
%expect-unused Unmatched 'UNMATCHED' 'UNARY'

%nonassoc ')'
%nonassoc 'else'
%left '||'
%left '&&'
%left '|'
%left '^'
%left '&'
%nonassoc '==' '!='
%nonassoc '<' '<=' '>' '>='
%left '<<' '>>'
%left '+' '-'
%left '*' '/' '%'
%left 'as' 'size'
%right '~' '!' 'UNARY'
%left '.' 'DOT_NUM' '[' ']'

%%
Program -> Result<Prg, ()>
  : { Ok(vec![]) }
  | Program Top { flatten($1, $2) }
  ;

Top -> Result<Top, ()>
  : Import    { $1 }
  | TypeDecl  { $1 }
  | ConstDecl { $1 }
  | FuncDecl  { $1 }
  | Module    { $1 }
  ;

PubOption -> Result<bool, ()>
  :       { Ok(false) }
  | 'pub' { Ok(true) }
  ;

Module -> Result<Top, ()>
  : PubOption 'mod' 'ID' '{' Program '}'
    { Ok(Top::ModDecl{ span : $span, public : $1?, name : span_of_tok($3)?, body : $5? }) };

PackageName -> Result<PackageName, ()>
  : 'ID'                  { Ok(vec![span_of_tok($1)?]) }
  | PackageName '::' 'ID' { flatten($1, span_of_tok($3)) }
  ;

PackageId -> Result<ImportName, ()>
  : PackageName           { Ok(($1?, None)) }
  | PackageName '::' '*'  { Ok(($1?, Some(span_of_tok($3)?))) }
  ;

Import -> Result<Top, ()>
  : 'use' PackageId ';' { Ok(Top::Import{ span : $span, name : $2? }) };

TypeVars -> Result<Vec<TypeVar>, ()>
  :                   { Ok(vec![]) }
  | '<' TypeVarsI '>' { Ok($2?.into_iter().flatten().collect()) }
  ;
TypeVarsI -> Result<VecDeque<Vec<TypeVar>>, ()>
  :                         { Ok(VecDeque::new()) }
  | TypeVar                 { Ok(VecDeque::from([$1?])) }
  | TypeVar ',' TypeVarsI   { cons_deque($1, $3) }
  ;
TypeVar -> Result<Vec<TypeVar>, ()>
  : IdList ':' Kind { let kind = $3?;
                      Ok($1?.into_iter()
                            .map(|n| TypeVar { span : $span, name : n, kind : kind })
                            .collect()) }
  ;
IdList -> Result<Vec<Id>, ()>
  : 'ID'            { Ok(vec![span_of_tok($1)?]) }
  | IdList ',' 'ID' { flatten($1, span_of_tok($3)) }
  ;

Kind -> Result<Kind, ()>
  : 'type'    { Ok(Kind::Type) }
  | 'usize'   { Ok(Kind::USize) }
  | 'number'  { Ok(Kind::Number) }
  | 'integer' { Ok(Kind::Integer) }
  | 'float'   { Ok(Kind::Float) }
  ;

TypeDecl -> Result<Top, ()>
  : PubOption 'type' 'ID' TypeVars '=' TypeDef ';'
    { Ok(Top::TypeDecl{ span : $span, public : $1?, name : span_of_tok($3)?, ty_vars : $4?,
                        body : $6? } )};

TypeDef -> Result<TyDef, ()>
  : Type
      { Ok(TyDef::TypeAlias{ span : $span, body : $1? }) }
  | PubOption 'struct' '{' ObjFields '}'
      { Ok(TyDef::Struct{ span : $span, public : $1?, fields : $4? }) }
  | PubOption 'union'  '{' ObjFields '}'
      { Ok(TyDef::Union { span : $span, public : $1?, fields : $4? }) }
  ;

ObjFields -> Result<Vec<ObjField>, ()>
  : ObjFieldList { Ok($1?.into_iter().collect()) }
  ;
ObjFieldList -> Result<VecDeque<ObjField>, ()>
  :                           { Ok(VecDeque::new()) }
  | ObjField                  { Ok(VecDeque::from([$1?])) }
  | ObjField ',' ObjFieldList { let mut lst = $3?; lst.push_front($1?); Ok(lst) }
  | ObjField ';' ObjFieldList { let mut lst = $3?; lst.push_front($1?); Ok(lst) }
  ;
ObjField -> Result<ObjField, ()>
  : PubOption 'ID'
        { Ok(ObjField{ span : $span, public : $1?, name : span_of_tok($2)?, typ : None }) }
  | PubOption 'ID' ':' Type
        { Ok(ObjField{ span : $span, public : $1?, name : span_of_tok($2)?, typ : Some($4?) }) }
  ;

Type -> Result<Type, ()>
  : PrimType
        { Ok(Type::PrimType{ span : $span, typ : $1? }) }
  | '_'
        { Ok(Type::WildType{ span : $span }) }
  | '(' Types ')'
        { Ok(Type::TupleType{ span : $span, tys : $2? }) }
  | PackageName
        { Ok(Type::NamedType{ span : $span, name : $1?, args : None }) }
  | PackageName '::' '<' TypeExprs '>'
        { Ok(Type::NamedType{ span : $span, name : $1?, args : Some($4?) }) }
  | Type '[' TypeExprs ']'
        { Ok(Type::ArrayType{ span : $span, elem : Box::new($1?), dims : $3? }) }
  ;
Types -> Result<Vec<Type>, ()>
  :                   { Ok(vec![]) }
  | Type              { Ok(vec![$1?]) }
  | TypesS ',' Type   { flatten($1, $3) }
  ;
TypesS -> Result<Vec<Type>, ()>
  : Type              { Ok(vec![$1?]) }
  | TypesS ',' Type   { flatten($1, $3) }
  ;
PrimType -> Result<Primitive, ()>
  : 'bool'  { Ok(Primitive::Bool) }
  | 'i8'    { Ok(Primitive::I8) }
  | 'u8'    { Ok(Primitive::U8) }
  | 'i16'   { Ok(Primitive::I16) }
  | 'u16'   { Ok(Primitive::U16) }
  | 'i32'   { Ok(Primitive::I32) }
  | 'u32'   { Ok(Primitive::U32) }
  | 'i64'   { Ok(Primitive::I64) }
  | 'u64'   { Ok(Primitive::U64) }
  | 'usize' { Ok(Primitive::USize) }
  | 'fp8'   { Ok(Primitive::FP8) }
  | 'bf16'  { Ok(Primitive::BF16) }
  | 'f32'   { Ok(Primitive::F32) }
  | 'f64'   { Ok(Primitive::F64) }
  | 'void'  { Ok(Primitive::Void) }
  ;

ConstDecl -> Result<Top, ()>
  : PubOption 'const' 'ID' '=' Expr ';'
        { Ok(Top::ConstDecl{ span : $span, public : $1?, name : span_of_tok($3)?, ty : None,
                             body : $5? }) }
  | PubOption 'const' 'ID' ':' Type '=' Expr ';'
        { Ok(Top::ConstDecl{ span : $span, public : $1?, name : span_of_tok($3)?, ty : Some($5?),
                             body : $7? }) }
  ;

FuncDecl -> Result<Top, ()>
  : PubOption 'fn' 'ID' TypeVars '(' Arguments ')' Stmts
      { Ok(Top::FuncDecl{ span : $span, public : $1?, attr : None, name : span_of_tok($3)?,
                          ty_vars : $4?, args : $6?, rets: vec![], body : $8? }) }
  | 'FUNC_ATTR' PubOption 'fn' 'ID' TypeVars '(' Arguments ')' Stmts
      { Ok(Top::FuncDecl{ span : $span, public : $2?, attr : Some(span_of_tok($1)?),
                          name : span_of_tok($4)?, ty_vars : $5?, args : $7?, rets: vec![],
                          body : $9? }) }
  | PubOption 'fn' 'ID' TypeVars '(' Arguments ')' '->' Types Stmts
      { Ok(Top::FuncDecl{ span : $span, public : $1?, attr : None, name : span_of_tok($3)?,
                          ty_vars : $4?, args : $6?, rets: $9?, body : $10? }) }
  | 'FUNC_ATTR' PubOption 'fn' 'ID' TypeVars '(' Arguments ')' '->' Types Stmts
      { Ok(Top::FuncDecl{ span : $span, public : $2?, attr : Some(span_of_tok($1)?),
                          name : span_of_tok($4)?, ty_vars : $5?, args : $7?, rets: $10?,
                          body : $11? }) }
  ;
Arguments -> Result<Vec<(Option<Span>, VarBind)>, ()>
  : ArgumentsList { Ok($1?.into_iter().collect()) }
  ;
ArgumentsList -> Result<VecDeque<(Option<Span>, VarBind)>, ()>
  :                             { Ok(VecDeque::new()) }
  | ArgBind                     { Ok(VecDeque::from([$1?])) }
  | ArgBind ',' ArgumentsList   { let mut lst = $3?; lst.push_front($1?); Ok(lst) }
  ;
ArgBind -> Result<(Option<Span>, VarBind), ()>
  : 'inout' VarBind { Ok((Some(span_of_tok($1)?), $2?)) }
  | VarBind         { Ok((None, $1?)) }
  ;

VarBind -> Result<VarBind, ()>
  : Pattern          { Ok(VarBind{ span : $span, pattern : $1?, typ : None }) }
  | Pattern ':' Type { Ok(VarBind{ span : $span, pattern : $1?, typ : Some($3?) }) }
  ;

LetBind -> Result<LetBind, ()>
  : VarBind { 
      let VarBind { span, pattern, typ } = $1?;
      Ok(LetBind::Single { span, pattern, typ }) 
  }
  | PatternsCommaS ',' Pattern {
      let mut pats = $1?;
      pats.push($3?);
      Ok(LetBind::Multi { span: $span, patterns: pats })
  }
  ;

Pattern -> Result<Pattern, ()>
  : '_'                   { Ok(Pattern::Wildcard { span : $span }) }
  | IntLit                { let (span, base) = $1?;
                            Ok(Pattern::IntLit { span : span, base : base }) }
  | PackageName           { Ok(Pattern::Variable { span : $span, name : $1? }) }
  | '(' PatternsComma ')' { Ok(Pattern::TuplePattern { span : $span, pats : $2? }) }
  | PackageName '{' StructPatterns '}'
                          { let (pats, ignore_other) = $3?;
                            let pats = pats.into_iter().collect();
                            Ok(Pattern::StructPattern { span : $span, name : $1?, pats, ignore_other }) }
  | PackageName '(' PatternsComma ')'
                          { Ok(Pattern::UnionPattern { span : $span, name : $1?, pats : $3? }) }
  ;
PatternsComma -> Result<Vec<Pattern>, ()>
  :                             { Ok(vec![]) }
  | Pattern                     { Ok(vec![$1?]) }
  | PatternsCommaS ',' Pattern  { flatten($1, $3) }
  ;
PatternsCommaS -> Result<Vec<Pattern>, ()>
  : Pattern                     { Ok(vec![$1?]) }
  | PatternsCommaS ',' Pattern  { flatten($1, $3) }
  ;
StructPatterns -> Result<(VecDeque<(Id, Pattern)>, bool), ()>
  :                                     { Ok((VecDeque::new(), false)) }
  | '..'                                { Ok((VecDeque::new(), true)) }
  | 'ID'                                { let span = span_of_tok($1)?;
                                          let pattern = Pattern::Variable { span, name: vec![span] };
                                          Ok((VecDeque::from([(span, pattern)]), false)) }
  | 'ID' ':' Pattern                    { let span = span_of_tok($1)?;
                                          Ok((VecDeque::from([(span, $3?)]), false)) }
  | 'ID' ',' StructPatterns             { let span = span_of_tok($1)?;
                                          let pattern = Pattern::Variable { span, name: vec![span] };
                                          let (mut fields, ignore_rest) = $3?;
                                          fields.push_front((span, pattern));
                                          Ok((fields, ignore_rest)) }
  | 'ID' ':' Pattern ',' StructPatterns { let span = span_of_tok($1)?;
                                          let (mut fields, ignore_rest) = $5?;
                                          fields.push_front((span, $3?));
                                          Ok((fields, ignore_rest)) }
  ;

Stmt -> Result<Stmt, ()>
  : 'let' LetBind ';'
      { Ok(Stmt::LetStmt{ span : $span, var : $2?, init : None }) }
  | 'let' LetBind '=' Expr ';'
      { Ok(Stmt::LetStmt{ span : $span, var : $2?, init : Some($4?) }) }
  | 'const' VarBind ';'
      { Ok(Stmt::ConstStmt{ span : $span, var : $2?, init : None }) }
  | 'const' VarBind '=' Expr ';'
      { Ok(Stmt::ConstStmt{ span : $span, var : $2?, init : Some($4?) }) }
  | LExpr   '=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::None,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr  '+=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::Add,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr  '-=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::Sub,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr  '*=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::Mul,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr  '/=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::Div,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr  '%=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::Mod,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr  '&=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::BitAnd,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr  '|=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::BitOr,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr  '^=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::Xor,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr '&&=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::LogAnd,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr '||=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::LogOr,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr '<<=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::LShift,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | LExpr '>>=' Expr ';'
      { Ok(Stmt::AssignStmt{ span : $span, lhs : $1?, assign : AssignOp::RShift,
                             assign_span : span_of_tok($2)?, rhs : $3? }) }
  | IfStmt
      { $1 }
  | 'match' NonStructExpr Cases
      { Ok(Stmt::MatchStmt{ span : $span, expr : $2?, body : $3? }) }
  | 'for' VarBind '=' NonStructExpr 'to' NonStructExpr Stmts
      { Ok(Stmt::ForStmt{ span : $span, var : $2?, init : $4?, bound : $6?,
                          inclusive: false, step : None, body : Box::new($7?) }) }
  | 'for' VarBind '=' NonStructExpr 'to' NonStructExpr 'by' SignedIntLit Stmts
      { Ok(Stmt::ForStmt{ span : $span, var : $2?, init : $4?, bound : $6?,
                          inclusive: false, step : Some($8?), body : Box::new($9?) }) }
  | 'for' VarBind 'in' NonStructExpr '..' NonStructExpr Stmts
      { Ok(Stmt::ForStmt{ span: $span, var: $2?, init: $4?, bound: $6?,
                          inclusive: false, step: None, body: Box::new($7?) }) }
  | 'for' VarBind 'in' NonStructExpr '..' '=' NonStructExpr Stmts
      { Ok(Stmt::ForStmt{ span: $span, var: $2?, init: $4?, bound: $7?,
                          inclusive: true, step: None, body: Box::new($8?) }) }
  | 'while' NonStructExpr Stmts
      { Ok(Stmt::WhileStmt{ span : $span, cond : $2?, body : Box::new($3?) }) }
  | 'return' Exprs ';'
      { Ok(Stmt::ReturnStmt{ span : $span, vals: $2?}) }
  | 'break' ';'
      { Ok(Stmt::BreakStmt{ span : $span }) }
  | 'continue' ';'
      { Ok(Stmt::ContinueStmt{ span : $span }) }
  | Stmts
      { $1 }
  | PackageName '(' Params ')' ';'
      { Ok(Stmt::CallStmt{ span : $span, name : $1?, ty_args : None, args : $3? }) }
  | PackageName '::' '<' TypeExprs '>' '(' Params ')' ';'
      { Ok(Stmt::CallStmt{ span : $span, name : $1?, ty_args : Some($4?), args : $7? }) }
  | Label Stmt
      { Ok(Stmt::LabeledStmt { span : $span, label : $1?, stmt : Box::new($2?) }) }
  ;
Label -> Result<Span, ()> : 'LABEL' { Ok($span) };
Stmts -> Result<Stmt, ()>
  : '{' StmtList '}'  { Ok(Stmt::BlockStmt{ span : $span, body : $2? }) };
StmtList -> Result<Vec<Stmt>, ()>
  :                { Ok(vec![]) }
  | StmtList Stmt  { flatten($1, $2) }
  ;

IfStmt -> Result<Stmt, ()>
  : 'if' NonStructExpr Stmts
      { Ok(Stmt::IfStmt{ span : $span, cond : $2?, thn : Box::new($3?), els : None }) }
  | 'if' NonStructExpr Stmts 'else' IfStmt
      { Ok(Stmt::IfStmt{ span : $span, cond : $2?, thn : Box::new($3?),
                         els : Some(Box::new($5?)) }) }
  | 'if' NonStructExpr Stmts 'else' Stmts
      { Ok(Stmt::IfStmt{ span : $span, cond : $2?, thn : Box::new($3?),
                         els : Some(Box::new($5?)) }) }
  ;

Cases -> Result<Vec<Case>, ()>
  : '{' CaseList '}'  { $2 }
  ;
CaseList -> Result<Vec<Case>, ()>
  : Case          { Ok(vec![$1?]) }
  | CaseList Case { flatten($1, $2) }
  ;
Case -> Result<Case, ()>
  : Patterns '=>' Stmt { Ok(Case{ span : $span, pat : $1?, body : $3? }) };
Patterns -> Result<Vec<Pattern>, ()>
  : Pattern               { Ok(vec![$1?]) }
  | '|' Pattern           { Ok(vec![$2?]) }
  | Patterns '|' Pattern  { flatten($1, $3) }
  ;

LExpr -> Result<LExpr, ()>
  : 'ID'                { Ok(LExpr::VariableLExpr{ span : span_of_tok($1)? }) }
  | LExpr '.' 'ID'      { Ok(LExpr::FieldLExpr{ span : $span, lhs : Box::new($1?),
                                                rhs : span_of_tok($3)? }) }
  | LExpr 'DOT_NUM'     { Ok(LExpr::NumFieldLExpr { span : $span, lhs : Box::new($1?),
                                                    rhs : span_of_tok($2)? }) }
  | LExpr '[' Exprs ']' { Ok(LExpr::IndexLExpr{ span : $span, lhs : Box::new($1?), index : $3? }) }
  ;

SignedIntLit -> Result<(bool, Span, IntBase), ()>
  : '+' IntLit { Ok((false, $2?.0, $2?.1)) }
  |     IntLit { Ok((false, $1?.0, $1?.1)) }
  | '-' IntLit { Ok((true, $2?.0, $2?.1)) }
  ;

IntLit -> Result<(Span, IntBase), ()>
  : 'INT'     { Ok(($span, IntBase::Decimal)) }
  | 'HEX_INT' { Ok(($span, IntBase::Hexadecimal)) }
  | 'BIN_INT' { Ok(($span, IntBase::Binary)) }
  | 'OCT_INT' { Ok(($span, IntBase::Octal)) }
  ;

Exprs -> Result<Vec<Expr>, ()>
  :                 { Ok(vec![]) }
  | Expr            { Ok(vec![$1?]) }
  | ExprsS ',' Expr { flatten($1, $3) }
  ;
ExprsS -> Result<Vec<Expr>, ()>
  : Expr            { Ok(vec![$1?]) }
  | ExprsS ',' Expr { flatten($1, $3) }
  ;

Expr -> Result<Expr, ()>
  : PackageName
      { Ok(Expr::Variable{ span : $span, name : $1? }) }
  | Expr '.' 'ID'
      { Ok(Expr::Field{ span : $span, lhs : Box::new($1?), rhs : span_of_tok($3)? }) }
  | Expr 'DOT_NUM'
      { Ok(Expr::NumField{ span : $span, lhs : Box::new($1?), rhs : span_of_tok($2)? }) }
  | Expr '[' Exprs ']'
      { Ok(Expr::ArrIndex{ span : $span, lhs : Box::new($1?), index : $3? }) }
  | '(' Exprs ')'
      { Ok(Expr::Tuple{ span : $span, exprs : $2? }) }
  | PackageName '{' IdExprs '}'
      { Ok(Expr::Struct{ span : $span, name : $1?, ty_args : None, exprs : $3? }) }
  | PackageName '::' '<' TypeExprs '>' '{' IdExprs '}'
      { Ok(Expr::Struct{ span : $span, name : $1?, ty_args : Some($4?), exprs : $7? }) }
  | 'true'
      { Ok(Expr::BoolLit{ span : $span, value : true }) }
  | 'false'
      { Ok(Expr::BoolLit{ span : $span, value : false }) }
  | IntLit
      { let (span, base) = $1?;
        Ok(Expr::IntLit{ span : span, base : base }) }
  | 'FLOAT_LIT'
      { Ok(Expr::FloatLit{ span : $span }) }
  | '-' Expr %prec 'UNARY'
      { Ok(Expr::UnaryExpr{ span : $span, op : UnaryOp::Negation, expr : Box::new($2?)}) }
  | '~' Expr
      { Ok(Expr::UnaryExpr{ span : $span, op : UnaryOp::BitwiseNot, expr : Box::new($2?)}) }
  | '!' Expr
      { Ok(Expr::UnaryExpr{ span : $span, op : UnaryOp::LogicalNot, expr : Box::new($2?)}) }
  | Expr 'as' Type
      { Ok(Expr::CastExpr{ span : $span, expr : Box::new($1?), typ : $3?}) }
  | Expr '+' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Add, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '-' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Sub, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '*' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Mul, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '/' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Div, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '%' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Mod, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '&' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::BitAnd, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '&&' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::LogAnd, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '|' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::BitOr, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '||' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::LogOr, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '^' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Xor, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '<' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Lt, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '<=' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Le, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '>' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Gt, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '>=' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Ge, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '==' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Eq, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '!=' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Neq, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '<<' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::LShift, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | Expr '>>' Expr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::RShift, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | 'if' Expr 'then' Expr 'else' Expr
      { Ok(Expr::CondExpr{ span: $span, cond : Box::new($2?), thn : Box::new($4?), els : Box::new($6?) })}
  | PackageName '(' Params ')'
      { Ok(Expr::CallExpr{ span : $span, name : $1?, ty_args : None, args: $3? }) }
  | PackageName '::' '<' TypeExprs '>' '(' Params ')'
      { Ok(Expr::CallExpr{ span : $span, name : $1?, ty_args : Some($4?), args: $7? }) }
  | PackageName '!' '(' Params ')'
      { Ok(Expr::IntrinsicExpr{ span : $span, name : $1?, ty_args : None, args: $4? }) }
  | PackageName '!' '::' '<' TypeExprs '>' '(' Params ')'
      { Ok(Expr::IntrinsicExpr{ span : $span, name : $1?, ty_args : Some($5?), args: $8? }) }
  ;
IdExprs -> Result<Vec<(Id, Expr)>, ()>
  : IdExprList  { Ok($1?.into_iter().collect()) }
  ;
IdExprList -> Result<VecDeque<(Id, Expr)>, ()>
  :                       { Ok(VecDeque::new()) }
  | IdExpr                { Ok(VecDeque::from([$1?])) }
  | IdExpr ',' IdExprList { let mut lst = $3?; lst.push_front($1?); Ok(lst) }
  ;
IdExpr -> Result<(Id, Expr), ()>
  : 'ID' ':' Expr               { Ok((span_of_tok($1)?, $3?)) }
  | 'ID' '=' Expr               { Ok((span_of_tok($1)?, $3?)) }
  ;
Params -> Result<Vec<(bool, Expr)>, ()>
  :                       { Ok(vec![]) }
  | Expr                  { Ok(vec![(false, $1?)]) }
  | '&' Expr              { Ok(vec![(true, $2?)]) }
  | ParamsS ',' Expr      { flatten($1, Ok((false, $3?))) }
  | ParamsS ',' '&' Expr  { flatten($1, Ok((true, $4?))) }
  ;
ParamsS -> Result<Vec<(bool, Expr)>, ()>
  : Expr                  { Ok(vec![(false, $1?)]) }
  | '&' Expr              { Ok(vec![(true, $2?)]) }
  | ParamsS ',' Expr      { flatten($1, Ok((false, $3?))) }
  | ParamsS ',' '&' Expr  { flatten($1, Ok((true, $4?))) }
  ;

NonStructExpr -> Result<Expr, ()>
  : PackageName
      { Ok(Expr::Variable{ span : $span, name : $1? }) }
  | NonStructExpr '.' 'ID'
      { Ok(Expr::Field{ span : $span, lhs : Box::new($1?), rhs : span_of_tok($3)? }) }
  | NonStructExpr 'DOT_NUM'
      { Ok(Expr::NumField{ span : $span, lhs : Box::new($1?), rhs : span_of_tok($2)? }) }
  | NonStructExpr '[' Exprs ']'
      { Ok(Expr::ArrIndex{ span : $span, lhs : Box::new($1?), index : $3? }) }
  | '(' Exprs ')'
      { Ok(Expr::Tuple{ span : $span, exprs : $2? }) }
  | 'true'
      { Ok(Expr::BoolLit{ span : $span, value : true }) }
  | 'false'
      { Ok(Expr::BoolLit{ span : $span, value : false }) }
  | IntLit
      { let (span, base) = $1?;
        Ok(Expr::IntLit{ span : span, base : base }) }
  | 'FLOAT_LIT'
      { Ok(Expr::FloatLit{ span : $span }) }
  | '-' NonStructExpr %prec 'UNARY'
      { Ok(Expr::UnaryExpr{ span : $span, op : UnaryOp::Negation, expr : Box::new($2?)}) }
  | '~' NonStructExpr
      { Ok(Expr::UnaryExpr{ span : $span, op : UnaryOp::BitwiseNot, expr : Box::new($2?)}) }
  | '!' NonStructExpr
      { Ok(Expr::UnaryExpr{ span : $span, op : UnaryOp::LogicalNot, expr : Box::new($2?)}) }
  | NonStructExpr 'as' Type
      { Ok(Expr::CastExpr{ span : $span, expr : Box::new($1?), typ : $3?}) }
  | NonStructExpr '+' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Add, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '-' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Sub, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '*' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Mul, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '/' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Div, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '%' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Mod, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '&' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::BitAnd, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '&&' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::LogAnd, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '|' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::BitOr, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '||' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::LogOr, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '^' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Xor, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '<' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Lt, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '<=' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Le, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '>' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Gt, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '>=' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Ge, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '==' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Eq, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '!=' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::Neq, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '<<' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::LShift, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | NonStructExpr '>>' NonStructExpr
      { Ok(Expr::BinaryExpr{ span: $span, op: BinaryOp::RShift, lhs: Box::new($1?), rhs: Box::new($3?)}) }
  | 'if' NonStructExpr 'then' NonStructExpr 'else' NonStructExpr
      { Ok(Expr::CondExpr{ span: $span, cond : Box::new($2?), thn : Box::new($4?), els : Box::new($6?) })}
  | PackageName '(' Params ')'
      { Ok(Expr::CallExpr{ span : $span, name : $1?, ty_args : None, args: $3? }) }
  | PackageName '::' '<' TypeExprs '>' '(' Params ')'
      { Ok(Expr::CallExpr{ span : $span, name : $1?, ty_args : Some($4?), args: $7? }) }
  ;

TypeExprs -> Result<Vec<TypeExpr>, ()>
  :                         { Ok(vec![]) }
  | TypeExpr                { Ok(vec![$1?]) }
  | TypeExprsS ',' TypeExpr { flatten($1, $3) }
  ;
TypeExprsS -> Result<Vec<TypeExpr>, ()>
  : TypeExpr                { Ok(vec![$1?]) }
  | TypeExprsS ',' TypeExpr { flatten($1, $3) }
  ;
TypeExpr -> Result<TypeExpr, ()>
  : PrimType
      { Ok(TypeExpr::PrimType{ span : $span, typ : $1? }) }
  | '_'
      { Ok(TypeExpr::WildcardType { span : $span }) }
  | '(' TypeExprs ')'
      { Ok(TypeExpr::TupleType{ span : $span, tys : $2? }) }
  | PackageName
      { Ok(TypeExpr::NamedTypeExpr{ span : $span, name : $1?, args : None}) }
  | PackageName '::' '<' TypeExprs '>'
      { Ok(TypeExpr::NamedTypeExpr{ span : $span, name : $1?, args : Some($4?) }) }
  | TypeExpr '[' TypeExprs ']'
      { Ok(TypeExpr::ArrayTypeExpr{ span : $span, elem : Box::new($1?), dims : $3? }) }
  | IntLit
      { let (span, base) = $1?;
        Ok(TypeExpr::IntLiteral{ span : span, base : base }) }
  | '-' TypeExpr %prec UNARY
      { Ok(TypeExpr::Negative{ span : $span, expr : Box::new($2?)}) }
  | TypeExpr '+' TypeExpr
      { Ok(TypeExpr::Add{ span : $span, lhs : Box::new($1?), rhs : Box::new($3?) }) }
  | TypeExpr '-' TypeExpr
      { Ok(TypeExpr::Sub{ span : $span, lhs : Box::new($1?), rhs : Box::new($3?) }) }
  | TypeExpr '*' TypeExpr
      { Ok(TypeExpr::Mul{ span : $span, lhs : Box::new($1?), rhs : Box::new($3?) }) }
  | TypeExpr '/' TypeExpr
      { Ok(TypeExpr::Div{ span : $span, lhs : Box::new($1?), rhs : Box::new($3?) }) }
  ;

Unmatched -> (): 'UNMATCHED' { };
%%

use cfgrammar::Span;
use lrlex::{DefaultLexeme, DefaultLexerTypes};
use lrpar::NonStreamingLexer;
use std::collections::VecDeque;

fn flatten<T>(lhs: Result<Vec<T>, ()>, rhs: Result<T, ()>) -> Result<Vec<T>, ()> {
  let mut flt = lhs?;
  flt.push(rhs?);
  Ok(flt)
}

fn cons_deque<T>(lhs : Result<T, ()>, rhs : Result<VecDeque<T>, ()>) -> Result<VecDeque<T>, ()> {
  let mut lst = rhs?;
  lst.push_front(lhs?);
  Ok(lst)
}

fn span_of_tok(t : Result<DefaultLexeme, DefaultLexeme>) -> Result<Span, ()> {
  t.map_err(|_| ()).map(|l| l.span())
}

fn res_pair<A, B>(x : Result<A, ()>, y : Result<B, ()>) -> Result<(A, B), ()> {
  Ok((x?, y?))
}

pub type Prg = Vec<Top>;
pub type Id = Span;
pub type PackageName = Vec<Span>;
pub type ImportName  = (PackageName, Option<Span>); // option is the wildcard *

#[derive(Debug, Copy, Clone)]
pub enum Kind { Type, USize, Number, Integer, Float }
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Primitive { Bool, I8, U8, I16, U16, I32, U32, I64, U64, USize, FP8, BF16, F32, F64, Void }
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum AssignOp { None, Add, Sub, Mul, Div, Mod, BitAnd, BitOr, Xor, LogAnd, LogOr,
                    LShift, RShift }
#[derive(Debug, Copy, Clone)]
pub enum IntBase { Binary, Octal, Decimal, Hexadecimal }
#[derive(Debug, Copy, Clone)]
pub enum UnaryOp { Negation, BitwiseNot, LogicalNot }
#[derive(Debug, Copy, Clone)]
pub enum BinaryOp { Add, Sub, Mul, Div, Mod, BitAnd, LogAnd, BitOr, LogOr, Xor,
                    Lt, Le, Gt, Ge, Eq, Neq, LShift, RShift }

#[derive(Debug)]
pub struct ObjField { pub span : Span, pub public : bool, pub name : Id, pub typ : Option<Type> }
#[derive(Debug)]
pub struct TypeVar { pub span : Span, pub name : Id, pub kind : Kind }
#[derive(Debug)]
pub struct VarBind { pub span : Span, pub pattern : Pattern, pub typ : Option<Type> }
#[derive(Debug)]
pub struct Case { pub span : Span, pub pat : Vec<Pattern>, pub body : Stmt }

// Let bindings are different from other bindings because they can be used to
// destruct multi-return function values, and so can actually contain multiple
// patterns
#[derive(Debug)]
pub enum LetBind {
  Single { span: Span, pattern: Pattern, typ: Option<Type> },
  Multi  { span: Span, patterns: Vec<Pattern> },
}

#[derive(Debug)]
pub enum Top {
  Import    { span : Span, name : ImportName },
  TypeDecl  { span : Span, public : bool, name : Id, ty_vars : Vec<TypeVar>, body : TyDef },
  ConstDecl { span : Span, public : bool, name : Id, ty : Option<Type>, body : Expr },
  FuncDecl  { span : Span, public : bool, attr : Option<Span>, name : Id, ty_vars : Vec<TypeVar>,
              args : Vec<(Option<Span>, VarBind)>, // option is for inout
              rets : Vec<Type>, body : Stmt },
  ModDecl   { span : Span, public : bool, name : Id, body : Vec<Top> },
}

#[derive(Debug)]
pub enum TyDef {
  TypeAlias { span : Span, body : Type },
  Struct    { span : Span, public : bool, fields : Vec<ObjField> },
  Union     { span : Span, public : bool, fields : Vec<ObjField> },
}

#[derive(Debug)]
pub enum Type {
  PrimType  { span : Span, typ : Primitive },
  WildType  { span : Span },
  TupleType { span : Span, tys : Vec<Type> },
  NamedType { span : Span, name : PackageName, args : Option<Vec<TypeExpr>> },
  ArrayType { span : Span, elem : Box<Type>, dims : Vec<TypeExpr> },
}

#[derive(Debug)]
pub enum Stmt {
  LetStmt    { span : Span, var : LetBind, init : Option<Expr> },
  ConstStmt  { span : Span, var : VarBind, init : Option<Expr> },
  AssignStmt { span : Span, lhs : LExpr, assign : AssignOp, assign_span : Span, rhs : Expr },
  IfStmt     { span : Span, cond : Expr, thn : Box<Stmt>, els : Option<Box<Stmt>> },
  MatchStmt  { span : Span, expr : Expr, body : Vec<Case> },
  // The step records: negative, number, base, inclusive records whether the bound is included in the range
  ForStmt    { span : Span, var : VarBind, init : Expr, bound : Expr,
               inclusive: bool, step : Option<(bool, Span, IntBase)>, body : Box<Stmt> },
  WhileStmt  { span : Span, cond : Expr, body : Box<Stmt> },
  ReturnStmt { span : Span, vals : Vec<Expr> },
  BreakStmt  { span : Span },
  ContinueStmt { span : Span },
  BlockStmt    { span : Span, body : Vec<Stmt> },
  CallStmt     { span : Span, name : PackageName, ty_args : Option<Vec<TypeExpr>>,
                 args : Vec<(bool, Expr)> }, // bool indicates & (for inouts)
  LabeledStmt  { span : Span, label : Span, stmt : Box<Stmt> },
}

#[derive(Debug)]
pub enum Pattern {
  Wildcard         { span : Span },
  IntLit           { span : Span, base : IntBase },
  Variable         { span : Span, name : PackageName },
  TuplePattern     { span : Span, pats : Vec<Pattern> },
  // Ignore other indicates the pattern ended with .. and so there may be other fields that were not listed
  StructPattern    { span : Span, name : PackageName, pats : Vec<(Id, Pattern)>, ignore_other: bool },
  UnionPattern     { span : Span, name : PackageName, pats : Vec<Pattern> },
}

#[derive(Debug)]
pub enum LExpr {
  VariableLExpr { span : Span },
  FieldLExpr    { span : Span, lhs : Box<LExpr>, rhs : Id },
  NumFieldLExpr { span : Span, lhs : Box<LExpr>, rhs : Span },
  IndexLExpr    { span : Span, lhs : Box<LExpr>, index : Vec<Expr> },
}

#[derive(Debug)]
pub enum Expr {
  Variable      { span : Span, name : PackageName },
  Field         { span : Span, lhs : Box<Expr>, rhs : Id },
  NumField      { span : Span, lhs : Box<Expr>, rhs : Span },
  ArrIndex      { span : Span, lhs : Box<Expr>, index : Vec<Expr> },
  Tuple         { span : Span, exprs : Vec<Expr> },
  Struct        { span : Span, name : PackageName, ty_args : Option<Vec<TypeExpr>>,
                  exprs : Vec<(Id, Expr)> },
  BoolLit       { span : Span, value : bool },
  IntLit        { span : Span, base : IntBase },
  FloatLit      { span : Span },
  UnaryExpr     { span : Span, op : UnaryOp, expr : Box<Expr> },
  BinaryExpr    { span : Span, op : BinaryOp, lhs : Box<Expr>, rhs : Box<Expr> },
  CastExpr      { span : Span, expr : Box<Expr>, typ : Type },
  CondExpr      { span : Span, cond : Box<Expr>, thn : Box<Expr>, els : Box<Expr> },
  CallExpr      { span : Span, name : PackageName, ty_args : Option<Vec<TypeExpr>>,
                  args : Vec<(bool, Expr)> }, // bool indicates & (for inouts)
  IntrinsicExpr { span : Span, name : PackageName, ty_args : Option<Vec<TypeExpr>>,
                  args : Vec<(bool, Expr)> },
}

#[derive(Debug, Clone)]
pub enum TypeExpr {
  PrimType        { span : Span, typ : Primitive },
  WildcardType    { span : Span },
  TupleType       { span : Span, tys : Vec<TypeExpr> },
  NamedTypeExpr   { span : Span, name : PackageName, args : Option<Vec<TypeExpr>> },
  ArrayTypeExpr   { span : Span, elem : Box<TypeExpr>, dims : Vec<TypeExpr> },
  IntLiteral      { span : Span, base : IntBase },
  Negative        { span : Span, expr : Box<TypeExpr> },
  Add             { span : Span, lhs : Box<TypeExpr>, rhs : Box<TypeExpr> },
  Sub             { span : Span, lhs : Box<TypeExpr>, rhs : Box<TypeExpr> },
  Mul             { span : Span, lhs : Box<TypeExpr>, rhs : Box<TypeExpr> },
  Div             { span : Span, lhs : Box<TypeExpr>, rhs : Box<TypeExpr> },
}

pub trait Spans {
  fn span(&self) -> Span;
}

impl Spans for Expr {
  fn span(&self) -> Span {
    match self {
        Expr::Variable      { span, .. }
      | Expr::Field         { span, .. }
      | Expr::NumField      { span, .. }
      | Expr::ArrIndex      { span, .. }
      | Expr::Tuple         { span, .. }
      | Expr::Struct        { span, .. }
      | Expr::BoolLit       { span, .. }
      | Expr::IntLit        { span, .. }
      | Expr::FloatLit      { span }
      | Expr::UnaryExpr     { span, .. }
      | Expr::BinaryExpr    { span, .. }
      | Expr::CastExpr      { span, .. }
      | Expr::CondExpr      { span, .. }
      | Expr::CallExpr      { span, .. }
      | Expr::IntrinsicExpr { span, .. }
        => *span
    }
  }
}

impl Spans for Stmt {
  fn span(&self) -> Span {
    match self {
        Stmt::LetStmt      { span, .. }
      | Stmt::ConstStmt    { span, .. }
      | Stmt::AssignStmt   { span, .. }
      | Stmt::IfStmt       { span, .. }
      | Stmt::MatchStmt    { span, .. }
      | Stmt::ForStmt      { span, .. }
      | Stmt::WhileStmt    { span, .. }
      | Stmt::ReturnStmt   { span, .. }
      | Stmt::BreakStmt    { span, .. }
      | Stmt::ContinueStmt { span, .. }
      | Stmt::BlockStmt    { span, .. }
      | Stmt::CallStmt     { span, .. }
      | Stmt::LabeledStmt  { span, .. }
        => *span
    }
  }
}

impl Spans for TypeExpr {
  fn span(&self) -> Span {
    match self {
      TypeExpr::PrimType          { span, .. }
      | TypeExpr::WildcardType    { span, .. }
      | TypeExpr::TupleType       { span, .. }
      | TypeExpr::NamedTypeExpr   { span, .. }
      | TypeExpr::ArrayTypeExpr   { span, .. }
      | TypeExpr::IntLiteral      { span, .. }
      | TypeExpr::Negative        { span, .. }
      | TypeExpr::Add             { span, .. }
      | TypeExpr::Sub             { span, .. }
      | TypeExpr::Mul             { span, .. }
      | TypeExpr::Div             { span, .. }
        => *span
    }
  }
}

impl IntBase {
  pub fn base(&self) -> u32 {
    match self {
      IntBase::Binary      => 2,
      IntBase::Octal       => 8,
      IntBase::Decimal     => 10,
      IntBase::Hexadecimal => 16,
    }
  }

  pub fn string(&self, lexer : &dyn NonStreamingLexer<DefaultLexerTypes<u32>>, span : Span) -> String {
    let full_string = lexer.span_str(span);
    match self {
      IntBase::Decimal => full_string.to_string(),
      _ => full_string[2..].to_string(),
    }
  }
}
