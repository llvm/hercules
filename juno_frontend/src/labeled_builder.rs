use hercules_ir::build::*;
use hercules_ir::ir::*;

use juno_utils::stringtab::StringTable;

// A label-tracking code generator which tracks the current function that we're
// generating code for and the what labels apply to the nodes being created
// FIXME: The builder public is currently public to avoid code duplication and
// we're just allowing types and dynamic constants to be constructed on the
// builder directly, rather than on this labeled builder (since types and
// dynamic constants do not require labeling)
pub struct LabeledBuilder<'a> {
    pub builder: Builder<'a>,
    function: Option<FunctionID>,
    label_stack: Vec<LabelID>,
    label_tab: StringTable,
}
impl<'a> LabeledBuilder<'a> {
    pub fn create(labels: StringTable) -> LabeledBuilder<'a> {
        LabeledBuilder {
            builder: Builder::create(),
            function: None,
            label_stack: vec![],
            label_tab: labels,
        }
    }

    pub fn finish(self) -> Module {
        self.builder.finish()
    }

    pub fn create_function(
        &mut self,
        name: &str,
        param_types: Vec<TypeID>,
        return_types: Vec<TypeID>,
        num_dynamic_constants: u32,
        entry: bool,
    ) -> Result<(FunctionID, NodeID), String> {
        let (func, entry) = self.builder.create_function(
            name,
            param_types,
            return_types,
            num_dynamic_constants,
            entry,
        )?;

        Ok((func, entry))
    }

    pub fn set_function(&mut self, func: FunctionID) {
        self.function = Some(func);
        assert!(self.label_stack.is_empty());
    }

    pub fn push_label(&mut self, label: usize) {
        let Some(label_str) = self.label_tab.lookup_id(label) else {
            panic!("Label missing from string table")
        };
        let label_id = self.builder.add_label(&label_str);
        self.label_stack.push(label_id);
    }

    pub fn pop_label(&mut self) {
        let Some(_) = self.label_stack.pop() else {
            panic!("No label to pop")
        };
    }

    fn allocate_node_labeled(&mut self, label: Vec<LabelID>) -> NodeBuilder {
        let Some(func) = self.function else {
            panic!("Cannot allocate node without function")
        };

        let mut builder = self.builder.allocate_node(func);
        builder.add_labels(label.into_iter());
        builder
    }

    pub fn allocate_node(&mut self) -> NodeBuilder {
        self.allocate_node_labeled(self.label_stack.clone())
    }

    pub fn allocate_node_labeled_with(&mut self, other: NodeID) -> NodeBuilder {
        let Some(func) = self.function else {
            panic!("Cannot allocate node without function")
        };
        let labels = self
            .builder
            .get_labels(func, other)
            .iter()
            .cloned()
            .collect::<Vec<_>>();
        self.allocate_node_labeled(labels)
    }

    pub fn add_node(&mut self, builder: NodeBuilder) {
        let Ok(()) = self.builder.add_node(builder) else {
            panic!("Node not built")
        };
    }
}
