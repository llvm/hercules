use cfgrammar::Span;
use lrlex::DefaultLexerTypes;
use lrpar::NonStreamingLexer;

use std::fmt;

// A location in the program, used in error messages
#[derive(Copy, Clone, Debug)]
pub struct Location {
    start_line: usize,
    start_column: usize,
    end_line: usize,
    end_column: usize,
}

impl Location {
    pub fn fake() -> Location {
        Location {
            start_line: 0,
            start_column: 0,
            end_line: 0,
            end_column: 0,
        }
    }
}

// Conversion from span to internal locations
pub fn span_to_loc(span: Span, lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>) -> Location {
    let ((start_line, start_column), (end_line, end_column)) = lexer.line_col(span);
    Location {
        start_line,
        start_column,
        end_line,
        end_column,
    }
}

// Printing locations
impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}, {} -- {}, {}",
            self.start_line, self.start_column, self.end_line, self.end_column
        )
    }
}
