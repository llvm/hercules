/* Definitions of the set of intrinsic functions in Juno */
use phf::phf_map;

use crate::parser;
use crate::types::{Primitive, Type, TypeSolver};

// How intrinsics are identified in the Hercules IR
pub type IntrinsicIdentity = hercules_ir::ir::Intrinsic;

// Information about a single intrinsic, including its type information and how it will be
// identified in the Hercules IR
#[derive(Clone)]
pub struct IntrinsicInfo {
    pub id: IntrinsicIdentity,
    pub kinds: &'static [parser::Kind],
    pub typ: fn(&Vec<Type>, &mut TypeSolver) -> (Vec<Type>, Type),
}

// The type for a function which takes one argument of a variable type and
// returns a value of that same type
fn var_type(ty_args: &Vec<Type>, _: &mut TypeSolver) -> (Vec<Type>, Type) {
    (vec![ty_args[0]], ty_args[0])
}

// Type type for a function which takes two arguments of the same variable type
// and returns a value of that same type
fn var2_type(ty_args: &Vec<Type>, _: &mut TypeSolver) -> (Vec<Type>, Type) {
    (vec![ty_args[0], ty_args[0]], ty_args[0])
}

fn pow_type(ty_args: &Vec<Type>, types: &mut TypeSolver) -> (Vec<Type>, Type) {
    (
        vec![ty_args[0], types.new_primitive(Primitive::U32)],
        ty_args[0],
    )
}

fn powi_type(ty_args: &Vec<Type>, types: &mut TypeSolver) -> (Vec<Type>, Type) {
    (
        vec![ty_args[0], types.new_primitive(Primitive::I32)],
        ty_args[0],
    )
}

static INTRINSICS: phf::Map<&'static str, IntrinsicInfo> = phf_map! {
    "abs"   => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Abs,
        kinds   : &[parser::Kind::Number],
        typ     : var_type,
    },
    "acos" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::ACos,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "acosh" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::ACosh,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "asin" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::ASin,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "asinh" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::ASinh,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "atan" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::ATan,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "atan2" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::ATan2,
        kinds   : &[parser::Kind::Float],
        typ     : var2_type,
    },
    "atanh" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::ATanh,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "cbrt" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Cbrt,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "ceil" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Ceil,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "cos" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Cos,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "cosh" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Cos,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "exp" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Exp,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "exp2" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Exp2,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "exp_m1" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::ExpM1,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "floor" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Floor,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "ln" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Ln,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "ln_1p" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Ln1P,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "log" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Log,
        kinds   : &[parser::Kind::Float],
        typ     : var2_type,
    },
    "log10" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Log10,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "log2" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Log2,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "max"   => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Max,
        kinds   : &[parser::Kind::Number],
        typ     : var2_type,
    },
    "min"   => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Min,
        kinds   : &[parser::Kind::Number],
        typ     : var2_type,
    },
    "pow" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Pow,
        kinds   : &[parser::Kind::Integer],
        typ     : pow_type,
    },
    "powf" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Powf,
        kinds   : &[parser::Kind::Float],
        typ     : var2_type,
    },
    "powi" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Powi,
        kinds   : &[parser::Kind::Float],
        typ     : powi_type,
    },
    "round" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Round,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "sin" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Sin,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "sinh" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Sinh,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "sqrt" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Sqrt,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "tan" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Tan,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
    "tanh" => IntrinsicInfo {
        id      : hercules_ir::ir::Intrinsic::Tanh,
        kinds   : &[parser::Kind::Float],
        typ     : var_type,
    },
};

pub fn lookup(nm: &str) -> Option<&IntrinsicInfo> {
    INTRINSICS.get(nm)
}
