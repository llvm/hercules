use std::collections::{HashMap, HashSet, LinkedList};
use std::fmt;
use std::fs::File;
use std::io::Read;

use cfgrammar::Span;
use lrlex::DefaultLexerTypes;
use lrpar::NonStreamingLexer;

use ordered_float::OrderedFloat;

use crate::dynconst::DynConst;
use crate::intrinsics;
use crate::locs::{span_to_loc, Location};
use crate::parser;
use crate::parser::*;
use crate::types;
use crate::types::{Either, Type, TypeSolver};

use juno_utils::env::Env;
use juno_utils::stringtab::StringTable;

// Definitions and data structures for semantic analysis

// Entities in the environment
enum Entity {
    // A variable has a variable number to distinguish shadowing
    Variable {
        variable: usize,
        typ: Type,
        is_const: bool,
    },
    Type {
        type_args: Vec<parser::Kind>,
        value: Type,
    },
    DynConst {
        value: DynConst,
    },
    Constant {
        value: Constant,
    },
    // For functions we track an index, its type parameters, its argument types and if they are
    // inout, and its return type
    Function {
        index: usize,
        type_args: Vec<parser::Kind>,
        args: Vec<(types::Type, bool)>,
        return_types: Vec<types::Type>,
    },
}

// Constant values
#[derive(Clone, Debug)]
pub enum Literal {
    Unit,
    Bool(bool),
    Integer(u64),
    Float(f64),
    Tuple(Vec<Constant>),
    Sum(usize, Box<Constant>), // The tag and value
}
pub type Constant = (Literal, Type);

impl PartialEq for Literal {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Literal::Unit, Literal::Unit) => true,
            (Literal::Bool(b), Literal::Bool(c)) => b == c,
            (Literal::Integer(i), Literal::Integer(j)) => i == j,
            (Literal::Float(i), Literal::Float(j)) => OrderedFloat(*i) == OrderedFloat(*j),
            (Literal::Tuple(fs), Literal::Tuple(gs)) => fs == gs,
            (Literal::Sum(i, v), Literal::Sum(j, u)) => i == j && *v == *u,
            _ => false,
        }
    }
}

impl Eq for Literal {}

// Convert spans into uids in the String Table
fn intern_id(
    n: &Span,
    lex: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
) -> usize {
    stringtab.lookup_string(lex.span_str(*n).to_string())
}

fn intern_package_name(
    n: &PackageName,
    lex: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
) -> Vec<usize> {
    let mut res = vec![];
    for s in n {
        res.push(intern_id(s, lex, stringtab));
    }
    res
}

// Error Messages
pub enum ErrorMessage {
    NotImplemented(Location, String),
    IOError(String),
    SyntaxError(String),
    SemanticError(Location, String), // Other errors, with a location and description
    // Undefined variable at location, variable name
    UndefinedVariable(Location, String),
    // Kind error at location, expected, actual)
    KindError(Location, String, String),
    // Type error at location, expected type, actual type)
    TypeError(Location, String, String),
}

// Printing for error messages
impl fmt::Display for ErrorMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ErrorMessage::NotImplemented(loc, msg) => {
                write!(f, "Error ({}). Feature not implemented : {}", loc, msg)
            }
            ErrorMessage::IOError(msg) => {
                write!(f, "Error: {}", msg)
            }
            ErrorMessage::SyntaxError(msg) => {
                write!(f, "Syntax Error : {}", msg)
            }
            ErrorMessage::SemanticError(loc, msg) => {
                write!(f, "Error ({}). {}", loc, msg)
            }
            ErrorMessage::UndefinedVariable(loc, name) => {
                write!(f, "Error ({}). Undefined variable '{}'", loc, name)
            }
            ErrorMessage::KindError(loc, expected, actual) => {
                write!(
                    f,
                    "Error ({}). Expected {} but found {}",
                    loc, expected, actual
                )
            }
            ErrorMessage::TypeError(loc, expected, actual) => {
                write!(
                    f,
                    "Error ({}). Type error, expected {} but found {}",
                    loc, expected, actual
                )
            }
        }
    }
}

pub type ErrorMessages = LinkedList<ErrorMessage>;

// Constructors and combiners for error messages
fn singleton_error(err: ErrorMessage) -> ErrorMessages {
    LinkedList::from([err])
}

fn append_errors2<A, B>(
    x: Result<A, ErrorMessages>,
    y: Result<B, ErrorMessages>,
) -> Result<(A, B), ErrorMessages> {
    match (x, y) {
        (Err(mut err_x), Err(mut err_y)) => {
            err_x.append(&mut err_y);
            Err(err_x)
        }
        (Err(err_x), _) => Err(err_x),
        (_, Err(err_y)) => Err(err_y),
        (Ok(x), Ok(y)) => Ok((x, y)),
    }
}

fn append_errors3<A, B, C>(
    x: Result<A, ErrorMessages>,
    y: Result<B, ErrorMessages>,
    z: Result<C, ErrorMessages>,
) -> Result<(A, B, C), ErrorMessages> {
    let xy = append_errors2(x, y);
    let xyz = append_errors2(xy, z);

    match xyz {
        Err(errs) => Err(errs),
        Ok(((x, y), z)) => Ok((x, y, z)),
    }
}

// Normalized AST forms after semantic analysis
// These include type information at all expression nodes, and remove names and locations
pub struct Prg {
    pub types: TypeSolver,
    pub funcs: Vec<Function>,
    pub labels: StringTable,
}

// The function stores information for code-generation. The type information therefore is not the
// type information that is needed for type checking code that uses this function.
// In particular, the return type accounts for the type of inout arguments
pub struct Function {
    pub name: String,
    pub num_dyn_consts: usize,
    pub num_type_args: usize,
    pub arguments: Vec<(usize, Type)>,
    pub return_types: Vec<Type>,
    pub body: Stmt,
    pub entry: bool,
}

// Normalized statements differ in a number of ways from the form from the parser:
// 1. Let/Const are replaced by assignments (since we've already typed checked)
// 2. Assignment operators of the form lhs X= rhs are replaced by assignments of the form lhs = lhs
//    X rhs, and assignments now always occur to variables, so modification of fields/values in an
//    array are replaced by appropriate expressions which write to those portions of the value
// 3. Match statements are eliminated except for those on union types, and only one match per level
// 4. Both for and while loops are combined into a single loop form
// 5. Call statements are transformed to call expressions, with a new statement form for
//    expressions
// Additional notes
// - Returns in this AST include the inout values
pub enum Stmt {
    AssignStmt {
        var: usize,
        val: Expr,
    },
    IfStmt {
        cond: Expr,
        thn: Box<Stmt>,
        els: Option<Box<Stmt>>,
    },
    // TODO: Not implemented
    //MatchStmt    { expr : Expr, cases : Vec<usize>, body : Vec<Stmt> },
    LoopStmt {
        cond: Expr,
        update: Option<Box<Stmt>>,
        body: Box<Stmt>,
    },
    ReturnStmt {
        exprs: Vec<Expr>,
    },
    BreakStmt {},
    ContinueStmt {},
    BlockStmt {
        body: Vec<Stmt>,
    },
    ExprStmt {
        expr: Expr,
    },
    LabeledStmt {
        label: usize,
        stmt: Box<Stmt>,
    },
}

// Normalized expressions differ in a number of ways:
// 1. All expressions store their type
// 2. Field, index, and array access expressions are all replaced by a read node (like the IR) and
//    we add a write expression that is used to simplify the assignment operation
// 3. Structs are eliminated and replaced by tuples
// 4. Unions are now tagged by a number rather than name and are also now separated from function
//    calls
// 5. The unary and binary operations no longer contain boolean operations, instead those are
//    expressed using the conditional expression
// 6. Functions are now identified by number, and arguments to functions are now represented as
//    either an expression or a variable number for the inout arguments
//    TODO: Technically inout arguments could be any l-expression
// 7. There's an additional Zero which is used to construct the default of a type
#[derive(Clone, Debug)]
pub enum Expr {
    Variable {
        var: usize,
        typ: Type,
    },
    DynConst {
        val: DynConst,
        typ: Type,
    },
    Read {
        index: Vec<Index>,
        val: Box<Expr>,
        typ: Type,
    },
    Write {
        index: Vec<Index>,
        val: Box<Expr>,
        rep: Box<Expr>,
        typ: Type,
    },
    Tuple {
        vals: Vec<Expr>,
        typ: Type,
    },
    Union {
        tag: usize,
        val: Box<Expr>,
        typ: Type,
    },
    Constant {
        val: Constant,
        typ: Type,
    },
    Zero {
        typ: Type,
    },
    UnaryExp {
        op: UnaryOp,
        expr: Box<Expr>,
        typ: Type,
    },
    BinaryExp {
        op: BinaryOp,
        lhs: Box<Expr>,
        rhs: Box<Expr>,
        typ: Type,
    },
    CastExpr {
        expr: Box<Expr>,
        typ: Type,
    },
    CondExpr {
        cond: Box<Expr>,
        thn: Box<Expr>,
        els: Box<Expr>,
        typ: Type,
    },
    CallExpr {
        func: usize,
        ty_args: Vec<Type>,
        dyn_consts: Vec<DynConst>,
        args: Vec<Either<Expr, usize>>,
        // Include the number of Juno returns (i.e. non-inouts) for codegen
        num_returns: usize,
        typ: Type,
    },
    // A projection from a call
    CallExtract {
        call: Box<Expr>,
        index: usize,
        typ: Type,
    },
    Intrinsic {
        id: intrinsics::IntrinsicIdentity,
        ty_args: Vec<Type>,
        args: Vec<Expr>,
        typ: Type,
    },
}

#[derive(Clone, Debug)]
pub enum Index {
    Field(usize),
    Variant(usize),
    Array(Vec<Expr>),
}

#[derive(Clone, Debug)]
pub enum UnaryOp {
    Negation,
    BitwiseNot,
}
#[derive(Clone, Debug)]
pub enum BinaryOp {
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    BitAnd,
    BitOr,
    Xor,
    Lt,
    Le,
    Gt,
    Ge,
    Eq,
    Neq,
    LShift,
    RShift,
}

fn convert_assign_op(op: parser::AssignOp) -> BinaryOp {
    match op {
        AssignOp::None => panic!("Do not call convert_assign_op on AssignOp::None"),
        AssignOp::Add => BinaryOp::Add,
        AssignOp::Sub => BinaryOp::Sub,
        AssignOp::Mul => BinaryOp::Mul,
        AssignOp::Div => BinaryOp::Div,
        AssignOp::Mod => BinaryOp::Mod,
        AssignOp::BitAnd => BinaryOp::BitAnd,
        AssignOp::BitOr => BinaryOp::BitOr,
        AssignOp::Xor => BinaryOp::Xor,
        AssignOp::LShift => BinaryOp::LShift,
        AssignOp::RShift => BinaryOp::RShift,
        AssignOp::LogAnd => panic!("Do not call convert_assign_op on AssignOp::LogAnd"),
        AssignOp::LogOr => panic!("Do not call convert_assign_op on AssignOp::LogOr"),
    }
}

fn convert_binary_op(op: parser::BinaryOp) -> BinaryOp {
    match op {
        parser::BinaryOp::Add => BinaryOp::Add,
        parser::BinaryOp::Sub => BinaryOp::Sub,
        parser::BinaryOp::Mul => BinaryOp::Mul,
        parser::BinaryOp::Div => BinaryOp::Div,
        parser::BinaryOp::Mod => BinaryOp::Mod,
        parser::BinaryOp::BitAnd => BinaryOp::BitAnd,
        parser::BinaryOp::BitOr => BinaryOp::BitOr,
        parser::BinaryOp::Xor => BinaryOp::Xor,
        parser::BinaryOp::Lt => BinaryOp::Lt,
        parser::BinaryOp::Le => BinaryOp::Le,
        parser::BinaryOp::Gt => BinaryOp::Gt,
        parser::BinaryOp::Ge => BinaryOp::Ge,
        parser::BinaryOp::Eq => BinaryOp::Eq,
        parser::BinaryOp::Neq => BinaryOp::Neq,
        parser::BinaryOp::LShift => BinaryOp::LShift,
        parser::BinaryOp::RShift => BinaryOp::RShift,
        parser::BinaryOp::LogAnd => panic!("Do not call convert_binary_op on BinaryOp::LogAnd"),
        parser::BinaryOp::LogOr => panic!("Do not call convert_binary_op on BinaryOp::LogOr"),
    }
}

// Be able to access the type of an expression easily
impl Expr {
    pub fn get_type(&self) -> Type {
        match self {
            Expr::Variable { typ, .. }
            | Expr::DynConst { typ, .. }
            | Expr::Read { typ, .. }
            | Expr::Write { typ, .. }
            | Expr::Tuple { typ, .. }
            | Expr::Union { typ, .. }
            | Expr::Constant { typ, .. }
            | Expr::UnaryExp { typ, .. }
            | Expr::BinaryExp { typ, .. }
            | Expr::CastExpr { typ, .. }
            | Expr::CondExpr { typ, .. }
            | Expr::CallExpr { typ, .. }
            | Expr::CallExtract { typ, .. }
            | Expr::Intrinsic { typ, .. }
            | Expr::Zero { typ } => *typ,
        }
    }
}

// Helper function to unparse types
fn unparse_type(types: &TypeSolver, typ: Type, stringtab: &StringTable) -> String {
    types.to_string(typ, &|n| stringtab.lookup_id(n).unwrap())
}

// Start of parsing and semantic analysis

// Loads the contents of the given file name, parses, and performs semantic analysis
pub fn parse_and_analyze(src_file: String) -> Result<Prg, ErrorMessages> {
    if let Ok(mut file) = File::open(src_file) {
        let mut contents = String::new();
        if let Ok(_) = file.read_to_string(&mut contents) {
            let lexerdef = lexer::lexerdef();
            let lexer = lexerdef.lexer(&contents);
            let (res, errs) = parser::parse(&lexer);

            if errs.is_empty() {
                match res {
                    None => Err(singleton_error(ErrorMessage::SyntaxError(
                        "Parser did not return".to_string(),
                    ))),
                    Some(Err(())) => Err(singleton_error(ErrorMessage::SyntaxError(
                        "Unspecified parse error".to_string(),
                    ))),
                    Some(Ok(r)) => analyze_program(r, &lexer),
                }
            } else {
                Err(errs
                    .iter()
                    .map(|e| ErrorMessage::SyntaxError(e.pp(&lexer, &parser::token_epp)))
                    .collect())
            }
        } else {
            Err(singleton_error(ErrorMessage::IOError(
                "Unable to read input file".to_string(),
            )))
        }
    } else {
        Err(singleton_error(ErrorMessage::IOError(
            "Unable to open input file".to_string(),
        )))
    }
}

fn analyze_program(
    prg: parser::Prg,
    lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
) -> Result<Prg, ErrorMessages> {
    let mut stringtab = StringTable::new();
    let mut labels = StringTable::new();
    let mut env: Env<usize, Entity> = Env::new();
    let mut types = TypeSolver::new();

    let mut res = vec![];

    env.open_scope();

    for top in prg {
        match top {
            parser::Top::Import { span, name: _ } => Err(singleton_error(
                ErrorMessage::NotImplemented(span_to_loc(span, lexer), "imports".to_string()),
            ))?,
            parser::Top::TypeDecl {
                span: _,
                public: _,
                name,
                ty_vars,
                body,
            } => {
                // TODO: Handle public
                env.open_scope(); // Create a new scope for the body (for type variables)

                // Add the type variables to the environment
                let mut num_type = 0;
                let mut num_dyn_const = 0;
                // Track the kinds of the variables
                let mut kinds = vec![];

                let mut dyn_const_names = vec![];

                for TypeVar {
                    span: _,
                    name,
                    kind,
                } in ty_vars
                {
                    let nm = intern_id(&name, lexer, &mut stringtab);
                    kinds.push(kind);

                    match kind {
                        Kind::USize => {
                            dyn_const_names.push(nm);
                            num_dyn_const += 1;
                        }
                        _ => {
                            let typ = types.new_type_var(nm, num_type, kind);
                            env.insert(
                                nm,
                                Entity::Type {
                                    type_args: vec![],
                                    value: typ,
                                },
                            );
                            num_type += 1;
                        }
                    }
                }

                for (idx, nm) in dyn_const_names.into_iter().enumerate() {
                    env.insert(
                        nm,
                        Entity::DynConst {
                            value: DynConst::dynamic_constant(idx, num_dyn_const),
                        },
                    );
                }

                let nm = intern_id(&name, lexer, &mut stringtab);
                let typ = process_type_def(
                    body,
                    nm,
                    num_dyn_const,
                    lexer,
                    &mut stringtab,
                    &mut env,
                    &mut types,
                )?;

                // Insert information into the global scope
                env.close_scope();
                env.insert(
                    nm,
                    Entity::Type {
                        type_args: kinds,
                        value: typ,
                    },
                );
            }
            parser::Top::ConstDecl {
                span,
                public: _,
                name,
                ty,
                body,
            } => {
                // TODO: Handle public
                let nm = intern_id(&name, lexer, &mut stringtab);
                let val =
                    process_expr_as_constant(body, 0, lexer, &mut stringtab, &mut env, &mut types)?;

                // Check type (if specified)
                if ty.is_some() {
                    let ty = process_type(
                        ty.unwrap(),
                        0,
                        lexer,
                        &mut stringtab,
                        &env,
                        &mut types,
                        true,
                    )?;
                    if !types.unify(ty, val.1) {
                        return Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(span, lexer),
                            unparse_type(&types, ty, &stringtab),
                            unparse_type(&types, val.1, &stringtab),
                        )));
                    }
                }

                env.insert(nm, Entity::Constant { value: val });
            }
            parser::Top::FuncDecl {
                span,
                public: _,
                attr,
                name,
                ty_vars,
                args,
                rets,
                body,
            } => {
                // TODO: Handle public
                env.open_scope(); // Open a new scope immediately to put type variables in

                let attributes = match attr {
                    None => vec![],
                    Some(attr) => {
                        let attr_str = lexer.span_str(attr);
                        attr_str[2..attr_str.len() - 1]
                            .split(',')
                            .map(|s| s.trim())
                            .collect::<Vec<_>>()
                    }
                };

                // Determine whether this function is marked as an entry point
                let entry = attributes.contains(&"entry");

                // Process the type variables and add them into the environment
                let mut num_dyn_const = 0;
                let mut num_type_var = 0;
                let mut type_kinds = vec![];

                let mut dyn_const_names = vec![];

                for TypeVar {
                    span: _,
                    name,
                    kind,
                } in ty_vars
                {
                    type_kinds.push(kind);
                    let nm = intern_id(&name, lexer, &mut stringtab);
                    match kind {
                        Kind::USize => {
                            dyn_const_names.push(nm);
                            num_dyn_const += 1;
                        }
                        _ => {
                            let typ = types.new_type_var(nm, num_type_var, kind);
                            env.insert(
                                nm,
                                Entity::Type {
                                    type_args: vec![],
                                    value: typ,
                                },
                            );
                            num_type_var += 1;
                        }
                    }
                }
                for (idx, nm) in dyn_const_names.into_iter().enumerate() {
                    env.insert(
                        nm,
                        Entity::DynConst {
                            value: DynConst::dynamic_constant(idx, num_dyn_const),
                        },
                    );
                }

                if num_type_var > 0 && entry {
                    return Err(singleton_error(ErrorMessage::SemanticError(
                        span_to_loc(span, lexer),
                        "Function with 'entry' attribute cannot have type variables".to_string(),
                    )));
                }

                // Process arguments
                // We collect the list of the argument types, whether they are inout, and their
                // unique variable number
                let mut arg_info: Vec<(Type, bool, usize)> = vec![];
                // We collect the list of expressions that should be returned for the inout
                // arguments
                let mut inouts: Vec<Expr> = vec![];

                // A collection of errors we encounter processing the arguments
                let mut errors = LinkedList::new();
                // Any statements that need to go at the beginning of the function to handle
                // patterns in the arguments
                let mut stmts = vec![];

                for (inout, VarBind { span, pattern, typ }) in args {
                    let typ = typ.unwrap_or(parser::Type::WildType { span });

                    match process_type(
                        typ,
                        num_dyn_const,
                        lexer,
                        &mut stringtab,
                        &env,
                        &mut types,
                        true,
                    ) {
                        Ok(ty) => {
                            let var = env.uniq();

                            arg_info.push((ty, inout.is_some(), var));

                            match process_irrefutable_pattern(
                                pattern,
                                false,
                                var,
                                ty,
                                lexer,
                                &mut stringtab,
                                &mut env,
                                &mut types,
                                inout.is_some(),
                            ) {
                                Ok((prep, expr)) => {
                                    stmts.extend(prep);
                                    if inout.is_some() {
                                        inouts.push(expr.unwrap());
                                    }
                                }
                                Err(mut errs) => {
                                    errors.append(&mut errs);
                                }
                            }
                        }
                        Err(mut errs) => {
                            errors.append(&mut errs);
                        }
                    }
                }

                let return_types = rets
                    .into_iter()
                    .map(|ty| {
                        match process_type(
                            ty,
                            num_dyn_const,
                            lexer,
                            &mut stringtab,
                            &env,
                            &mut types,
                            true,
                        ) {
                            Ok(ty) => ty,
                            Err(mut errs) => {
                                errors.append(&mut errs);
                                // Type we return doesn't matter, error will be propagated upwards
                                // next, but need to return something
                                types.new_primitive(types::Primitive::Unit)
                            }
                        }
                    })
                    .collect::<Vec<_>>();

                if !errors.is_empty() {
                    return Err(errors);
                }

                // Compute the proper type accounting for the inouts (which become returns)
                let inout_types = inouts.iter().map(|e| e.get_type()).collect::<Vec<_>>();
                let pure_return_types = return_types
                    .clone()
                    .into_iter()
                    .chain(inout_types.into_iter())
                    .collect::<Vec<_>>();

                // Finally, we have a properly built environment and we can
                // start processing the body
                let (mut body, end_reachable) = process_stmt(
                    body,
                    num_dyn_const,
                    lexer,
                    &mut stringtab,
                    &mut env,
                    &mut types,
                    false,
                    &return_types,
                    &inouts,
                    &mut labels,
                )?;

                if end_reachable {
                    // The end of a function being reachable (i.e. there is some possible path
                    // where there is no return statement) is an error unless the return type is
                    // empty
                    if return_types.is_empty() {
                        // Insert return at the end
                        body = Stmt::BlockStmt {
                            body: vec![body, generate_return(vec![], &inouts)],
                        };
                    } else {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            "May reach end of control without return".to_string(),
                        )))?
                    }
                }

                // Add the code for initializing arguments
                stmts.push(body);
                body = Stmt::BlockStmt { body: stmts };

                env.close_scope();

                // Add the function to the global environment
                let nm = intern_id(&name, lexer, &mut stringtab);
                env.insert(
                    nm,
                    Entity::Function {
                        index: res.len(),
                        type_args: type_kinds,
                        args: arg_info
                            .iter()
                            .map(|(ty, is, _)| (*ty, *is))
                            .collect::<Vec<_>>(),
                        return_types,
                    },
                );

                // Add the function definition to the list of functions
                res.push(Function {
                    name: lexer.span_str(name).to_string(),
                    num_dyn_consts: num_dyn_const,
                    num_type_args: num_type_var,
                    arguments: arg_info
                        .iter()
                        .map(|(t, _, v)| (*v, *t))
                        .collect::<Vec<_>>(),
                    return_types: pure_return_types,
                    body: body,
                    entry: entry,
                });
            }
            parser::Top::ModDecl {
                span,
                public: _,
                name: _,
                body: _,
            } => Err(singleton_error(ErrorMessage::NotImplemented(
                span_to_loc(span, lexer),
                "modules".to_string(),
            )))?,
        }

        // After each top level construct, verify that our types are solved
        // If not report an error
        match types.solve() {
            Ok(()) => (),
            Err((kind, loc)) => {
                return Err(singleton_error(ErrorMessage::SemanticError(
                    loc,
                    format!(
                        "unconstrained type, not constrained beyond {}",
                        kind.to_string()
                    ),
                )));
            }
        }
    }

    Ok(Prg {
        types,
        funcs: res,
        labels,
    })
}

fn process_type_def(
    def: parser::TyDef,
    name: usize,
    num_dyn_const: usize,
    lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
    env: &mut Env<usize, Entity>,
    types: &mut TypeSolver,
) -> Result<Type, ErrorMessages> {
    match def {
        parser::TyDef::TypeAlias { span: _, body } => {
            process_type(body, num_dyn_const, lexer, stringtab, env, types, false)
        }
        parser::TyDef::Struct {
            span: _,
            public: _,
            fields,
        } => {
            // TODO: handle public correctly (and field public)

            let mut field_list = vec![];
            let mut field_map = HashMap::new();
            let mut errors = LinkedList::new();

            for ObjField {
                span,
                public: _,
                name,
                typ,
            } in fields
            {
                let nm = intern_id(&name, lexer, stringtab);
                match typ {
                    None => {
                        errors.push_back(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            "struct fields must have a type".to_string(),
                        ));
                    }
                    Some(ty) => {
                        match process_type(ty, num_dyn_const, lexer, stringtab, env, types, false) {
                            Ok(typ) => {
                                let idx = field_list.len();
                                field_list.push(typ);
                                field_map.insert(nm, idx);
                            }
                            Err(mut errs) => errors.append(&mut errs),
                        }
                    }
                }
            }

            if !errors.is_empty() {
                Err(errors)
            } else {
                Ok(types.new_struct(name, env.uniq(), field_list, field_map))
            }
        }
        parser::TyDef::Union {
            span: _,
            public: _,
            fields,
        } => {
            // TODO: handle public correctly
            let mut constr_list = vec![];
            let mut constr_map = HashMap::new();
            let mut errors = LinkedList::new();

            for ObjField {
                span,
                public,
                name,
                typ,
            } in fields
            {
                if public {
                    errors.push_back(
                        ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            "union constructors cannot be marked public, all constructors share the visibility of the union".to_string()));
                } else {
                    let nm = intern_id(&name, lexer, stringtab);
                    match typ {
                        None => {
                            let idx = constr_list.len();
                            constr_list.push(types.new_primitive(types::Primitive::Unit));
                            constr_map.insert(nm, idx);
                        }
                        Some(ty) => {
                            match process_type(
                                ty,
                                num_dyn_const,
                                lexer,
                                stringtab,
                                env,
                                types,
                                false,
                            ) {
                                Ok(typ) => {
                                    let idx = constr_list.len();
                                    constr_list.push(typ);
                                    constr_map.insert(nm, idx);
                                }
                                Err(mut errs) => errors.append(&mut errs),
                            }
                        }
                    }
                }
            }

            if !errors.is_empty() {
                Err(errors)
            } else {
                Ok(types.new_union(name, env.uniq(), constr_list, constr_map))
            }
        }
    }
}

fn process_type(
    typ: parser::Type,
    num_dyn_const: usize,
    lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
    env: &Env<usize, Entity>,
    types: &mut TypeSolver,
    can_infer: bool,
) -> Result<Type, ErrorMessages> {
    match typ {
        parser::Type::PrimType { span: _, typ } => Ok(types.new_primitive(convert_primitive(typ))),
        parser::Type::WildType { span } => {
            if can_infer {
                Ok(types.new_of_kind(parser::Kind::Type, span_to_loc(span, lexer)))
            } else {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    "cannot infer type in this context".to_string(),
                )))
            }
        }
        parser::Type::TupleType { span: _, tys } => {
            let mut fields = vec![];
            let mut errors = LinkedList::new();

            for ty in tys {
                match process_type(ty, num_dyn_const, lexer, stringtab, env, types, can_infer) {
                    Ok(t) => fields.push(t),
                    Err(mut errs) => errors.append(&mut errs),
                }
            }

            if !errors.is_empty() {
                Err(errors)
            } else {
                if fields.len() == 1 {
                    Ok(fields.pop().expect("Length"))
                } else {
                    Ok(types.new_tuple(fields))
                }
            }
        }
        parser::Type::NamedType { span, name, args } => {
            if name.len() != 1 {
                Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )))
            } else {
                let id = intern_package_name(&name, lexer, stringtab);
                let nm = id[0];
                match env.lookup(&nm) {
                    Some(Entity::Type { type_args, value }) => {
                        if args.is_none() && type_args.len() != 0 && !can_infer {
                            Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(span, lexer),
                                format!(
                                    "Expected {} type arguments, provided none",
                                    type_args.len()
                                ),
                            )))?
                        }

                        // If we did not provide type arguments (but we can
                        // infer types) then we make all of the type arguments
                        // wild cards
                        let args = args.unwrap_or_else(|| {
                            vec![parser::TypeExpr::WildcardType { span: span }; type_args.len()]
                        });

                        if args.len() != type_args.len() {
                            Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(span, lexer),
                                format!(
                                    "Expected {} type arguments, provided {}",
                                    type_args.len(),
                                    args.len()
                                ),
                            )))?
                        }

                        // Process the type arguments, ensuring they match the given kinds
                        let mut type_vars = vec![];
                        let mut dynamic_constants = vec![];
                        let mut errors = LinkedList::new();

                        for (arg, kind) in args.into_iter().zip(type_args.iter()) {
                            let arg_span = arg.span();
                            match kind {
                                parser::Kind::USize => {
                                    match process_type_expr_as_expr(
                                        arg,
                                        num_dyn_const,
                                        lexer,
                                        stringtab,
                                        env,
                                        types,
                                    ) {
                                        Err(mut errs) => errors.append(&mut errs),
                                        Ok(val) => dynamic_constants.push(val),
                                    }
                                }
                                _ => {
                                    match process_type_expr_as_type(
                                        arg,
                                        num_dyn_const,
                                        lexer,
                                        stringtab,
                                        env,
                                        types,
                                        can_infer,
                                    ) {
                                        Err(mut errs) => errors.append(&mut errs),
                                        Ok(typ) => {
                                            if types.unify_kind(typ, *kind) {
                                                type_vars.push(typ);
                                            } else {
                                                errors.push_back(ErrorMessage::KindError(
                                                    span_to_loc(arg_span, lexer),
                                                    kind.to_string(),
                                                    unparse_type(types, typ, stringtab),
                                                ));
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if !errors.is_empty() {
                            Err(errors)?
                        }

                        if type_vars.len() == 0 && dynamic_constants.len() == 0 {
                            Ok(*value)
                        } else {
                            if let Some(res) =
                                types.instantiate(*value, &type_vars, &dynamic_constants)
                            {
                                Ok(res)
                            } else {
                                Err(singleton_error(ErrorMessage::SemanticError(
                                    span_to_loc(span, lexer),
                                    "Failure in variable substitution".to_string(),
                                )))
                            }
                        }
                    }
                    Some(_) => Err(singleton_error(ErrorMessage::KindError(
                        span_to_loc(span, lexer),
                        "type".to_string(),
                        "value".to_string(),
                    ))),
                    None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                        span_to_loc(span, lexer),
                        stringtab.lookup_id(nm).unwrap(),
                    ))),
                }
            }
        }
        parser::Type::ArrayType {
            span: _,
            elem,
            dims,
        } => {
            let mut dimensions = vec![];
            let mut errors = LinkedList::new();

            let element = process_type(
                *elem,
                num_dyn_const,
                lexer,
                stringtab,
                env,
                types,
                can_infer,
            );

            for dim in dims {
                match process_type_expr_as_expr(dim, num_dyn_const, lexer, stringtab, env, types) {
                    Err(mut errs) => errors.append(&mut errs),
                    Ok(ex) => dimensions.push(ex),
                }
            }

            match element {
                Err(mut errs) => {
                    errs.append(&mut errors);
                    Err(errs)
                }
                Ok(element_type) => {
                    if !errors.is_empty() {
                        Err(errors)
                    } else {
                        if types.is_array(element_type) {
                            let elem_type = types.get_element_type(element_type).unwrap();
                            let mut inner_dims = types.get_dimensions(element_type).unwrap();

                            dimensions.append(&mut inner_dims);
                            Ok(types.new_array(elem_type, dimensions))
                        } else {
                            Ok(types.new_array(element_type, dimensions))
                        }
                    }
                }
            }
        }
    }
}

fn process_type_expr_as_expr(
    exp: parser::TypeExpr,
    num_dyn_const: usize,
    lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
    env: &Env<usize, Entity>,
    types: &mut TypeSolver,
) -> Result<DynConst, ErrorMessages> {
    match exp {
        parser::TypeExpr::PrimType { span, .. }
        | parser::TypeExpr::WildcardType { span }
        | parser::TypeExpr::TupleType { span, .. }
        | parser::TypeExpr::ArrayTypeExpr { span, .. } => {
            Err(singleton_error(ErrorMessage::KindError(
                span_to_loc(span, lexer),
                "dynamic constant expression".to_string(),
                "type".to_string(),
            )))
        }

        parser::TypeExpr::NamedTypeExpr { span, name, args } => {
            if name.len() != 1 {
                Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )))
            } else {
                let id = intern_package_name(&name, lexer, stringtab);
                let nm = id[0];
                match env.lookup(&nm) {
                    Some(Entity::DynConst { value }) => {
                        if args.is_some() {
                            Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(span, lexer),
                                format!("No type arguments exists on dynamic constants"),
                            )))
                        } else {
                            Ok(value.clone())
                        }
                    }
                    Some(Entity::Constant { value: (val, typ) }) => match val {
                        Literal::Integer(val) => Ok(DynConst::constant(*val as i64, num_dyn_const)),
                        _ => Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(span, lexer),
                            "usize".to_string(),
                            unparse_type(types, *typ, stringtab),
                        ))),
                    },
                    Some(Entity::Variable { .. }) => Err(singleton_error(ErrorMessage::KindError(
                        span_to_loc(span, lexer),
                        "dynamic constant expression".to_string(),
                        "runtime variable".to_string(),
                    ))),
                    Some(Entity::Type { .. }) => Err(singleton_error(ErrorMessage::KindError(
                        span_to_loc(span, lexer),
                        "dynamic constant expression".to_string(),
                        "type".to_string(),
                    ))),
                    Some(Entity::Function { .. }) => Err(singleton_error(ErrorMessage::KindError(
                        span_to_loc(span, lexer),
                        "dynamic constant expression".to_string(),
                        "function".to_string(),
                    ))),
                    None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                        span_to_loc(span, lexer),
                        stringtab.lookup_id(nm).unwrap(),
                    ))),
                }
            }
        }
        parser::TypeExpr::IntLiteral { span, base } => {
            let res = i64::from_str_radix(&base.string(lexer, span), base.base());
            assert!(res.is_ok(), "Internal Error: Int literal is not an integer");
            Ok(DynConst::constant(res.unwrap(), num_dyn_const))
        }
        parser::TypeExpr::Negative { span: _, expr } => Ok(DynConst::negate(
            &process_type_expr_as_expr(*expr, num_dyn_const, lexer, stringtab, env, types)?,
        )),
        parser::TypeExpr::Add { span: _, lhs, rhs } => {
            let lhs_res =
                process_type_expr_as_expr(*lhs, num_dyn_const, lexer, stringtab, env, types)?;
            let rhs_res =
                process_type_expr_as_expr(*rhs, num_dyn_const, lexer, stringtab, env, types)?;
            Ok(DynConst::add(&lhs_res, &rhs_res))
        }
        parser::TypeExpr::Sub { span: _, lhs, rhs } => {
            let lhs_res =
                process_type_expr_as_expr(*lhs, num_dyn_const, lexer, stringtab, env, types)?;
            let rhs_res =
                process_type_expr_as_expr(*rhs, num_dyn_const, lexer, stringtab, env, types)?;
            Ok(DynConst::sub(&lhs_res, &rhs_res))
        }
        parser::TypeExpr::Mul { span: _, lhs, rhs } => {
            let lhs_res =
                process_type_expr_as_expr(*lhs, num_dyn_const, lexer, stringtab, env, types)?;
            let rhs_res =
                process_type_expr_as_expr(*rhs, num_dyn_const, lexer, stringtab, env, types)?;
            Ok(DynConst::mul(&lhs_res, &rhs_res))
        }
        parser::TypeExpr::Div { span, lhs, rhs } => {
            let lhs_res =
                process_type_expr_as_expr(*lhs, num_dyn_const, lexer, stringtab, env, types)?;
            let rhs_res =
                process_type_expr_as_expr(*rhs, num_dyn_const, lexer, stringtab, env, types)?;
            if let Some(res) = DynConst::div(&lhs_res, &rhs_res) {
                Ok(res)
            } else {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    "Division by dynamic constant expression failed".to_string(),
                )))
            }
        }
    }
}

fn process_type_expr_as_type(
    exp: parser::TypeExpr,
    num_dyn_const: usize,
    lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
    env: &Env<usize, Entity>,
    types: &mut TypeSolver,
    can_infer: bool,
) -> Result<Type, ErrorMessages> {
    match exp {
        parser::TypeExpr::IntLiteral { span, .. }
        | parser::TypeExpr::Negative { span, .. }
        | parser::TypeExpr::Add { span, .. }
        | parser::TypeExpr::Sub { span, .. }
        | parser::TypeExpr::Mul { span, .. }
        | parser::TypeExpr::Div { span, .. } => Err(singleton_error(ErrorMessage::KindError(
            span_to_loc(span, lexer),
            "type".to_string(),
            "expression".to_string(),
        ))),
        parser::TypeExpr::PrimType { span: _, typ } => {
            Ok(types.new_primitive(convert_primitive(typ)))
        }
        parser::TypeExpr::WildcardType { span } => {
            if can_infer {
                Ok(types.new_of_kind(parser::Kind::Type, span_to_loc(span, lexer)))
            } else {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    "cannot infer type in this context".to_string(),
                )))
            }
        }
        parser::TypeExpr::TupleType { span: _, tys } => {
            let mut fields = vec![];
            let mut errors = LinkedList::new();

            for ty in tys {
                match process_type_expr_as_type(
                    ty,
                    num_dyn_const,
                    lexer,
                    stringtab,
                    env,
                    types,
                    can_infer,
                ) {
                    Ok(t) => fields.push(t),
                    Err(mut errs) => errors.append(&mut errs),
                }
            }

            if !errors.is_empty() {
                Err(errors)
            } else {
                if fields.len() == 1 {
                    Ok(fields.pop().expect("Length"))
                } else {
                    Ok(types.new_tuple(fields))
                }
            }
        }
        parser::TypeExpr::ArrayTypeExpr {
            span: _,
            elem,
            dims,
        } => {
            let mut dimensions = vec![];
            let mut errors = LinkedList::new();

            let element = process_type_expr_as_type(
                *elem,
                num_dyn_const,
                lexer,
                stringtab,
                env,
                types,
                can_infer,
            );

            for dim in dims {
                match process_type_expr_as_expr(dim, num_dyn_const, lexer, stringtab, env, types) {
                    Err(mut errs) => errors.append(&mut errs),
                    Ok(ex) => dimensions.push(ex),
                }
            }

            match element {
                Err(mut errs) => {
                    errs.append(&mut errors);
                    Err(errs)
                }
                Ok(element_type) => {
                    if !errors.is_empty() {
                        Err(errors)
                    } else {
                        if types.is_array(element_type) {
                            let elem_type = types.get_element_type(element_type).unwrap();
                            let mut inner_dims = types.get_dimensions(element_type).unwrap();

                            dimensions.append(&mut inner_dims);
                            Ok(types.new_array(elem_type, dimensions))
                        } else {
                            Ok(types.new_array(element_type, dimensions))
                        }
                    }
                }
            }
        }
        parser::TypeExpr::NamedTypeExpr { span, name, args } => {
            if name.len() != 1 {
                Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )))
            } else {
                let id = intern_package_name(&name, lexer, stringtab);
                let nm = id[0];
                match env.lookup(&nm) {
                    Some(Entity::Type { type_args, value }) => {
                        if args.is_none() && type_args.len() != 0 && !can_infer {
                            Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(span, lexer),
                                format!(
                                    "Expected {} type arguments, provided none",
                                    type_args.len()
                                ),
                            )))?
                        }

                        let args = args.unwrap_or_else(|| {
                            vec![parser::TypeExpr::WildcardType { span: span }; type_args.len()]
                        });

                        if args.len() != type_args.len() {
                            Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(span, lexer),
                                format!(
                                    "Expected {} type arguments, provided {}",
                                    type_args.len(),
                                    args.len()
                                ),
                            )))?
                        }

                        // Process the type arguments, ensuring they match the given kinds
                        let mut type_vars = vec![];
                        let mut dynamic_constants = vec![];
                        let mut errors = LinkedList::new();

                        for (arg, kind) in args.into_iter().zip(type_args.iter()) {
                            let arg_span = arg.span();
                            match kind {
                                parser::Kind::USize => {
                                    match process_type_expr_as_expr(
                                        arg,
                                        num_dyn_const,
                                        lexer,
                                        stringtab,
                                        env,
                                        types,
                                    ) {
                                        Err(mut errs) => errors.append(&mut errs),
                                        Ok(val) => dynamic_constants.push(val),
                                    }
                                }
                                _ => {
                                    match process_type_expr_as_type(
                                        arg,
                                        num_dyn_const,
                                        lexer,
                                        stringtab,
                                        env,
                                        types,
                                        can_infer,
                                    ) {
                                        Err(mut errs) => errors.append(&mut errs),
                                        Ok(typ) => {
                                            if types.unify_kind(typ, *kind) {
                                                type_vars.push(typ);
                                            } else {
                                                errors.push_back(ErrorMessage::KindError(
                                                    span_to_loc(arg_span, lexer),
                                                    kind.to_string(),
                                                    unparse_type(types, typ, stringtab),
                                                ));
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if !errors.is_empty() {
                            Err(errors)?
                        }

                        if type_vars.len() == 0 && dynamic_constants.len() == 0 {
                            Ok(*value)
                        } else {
                            if let Some(res) =
                                types.instantiate(*value, &type_vars, &dynamic_constants)
                            {
                                Ok(res)
                            } else {
                                Err(singleton_error(ErrorMessage::SemanticError(
                                    span_to_loc(span, lexer),
                                    "Failure in variable substitution".to_string(),
                                )))
                            }
                        }
                    }
                    Some(_) => Err(singleton_error(ErrorMessage::KindError(
                        span_to_loc(span, lexer),
                        "type".to_string(),
                        "value".to_string(),
                    ))),
                    None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                        span_to_loc(span, lexer),
                        stringtab.lookup_id(nm).unwrap(),
                    ))),
                }
            }
        }
    }
}

// Normalizes the given statement, and returns the normalized statement plus whether a statement
// after the analyzed one is reachable or not
fn process_stmt(
    stmt: parser::Stmt,
    num_dyn_const: usize,
    lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
    env: &mut Env<usize, Entity>,
    types: &mut TypeSolver,
    in_loop: bool,
    return_types: &[Type],
    inouts: &Vec<Expr>,
    labels: &mut StringTable,
) -> Result<(Stmt, bool), ErrorMessages> {
    match stmt {
        parser::Stmt::LetStmt { span, var, init } => {
            let (_, pattern, typ) = match var {
                LetBind::Single { span, pattern, typ } => (span, Either::Left(pattern), typ),
                LetBind::Multi { span, patterns } => (span, Either::Right(patterns), None),
            };

            if typ.is_none() && init.is_none() {
                return Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    "Must specify either type or initial value".to_string(),
                )));
            }

            let ty = match typ {
                None => None,
                Some(t) => Some(process_type(
                    t,
                    num_dyn_const,
                    lexer,
                    stringtab,
                    env,
                    types,
                    true,
                )?),
            };

            let var = env.uniq();

            let (val, exp_loc) = match init {
                Some(exp) => {
                    let loc = span_to_loc(exp.span(), lexer);
                    (
                        process_expr(exp, num_dyn_const, lexer, stringtab, env, types)?,
                        loc,
                    )
                }
                None => (
                    Expr::Zero {
                        typ: ty.expect("From Above"),
                    },
                    Location::fake(),
                ),
            };
            let typ = val.get_type();

            if let Some(ty) = ty {
                if !types.unify(ty, typ) {
                    return Err(singleton_error(ErrorMessage::TypeError(
                        exp_loc,
                        unparse_type(types, ty, stringtab),
                        unparse_type(types, typ, stringtab),
                    )));
                }
            }

            let mut res = vec![];
            res.push(Stmt::AssignStmt { var, val });

            match pattern {
                Either::Left(pattern) => {
                    if let Some(return_types) = types.get_return_types(typ) {
                        return Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!("Expected {} patterns, found 1 pattern", return_types.len()),
                        )));
                    }
                    res.extend(
                        process_irrefutable_pattern(
                            pattern, false, var, typ, lexer, stringtab, env, types, false,
                        )?
                        .0,
                    );
                }
                Either::Right(patterns) => {
                    let Some(return_types) = types.get_return_types(typ) else {
                        return Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!("Expected 1 pattern, found {} patterns", patterns.len()),
                        )));
                    };
                    if return_types.len() != patterns.len() {
                        return Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!(
                                "Expected {} pattern, found {} patterns",
                                return_types.len(),
                                patterns.len()
                            ),
                        )));
                    }

                    // Process each pattern after extracting the appropriate value from the call
                    for (index, (pat, ret_typ)) in patterns
                        .into_iter()
                        .zip(return_types.clone().into_iter())
                        .enumerate()
                    {
                        let extract_var = env.uniq();
                        res.push(Stmt::AssignStmt {
                            var: extract_var,
                            val: Expr::CallExtract {
                                call: Box::new(Expr::Variable { var, typ }),
                                index,
                                typ: ret_typ,
                            },
                        });
                        res.extend(
                            process_irrefutable_pattern(
                                pat,
                                false,
                                extract_var,
                                ret_typ,
                                lexer,
                                stringtab,
                                env,
                                types,
                                false,
                            )?
                            .0,
                        );
                    }
                }
            }

            Ok((Stmt::BlockStmt { body: res }, true))
        }
        parser::Stmt::ConstStmt {
            span,
            var:
                VarBind {
                    span: _v_span,
                    pattern,
                    typ,
                },
            init,
        } => {
            if typ.is_none() && init.is_none() {
                return Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    "Must specify either type or initial value".to_string(),
                )));
            }

            let ty = match typ {
                None => None,
                Some(t) => Some(process_type(
                    t,
                    num_dyn_const,
                    lexer,
                    stringtab,
                    env,
                    types,
                    true,
                )?),
            };

            let var = env.uniq();

            let (val, exp_loc) = match init {
                Some(exp) => {
                    let loc = span_to_loc(exp.span(), lexer);
                    (
                        process_expr(exp, num_dyn_const, lexer, stringtab, env, types)?,
                        loc,
                    )
                }
                None => (
                    Expr::Zero {
                        typ: ty.expect("From Above"),
                    },
                    Location::fake(),
                ),
            };
            let typ = val.get_type();

            if let Some(ty) = ty {
                if !types.unify(ty, typ) {
                    return Err(singleton_error(ErrorMessage::TypeError(
                        exp_loc,
                        unparse_type(types, ty, stringtab),
                        unparse_type(types, typ, stringtab),
                    )));
                }
            }

            let mut res = vec![];
            res.push(Stmt::AssignStmt { var, val });
            res.extend(
                process_irrefutable_pattern(
                    pattern, false, var, typ, lexer, stringtab, env, types, false,
                )?
                .0,
            );

            Ok((Stmt::BlockStmt { body: res }, true))
        }
        parser::Stmt::AssignStmt {
            span: _,
            lhs,
            assign,
            assign_span,
            rhs,
        } => {
            let lhs_res = process_lexpr(lhs, num_dyn_const, lexer, stringtab, env, types);
            let rhs_res = process_expr(rhs, num_dyn_const, lexer, stringtab, env, types);
            let (((var, var_typ), (exp_typ, index)), val) = append_errors2(lhs_res, rhs_res)?;
            let typ = val.get_type();

            // Perform the appropriate type checking
            match assign {
                AssignOp::None => {
                    if !types.unify(exp_typ, typ) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(assign_span, lexer),
                            unparse_type(types, exp_typ, stringtab),
                            unparse_type(types, typ, stringtab),
                        )))?
                    }
                }
                AssignOp::Add | AssignOp::Sub | AssignOp::Mul | AssignOp::Div => {
                    if !types.unify(exp_typ, typ) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(assign_span, lexer),
                            unparse_type(types, exp_typ, stringtab),
                            unparse_type(types, typ, stringtab),
                        )))?
                    }
                    if !types.unify_kind(exp_typ, parser::Kind::Number) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(assign_span, lexer),
                            "number".to_string(),
                            unparse_type(types, exp_typ, stringtab),
                        )))?
                    }
                }
                AssignOp::Mod
                | AssignOp::BitAnd
                | AssignOp::BitOr
                | AssignOp::Xor
                | AssignOp::LShift
                | AssignOp::RShift => {
                    if !types.unify(exp_typ, typ) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(assign_span, lexer),
                            unparse_type(types, exp_typ, stringtab),
                            unparse_type(types, typ, stringtab),
                        )))?
                    }
                    if !types.unify_kind(exp_typ, parser::Kind::Integer) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(assign_span, lexer),
                            "integer".to_string(),
                            unparse_type(types, exp_typ, stringtab),
                        )))?
                    }
                }
                AssignOp::LogAnd | AssignOp::LogOr => {
                    if !types.unify(exp_typ, typ) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(assign_span, lexer),
                            unparse_type(types, exp_typ, stringtab),
                            unparse_type(types, typ, stringtab),
                        )))?
                    }
                    if !types.unify_bool(exp_typ) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(assign_span, lexer),
                            "bool".to_string(),
                            unparse_type(types, exp_typ, stringtab),
                        )))?
                    }
                }
            }

            let empty_index = index.is_empty();
            let rhs_var = Expr::Variable {
                var: var,
                typ: var_typ,
            };

            let rhs_val = if empty_index {
                rhs_var
            } else {
                Expr::Read {
                    index: index.clone(),
                    val: Box::new(rhs_var),
                    typ: exp_typ,
                }
            };

            // Construct the right-hand side for the normalized expression; for x= operations this
            // will construct the read and the operation; the write is left for after this since it
            // is common to all cases
            let result_rhs = match assign {
                AssignOp::None => val,
                AssignOp::Add
                | AssignOp::Sub
                | AssignOp::Mul
                | AssignOp::Div
                | AssignOp::Mod
                | AssignOp::BitAnd
                | AssignOp::BitOr
                | AssignOp::Xor
                | AssignOp::LShift
                | AssignOp::RShift => Expr::BinaryExp {
                    op: convert_assign_op(assign),
                    lhs: Box::new(rhs_val),
                    rhs: Box::new(val),
                    typ: typ,
                },
                // For x &&= y we convert to if x then y else false
                AssignOp::LogAnd => {
                    Expr::CondExpr {
                        cond: Box::new(rhs_val),
                        thn: Box::new(val),
                        // We know that the expected type is bool, so just use it to avoid
                        // creating additional new types
                        els: Box::new(Expr::Constant {
                            val: (Literal::Bool(false), exp_typ),
                            typ: exp_typ,
                        }),
                        typ: typ,
                    }
                }
                // For x ||= y we convert to if x then true else y
                AssignOp::LogOr => Expr::CondExpr {
                    cond: Box::new(rhs_val),
                    thn: Box::new(Expr::Constant {
                        val: (Literal::Bool(true), exp_typ),
                        typ: exp_typ,
                    }),
                    els: Box::new(val),
                    typ: typ,
                },
            };

            let write_exp = if empty_index {
                result_rhs
            } else {
                Expr::Write {
                    index: index,
                    val: Box::new(Expr::Variable {
                        var: var,
                        typ: var_typ,
                    }),
                    rep: Box::new(result_rhs),
                    typ: var_typ,
                }
            };

            Ok((
                Stmt::AssignStmt {
                    var: var,
                    val: write_exp,
                },
                true,
            ))
        }
        parser::Stmt::IfStmt {
            span: _,
            cond,
            thn,
            els,
        } => {
            let cond_span = cond.span();
            let cond_res = process_expr(cond, num_dyn_const, lexer, stringtab, env, types);

            env.open_scope();
            let thn_res = process_stmt(
                *thn,
                num_dyn_const,
                lexer,
                stringtab,
                env,
                types,
                in_loop,
                return_types,
                inouts,
                labels,
            );
            env.close_scope();

            env.open_scope();
            let els_res = match els {
                None => Ok((None, true)),
                Some(stmt) => process_stmt(
                    *stmt,
                    num_dyn_const,
                    lexer,
                    stringtab,
                    env,
                    types,
                    in_loop,
                    return_types,
                    inouts,
                    labels,
                )
                .map(|(s, b)| (Some(s), b)),
            };
            env.close_scope();

            let (cond_exp, (thn_body, thn_fall), (els_body, els_fall)) =
                append_errors3(cond_res, thn_res, els_res)?;
            let cond_typ = cond_exp.get_type();

            if !types.unify_bool(cond_typ) {
                Err(singleton_error(ErrorMessage::TypeError(
                    span_to_loc(cond_span, lexer),
                    "bool".to_string(),
                    unparse_type(types, cond_typ, stringtab),
                )))?
            }

            Ok((
                Stmt::IfStmt {
                    cond: cond_exp,
                    thn: Box::new(thn_body),
                    els: els_body.map(|s| Box::new(s)),
                },
                thn_fall || els_fall,
            ))
        }
        parser::Stmt::MatchStmt {
            span,
            expr: _,
            body: _,
        } => Err(singleton_error(ErrorMessage::NotImplemented(
            span_to_loc(span, lexer),
            "match statements".to_string(),
        ))),
        parser::Stmt::ForStmt {
            span: _,
            var:
                VarBind {
                    span: v_span,
                    pattern,
                    typ,
                },
            init,
            bound,
            inclusive,
            step,
            body,
        } => {
            let (var, var_name, var_type) = match pattern {
                Pattern::Variable { span, name } => {
                    if name.len() != 1 {
                        return Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            "Bound variables must be local names, without a package separator"
                                .to_string(),
                        )));
                    }

                    let nm = intern_package_name(&name, lexer, stringtab)[0];
                    let var_type = match typ {
                        None => types.new_primitive(types::Primitive::U64),
                        Some(t) => {
                            let ty =
                                process_type(t, num_dyn_const, lexer, stringtab, env, types, true)?;
                            if !types.unify_kind(ty, parser::Kind::Integer) {
                                return Err(singleton_error(ErrorMessage::SemanticError(
                                    span_to_loc(v_span, lexer),
                                    "For loop variables must be integers".to_string(),
                                )));
                            }
                            ty
                        }
                    };

                    let var = env.uniq();
                    (var, nm, var_type)
                }
                _ => {
                    return Err(singleton_error(ErrorMessage::SemanticError(
                        span_to_loc(v_span, lexer),
                        "for loop index must be a variable".to_string(),
                    )));
                }
            };

            // Evaluate the initial value, bound, and step
            let init_span = init.span();
            let bound_span = bound.span();

            let init_res = process_expr(init, num_dyn_const, lexer, stringtab, env, types);
            let bound_res = process_expr(bound, num_dyn_const, lexer, stringtab, env, types);

            // The step is tracked as a pair of the step's amount (always positive) and whether the
            // step should be positive or negative
            let (step_val, step_pos) = match step {
                None => (1, true),
                Some((negative, span, base)) => {
                    let val = u64::from_str_radix(&base.string(lexer, span), base.base());
                    assert!(val.is_ok(), "Internal Error: Int literal is not an integer");
                    let num = val.unwrap();
                    if num == 0 {
                        return Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            "For loop step cannot be 0".to_string(),
                        )));
                    }

                    (num, !negative)
                }
            };

            let (init_val, bound_val) = append_errors2(init_res, bound_res)?;
            let init_typ = init_val.get_type();
            let bound_typ = bound_val.get_type();

            // Verify that the types of the initial value and bound are correct
            let mut type_errors = LinkedList::new();
            if !types.unify(var_type, init_typ) {
                type_errors.push_back(ErrorMessage::TypeError(
                    span_to_loc(init_span, lexer),
                    unparse_type(types, var_type, stringtab),
                    unparse_type(types, init_typ, stringtab),
                ));
            }
            if !types.unify(var_type, bound_typ) {
                type_errors.push_back(ErrorMessage::TypeError(
                    span_to_loc(bound_span, lexer),
                    unparse_type(types, var_type, stringtab),
                    unparse_type(types, bound_typ, stringtab),
                ));
            }
            if !type_errors.is_empty() {
                Err(type_errors)?
            }

            // Create the scope for the body
            env.open_scope();
            env.insert(
                var_name,
                Entity::Variable {
                    variable: var,
                    typ: var_type,
                    is_const: true,
                },
            );

            // Process the body
            let (body, _) = process_stmt(
                *body,
                num_dyn_const,
                lexer,
                stringtab,
                env,
                types,
                true,
                return_types,
                inouts,
                labels,
            )?;

            env.close_scope();

            // We bind the initial value of the loop counter
            let init_eval = Stmt::AssignStmt {
                var: var,
                val: init_val,
            };

            // We create a new variable for the loop bound and we're going to bind the bound to
            // that value before the loop so that it is only evaluated once
            let bound_var = env.uniq();
            let bound_eval = Stmt::AssignStmt {
                var: bound_var,
                val: bound_val,
            };

            // There are four cases for the condition that we generate, though it always takes the
            // form var OP bound:
            // 1. The step is positive and the range is exclusive of the bound, OP = <
            // 2. The step is positive and the range is inclusive of the bound, OP = <=
            // 3. The step is negative and the range is exclusive of the bound, OP = >
            // 4. The step is negative and the range is inclusive of the bound, OP = >=
            let condition = Expr::BinaryExp {
                op: match (step_pos, inclusive) {
                    (true, false) => BinaryOp::Lt,
                    (true, true) => BinaryOp::Le,
                    (false, false) => BinaryOp::Gt,
                    (false, true) => BinaryOp::Ge,
                },
                lhs: Box::new(Expr::Variable {
                    var: var,
                    typ: var_type,
                }),
                rhs: Box::new(Expr::Variable {
                    var: bound_var,
                    typ: bound_typ,
                }),
                typ: types.new_primitive(types::Primitive::Bool),
            };

            // The update of the loop is var = var + step, unless the step is negative in which
            // case it is var = var - step
            let update = Stmt::AssignStmt {
                var: var,
                val: Expr::BinaryExp {
                    op: if step_pos {
                        BinaryOp::Add
                    } else {
                        BinaryOp::Sub
                    },
                    lhs: Box::new(Expr::Variable {
                        var: var,
                        typ: var_type,
                    }),
                    rhs: Box::new(Expr::Constant {
                        val: (Literal::Integer(step_val), var_type),
                        typ: var_type,
                    }),
                    typ: var_type,
                },
            };

            // Finally, the entire loop is constructed as:
            // Evaluate initial value
            // Evaluate bound value
            // Loop
            // Note that the statement after a loop is always assumed to be reachable
            Ok((
                Stmt::BlockStmt {
                    body: vec![
                        init_eval,
                        bound_eval,
                        Stmt::LoopStmt {
                            cond: condition,
                            update: Some(Box::new(update)),
                            body: Box::new(body),
                        },
                    ],
                },
                true,
            ))
        }
        parser::Stmt::WhileStmt {
            span: _,
            cond,
            body,
        } => {
            let cond_span = cond.span();
            let cond_res = process_expr(cond, num_dyn_const, lexer, stringtab, env, types);

            env.open_scope();
            let body_res = process_stmt(
                *body,
                num_dyn_const,
                lexer,
                stringtab,
                env,
                types,
                true,
                return_types,
                inouts,
                labels,
            );
            env.close_scope();

            let (cond_val, (body_stmt, _)) = append_errors2(cond_res, body_res)?;
            let cond_typ = cond_val.get_type();

            if !types.unify_bool(cond_typ) {
                Err(singleton_error(ErrorMessage::TypeError(
                    span_to_loc(cond_span, lexer),
                    "bool".to_string(),
                    unparse_type(types, cond_typ, stringtab),
                )))?
            }

            // Again, the statement after a loop is always considered reachable
            Ok((
                Stmt::LoopStmt {
                    cond: cond_val,
                    update: None,
                    body: Box::new(body_stmt),
                },
                true,
            ))
        }
        parser::Stmt::ReturnStmt { span, vals } => {
            if return_types.len() != vals.len() {
                return Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Expected {} return values found {}",
                        return_types.len(),
                        vals.len(),
                    ),
                )));
            }

            let return_vals = vals
                .into_iter()
                .zip(return_types.iter())
                .map(|(expr, typ)| {
                    let expr_span = expr.span();
                    let val = process_expr(expr, num_dyn_const, lexer, stringtab, env, types)?;
                    if types.unify(*typ, val.get_type()) {
                        Ok(val)
                    } else {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(expr_span, lexer),
                            unparse_type(types, *typ, stringtab),
                            unparse_type(types, val.get_type(), stringtab),
                        )))
                    }
                })
                .fold(Ok(vec![]), |res, val| match (res, val) {
                    (Ok(mut res), Ok(val)) => {
                        res.push(val);
                        Ok(res)
                    }
                    (Ok(_), Err(msg)) => Err(msg),
                    (Err(msg), Ok(_)) => Err(msg),
                    (Err(mut msgs), Err(msg)) => {
                        msgs.extend(msg);
                        Err(msgs)
                    }
                })?;

            // We return both the actual return values and the inout arguments
            // Statements after a return are never reachable
            Ok((generate_return(return_vals, inouts), false))
        }
        parser::Stmt::BreakStmt { span } => {
            if !in_loop {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    "Break not contained within loop".to_string(),
                )))?
            }

            // Code after a break is unreachable
            Ok((Stmt::BreakStmt {}, false))
        }
        parser::Stmt::ContinueStmt { span } => {
            if !in_loop {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    "Continue not contained within loop".to_string(),
                )))?
            }

            // Code after a continue is unreachable
            Ok((Stmt::ContinueStmt {}, false))
        }
        parser::Stmt::BlockStmt { span: _, body } => {
            // Blocks create a new scope for variables declared in them
            env.open_scope();

            let mut reachable = true;
            let mut errors = LinkedList::new();
            let mut res = vec![];

            for stmt in body {
                if !reachable {
                    Err(singleton_error(ErrorMessage::SemanticError(
                        span_to_loc(stmt.span(), lexer),
                        "Unreachable statement".to_string(),
                    )))?
                }

                match process_stmt(
                    stmt,
                    num_dyn_const,
                    lexer,
                    stringtab,
                    env,
                    types,
                    in_loop,
                    return_types,
                    inouts,
                    labels,
                ) {
                    Err(mut errs) => {
                        errors.append(&mut errs);
                    }
                    Ok((stmt, post_reachable)) => {
                        res.push(stmt);
                        reachable = post_reachable;
                    }
                }
            }

            env.close_scope();

            if !errors.is_empty() {
                Err(errors)
            } else {
                Ok((Stmt::BlockStmt { body: res }, reachable))
            }
        }
        parser::Stmt::CallStmt {
            span,
            name,
            ty_args,
            args,
        } => {
            // Call statements are lowered to call expressions which is made a statment using the
            // ExprStmt constructor
            // Code after a call is always reachable
            Ok((
                Stmt::ExprStmt {
                    expr: process_expr(
                        parser::Expr::CallExpr {
                            span,
                            name,
                            ty_args,
                            args,
                        },
                        num_dyn_const,
                        lexer,
                        stringtab,
                        env,
                        types,
                    )?,
                },
                true,
            ))
        }
        parser::Stmt::LabeledStmt {
            span: _,
            label,
            stmt,
        } => {
            let label_str = lexer.span_str(label)[1..].to_string();
            let label_id = labels.lookup_string(label_str);

            let (body, reach_end) = process_stmt(
                *stmt,
                num_dyn_const,
                lexer,
                stringtab,
                env,
                types,
                in_loop,
                return_types,
                inouts,
                labels,
            )?;
            Ok((
                Stmt::LabeledStmt {
                    label: label_id,
                    stmt: Box::new(body),
                },
                reach_end,
            ))
        }
    }
}

// Process an l-expression to produce the variable that's modified and its type along with the type
// of the piece being modified and a list of the index operations needed to access the accessed
// piece
// This should only be used for the left-hand side of an assignment since it will return an error
// if the variable that is accessed is marked as constant
fn process_lexpr(
    expr: parser::LExpr,
    num_dyn_const: usize,
    lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
    env: &mut Env<usize, Entity>,
    types: &mut TypeSolver,
) -> Result<((usize, Type), (Type, Vec<Index>)), ErrorMessages> {
    match expr {
        parser::LExpr::VariableLExpr { span } => {
            let nm = intern_id(&span, lexer, stringtab);
            match env.lookup(&nm) {
                Some(Entity::Variable {
                    variable,
                    typ,
                    is_const,
                }) => {
                    if *is_const {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!(
                                "Variable {} is const, cannot assign to it",
                                lexer.span_str(span)
                            ),
                        )))
                    } else {
                        Ok(((*variable, *typ), (*typ, vec![])))
                    }
                }
                Some(Entity::DynConst { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "{} is a dynamic constant, cannot assign to it",
                        lexer.span_str(span)
                    ),
                ))),
                Some(Entity::Constant { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "{} is a constant, cannot assign to it",
                        lexer.span_str(span)
                    ),
                ))),
                Some(Entity::Function { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "{} is a function, cannot assign to it",
                        lexer.span_str(span)
                    ),
                ))),
                Some(Entity::Type { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!("{} is a type, cannot assign to it", lexer.span_str(span)),
                ))),
                None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                    span_to_loc(span, lexer),
                    lexer.span_str(span).to_string(),
                ))),
            }
        }
        parser::LExpr::FieldLExpr { span, lhs, rhs } => {
            let ((var, var_typ), (idx_typ, mut idx)) =
                process_lexpr(*lhs, num_dyn_const, lexer, stringtab, env, types)?;
            let field_nm = intern_id(&rhs, lexer, stringtab);

            match types.get_field(idx_typ, field_nm) {
                None => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Type {} does not possess field {}",
                        unparse_type(types, idx_typ, stringtab),
                        stringtab.lookup_id(field_nm).unwrap()
                    ),
                ))),
                Some((field_idx, field_type)) => {
                    idx.push(Index::Field(field_idx));
                    Ok(((var, var_typ), (field_type, idx)))
                }
            }
        }
        parser::LExpr::NumFieldLExpr { span, lhs, rhs } => {
            let ((var, var_typ), (idx_typ, mut idx)) =
                process_lexpr(*lhs, num_dyn_const, lexer, stringtab, env, types)?;

            // Identify the field number; to do this we remove the first character of the string of
            // the right-hand side since the ".###" is lexed as a single token
            let num = lexer.span_str(rhs)[1..]
                .parse::<usize>()
                .expect("From lexical analysis");

            match types.get_index(idx_typ, num) {
                None => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Type {} does not possess index {}",
                        unparse_type(types, idx_typ, stringtab),
                        num
                    ),
                ))),
                Some(field_type) => {
                    idx.push(Index::Field(num));
                    Ok(((var, var_typ), (field_type, idx)))
                }
            }
        }
        parser::LExpr::IndexLExpr { span, lhs, index } => {
            let ((var, var_typ), (idx_typ, mut idx)) =
                process_lexpr(*lhs, num_dyn_const, lexer, stringtab, env, types)?;

            let mut indices = vec![];
            let mut errors = LinkedList::new();
            for idx in index {
                let idx_span = idx.span();
                match process_expr(idx, num_dyn_const, lexer, stringtab, env, types) {
                    Err(mut errs) => errors.append(&mut errs),
                    Ok(exp) => {
                        let typ = exp.get_type();
                        if !types.unify_u64(typ) {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(idx_span, lexer),
                                "usize".to_string(),
                                unparse_type(types, typ, stringtab),
                            ));
                        } else {
                            indices.push(exp);
                        }
                    }
                }
            }

            if !errors.is_empty() {
                Err(errors)?
            }

            if !types.is_array(idx_typ) {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Array index does not apply to type {}",
                        unparse_type(types, idx_typ, stringtab)
                    ),
                )))?
            }

            let num_dims = types.get_num_dimensions(idx_typ).unwrap();
            if indices.len() < num_dims {
                Err(singleton_error(
                        ErrorMessage::NotImplemented(
                            span_to_loc(span, lexer),
                            format!("fewer array indices than dimensions, array has {} dimensions but using {} indices",
                                    num_dims, indices.len()))))
            } else if indices.len() > num_dims {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Too many array indices, array has {} dimensions but using {} indices",
                        num_dims,
                        indices.len()
                    ),
                )))
            } else {
                idx.push(Index::Array(indices));
                Ok((
                    (var, var_typ),
                    (types.get_element_type(idx_typ).unwrap(), idx),
                ))
            }
        }
    }
}

fn process_expr_as_constant(
    expr: parser::Expr,
    num_dyn_const: usize,
    lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
    env: &mut Env<usize, Entity>,
    types: &mut TypeSolver,
) -> Result<Constant, ErrorMessages> {
    match expr {
        parser::Expr::Variable { span, name } => {
            if name.len() != 1 {
                Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )))?
            }
            let nm = intern_package_name(&name, lexer, stringtab)[0];

            match env.lookup(&nm) {
                Some(Entity::Variable { .. }) => {
                    panic!("Constant should not be evaluated in an environment with variables")
                }
                Some(Entity::DynConst { .. }) => {
                    panic!(
                        "Constant should not be evaluated in an environment with dynamic constants"
                    )
                }
                Some(Entity::Constant { value }) => Ok(value.clone()),
                Some(Entity::Function { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "{} is a function, expected a value",
                        stringtab.lookup_id(nm).unwrap()
                    ),
                ))),
                Some(Entity::Type { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "{} is a type, expected a value",
                        stringtab.lookup_id(nm).unwrap()
                    ),
                ))),
                None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                    span_to_loc(span, lexer),
                    stringtab.lookup_id(nm).unwrap(),
                ))),
            }
        }
        parser::Expr::Field { span, lhs, rhs } => {
            let field_name = intern_id(&rhs, lexer, stringtab);
            let (lit, typ) =
                process_expr_as_constant(*lhs, num_dyn_const, lexer, stringtab, env, types)?;

            match types.get_field(typ, field_name) {
                None => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Type {} does not possess field {}",
                        unparse_type(types, typ, stringtab),
                        stringtab.lookup_id(field_name).unwrap()
                    ),
                ))),
                Some((field_idx, _)) => {
                    let Literal::Tuple(fields) = lit else {
                        panic!("Wrong constant constructor")
                    };
                    Ok(fields[field_idx].clone())
                }
            }
        }
        parser::Expr::NumField { span, lhs, rhs } => {
            let (lit, typ) =
                process_expr_as_constant(*lhs, num_dyn_const, lexer, stringtab, env, types)?;

            let num = lexer.span_str(rhs)[1..]
                .parse::<usize>()
                .expect("From lexical analysis");

            match types.get_index(typ, num) {
                None => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Type {} does not possess index {}",
                        unparse_type(types, typ, stringtab),
                        num
                    ),
                ))),
                Some(_) => {
                    let Literal::Tuple(fields) = lit else {
                        panic!("Wrong constant constructor")
                    };
                    Ok(fields[num].clone())
                }
            }
        }
        parser::Expr::ArrIndex { span, .. } => Err(singleton_error(ErrorMessage::SemanticError(
            span_to_loc(span, lexer),
            format!("Arrays are not allowed in constants"),
        ))),
        parser::Expr::Tuple { span: _, mut exprs } => {
            if exprs.len() == 1 {
                return process_expr_as_constant(
                    exprs.pop().unwrap(),
                    num_dyn_const,
                    lexer,
                    stringtab,
                    env,
                    types,
                );
            }
            if exprs.len() == 0 {
                return Ok((Literal::Unit, types.new_primitive(types::Primitive::Unit)));
            }

            let mut vals = vec![];
            let mut typs = vec![];
            let mut errors = LinkedList::new();

            for exp in exprs {
                match process_expr_as_constant(exp, num_dyn_const, lexer, stringtab, env, types) {
                    Err(mut errs) => errors.append(&mut errs),
                    Ok((lit, typ)) => {
                        typs.push(typ);
                        vals.push((lit, typ));
                    }
                }
            }

            if !errors.is_empty() {
                Err(errors)
            } else {
                Ok((Literal::Tuple(vals), types.new_tuple(typs)))
            }
        }
        parser::Expr::Struct {
            span,
            name,
            ty_args,
            exprs,
        } => {
            if name.len() != 1 {
                Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )))?
            }

            let struct_nm = intern_package_name(&name, lexer, stringtab)[0];
            match env.lookup(&struct_nm) {
                Some(Entity::Variable { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "variable".to_string(),
                ))),
                Some(Entity::DynConst { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "dynamic constant".to_string(),
                ))),
                Some(Entity::Constant { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "constant".to_string(),
                ))),
                Some(Entity::Function { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "function".to_string(),
                ))),
                None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                    span_to_loc(span, lexer),
                    stringtab.lookup_id(struct_nm).unwrap(),
                ))),
                Some(Entity::Type {
                    type_args: kinds,
                    value: typ,
                }) => {
                    if !types.is_struct(*typ) {
                        Err(singleton_error(ErrorMessage::KindError(
                            span_to_loc(span, lexer),
                            "struct name".to_string(),
                            "non-struct type".to_string(),
                        )))?
                    }

                    let ty_args = ty_args.unwrap_or_else(|| {
                        vec![parser::TypeExpr::WildcardType { span: span }; kinds.len()]
                    });

                    if kinds.len() != ty_args.len() {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!(
                                "Expected {} type arguments, provided {}",
                                kinds.len(),
                                ty_args.len()
                            ),
                        )))?
                    }

                    // Verify that the type arguments we are provided are correct and collect the
                    // type variable and dynamic constant substitutions
                    let mut type_vars = vec![];
                    let mut dyn_consts = vec![];
                    let mut errors = LinkedList::new();

                    for (arg, kind) in ty_args.into_iter().zip(kinds.iter()) {
                        let arg_span = arg.span();
                        match kind {
                            parser::Kind::USize => {
                                match process_type_expr_as_expr(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(val) => dyn_consts.push(val),
                                }
                            }
                            _ => {
                                match process_type_expr_as_type(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                    true,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(typ) => {
                                        if types.unify_kind(typ, *kind) {
                                            type_vars.push(typ);
                                        } else {
                                            errors.push_back(ErrorMessage::KindError(
                                                span_to_loc(arg_span, lexer),
                                                kind.to_string(),
                                                unparse_type(types, typ, stringtab),
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }

                    let struct_type = if type_vars.len() == 0 && dyn_consts.len() == 0 {
                        *typ
                    } else {
                        if let Some(res) = types.instantiate(*typ, &type_vars, &dyn_consts) {
                            res
                        } else {
                            return Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(span, lexer),
                                "Failure in variable substitution".to_string(),
                            )));
                        }
                    };

                    // Check each field and construct the appropriate tuple
                    // Note that fields that are omitted will be initialized with their type's
                    // default value
                    let num_fields = types.get_num_struct_fields(struct_type).unwrap();

                    // Values for the fields, in order
                    let mut values: Vec<Option<Constant>> = vec![None; num_fields];

                    for (field_name, expr) in exprs {
                        let field_nm = intern_id(&field_name, lexer, stringtab);
                        let expr_span = expr.span();

                        match types.get_field(struct_type, field_nm) {
                            None => {
                                errors.push_back(ErrorMessage::SemanticError(
                                    span_to_loc(field_name, lexer),
                                    format!(
                                        "Struct {} does not have field {}",
                                        unparse_type(types, struct_type, stringtab),
                                        stringtab.lookup_id(field_nm).unwrap()
                                    ),
                                ));
                            }
                            Some((idx, field_typ)) => {
                                if values[idx].is_some() {
                                    errors.push_back(ErrorMessage::SemanticError(
                                        span_to_loc(field_name, lexer),
                                        format!(
                                            "Field {} defined multiple times",
                                            stringtab.lookup_id(field_nm).unwrap()
                                        ),
                                    ));
                                } else {
                                    match process_expr_as_constant(
                                        expr,
                                        num_dyn_const,
                                        lexer,
                                        stringtab,
                                        env,
                                        types,
                                    ) {
                                        Err(mut errs) => errors.append(&mut errs),
                                        Ok((lit, typ)) => {
                                            if !types.unify(field_typ, typ) {
                                                // Set the value at this index even though there's
                                                // an error so that we also report if the field is
                                                // defined multiple times
                                                values[idx] = Some((
                                                    Literal::Unit,
                                                    types.new_primitive(types::Primitive::Unit),
                                                ));
                                                errors.push_back(ErrorMessage::TypeError(
                                                    span_to_loc(expr_span, lexer),
                                                    unparse_type(types, field_typ, stringtab),
                                                    unparse_type(types, typ, stringtab),
                                                ));
                                            } else {
                                                values[idx] = Some((lit, typ));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }

                    if values.iter().any(|n| n.is_none()) {
                        Err(singleton_error(ErrorMessage::NotImplemented(
                            span_to_loc(span, lexer),
                            "constant struct with missing fields".to_string(),
                        )))?
                    }

                    // Construct the list of field values, filling in zero values as needed
                    let filled_fields = values.into_iter().map(|t| t.unwrap()).collect::<Vec<_>>();

                    Ok((Literal::Tuple(filled_fields), struct_type))
                }
            }
        }
        parser::Expr::BoolLit { span: _, value } => {
            let bool_typ = types.new_primitive(types::Primitive::Bool);
            Ok((Literal::Bool(value), bool_typ))
        }
        parser::Expr::IntLit { span, base } => {
            let res = u64::from_str_radix(&base.string(lexer, span), base.base());
            assert!(res.is_ok(), "Internal Error: Int literal is not an integer");

            let num_typ = types.new_of_kind(parser::Kind::Number, span_to_loc(span, lexer));
            Ok((Literal::Integer(res.unwrap()), num_typ))
        }
        parser::Expr::FloatLit { span } => {
            let res = lexer.span_str(span).parse::<f64>();
            assert!(res.is_ok(), "Internal Error: Float literal is not a float");

            let float_typ = types.new_of_kind(parser::Kind::Float, span_to_loc(span, lexer));
            Ok((Literal::Float(res.unwrap()), float_typ))
        }
        parser::Expr::UnaryExpr { span, op, expr } => {
            let (expr_lit, expr_typ) =
                process_expr_as_constant(*expr, num_dyn_const, lexer, stringtab, env, types)?;

            match op {
                parser::UnaryOp::Negation => {
                    if !types.unify_kind(expr_typ, parser::Kind::Number) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(span, lexer),
                            "number".to_string(),
                            unparse_type(types, expr_typ, stringtab),
                        )))
                    } else {
                        Ok((
                            match expr_lit {
                                Literal::Integer(i) => Literal::Integer(-(i as i64) as u64),
                                Literal::Float(f) => Literal::Float(-f),
                                _ => panic!("Incorrect literal constructor"),
                            },
                            expr_typ,
                        ))
                    }
                }
                parser::UnaryOp::BitwiseNot => {
                    if !types.unify_kind(expr_typ, parser::Kind::Integer) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(span, lexer),
                            "integer".to_string(),
                            unparse_type(types, expr_typ, stringtab),
                        )))
                    } else {
                        let Literal::Integer(i) = expr_lit else {
                            panic!("Incorrect literal constructor");
                        };
                        Ok((Literal::Integer(!i), expr_typ))
                    }
                }
                parser::UnaryOp::LogicalNot => {
                    if !types.unify_bool(expr_typ) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(span, lexer),
                            "bool".to_string(),
                            unparse_type(types, expr_typ, stringtab),
                        )))
                    } else {
                        let Literal::Bool(b) = expr_lit else {
                            panic!("Incorrect literal constructor");
                        };
                        Ok((Literal::Bool(!b), expr_typ))
                    }
                }
            }
        }
        parser::Expr::BinaryExpr {
            span: _,
            op,
            lhs,
            rhs,
        } => {
            let lhs_span = lhs.span();
            let rhs_span = rhs.span();

            let lhs_res =
                process_expr_as_constant(*lhs, num_dyn_const, lexer, stringtab, env, types);
            let rhs_res =
                process_expr_as_constant(*rhs, num_dyn_const, lexer, stringtab, env, types);

            let ((lhs_lit, lhs_typ), (rhs_lit, rhs_typ)) = append_errors2(lhs_res, rhs_res)?;

            // First, type-check
            match op {
                // Equality and inequality work on any types
                parser::BinaryOp::Eq | parser::BinaryOp::Neq => {
                    if !types.unify(lhs_typ, rhs_typ) {
                        return Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(rhs_span, lexer),
                            unparse_type(types, lhs_typ, stringtab),
                            unparse_type(types, rhs_typ, stringtab),
                        )));
                    }
                }
                // These work on any numbers
                parser::BinaryOp::Add
                | parser::BinaryOp::Sub
                | parser::BinaryOp::Mul
                | parser::BinaryOp::Div
                | parser::BinaryOp::Lt
                | parser::BinaryOp::Le
                | parser::BinaryOp::Gt
                | parser::BinaryOp::Ge => {
                    let mut errors = LinkedList::new();
                    let lhs_number = types.unify_kind(lhs_typ, parser::Kind::Number);
                    let rhs_number = types.unify_kind(rhs_typ, parser::Kind::Number);
                    let equal = types.unify(lhs_typ, rhs_typ);

                    if lhs_number && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(rhs_span, lexer),
                            unparse_type(types, lhs_typ, stringtab),
                            unparse_type(types, rhs_typ, stringtab),
                        ));
                    } else if rhs_number && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(lhs_span, lexer),
                            unparse_type(types, rhs_typ, stringtab),
                            unparse_type(types, lhs_typ, stringtab),
                        ));
                    } else {
                        // The types are equal or both are not numbers
                        if !lhs_number {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(lhs_span, lexer),
                                "number".to_string(),
                                unparse_type(types, lhs_typ, stringtab),
                            ));
                        }
                        if !rhs_number {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(rhs_span, lexer),
                                "number".to_string(),
                                unparse_type(types, rhs_typ, stringtab),
                            ));
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }
                }
                parser::BinaryOp::Mod
                | parser::BinaryOp::BitAnd
                | parser::BinaryOp::BitOr
                | parser::BinaryOp::Xor
                | parser::BinaryOp::LShift
                | parser::BinaryOp::RShift => {
                    let mut errors = LinkedList::new();
                    let lhs_integer = types.unify_kind(lhs_typ, parser::Kind::Integer);
                    let rhs_integer = types.unify_kind(rhs_typ, parser::Kind::Integer);
                    let equal = types.unify(lhs_typ, rhs_typ);

                    if lhs_integer && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(rhs_span, lexer),
                            unparse_type(types, lhs_typ, stringtab),
                            unparse_type(types, rhs_typ, stringtab),
                        ));
                    } else if rhs_integer && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(lhs_span, lexer),
                            unparse_type(types, rhs_typ, stringtab),
                            unparse_type(types, lhs_typ, stringtab),
                        ));
                    } else {
                        // The types are equal or both are not integers
                        if !lhs_integer {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(lhs_span, lexer),
                                "integer".to_string(),
                                unparse_type(types, lhs_typ, stringtab),
                            ));
                        }
                        if !rhs_integer {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(rhs_span, lexer),
                                "integer".to_string(),
                                unparse_type(types, rhs_typ, stringtab),
                            ));
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }
                }
                parser::BinaryOp::LogAnd | parser::BinaryOp::LogOr => {
                    let mut errors = LinkedList::new();
                    let lhs_bool = types.unify_bool(lhs_typ);
                    let rhs_bool = types.unify_bool(rhs_typ);
                    let equal = types.unify(lhs_typ, rhs_typ);

                    if lhs_bool && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(rhs_span, lexer),
                            unparse_type(types, lhs_typ, stringtab),
                            unparse_type(types, rhs_typ, stringtab),
                        ));
                    } else if rhs_bool && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(lhs_span, lexer),
                            unparse_type(types, rhs_typ, stringtab),
                            unparse_type(types, lhs_typ, stringtab),
                        ));
                    } else {
                        // The types are equal or both are not bools
                        if !lhs_bool {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(lhs_span, lexer),
                                "bool".to_string(),
                                unparse_type(types, lhs_typ, stringtab),
                            ));
                        }
                        if !rhs_bool {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(rhs_span, lexer),
                                "bool".to_string(),
                                unparse_type(types, rhs_typ, stringtab),
                            ));
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }
                }
            };

            match op {
                parser::BinaryOp::Add => match (lhs_lit, rhs_lit) {
                    (Literal::Integer(i), Literal::Integer(j)) => {
                        Ok((Literal::Integer(i + j), lhs_typ))
                    }
                    (Literal::Integer(i), Literal::Float(j)) => {
                        Ok((Literal::Float((i as f64) + j), lhs_typ))
                    }
                    (Literal::Float(i), Literal::Integer(j)) => {
                        Ok((Literal::Float(i + (j as f64)), lhs_typ))
                    }
                    (Literal::Float(i), Literal::Float(j)) => Ok((Literal::Float(i + j), lhs_typ)),
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::Sub => match (lhs_lit, rhs_lit) {
                    (Literal::Integer(i), Literal::Integer(j)) => {
                        Ok((Literal::Integer((i as i64 - j as i64) as u64), lhs_typ))
                    }
                    (Literal::Integer(i), Literal::Float(j)) => {
                        Ok((Literal::Float((i as f64) - j), lhs_typ))
                    }
                    (Literal::Float(i), Literal::Integer(j)) => {
                        Ok((Literal::Float(i - (j as f64)), lhs_typ))
                    }
                    (Literal::Float(i), Literal::Float(j)) => Ok((Literal::Float(i - j), lhs_typ)),
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::Mul => match (lhs_lit, rhs_lit) {
                    (Literal::Integer(i), Literal::Integer(j)) => {
                        Ok((Literal::Integer(i * j), lhs_typ))
                    }
                    (Literal::Integer(i), Literal::Float(j)) => {
                        Ok((Literal::Float((i as f64) * j), lhs_typ))
                    }
                    (Literal::Float(i), Literal::Integer(j)) => {
                        Ok((Literal::Float(i * (j as f64)), lhs_typ))
                    }
                    (Literal::Float(i), Literal::Float(j)) => Ok((Literal::Float(i * j), lhs_typ)),
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::Div => match (lhs_lit, rhs_lit) {
                    (Literal::Integer(i), Literal::Integer(j)) => {
                        Ok((Literal::Integer(i / j), lhs_typ))
                    }
                    (Literal::Integer(i), Literal::Float(j)) => {
                        Ok((Literal::Float((i as f64) / j), lhs_typ))
                    }
                    (Literal::Float(i), Literal::Integer(j)) => {
                        Ok((Literal::Float(i / (j as f64)), lhs_typ))
                    }
                    (Literal::Float(i), Literal::Float(j)) => Ok((Literal::Float(i / j), lhs_typ)),
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::Mod => match (lhs_lit, rhs_lit) {
                    (Literal::Integer(i), Literal::Integer(j)) => {
                        Ok((Literal::Integer(i % j), lhs_typ))
                    }
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::BitAnd => match (lhs_lit, rhs_lit) {
                    (Literal::Integer(i), Literal::Integer(j)) => {
                        Ok((Literal::Integer(i & j), lhs_typ))
                    }
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::BitOr => match (lhs_lit, rhs_lit) {
                    (Literal::Integer(i), Literal::Integer(j)) => {
                        Ok((Literal::Integer(i | j), lhs_typ))
                    }
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::Xor => match (lhs_lit, rhs_lit) {
                    (Literal::Integer(i), Literal::Integer(j)) => {
                        Ok((Literal::Integer(i ^ j), lhs_typ))
                    }
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::Lt => {
                    let bool_typ = types.new_primitive(types::Primitive::Bool);
                    match (lhs_lit, rhs_lit) {
                        (Literal::Integer(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i < j), bool_typ))
                        }
                        (Literal::Integer(i), Literal::Float(j)) => {
                            Ok((Literal::Bool((i as f64) < j), bool_typ))
                        }
                        (Literal::Float(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i < (j as f64)), bool_typ))
                        }
                        (Literal::Float(i), Literal::Float(j)) => {
                            Ok((Literal::Bool(i < j), bool_typ))
                        }
                        _ => panic!("Incorrect literal constructor"),
                    }
                }
                parser::BinaryOp::Le => {
                    let bool_typ = types.new_primitive(types::Primitive::Bool);
                    match (lhs_lit, rhs_lit) {
                        (Literal::Integer(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i <= j), bool_typ))
                        }
                        (Literal::Integer(i), Literal::Float(j)) => {
                            Ok((Literal::Bool((i as f64) <= j), bool_typ))
                        }
                        (Literal::Float(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i <= (j as f64)), bool_typ))
                        }
                        (Literal::Float(i), Literal::Float(j)) => {
                            Ok((Literal::Bool(i <= j), bool_typ))
                        }
                        _ => panic!("Incorrect literal constructor"),
                    }
                }
                parser::BinaryOp::Gt => {
                    let bool_typ = types.new_primitive(types::Primitive::Bool);
                    match (lhs_lit, rhs_lit) {
                        (Literal::Integer(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i > j), bool_typ))
                        }
                        (Literal::Integer(i), Literal::Float(j)) => {
                            Ok((Literal::Bool((i as f64) > j), bool_typ))
                        }
                        (Literal::Float(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i > (j as f64)), bool_typ))
                        }
                        (Literal::Float(i), Literal::Float(j)) => {
                            Ok((Literal::Bool(i > j), bool_typ))
                        }
                        _ => panic!("Incorrect literal constructor"),
                    }
                }
                parser::BinaryOp::Ge => {
                    let bool_typ = types.new_primitive(types::Primitive::Bool);
                    match (lhs_lit, rhs_lit) {
                        (Literal::Integer(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i >= j), bool_typ))
                        }
                        (Literal::Integer(i), Literal::Float(j)) => {
                            Ok((Literal::Bool((i as f64) >= j), bool_typ))
                        }
                        (Literal::Float(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i >= (j as f64)), bool_typ))
                        }
                        (Literal::Float(i), Literal::Float(j)) => {
                            Ok((Literal::Bool(i >= j), bool_typ))
                        }
                        _ => panic!("Incorrect literal constructor"),
                    }
                }
                parser::BinaryOp::Eq => {
                    let bool_typ = types.new_primitive(types::Primitive::Bool);
                    match (lhs_lit, rhs_lit) {
                        (Literal::Integer(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i == j), bool_typ))
                        }
                        (Literal::Integer(i), Literal::Float(j)) => {
                            Ok((Literal::Bool((i as f64) == j), bool_typ))
                        }
                        (Literal::Float(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i == (j as f64)), bool_typ))
                        }
                        (Literal::Float(i), Literal::Float(j)) => {
                            Ok((Literal::Bool(i == j), bool_typ))
                        }
                        (lhs_lit, rhs_lit) => Ok((Literal::Bool(lhs_lit == rhs_lit), bool_typ)),
                    }
                }
                parser::BinaryOp::Neq => {
                    let bool_typ = types.new_primitive(types::Primitive::Bool);
                    match (lhs_lit, rhs_lit) {
                        (Literal::Integer(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i != j), bool_typ))
                        }
                        (Literal::Integer(i), Literal::Float(j)) => {
                            Ok((Literal::Bool((i as f64) != j), bool_typ))
                        }
                        (Literal::Float(i), Literal::Integer(j)) => {
                            Ok((Literal::Bool(i != (j as f64)), bool_typ))
                        }
                        (Literal::Float(i), Literal::Float(j)) => {
                            Ok((Literal::Bool(i != j), bool_typ))
                        }
                        (lhs_lit, rhs_lit) => Ok((Literal::Bool(lhs_lit != rhs_lit), bool_typ)),
                    }
                }
                parser::BinaryOp::LShift => match (lhs_lit, rhs_lit) {
                    (Literal::Integer(i), Literal::Integer(j)) => {
                        Ok((Literal::Integer(i << j), lhs_typ))
                    }
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::RShift => match (lhs_lit, rhs_lit) {
                    (Literal::Integer(i), Literal::Integer(j)) => {
                        Ok((Literal::Integer(i >> j), lhs_typ))
                    }
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::LogAnd => match (lhs_lit, rhs_lit) {
                    (Literal::Bool(i), Literal::Bool(j)) => Ok((Literal::Bool(i && j), lhs_typ)),
                    _ => panic!("Incorrect literal constructor"),
                },
                parser::BinaryOp::LogOr => match (lhs_lit, rhs_lit) {
                    (Literal::Bool(i), Literal::Bool(j)) => Ok((Literal::Bool(i || j), lhs_typ)),
                    _ => panic!("Incorrect literal constructor"),
                },
            }
        }
        parser::Expr::CastExpr { span, expr, typ } => {
            // Cast between numeric types
            let expr_res =
                process_expr_as_constant(*expr, num_dyn_const, lexer, stringtab, env, types);
            // Inferring the type of a cast seems weird, so not allowing
            let type_res = process_type(typ, num_dyn_const, lexer, stringtab, env, types, false);

            let ((expr_lit, expr_typ), to_typ) = append_errors2(expr_res, type_res)?;

            if !types.unify_kind(expr_typ, parser::Kind::Number)
                || !types.unify_kind(to_typ, parser::Kind::Number)
            {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Can only cast between numeric types, cannot cast {} to {}",
                        unparse_type(types, expr_typ, stringtab),
                        unparse_type(types, to_typ, stringtab)
                    ),
                )))
            } else {
                if types.unify_kind(to_typ, parser::Kind::Integer) {
                    Ok((
                        match expr_lit {
                            Literal::Integer(i) => Literal::Integer(i),
                            Literal::Float(f) => Literal::Integer(f as u64),
                            _ => panic!("Incorrect literal constructor"),
                        },
                        to_typ,
                    ))
                } else {
                    assert!(
                        types.unify_kind(to_typ, parser::Kind::Float),
                        "Casting to type which is neither integer or float"
                    );
                    Ok((
                        match expr_lit {
                            Literal::Integer(i) => Literal::Float(i as f64),
                            Literal::Float(f) => Literal::Float(f),
                            _ => panic!("Incorrect literal constructor"),
                        },
                        to_typ,
                    ))
                }
            }
        }
        parser::Expr::CondExpr {
            span,
            cond,
            thn,
            els,
        } => {
            let cond_span = cond.span();

            let cond_res =
                process_expr_as_constant(*cond, num_dyn_const, lexer, stringtab, env, types);
            let thn_res =
                process_expr_as_constant(*thn, num_dyn_const, lexer, stringtab, env, types);
            let els_res =
                process_expr_as_constant(*els, num_dyn_const, lexer, stringtab, env, types);

            let ((cond_lit, cond_typ), (thn_lit, thn_typ), (els_lit, els_typ)) =
                append_errors3(cond_res, thn_res, els_res)?;

            let mut errors = LinkedList::new();

            if !types.unify_bool(cond_typ) {
                errors.push_back(ErrorMessage::TypeError(
                    span_to_loc(cond_span, lexer),
                    "bool".to_string(),
                    unparse_type(types, cond_typ, stringtab),
                ));
            }
            if !types.unify(thn_typ, els_typ) {
                errors.push_back(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Types of conditional branches do not match, have {} and {}",
                        unparse_type(types, thn_typ, stringtab),
                        unparse_type(types, els_typ, stringtab)
                    ),
                ));
            }

            if !errors.is_empty() {
                Err(errors)
            } else {
                let Literal::Bool(condition) = cond_lit else {
                    panic!("Incorrect literal constructor");
                };
                if condition {
                    Ok((thn_lit, thn_typ))
                } else {
                    Ok((els_lit, els_typ))
                }
            }
        }
        parser::Expr::CallExpr {
            span,
            name,
            ty_args,
            args,
        } => {
            // While calls cannot be evaluated as constants, enum values can be, so we need to
            // distinguish whether this is actually a call or the construction of some enum value
            if name.len() > 2 {
                Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )))?
            }

            let nm = intern_package_name(&name, lexer, stringtab);

            match env.lookup(&nm[0]) {
                Some(Entity::Variable { .. })
                | Some(Entity::DynConst { .. })
                | Some(Entity::Constant { .. })
                | Some(Entity::Function { .. })
                | None
                    if name.len() != 1 =>
                {
                    Err(singleton_error(ErrorMessage::NotImplemented(
                        span_to_loc(span, lexer),
                        "packages".to_string(),
                    )))
                }
                None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                    span_to_loc(name[0], lexer),
                    stringtab.lookup_id(nm[0]).unwrap(),
                ))),
                Some(Entity::Variable { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(name[0], lexer),
                    format!(
                        "{} is a variable, expected a function or union constructor",
                        stringtab.lookup_id(nm[0]).unwrap()
                    ),
                ))),
                Some(Entity::DynConst { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(name[0], lexer),
                    format!(
                        "{} is a dynamic constant, expected a function or union constructor",
                        stringtab.lookup_id(nm[0]).unwrap()
                    ),
                ))),
                Some(Entity::Constant { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(name[0], lexer),
                    format!(
                        "{} is a constant, expected a function or union constructor",
                        stringtab.lookup_id(nm[0]).unwrap()
                    ),
                ))),
                Some(Entity::Function { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(name[0], lexer),
                    format!("Function calls cannot be evaluated as a constant"),
                ))),
                Some(Entity::Type {
                    type_args: kinds,
                    value: typ,
                }) => {
                    if !types.is_union(*typ) {
                        if name.len() != 1 {
                            Err(singleton_error(ErrorMessage::NotImplemented(
                                span_to_loc(span, lexer),
                                "packages".to_string(),
                            )))?
                        } else {
                            Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(name[0], lexer),
                                format!(
                                    "{} is a type, expected a function or union constructor",
                                    stringtab.lookup_id(nm[0]).unwrap()
                                ),
                            )))?
                        }
                    }
                    if name.len() != 2 {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(name[0], lexer),
                            format!("Expected constructor name"),
                        )))?
                    }

                    if types.get_constructor_info(*typ, nm[1]).is_none() {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(name[1], lexer),
                            format!(
                                "{} is not a constructor of type {}",
                                stringtab.lookup_id(nm[1]).unwrap(),
                                unparse_type(types, *typ, stringtab)
                            ),
                        )))?
                    }

                    // Now, we know that we are constructing some union, we need to verify that
                    // the type arguments are appropriate
                    let ty_args = ty_args.unwrap_or_else(|| {
                        vec![parser::TypeExpr::WildcardType { span: span }; kinds.len()]
                    });

                    if kinds.len() != ty_args.len() {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!(
                                "Expected {} type arguments, provided {}",
                                kinds.len(),
                                ty_args.len()
                            ),
                        )))?
                    }

                    let mut type_vars = vec![];
                    let mut dyn_consts = vec![];
                    let mut errors = LinkedList::new();

                    for (arg, kind) in ty_args.into_iter().zip(kinds.iter()) {
                        let arg_span = arg.span();
                        match kind {
                            parser::Kind::USize => {
                                match process_type_expr_as_expr(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(val) => dyn_consts.push(val),
                                }
                            }
                            _ => {
                                match process_type_expr_as_type(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                    true,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(typ) => {
                                        if types.unify_kind(typ, *kind) {
                                            type_vars.push(typ);
                                        } else {
                                            errors.push_back(ErrorMessage::KindError(
                                                span_to_loc(arg_span, lexer),
                                                kind.to_string(),
                                                unparse_type(types, typ, stringtab),
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }

                    let union_type = if type_vars.len() == 0 && dyn_consts.len() == 0 {
                        *typ
                    } else {
                        if let Some(res) = types.instantiate(*typ, &type_vars, &dyn_consts) {
                            res
                        } else {
                            return Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(span, lexer),
                                "Failure in variable substitution".to_string(),
                            )));
                        }
                    };
                    let Some((constr_idx, constr_typ)) =
                        types.get_constructor_info(union_type, nm[1])
                    else {
                        panic!("From above");
                    };

                    // Now, process the arguments to ensure they has the type needed by this
                    // constructor
                    // To do this, since unions take a single argument, we process the arguments as
                    // a single tuple, reporting an error if inout is used anywhere
                    for (is_inout, arg) in args.iter() {
                        if *is_inout {
                            Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(arg.span(), lexer),
                                format!("Union constructors cannot be marked inout"),
                            )))?
                        }
                    }

                    let (body_lit, body_typ) = process_expr_as_constant(
                        parser::Expr::Tuple {
                            span: span,
                            exprs: args.into_iter().map(|(_, a)| a).collect::<Vec<_>>(),
                        },
                        num_dyn_const,
                        lexer,
                        stringtab,
                        env,
                        types,
                    )?;

                    if !types.unify(constr_typ, body_typ) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(span, lexer),
                            unparse_type(types, constr_typ, stringtab),
                            unparse_type(types, body_typ, stringtab),
                        )))
                    } else {
                        Ok((
                            Literal::Sum(constr_idx, Box::new((body_lit, body_typ))),
                            body_typ,
                        ))
                    }
                }
            }
        }
        parser::Expr::IntrinsicExpr { span, .. } => {
            Err(singleton_error(ErrorMessage::NotImplemented(
                span_to_loc(span, lexer),
                "Intrinsics evaluated as constants".to_string(),
            )))
        }
    }
}

fn process_expr(
    expr: parser::Expr,
    num_dyn_const: usize,
    lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
    env: &mut Env<usize, Entity>,
    types: &mut TypeSolver,
) -> Result<Expr, ErrorMessages> {
    match expr {
        parser::Expr::Variable { span, name } => {
            if name.len() != 1 {
                Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )))?
            }
            let nm = intern_package_name(&name, lexer, stringtab)[0];

            match env.lookup(&nm) {
                Some(Entity::Variable { variable, typ, .. }) => Ok(Expr::Variable {
                    var: *variable,
                    typ: *typ,
                }),
                Some(Entity::DynConst { value }) => {
                    let typ = types.new_primitive(types::Primitive::U64);
                    Ok(Expr::DynConst {
                        val: value.clone(),
                        typ: typ,
                    })
                }
                Some(Entity::Constant { value: (lit, typ) }) => Ok(Expr::Constant {
                    val: (lit.clone(), *typ),
                    typ: *typ,
                }),
                Some(Entity::Function { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "{} is a function, expected a value",
                        stringtab.lookup_id(nm).unwrap()
                    ),
                ))),
                Some(Entity::Type { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "{} is a type, expected a value",
                        stringtab.lookup_id(nm).unwrap()
                    ),
                ))),
                None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                    span_to_loc(span, lexer),
                    stringtab.lookup_id(nm).unwrap(),
                ))),
            }
        }
        parser::Expr::Field { span, lhs, rhs } => {
            let field_name = intern_id(&rhs, lexer, stringtab);
            let exp = process_expr(*lhs, num_dyn_const, lexer, stringtab, env, types)?;
            let exp_typ = exp.get_type();

            match types.get_field(exp_typ, field_name) {
                None => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Type {} does not possess field {}",
                        unparse_type(types, exp_typ, stringtab),
                        stringtab.lookup_id(field_name).unwrap()
                    ),
                ))),
                Some((field_idx, field_type)) => Ok(Expr::Read {
                    index: vec![Index::Field(field_idx)],
                    val: Box::new(exp),
                    typ: field_type,
                }),
            }
        }
        parser::Expr::NumField { span, lhs, rhs } => {
            let exp = process_expr(*lhs, num_dyn_const, lexer, stringtab, env, types)?;
            let exp_typ = exp.get_type();

            let num = lexer.span_str(rhs)[1..]
                .parse::<usize>()
                .expect("From lexical analysis");

            match types.get_index(exp_typ, num) {
                None => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Type {} does not possess index {}",
                        unparse_type(types, exp_typ, stringtab),
                        num
                    ),
                ))),
                Some(field_type) => Ok(Expr::Read {
                    index: vec![Index::Field(num)],
                    val: Box::new(exp),
                    typ: field_type,
                }),
            }
        }
        parser::Expr::ArrIndex { span, lhs, index } => {
            let exp = process_expr(*lhs, num_dyn_const, lexer, stringtab, env, types)?;
            let exp_typ = exp.get_type();

            let mut indices = vec![];
            let mut errors = LinkedList::new();
            for idx in index {
                let idx_span = idx.span();
                match process_expr(idx, num_dyn_const, lexer, stringtab, env, types) {
                    Err(mut errs) => errors.append(&mut errs),
                    Ok(exp) => {
                        let typ = exp.get_type();
                        if !types.unify_u64(typ) {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(idx_span, lexer),
                                "usize".to_string(),
                                unparse_type(types, typ, stringtab),
                            ));
                        } else {
                            indices.push(exp);
                        }
                    }
                }
            }

            if !errors.is_empty() {
                Err(errors)?
            }

            if !types.is_array(exp_typ) {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Array index does not apply to type {}",
                        unparse_type(types, exp_typ, stringtab)
                    ),
                )))?
            }

            let num_dims = types.get_num_dimensions(exp_typ).unwrap();
            if indices.len() < num_dims {
                Err(singleton_error(
                        ErrorMessage::NotImplemented(
                            span_to_loc(span, lexer),
                            format!("fewer array indices than dimensions, array has {} dimensions but using {} indices",
                                    num_dims, indices.len()))))
            } else if indices.len() > num_dims {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Too many array indices, array has {} dimensions but using {} indices",
                        num_dims,
                        indices.len()
                    ),
                )))
            } else {
                Ok(Expr::Read {
                    index: vec![Index::Array(indices)],
                    val: Box::new(exp),
                    typ: types.get_element_type(exp_typ).unwrap(),
                })
            }
        }
        parser::Expr::Tuple { span: _, mut exprs } => {
            if exprs.len() == 1 {
                return process_expr(
                    exprs.pop().unwrap(),
                    num_dyn_const,
                    lexer,
                    stringtab,
                    env,
                    types,
                );
            }
            if exprs.len() == 0 {
                let unit_type = types.new_primitive(types::Primitive::Unit);
                return Ok(Expr::Constant {
                    val: (Literal::Unit, unit_type),
                    typ: unit_type,
                });
            }

            let mut vals = vec![];
            let mut typs = vec![];
            let mut errors = LinkedList::new();

            for exp in exprs {
                match process_expr(exp, num_dyn_const, lexer, stringtab, env, types) {
                    Err(mut errs) => errors.append(&mut errs),
                    Ok(val) => {
                        typs.push(val.get_type());
                        vals.push(val);
                    }
                }
            }

            if !errors.is_empty() {
                Err(errors)
            } else {
                Ok(Expr::Tuple {
                    vals: vals,
                    typ: types.new_tuple(typs),
                })
            }
        }
        parser::Expr::Struct {
            span,
            name,
            ty_args,
            exprs,
        } => {
            if name.len() != 1 {
                Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )))?
            }

            let struct_nm = intern_package_name(&name, lexer, stringtab)[0];
            match env.lookup(&struct_nm) {
                Some(Entity::Variable { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "variable".to_string(),
                ))),
                Some(Entity::DynConst { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "dynamic constant".to_string(),
                ))),
                Some(Entity::Constant { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "constant".to_string(),
                ))),
                Some(Entity::Function { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "function".to_string(),
                ))),
                None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                    span_to_loc(span, lexer),
                    stringtab.lookup_id(struct_nm).unwrap(),
                ))),
                Some(Entity::Type {
                    type_args: kinds,
                    value: typ,
                }) => {
                    if !types.is_struct(*typ) {
                        Err(singleton_error(ErrorMessage::KindError(
                            span_to_loc(span, lexer),
                            "struct name".to_string(),
                            "non-struct type".to_string(),
                        )))?
                    }

                    let ty_args = ty_args.unwrap_or_else(|| {
                        vec![parser::TypeExpr::WildcardType { span: span }; kinds.len()]
                    });

                    if kinds.len() != ty_args.len() {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!(
                                "Expected {} type arguments, provided {}",
                                kinds.len(),
                                ty_args.len()
                            ),
                        )))?
                    }

                    // Verify that the type arguments we are provided are correct and collect the
                    // type variable and dynamic constant substitutions
                    let mut type_vars = vec![];
                    let mut dyn_consts = vec![];
                    let mut errors = LinkedList::new();

                    for (arg, kind) in ty_args.into_iter().zip(kinds.iter()) {
                        let arg_span = arg.span();
                        match kind {
                            parser::Kind::USize => {
                                match process_type_expr_as_expr(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(val) => dyn_consts.push(val),
                                }
                            }
                            _ => {
                                match process_type_expr_as_type(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                    true,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(typ) => {
                                        if types.unify_kind(typ, *kind) {
                                            type_vars.push(typ);
                                        } else {
                                            errors.push_back(ErrorMessage::KindError(
                                                span_to_loc(arg_span, lexer),
                                                kind.to_string(),
                                                unparse_type(types, typ, stringtab),
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }

                    let struct_type = if type_vars.len() == 0 && dyn_consts.len() == 0 {
                        *typ
                    } else {
                        if let Some(res) = types.instantiate(*typ, &type_vars, &dyn_consts) {
                            res
                        } else {
                            return Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(span, lexer),
                                "Failure in variable substitution".to_string(),
                            )));
                        }
                    };

                    // Check each field and construct the appropriate tuple
                    // Note that fields that are omitted will be initialized with their type's
                    // default value
                    let num_fields = types.get_num_struct_fields(struct_type).unwrap();

                    // Values for the fields, in order
                    let mut values: Vec<Option<Expr>> = vec![None; num_fields];

                    for (field_name, expr) in exprs {
                        let field_nm = intern_id(&field_name, lexer, stringtab);
                        let expr_span = expr.span();

                        match types.get_field(struct_type, field_nm) {
                            None => {
                                errors.push_back(ErrorMessage::SemanticError(
                                    span_to_loc(field_name, lexer),
                                    format!(
                                        "Struct {} does not have field {}",
                                        unparse_type(types, struct_type, stringtab),
                                        stringtab.lookup_id(field_nm).unwrap()
                                    ),
                                ));
                            }
                            Some((idx, field_typ)) => {
                                if values[idx].is_some() {
                                    errors.push_back(ErrorMessage::SemanticError(
                                        span_to_loc(field_name, lexer),
                                        format!(
                                            "Field {} defined multiple times",
                                            stringtab.lookup_id(field_nm).unwrap()
                                        ),
                                    ));
                                } else {
                                    match process_expr(
                                        expr,
                                        num_dyn_const,
                                        lexer,
                                        stringtab,
                                        env,
                                        types,
                                    ) {
                                        Err(mut errs) => errors.append(&mut errs),
                                        Ok(val) => {
                                            let val_typ = val.get_type();
                                            if !types.unify(field_typ, val_typ) {
                                                // Set the value at this index even though there's
                                                // an error so that we also report if the field is
                                                // defined multiple times
                                                values[idx] = Some(Expr::Zero { typ: field_typ });
                                                errors.push_back(ErrorMessage::TypeError(
                                                    span_to_loc(expr_span, lexer),
                                                    unparse_type(types, field_typ, stringtab),
                                                    unparse_type(types, val_typ, stringtab),
                                                ));
                                            } else {
                                                values[idx] = Some(val);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }

                    // Construct the list of field values, filling in zero values as needed
                    let filled_fields = values
                        .into_iter()
                        .enumerate()
                        .map(|(i, t)| {
                            t.unwrap_or(Expr::Zero {
                                typ: types.get_struct_field_type(struct_type, i).unwrap(),
                            })
                        })
                        .collect::<Vec<_>>();

                    Ok(Expr::Tuple {
                        vals: filled_fields,
                        typ: struct_type,
                    })
                }
            }
        }
        parser::Expr::BoolLit { span: _, value } => {
            let bool_typ = types.new_primitive(types::Primitive::Bool);
            Ok(Expr::Constant {
                val: (Literal::Bool(value), bool_typ),
                typ: bool_typ,
            })
        }
        parser::Expr::IntLit { span, base } => {
            let res = u64::from_str_radix(&base.string(lexer, span), base.base());
            assert!(res.is_ok(), "Internal Error: Int literal is not an integer");

            let num_typ = types.new_of_kind(parser::Kind::Number, span_to_loc(span, lexer));
            Ok(Expr::Constant {
                val: (Literal::Integer(res.unwrap()), num_typ),
                typ: num_typ,
            })
        }
        parser::Expr::FloatLit { span } => {
            let res = lexer.span_str(span).parse::<f64>();
            assert!(res.is_ok(), "Internal Error: Float literal is not a float");

            let float_typ = types.new_of_kind(parser::Kind::Float, span_to_loc(span, lexer));
            Ok(Expr::Constant {
                val: (Literal::Float(res.unwrap()), float_typ),
                typ: float_typ,
            })
        }
        parser::Expr::UnaryExpr { span, op, expr } => {
            let expr_val = process_expr(*expr, num_dyn_const, lexer, stringtab, env, types)?;
            let expr_typ = expr_val.get_type();

            match op {
                parser::UnaryOp::Negation => {
                    if !types.unify_kind(expr_typ, parser::Kind::Number) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(span, lexer),
                            "number".to_string(),
                            unparse_type(types, expr_typ, stringtab),
                        )))
                    } else {
                        Ok(Expr::UnaryExp {
                            op: UnaryOp::Negation,
                            expr: Box::new(expr_val),
                            typ: expr_typ,
                        })
                    }
                }
                parser::UnaryOp::BitwiseNot => {
                    if !types.unify_kind(expr_typ, parser::Kind::Integer) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(span, lexer),
                            "integer".to_string(),
                            unparse_type(types, expr_typ, stringtab),
                        )))
                    } else {
                        Ok(Expr::UnaryExp {
                            op: UnaryOp::BitwiseNot,
                            expr: Box::new(expr_val),
                            typ: expr_typ,
                        })
                    }
                }
                parser::UnaryOp::LogicalNot => {
                    if !types.unify_bool(expr_typ) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(span, lexer),
                            "bool".to_string(),
                            unparse_type(types, expr_typ, stringtab),
                        )))
                    } else {
                        // ! x is translated into if x then false else true
                        let val_true = Expr::Constant {
                            val: (Literal::Bool(true), expr_typ),
                            typ: expr_typ,
                        };
                        let val_false = Expr::Constant {
                            val: (Literal::Bool(false), expr_typ),
                            typ: expr_typ,
                        };
                        Ok(Expr::CondExpr {
                            cond: Box::new(expr_val),
                            thn: Box::new(val_false),
                            els: Box::new(val_true),
                            typ: expr_typ,
                        })
                    }
                }
            }
        }
        parser::Expr::BinaryExpr {
            span: _,
            op,
            lhs,
            rhs,
        } => {
            let lhs_span = lhs.span();
            let rhs_span = rhs.span();

            let lhs_res = process_expr(*lhs, num_dyn_const, lexer, stringtab, env, types);
            let rhs_res = process_expr(*rhs, num_dyn_const, lexer, stringtab, env, types);

            let (lhs_val, rhs_val) = append_errors2(lhs_res, rhs_res)?;
            let lhs_typ = lhs_val.get_type();
            let rhs_typ = rhs_val.get_type();

            // First, type-check
            match op {
                // Equality and inequality work on any types
                parser::BinaryOp::Eq | parser::BinaryOp::Neq => {
                    if !types.unify(lhs_typ, rhs_typ) {
                        return Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(rhs_span, lexer),
                            unparse_type(types, lhs_typ, stringtab),
                            unparse_type(types, rhs_typ, stringtab),
                        )));
                    }
                }
                // These work on any numbers
                parser::BinaryOp::Add
                | parser::BinaryOp::Sub
                | parser::BinaryOp::Mul
                | parser::BinaryOp::Div
                | parser::BinaryOp::Lt
                | parser::BinaryOp::Le
                | parser::BinaryOp::Gt
                | parser::BinaryOp::Ge => {
                    let mut errors = LinkedList::new();
                    let lhs_number = types.unify_kind(lhs_typ, parser::Kind::Number);
                    let rhs_number = types.unify_kind(rhs_typ, parser::Kind::Number);
                    let equal = types.unify(lhs_typ, rhs_typ);

                    if lhs_number && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(rhs_span, lexer),
                            unparse_type(types, lhs_typ, stringtab),
                            unparse_type(types, rhs_typ, stringtab),
                        ));
                    } else if rhs_number && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(lhs_span, lexer),
                            unparse_type(types, rhs_typ, stringtab),
                            unparse_type(types, lhs_typ, stringtab),
                        ));
                    } else {
                        // The types are equal or both are not numbers
                        if !lhs_number {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(lhs_span, lexer),
                                "number".to_string(),
                                unparse_type(types, lhs_typ, stringtab),
                            ));
                        }
                        if !rhs_number {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(rhs_span, lexer),
                                "number".to_string(),
                                unparse_type(types, rhs_typ, stringtab),
                            ));
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }
                }
                // These work on integer inputs
                parser::BinaryOp::Mod
                | parser::BinaryOp::BitAnd
                | parser::BinaryOp::BitOr
                | parser::BinaryOp::Xor
                | parser::BinaryOp::LShift
                | parser::BinaryOp::RShift => {
                    let mut errors = LinkedList::new();
                    let lhs_integer = types.unify_kind(lhs_typ, parser::Kind::Integer);
                    let rhs_integer = types.unify_kind(rhs_typ, parser::Kind::Integer);
                    let equal = types.unify(lhs_typ, rhs_typ);

                    if lhs_integer && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(rhs_span, lexer),
                            unparse_type(types, lhs_typ, stringtab),
                            unparse_type(types, rhs_typ, stringtab),
                        ));
                    } else if rhs_integer && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(lhs_span, lexer),
                            unparse_type(types, rhs_typ, stringtab),
                            unparse_type(types, lhs_typ, stringtab),
                        ));
                    } else {
                        // The types are equal or both are not integers
                        if !lhs_integer {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(lhs_span, lexer),
                                "integer".to_string(),
                                unparse_type(types, lhs_typ, stringtab),
                            ));
                        }
                        if !rhs_integer {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(rhs_span, lexer),
                                "integer".to_string(),
                                unparse_type(types, rhs_typ, stringtab),
                            ));
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }
                }
                // These work on boolean inputs
                parser::BinaryOp::LogAnd | parser::BinaryOp::LogOr => {
                    let mut errors = LinkedList::new();
                    let lhs_bool = types.unify_bool(lhs_typ);
                    let rhs_bool = types.unify_bool(rhs_typ);
                    let equal = types.unify(lhs_typ, rhs_typ);

                    if lhs_bool && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(rhs_span, lexer),
                            unparse_type(types, lhs_typ, stringtab),
                            unparse_type(types, rhs_typ, stringtab),
                        ));
                    } else if rhs_bool && !equal {
                        errors.push_back(ErrorMessage::TypeError(
                            span_to_loc(lhs_span, lexer),
                            unparse_type(types, rhs_typ, stringtab),
                            unparse_type(types, lhs_typ, stringtab),
                        ));
                    } else {
                        // The types are equal or both are not bools
                        if !lhs_bool {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(lhs_span, lexer),
                                "bool".to_string(),
                                unparse_type(types, lhs_typ, stringtab),
                            ));
                        }
                        if !rhs_bool {
                            errors.push_back(ErrorMessage::TypeError(
                                span_to_loc(rhs_span, lexer),
                                "bool".to_string(),
                                unparse_type(types, rhs_typ, stringtab),
                            ));
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }
                }
            };

            match op {
                // The binary operations are compiled into conditional expressions:
                // x && y = if x then y    else false
                // x || y = if x then true else y
                parser::BinaryOp::LogAnd => {
                    let false_val = Expr::Constant {
                        val: (Literal::Bool(false), lhs_typ),
                        typ: lhs_typ,
                    };
                    Ok(Expr::CondExpr {
                        cond: Box::new(lhs_val),
                        thn: Box::new(rhs_val),
                        els: Box::new(false_val),
                        typ: lhs_typ,
                    })
                }
                parser::BinaryOp::LogOr => {
                    let true_val = Expr::Constant {
                        val: (Literal::Bool(true), lhs_typ),
                        typ: lhs_typ,
                    };
                    Ok(Expr::CondExpr {
                        cond: Box::new(lhs_val),
                        thn: Box::new(true_val),
                        els: Box::new(rhs_val),
                        typ: lhs_typ,
                    })
                }
                // For comparison operators, the resulting type is a boolean, while for all other
                // operations the result is the same as the two operands
                parser::BinaryOp::Lt
                | parser::BinaryOp::Le
                | parser::BinaryOp::Gt
                | parser::BinaryOp::Ge
                | parser::BinaryOp::Eq
                | parser::BinaryOp::Neq => Ok(Expr::BinaryExp {
                    op: convert_binary_op(op),
                    lhs: Box::new(lhs_val),
                    rhs: Box::new(rhs_val),
                    typ: types.new_primitive(types::Primitive::Bool),
                }),
                _ => Ok(Expr::BinaryExp {
                    op: convert_binary_op(op),
                    lhs: Box::new(lhs_val),
                    rhs: Box::new(rhs_val),
                    typ: lhs_typ,
                }),
            }
        }
        parser::Expr::CastExpr { span, expr, typ } => {
            // For the moment at least, casting is only supported between numeric types, and all
            // numeric types can be cast to each other
            let expr_res = process_expr(*expr, num_dyn_const, lexer, stringtab, env, types);
            // Inferring the type of a cast seems weird, so not allowing
            let type_res = process_type(typ, num_dyn_const, lexer, stringtab, env, types, false);

            let (expr_val, to_typ) = append_errors2(expr_res, type_res)?;
            let expr_typ = expr_val.get_type();

            if !types.unify_kind(expr_typ, parser::Kind::Number)
                || !types.unify_kind(to_typ, parser::Kind::Number)
            {
                Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Can only cast between numeric types, cannot cast {} to {}",
                        unparse_type(types, expr_typ, stringtab),
                        unparse_type(types, to_typ, stringtab)
                    ),
                )))
            } else {
                Ok(Expr::CastExpr {
                    expr: Box::new(expr_val),
                    typ: to_typ,
                })
            }
        }
        parser::Expr::CondExpr {
            span,
            cond,
            thn,
            els,
        } => {
            let cond_span = cond.span();

            let cond_res = process_expr(*cond, num_dyn_const, lexer, stringtab, env, types);
            let thn_res = process_expr(*thn, num_dyn_const, lexer, stringtab, env, types);
            let els_res = process_expr(*els, num_dyn_const, lexer, stringtab, env, types);

            let (cond_val, thn_val, els_val) = append_errors3(cond_res, thn_res, els_res)?;

            let cond_typ = cond_val.get_type();
            let thn_typ = thn_val.get_type();
            let els_typ = els_val.get_type();

            let mut errors = LinkedList::new();

            if !types.unify_bool(cond_typ) {
                errors.push_back(ErrorMessage::TypeError(
                    span_to_loc(cond_span, lexer),
                    "bool".to_string(),
                    unparse_type(types, cond_typ, stringtab),
                ));
            }
            if !types.unify(thn_typ, els_typ) {
                errors.push_back(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Types of conditional branches do not match, have {} and {}",
                        unparse_type(types, thn_typ, stringtab),
                        unparse_type(types, els_typ, stringtab)
                    ),
                ));
            }

            if !errors.is_empty() {
                Err(errors)
            } else {
                Ok(Expr::CondExpr {
                    cond: Box::new(cond_val),
                    thn: Box::new(thn_val),
                    els: Box::new(els_val),
                    typ: thn_typ,
                })
            }
        }
        parser::Expr::CallExpr {
            span,
            name,
            ty_args,
            args,
        } => {
            // In the AST from the parser we have no way to distinguish between function calls and
            // union construction. We have to identify which case we're in here. We do this by
            // identifying whether the name (looking for the moment at just the first part of the
            // name) and determining whether it's a type or a function. Obviously we then report
            // errors if there are additional parts of the name
            if name.len() > 2 {
                Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )))?
            }

            let nm = intern_package_name(&name, lexer, stringtab);

            match env.lookup(&nm[0]) {
                Some(Entity::Variable { .. })
                | Some(Entity::DynConst { .. })
                | Some(Entity::Constant { .. })
                | Some(Entity::Function { .. })
                | None
                    if name.len() != 1 =>
                {
                    Err(singleton_error(ErrorMessage::NotImplemented(
                        span_to_loc(span, lexer),
                        "packages".to_string(),
                    )))
                }
                None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                    span_to_loc(name[0], lexer),
                    stringtab.lookup_id(nm[0]).unwrap(),
                ))),
                Some(Entity::Variable { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(name[0], lexer),
                    format!(
                        "{} is a variable, expected a function or union constructor",
                        stringtab.lookup_id(nm[0]).unwrap()
                    ),
                ))),
                Some(Entity::DynConst { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(name[0], lexer),
                    format!(
                        "{} is a dynamic constant, expected a function or union constructor",
                        stringtab.lookup_id(nm[0]).unwrap()
                    ),
                ))),
                Some(Entity::Constant { .. }) => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(name[0], lexer),
                    format!(
                        "{} is a constant, expected a function or union constructor",
                        stringtab.lookup_id(nm[0]).unwrap()
                    ),
                ))),
                Some(Entity::Type {
                    type_args: kinds,
                    value: typ,
                }) => {
                    if !types.is_union(*typ) {
                        if name.len() != 1 {
                            Err(singleton_error(ErrorMessage::NotImplemented(
                                span_to_loc(span, lexer),
                                "packages".to_string(),
                            )))?
                        } else {
                            Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(name[0], lexer),
                                format!(
                                    "{} is a type, expected a function or union constructor",
                                    stringtab.lookup_id(nm[0]).unwrap()
                                ),
                            )))?
                        }
                    }
                    if name.len() != 2 {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(name[0], lexer),
                            format!("Expected constructor name"),
                        )))?
                    }

                    if types.get_constructor_info(*typ, nm[1]).is_none() {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(name[1], lexer),
                            format!(
                                "{} is not a constructor of type {}",
                                stringtab.lookup_id(nm[1]).unwrap(),
                                unparse_type(types, *typ, stringtab)
                            ),
                        )))?
                    }

                    // Now, we know that we are constructing some union, we need to verify that
                    // the type arguments are appropriate
                    let ty_args = ty_args.unwrap_or_else(|| {
                        vec![parser::TypeExpr::WildcardType { span: span }; kinds.len()]
                    });

                    if kinds.len() != ty_args.len() {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!(
                                "Expected {} type arguments, provided {}",
                                kinds.len(),
                                ty_args.len()
                            ),
                        )))?
                    }

                    let mut type_vars = vec![];
                    let mut dyn_consts = vec![];
                    let mut errors = LinkedList::new();

                    for (arg, kind) in ty_args.into_iter().zip(kinds.iter()) {
                        let arg_span = arg.span();
                        match kind {
                            parser::Kind::USize => {
                                match process_type_expr_as_expr(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(val) => dyn_consts.push(val),
                                }
                            }
                            _ => {
                                match process_type_expr_as_type(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                    true,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(typ) => {
                                        if types.unify_kind(typ, *kind) {
                                            type_vars.push(typ);
                                        } else {
                                            errors.push_back(ErrorMessage::KindError(
                                                span_to_loc(arg_span, lexer),
                                                kind.to_string(),
                                                unparse_type(types, typ, stringtab),
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }

                    let union_type = if type_vars.len() == 0 && dyn_consts.len() == 0 {
                        *typ
                    } else {
                        if let Some(res) = types.instantiate(*typ, &type_vars, &dyn_consts) {
                            res
                        } else {
                            return Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(span, lexer),
                                "Failure in variable substitution".to_string(),
                            )));
                        }
                    };
                    let Some((constr_idx, constr_typ)) =
                        types.get_constructor_info(union_type, nm[1])
                    else {
                        panic!("From above");
                    };

                    // Now, process the arguments to ensure they has the type needed by this
                    // constructor
                    // To do this, since unions take a single argument, we process the arguments as
                    // a single tuple, reporting an error if inout is used anywhere
                    for (is_inout, arg) in args.iter() {
                        if *is_inout {
                            Err(singleton_error(ErrorMessage::SemanticError(
                                span_to_loc(arg.span(), lexer),
                                format!("Union constructors cannot be marked inout"),
                            )))?
                        }
                    }

                    let body = process_expr(
                        parser::Expr::Tuple {
                            span: span,
                            exprs: args.into_iter().map(|(_, a)| a).collect::<Vec<_>>(),
                        },
                        num_dyn_const,
                        lexer,
                        stringtab,
                        env,
                        types,
                    )?;
                    let body_typ = body.get_type();

                    if !types.unify(constr_typ, body_typ) {
                        Err(singleton_error(ErrorMessage::TypeError(
                            span_to_loc(span, lexer),
                            unparse_type(types, constr_typ, stringtab),
                            unparse_type(types, body_typ, stringtab),
                        )))
                    } else {
                        Ok(Expr::Union {
                            tag: constr_idx,
                            val: Box::new(body),
                            typ: union_type,
                        })
                    }
                }
                Some(Entity::Function {
                    index: function,
                    type_args: kinds,
                    args: func_args,
                    return_types,
                }) => {
                    let func = *function;

                    // Verify that the type arguments are appropriate
                    let ty_args = ty_args.unwrap_or_else(|| {
                        vec![parser::TypeExpr::WildcardType { span: span }; kinds.len()]
                    });

                    if kinds.len() != ty_args.len() {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!(
                                "Expected {} type arguments, provided {}",
                                kinds.len(),
                                ty_args.len()
                            ),
                        )))?
                    }

                    let mut type_vars = vec![];
                    let mut dyn_consts = vec![];
                    let mut errors = LinkedList::new();

                    for (arg, kind) in ty_args.into_iter().zip(kinds.iter()) {
                        let arg_span = arg.span();
                        match kind {
                            parser::Kind::USize => {
                                match process_type_expr_as_expr(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(val) => dyn_consts.push(val),
                                }
                            }
                            _ => {
                                match process_type_expr_as_type(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                    true,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(typ) => {
                                        if types.unify_kind(typ, *kind) {
                                            type_vars.push(typ);
                                        } else {
                                            errors.push_back(ErrorMessage::KindError(
                                                span_to_loc(arg_span, lexer),
                                                kind.to_string(),
                                                unparse_type(types, typ, stringtab),
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }

                    let arg_types = if type_vars.len() == 0 && dyn_consts.len() == 0 {
                        func_args.clone()
                    } else {
                        let mut tys = vec![];
                        for (t, inout) in func_args {
                            tys.push((
                                if let Some(res) = types.instantiate(*t, &type_vars, &dyn_consts) {
                                    res
                                } else {
                                    return Err(singleton_error(ErrorMessage::SemanticError(
                                        span_to_loc(span, lexer),
                                        "Failure in variable substitution".to_string(),
                                    )));
                                },
                                *inout,
                            ));
                        }
                        tys
                    };
                    let return_types = return_types
                        .iter()
                        .map(|t| types.instantiate(*t, &type_vars, &dyn_consts))
                        .collect::<Option<Vec<_>>>();
                    let Some(return_types) = return_types else {
                        return Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            "Failure in variable substitution".to_string(),
                        )));
                    };

                    // Now, process the arguments to ensure they has the type needed by this
                    // function
                    let mut arg_vals: Vec<Either<Expr, usize>> = vec![];
                    let mut errors = LinkedList::new();

                    for ((is_inout, arg), (arg_typ, expect_inout)) in
                        args.into_iter().zip(arg_types.into_iter())
                    {
                        let arg_span = arg.span();

                        if is_inout && !expect_inout {
                            errors.push_back(ErrorMessage::SemanticError(
                                span_to_loc(arg_span, lexer),
                                format!("Argument should be inout"),
                            ));
                        } else if !is_inout && expect_inout {
                            errors.push_back(ErrorMessage::SemanticError(
                                span_to_loc(arg_span, lexer),
                                format!("Argument should not be inout"),
                            ));
                        } else if is_inout {
                            // If the argument is an inout then it needs to just be a variable
                            match process_expr(arg, num_dyn_const, lexer, stringtab, env, types) {
                                Err(mut errs) => errors.append(&mut errs),
                                Ok(Expr::Variable { var, typ }) => {
                                    if !types.unify(arg_typ, typ) {
                                        errors.push_back(ErrorMessage::TypeError(
                                            span_to_loc(arg_span, lexer),
                                            unparse_type(types, arg_typ, stringtab),
                                            unparse_type(types, typ, stringtab),
                                        ));
                                    } else {
                                        arg_vals.push(Either::Right(var));
                                    }
                                }
                                Ok(_) => {
                                    errors.push_back(ErrorMessage::SemanticError(
                                        span_to_loc(arg_span, lexer),
                                        format!("An inout argument must just be a variable"),
                                    ));
                                }
                            }
                        } else {
                            match process_expr(arg, num_dyn_const, lexer, stringtab, env, types) {
                                Err(mut errs) => errors.append(&mut errs),
                                Ok(exp) => {
                                    if !types.unify(arg_typ, exp.get_type()) {
                                        errors.push_back(ErrorMessage::TypeError(
                                            span_to_loc(arg_span, lexer),
                                            unparse_type(types, arg_typ, stringtab),
                                            unparse_type(types, exp.get_type(), stringtab),
                                        ));
                                    } else {
                                        arg_vals.push(Either::Left(exp));
                                    }
                                }
                            }
                        }
                    }

                    if !errors.is_empty() {
                        Err(errors)
                    } else {
                        let single_type = if return_types.len() == 1 {
                            Some(return_types[0])
                        } else {
                            None
                        };
                        let num_returns = return_types.len();
                        let call = Expr::CallExpr {
                            func,
                            ty_args: type_vars,
                            dyn_consts,
                            args: arg_vals,
                            num_returns,
                            typ: types.new_multi_return(return_types),
                        };
                        if let Some(return_type) = single_type {
                            Ok(Expr::CallExtract {
                                call: Box::new(call),
                                index: 0,
                                typ: return_type,
                            })
                        } else {
                            Ok(call)
                        }
                    }
                }
            }
        }
        parser::Expr::IntrinsicExpr {
            span,
            name,
            ty_args,
            args,
        } => {
            if name.len() != 1 {
                Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )))?
            }

            let nm = lexer.span_str(name[0]);
            match intrinsics::lookup(nm) {
                None => Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!("Undefined intrinsic {}", nm),
                ))),
                Some(intrinsic) => {
                    let kinds = intrinsic.kinds;

                    let ty_args = ty_args.unwrap_or_else(|| {
                        vec![parser::TypeExpr::WildcardType { span: span }; kinds.len()]
                    });

                    if ty_args.len() != kinds.len() {
                        Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!(
                                "Expected {} type arguments, provided {}",
                                kinds.len(),
                                ty_args.len()
                            ),
                        )))?
                    }

                    let mut type_vars = vec![];
                    let mut errors = LinkedList::new();

                    for (arg, kind) in ty_args.into_iter().zip(kinds.iter()) {
                        let arg_span = arg.span();
                        match kind {
                            parser::Kind::USize => {
                                panic!("Intrinsics do not support dynamic constants");
                            }
                            _ => {
                                match process_type_expr_as_type(
                                    arg,
                                    num_dyn_const,
                                    lexer,
                                    stringtab,
                                    env,
                                    types,
                                    true,
                                ) {
                                    Err(mut errs) => errors.append(&mut errs),
                                    Ok(typ) => {
                                        if types.unify_kind(typ, *kind) {
                                            type_vars.push(typ);
                                        } else {
                                            errors.push_back(ErrorMessage::KindError(
                                                span_to_loc(arg_span, lexer),
                                                kind.to_string(),
                                                unparse_type(types, typ, stringtab),
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if !errors.is_empty() {
                        return Err(errors);
                    }

                    let (arg_types, return_typ) = (intrinsic.typ)(&type_vars, types);

                    // Now, process the arguments to ensure they has the type needed by this
                    // intrinsic
                    let mut arg_vals: Vec<Expr> = vec![];
                    let mut errors = LinkedList::new();

                    for ((is_inout, arg), arg_typ) in args.into_iter().zip(arg_types.into_iter()) {
                        let arg_span = arg.span();

                        if is_inout {
                            errors.push_back(ErrorMessage::SemanticError(
                                span_to_loc(arg_span, lexer),
                                format!("Arguments to intrinsics cannot be inout"),
                            ));
                        } else {
                            match process_expr(arg, num_dyn_const, lexer, stringtab, env, types) {
                                Err(mut errs) => errors.append(&mut errs),
                                Ok(exp) => {
                                    if !types.unify(arg_typ, exp.get_type()) {
                                        errors.push_back(ErrorMessage::TypeError(
                                            span_to_loc(arg_span, lexer),
                                            unparse_type(types, arg_typ, stringtab),
                                            unparse_type(types, exp.get_type(), stringtab),
                                        ));
                                    } else {
                                        arg_vals.push(exp);
                                    }
                                }
                            }
                        }
                    }

                    if !errors.is_empty() {
                        Err(errors)
                    } else {
                        Ok(Expr::Intrinsic {
                            id: intrinsic.id,
                            ty_args: type_vars,
                            args: arg_vals,
                            typ: return_typ,
                        })
                    }
                }
            }
        }
    }
}

fn generate_return(mut exprs: Vec<Expr>, inouts: &[Expr]) -> Stmt {
    exprs.extend_from_slice(inouts);
    Stmt::ReturnStmt { exprs: exprs }
}

fn convert_primitive(prim: parser::Primitive) -> types::Primitive {
    match prim {
        parser::Primitive::Bool => types::Primitive::Bool,
        parser::Primitive::I8 => types::Primitive::I8,
        parser::Primitive::U8 => types::Primitive::U8,
        parser::Primitive::I16 => types::Primitive::I16,
        parser::Primitive::U16 => types::Primitive::U16,
        parser::Primitive::I32 => types::Primitive::I32,
        parser::Primitive::U32 => types::Primitive::U32,
        parser::Primitive::I64 => types::Primitive::I64,
        parser::Primitive::U64 => types::Primitive::U64,
        parser::Primitive::USize => types::Primitive::U64,
        parser::Primitive::FP8 => types::Primitive::FP8,
        parser::Primitive::BF16 => types::Primitive::BF16,
        parser::Primitive::F32 => types::Primitive::F32,
        parser::Primitive::F64 => types::Primitive::F64,
        parser::Primitive::Void => types::Primitive::Unit,
    }
}

// Processes an irrefutable pattern by extracting the pieces from the given variable which has the
// given type. Adds any variables in that pattern to the environment and returns a list of
// statements that handle the pattern
// If the return_expr variable is set to true, it also returns an expression that reconstructs the
// value of the variable from the fields created by the pattern, this is used for inout variables
// where a pattern in its binding creates variables whose final values need to be returned
fn process_irrefutable_pattern(
    pat: parser::Pattern,
    is_const: bool,
    var: usize,
    typ: Type,
    lexer: &dyn NonStreamingLexer<DefaultLexerTypes<u32>>,
    stringtab: &mut StringTable,
    env: &mut Env<usize, Entity>,
    types: &mut TypeSolver,
    return_expr: bool,
) -> Result<(Vec<Stmt>, Option<Expr>), ErrorMessages> {
    match pat {
        Pattern::Wildcard { .. } => Ok((
            vec![],
            if return_expr {
                Some(Expr::Variable { var, typ })
            } else {
                None
            },
        )),
        Pattern::Variable { span, name } => {
            if name.len() != 1 {
                return Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    "Bound variables must be local names, without a package separator".to_string(),
                )));
            }
            assert!(types.get_return_types(typ).is_none());

            let nm = intern_package_name(&name, lexer, stringtab)[0];
            let variable = env.uniq();
            env.insert(
                nm,
                Entity::Variable {
                    variable,
                    typ,
                    is_const,
                },
            );

            Ok((
                vec![Stmt::AssignStmt {
                    var: variable,
                    val: Expr::Variable { var, typ },
                }],
                if return_expr {
                    Some(Expr::Variable { var: variable, typ })
                } else {
                    None
                },
            ))
        }
        Pattern::TuplePattern { span, pats } => {
            let Some(fields) = types.get_fields(typ) else {
                return Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Type {} is not a tuple",
                        unparse_type(types, typ, stringtab),
                    ),
                )));
            };
            let fields = fields.clone();

            if fields.len() != pats.len() {
                return Err(singleton_error(ErrorMessage::SemanticError(
                    span_to_loc(span, lexer),
                    format!(
                        "Expected {} fields, pattern has {}",
                        fields.len(),
                        pats.len()
                    ),
                )));
            }

            let mut res = vec![];
            let mut exprs = vec![];
            let mut errors = LinkedList::new();

            for (idx, (pat, field)) in pats.into_iter().zip(fields.into_iter()).enumerate() {
                // Extract this field from the current value
                let variable = env.uniq();
                res.push(Stmt::AssignStmt {
                    var: variable,
                    val: Expr::Read {
                        index: vec![Index::Field(idx)],
                        val: Box::new(Expr::Variable { var, typ }),
                        typ: field,
                    },
                });

                match process_irrefutable_pattern(
                    pat,
                    is_const,
                    variable,
                    field,
                    lexer,
                    stringtab,
                    env,
                    types,
                    return_expr,
                ) {
                    Ok((stmts, expr)) => {
                        res.extend(stmts);
                        if return_expr {
                            exprs.push(expr.unwrap());
                        }
                    }
                    Err(errs) => errors.extend(errs),
                }
            }

            if errors.is_empty() {
                Ok((
                    res,
                    if return_expr {
                        Some(Expr::Tuple { vals: exprs, typ })
                    } else {
                        None
                    },
                ))
            } else {
                Err(errors)
            }
        }
        Pattern::StructPattern {
            span,
            name,
            pats,
            ignore_other,
        } => {
            if name.len() != 1 {
                return Err(singleton_error(ErrorMessage::NotImplemented(
                    span_to_loc(span, lexer),
                    "packages".to_string(),
                )));
            }

            let struct_nm = intern_package_name(&name, lexer, stringtab)[0];
            match env.lookup(&struct_nm) {
                Some(Entity::Variable { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "variable".to_string(),
                ))),
                Some(Entity::DynConst { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "dynamic constant".to_string(),
                ))),
                Some(Entity::Constant { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "constant".to_string(),
                ))),
                Some(Entity::Function { .. }) => Err(singleton_error(ErrorMessage::KindError(
                    span_to_loc(span, lexer),
                    "struct name".to_string(),
                    "function".to_string(),
                ))),
                None => Err(singleton_error(ErrorMessage::UndefinedVariable(
                    span_to_loc(span, lexer),
                    stringtab.lookup_id(struct_nm).unwrap(),
                ))),
                Some(Entity::Type {
                    type_args: _,
                    value: struct_typ,
                }) => {
                    let struct_typ = *struct_typ;

                    if !types.is_struct(struct_typ) {
                        return Err(singleton_error(ErrorMessage::KindError(
                            span_to_loc(span, lexer),
                            "struct name".to_string(),
                            "non-struct type".to_string(),
                        )));
                    }

                    if !types.unify(typ, struct_typ) {
                        return Err(singleton_error(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!(
                                "Expected a pattern for type {} but found pattern for type {}",
                                unparse_type(types, typ, stringtab),
                                unparse_type(types, struct_typ, stringtab)
                            ),
                        )));
                    }

                    // Fields that have already been used
                    let mut unused_fields = types
                        .get_field_names(struct_typ)
                        .unwrap()
                        .into_iter()
                        .collect::<HashSet<_>>();
                    let mut res = vec![];
                    let mut exprs = vec![None; unused_fields.len()];
                    let mut errors = LinkedList::new();

                    for (field_name, pat) in pats {
                        let field_nm = intern_id(&field_name, lexer, stringtab);
                        match types.get_field(struct_typ, field_nm) {
                            None => {
                                errors.push_back(ErrorMessage::SemanticError(
                                    span_to_loc(field_name, lexer),
                                    format!(
                                        "Struct {} does not have field {}",
                                        unparse_type(types, struct_typ, stringtab),
                                        stringtab.lookup_id(field_nm).unwrap()
                                    ),
                                ));
                            }
                            Some((idx, field_typ)) => {
                                if !unused_fields.contains(&field_nm) {
                                    errors.push_back(ErrorMessage::SemanticError(
                                        span_to_loc(field_name, lexer),
                                        format!(
                                            "Field {} appears multiple times in pattern",
                                            stringtab.lookup_id(field_nm).unwrap()
                                        ),
                                    ));
                                } else {
                                    unused_fields.remove(&field_nm);
                                    let variable = env.uniq();
                                    res.push(Stmt::AssignStmt {
                                        var: variable,
                                        val: Expr::Read {
                                            index: vec![Index::Field(idx)],
                                            val: Box::new(Expr::Variable { var, typ }),
                                            typ: field_typ,
                                        },
                                    });
                                    match process_irrefutable_pattern(
                                        pat,
                                        is_const,
                                        variable,
                                        field_typ,
                                        lexer,
                                        stringtab,
                                        env,
                                        types,
                                        return_expr,
                                    ) {
                                        Ok((stmts, expr)) => {
                                            res.extend(stmts);
                                            if return_expr {
                                                exprs[idx] = expr;
                                            }
                                        }
                                        Err(errs) => errors.extend(errs),
                                    }
                                }
                            }
                        }
                    }

                    if !unused_fields.is_empty() && !ignore_other {
                        errors.push_back(ErrorMessage::SemanticError(
                            span_to_loc(span, lexer),
                            format!(
                                "Pattern is missing fields: {}",
                                unused_fields
                                    .into_iter()
                                    .map(|i| stringtab.lookup_id(i).unwrap())
                                    .collect::<Vec<_>>()
                                    .join(", ")
                            ),
                        ));
                    } else if return_expr {
                        for field in unused_fields {
                            let (idx, field_typ) = types.get_field(struct_typ, field).unwrap();
                            let variable = env.uniq();
                            res.push(Stmt::AssignStmt {
                                var: variable,
                                val: Expr::Read {
                                    index: vec![Index::Field(idx)],
                                    val: Box::new(Expr::Variable { var, typ }),
                                    typ: field_typ,
                                },
                            });
                            exprs[idx] = Some(Expr::Variable {
                                var: variable,
                                typ: field_typ,
                            });
                        }
                    }

                    if !errors.is_empty() {
                        Err(errors)
                    } else {
                        Ok((
                            res,
                            if return_expr {
                                Some(Expr::Tuple {
                                    vals: exprs.into_iter().map(|v| v.unwrap()).collect(),
                                    typ,
                                })
                            } else {
                                None
                            },
                        ))
                    }
                }
            }
        }
        Pattern::IntLit { span, .. } | Pattern::UnionPattern { span, .. } => {
            Err(singleton_error(ErrorMessage::SemanticError(
                span_to_loc(span, lexer),
                "Expected an irrefutable pattern, but pattern is refutable".to_string(),
            )))
        }
    }
}
