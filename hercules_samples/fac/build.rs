use juno_build::JunoCompiler;

fn main() {
    #[cfg(not(feature = "cuda"))]
    {
        JunoCompiler::new()
            .ir_in_src("fac.hir")
            .unwrap()
            .build()
            .unwrap();
    }
    #[cfg(feature = "cuda")]
    {
        JunoCompiler::new()
            .ir_in_src("fac.hir")
            .unwrap()
            .schedule_in_src("gpu.sch")
            .unwrap()
            .build()
            .unwrap();
    }
}
