#![feature(concat_idents)]

use hercules_rt::runner;

juno_build::juno!("fac");

fn main() {
    async_std::task::block_on(async {
        let mut r = runner!(fac);
        let f = r.run(8).await;
        println!("{}", f);
        assert_eq!(f, 40320);
    });
}

#[test]
fn fac_test() {
    main();
}
