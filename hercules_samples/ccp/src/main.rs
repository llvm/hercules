#![feature(concat_idents)]

use hercules_rt::runner;

juno_build::juno!("ccp");

fn main() {
    async_std::task::block_on(async {
        let mut r = runner!(tricky);
        let x = r.run(7).await;
        assert_eq!(x, 1);
    });
}

#[test]
fn ccp_test() {
    main();
}
