#![feature(concat_idents)]

use hercules_rt::runner;

juno_build::juno!("call");

fn main() {
    async_std::task::block_on(async {
        let mut r = runner!(myfunc);
        let x = r.run(7).await;
        assert_eq!(x, 71);
        let mut r = runner!(add);
        let y = r.run(10, 2, 18).await;
        assert_eq!(y, 30);
    });
}

#[test]
fn call_test() {
    main();
}
