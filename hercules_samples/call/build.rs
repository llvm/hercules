use juno_build::JunoCompiler;

fn main() {
    JunoCompiler::new()
        .ir_in_src("call.hir")
        .unwrap()
        .schedule_in_src("sched.sch")
        .unwrap()
        .build()
        .unwrap();
}
