use juno_build::JunoCompiler;

fn main() {
    JunoCompiler::new()
        .ir_in_src("matmul.hir")
        .unwrap()
        .schedule_in_src(if cfg!(feature = "cuda") {
            "gpu.sch"
        } else {
            "cpu.sch"
        })
        .unwrap()
        .build()
        .unwrap();
}
