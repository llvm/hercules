gvn(*);
phi-elim(*);
dce(*);

auto-outline(*);

ip-sroa(*);
sroa(*);
dce(*);
fixpoint {
  infer-schedules(*);
}
fork-split(*);
unforkify(*);
dce(*);
gvn(*);
phi-elim(*);
dce(*);

gcm(*);
