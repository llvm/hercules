no-memset(matmul@c);

gvn(*);
phi-elim(*);
dce(*);

let out = auto-outline(*);
gpu(out.matmul);

ip-sroa(*);
sroa(*);
dce(*);
gvn(*);
phi-elim(*);
dce(*);

forkify(*);
fixpoint {
  infer-schedules(*);
}

gcm(*);
