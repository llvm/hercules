#![feature(concat_idents)]

#[cfg(feature = "cuda")]
use hercules_rt::CUDABox;
use hercules_rt::{runner, HerculesImmBox, HerculesImmBoxTo};

juno_build::juno!("dot");

fn main() {
    async_std::task::block_on(async {
        let a: Box<[f32; 8]> = Box::new([0.0, 1.0, 0.0, 2.0, 0.0, 3.0, 0.0, 4.0]);
        let b: Box<[f32; 8]> = Box::new([0.0, 5.0, 0.0, 6.0, 0.0, 7.0, 0.0, 8.0]);
        let a = HerculesImmBox::from(a.as_ref() as &[f32]);
        let b = HerculesImmBox::from(b.as_ref() as &[f32]);
        let mut r = runner!(dot);
        let c = r.run(8, a.to(), b.to()).await;
        println!("{}", c);
        assert_eq!(c, 70.0);
    });
}

#[test]
fn dot_test() {
    main();
}
