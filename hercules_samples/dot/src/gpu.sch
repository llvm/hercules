gvn(*);
phi-elim(*);
dce(*);

let out = auto-outline(*);
gpu(out.dot);

ip-sroa(*);
sroa(*);
dce(*);
gvn(*);
phi-elim(*);
dce(*);

infer-schedules(*);

gcm(*);
