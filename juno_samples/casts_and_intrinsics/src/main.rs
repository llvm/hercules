#![feature(concat_idents)]

use hercules_rt::runner;

juno_build::juno!("casts_and_intrinsics");

fn main() {
    async_std::task::block_on(async {
        let mut r = runner!(casts_and_intrinsics);
        let output = r.run(15.7).await;
        println!("{}", output);
        assert_eq!(output, 4);
    });
}

#[test]
fn casts_and_intrinsics_test() {
    main();
}
