gvn(*);
phi-elim(*);
dce(*);

let out = auto-outline(*);
gpu(out.casts_and_intrinsics);

ip-sroa(*);
sroa(*);
dce(*);
gvn(*);
phi-elim(*);
dce(*);

infer-schedules(*);

gcm(*);
