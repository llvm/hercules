#![feature(concat_idents)]

use hercules_rt::{runner, HerculesImmBox, HerculesImmBoxTo};

juno_build::juno!("fork_join_tests");

fn main() {
    #[cfg(not(feature = "cuda"))]
    let assert = |correct: &Vec<i32>, output: hercules_rt::HerculesCPURefMut<'_>| {
        assert_eq!(output.as_slice::<i32>(), correct);
    };

    #[cfg(feature = "cuda")]
    let assert = |correct: &Vec<i32>, output: hercules_rt::HerculesCUDARefMut<'_>| {
        let mut dst = vec![0i32; correct.len()];
        let output = output.to_cpu_ref(&mut dst);
        assert_eq!(output.as_slice::<i32>(), correct);
    };

    async_std::task::block_on(async {
        let mut r = runner!(test1);
        let output = r.run(5).await;
        let correct = vec![5i32; 16];
        assert(&correct, output);

        let mut r = runner!(test2);
        let output = r.run(3).await;
        let correct = vec![24i32; 20];
        assert(&correct, output);

        let mut r = runner!(test3);
        let output = r.run(0).await;
        let correct = vec![11, 10, 9, 10, 9, 8, 9, 8, 7];
        assert(&correct, output);

        let mut r = runner!(test4);
        let output = r.run(9).await;
        let correct = vec![63i32; 16];
        assert(&correct, output);

        let mut r = runner!(test5);
        let output = r.run(4).await;
        let correct = vec![7i32; 4];
        assert(&correct, output);

        let mut r = runner!(test6);
        let output = r.run(73).await;
        let correct = (73i32..73i32 + 1024i32).collect();
        assert(&correct, output);

        let mut r = runner!(test7);
        let output = r.run(42).await;
        let correct: i32 = (42i32..42i32 + 32i32).sum();
        assert_eq!(correct, output);

        let mut r = runner!(test8);
        let output = r.run(0).await;
        let correct = vec![10, 17, 24, 31, 38, 45, 52, 59];
        assert(&correct, output);

        let mut r = runner!(test9);
        let input = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];
        let input = HerculesImmBox::from(&input as &[i32]);
        let output = r.run(3, 3, input.to()).await;
        let correct = vec![
            1 + 2 + 4 + 5,
            1 + 2 + 3 + 4 + 5 + 6,
            2 + 3 + 5 + 6,
            1 + 2 + 4 + 5 + 7 + 8,
            1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9,
            2 + 3 + 5 + 6 + 8 + 9,
            4 + 5 + 7 + 8,
            4 + 5 + 6 + 7 + 8 + 9,
            5 + 6 + 8 + 9,
        ];
        assert(&correct, output);

        let mut r = runner!(test10);
        let output = r.run(1, 2, 5).await;
        let correct = 17i32;
        assert_eq!(correct, output);

        let mut r = runner!(test11);
        let k1 = vec![0, 4, 3, 7, 3, 4, 2, 1];
        let k2 = vec![6, 4, 3, 2, 4, 1, 0, 5];
        let v = vec![3, -499, 4, 32, -2, 55, -74, 10];
        let mut correct = 0;
        for i in 0..8 {
            correct += v[k1[k2[i] as usize] as usize];
        }
        let k1 = HerculesImmBox::from(&k1 as &[i32]);
        let k2 = HerculesImmBox::from(&k2 as &[i32]);
        let v = HerculesImmBox::from(&v as &[i32]);
        let output = r.run(k1.to(), k2.to(), v.to()).await;
        assert_eq!(output, correct);
    });
}

#[test]
fn fork_join_test() {
    main();
}
