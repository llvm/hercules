ccp(*);
gvn(*);
phi-elim(*);
dce(*);

let auto = auto-outline(test1, test2, test3, test4, test5, test7, test8, test9, test10, test11);
cpu(auto.test1);
cpu(auto.test2);
cpu(auto.test3);
cpu(auto.test4);
cpu(auto.test5);
cpu(auto.test7);
cpu(auto.test8);
cpu(auto.test9);
cpu(auto.test10);
cpu(auto.test11);

let test1_cpu = auto.test1;
rename["test1_cpu"](test1_cpu);

ip-sroa(*);
sroa(*);
dce(*);
ccp(*);
gvn(*);
phi-elim(*);
dce(*);

lift-dc-math(*);
loop-bound-canon(*);

fixpoint panic after 20 {
  forkify(*);
  fork-guard-elim(*);
  fork-coalesce(*);
  dce(*);  
}

dce(*);
gvn(*);

gvn(*);
phi-elim(*);
dce(*);

fixpoint panic after 20 {
  infer-schedules(*);
}

let out = fork-split(test1_cpu);
let first_fork = out.test1_cpu.fj1;

fixpoint panic after 20 {
  unroll(auto.test1);
}

fork-split(auto.test2, auto.test3, auto.test4, auto.test5, auto.test9);
gvn(*);
phi-elim(*);
dce(*);
unforkify(auto.test2, auto.test3, auto.test4, auto.test5, auto.test9);
ccp(*);
gvn(*);
phi-elim(*);
dce(*);

async-call(test6@call);
no-memset(test6@const);
fork-tile[2, 0, false, false](test6@loop);
let out = fork-split(test6@loop);
let out = outline(out.test6.fj1);
cpu(out);
ip-sroa(*);
sroa(*);
unforkify(out);

dce(*);
ccp(*);
gvn(*);
phi-elim(*);
dce(*);

unforkify(auto.test7@loop2);
array-slf(auto.test7);
ccp(auto.test7);
dce(auto.test7);
simplify-cfg(auto.test7);
dce(auto.test7);

let fission = fork-fission-bufferize[test8@loop, test8@bufferize1](auto.test8);
dce(auto.test8);
unforkify(auto.test8);
dce(auto.test8);
ccp(auto.test8);
dce(auto.test8);
simplify-cfg(auto.test8);
dce(auto.test8);

fork-split(auto.test10);
let fission_out = fork-fission-reduces[test10@loop3](auto.test10);
fork-fusion(fission_out.test10_8.fj0, fission_out.test10_8.fj1);
dce(auto.test10);
simplify-cfg(auto.test10);
dce(auto.test10);
fork-split(auto.test10);
unforkify(auto.test10);

array-slf(auto.test11);
ccp(auto.test11);
dce(auto.test11);
simplify-cfg(auto.test11);
dce(auto.test11);
unforkify(auto.test11);

gcm(*);
