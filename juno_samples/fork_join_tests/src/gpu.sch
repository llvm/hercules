parallel-reduce(test4@reduce);
parallel-reduce(test5@reduce);
no-memset(test1@const);
no-memset(test3@const1);
no-memset(test3@const2);
no-memset(test3@const3);
no-memset(test6@const);
no-memset(test8@const1);
no-memset(test8@const2);
no-memset(test9@const);
no-memset(test10@const);
no-memset(test11@const);

gvn(*);
phi-elim(*);
dce(*);

let auto = auto-outline(test1, test2, test3, test4, test5, test7, test8, test9, test10, test11);
gpu(auto.test1);
gpu(auto.test2);
gpu(auto.test3);
gpu(auto.test4);
gpu(auto.test5);
gpu(auto.test7);
gpu(auto.test8);
gpu(auto.test9);
gpu(auto.test10);
gpu(auto.test11);

ip-sroa(*);
sroa(*);
dce(*);
gvn(*);
phi-elim(*);
dce(*);

fixpoint panic after 20 {
  forkify(*);
  fork-guard-elim(*);
}

fixpoint panic after 20 {
  fork-coalesce(auto.test1, auto.test3, auto.test4, auto.test5, auto.test7, auto.test8, auto.test9);
}

gvn(*);
phi-elim(*);
dce(*);

fixpoint panic after 20 {
  infer-schedules(*);
}

fork-coalesce(auto.test2@loop2);
dce(auto.test2);
reduce-slf(auto.test2);
slf(auto.test2);
infer-schedules(auto.test2);
fork-interchange[0, 1](auto.test2);

inline(test6);
fork-tile[32, 0, false, true](test6@loop);
let out = fork-split(test6@loop);
let out = auto-outline(test6);
gpu(out.test6);

array-slf(auto.test7);
ccp(auto.test7);
dce(auto.test7);
simplify-cfg(auto.test7);
dce(auto.test7);

let fission = fork-fission-bufferize[test8@loop, test8@bufferize1](auto.test8);
dce(auto.test8);
ccp(auto.test8);
dce(auto.test8);
simplify-cfg(auto.test8);
dce(auto.test8);

no-memset(test9@const);

fork-split(auto.test10);
fork-fission-reduces[test10@loop3](auto.test10);

dce(auto.test10);
simplify-cfg(auto.test10);
dce(auto.test10);

ip-sroa(*);
sroa(*);
dce(*);
ccp(*);
gvn(*);
phi-elim(*);
dce(*);

gcm(*);
float-collections(test2, auto.test2, test4, auto.test4, test5, auto.test5, test10, auto.test10);
gcm(*);
