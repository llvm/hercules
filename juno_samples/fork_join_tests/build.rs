use juno_build::JunoCompiler;

fn main() {
    #[cfg(not(feature = "cuda"))]
    {
        JunoCompiler::new()
            .file_in_src("fork_join_tests.jn")
            .unwrap()
            .schedule_in_src("cpu.sch")
            .unwrap()
            .build()
            .unwrap();
    }
    #[cfg(feature = "cuda")]
    {
        JunoCompiler::new()
            .file_in_src("fork_join_tests.jn")
            .unwrap()
            .schedule_in_src("gpu.sch")
            .unwrap()
            .build()
            .unwrap();
    }
}
