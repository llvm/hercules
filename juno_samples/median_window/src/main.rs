#![feature(concat_idents)]

juno_build::juno!("median");

use hercules_rt::{runner, HerculesImmBox, HerculesImmBoxTo};

fn main() {
    let m = vec![
        86, 72, 14, 5, 55, 25, 98, 89, 3, 66, 44, 81, 27, 3, 40, 18, 4, 57, 93, 34, 70, 50, 50, 18,
        34,
    ];
    let m = HerculesImmBox::from(m.as_slice());

    let mut r = runner!(median_window);
    let res = async_std::task::block_on(async { r.run(m.to()).await });
    assert_eq!(res, 57);
}

#[test]
fn test_median_window() {
    main()
}
