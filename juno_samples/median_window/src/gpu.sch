gvn(*);
phi-elim(*);
dce(*);

inline(*);
delete-uncalled(*);

let out = auto-outline(*);
gpu(out.median_window);

ip-sroa(*);
sroa(*);
dce(*);
gvn(*);
phi-elim(*);
dce(*);

forkify(*);
fork-guard-elim(*);
lift-dc-math(*);
forkify(*);
fork-guard-elim(*);
fork-unroll(out.median_window@outer);
lift-dc-math(*);
fixpoint {
  forkify(*);
  fork-guard-elim(*);
  fork-unroll(*);
}
ccp(*);
gvn(*);
dce(*);

array-to-product(*);
sroa(*);
phi-elim(*);
predication(*);
simplify-cfg(*);
dce(*);
gvn(*);

gcm(*);

