use juno_build::JunoCompiler;

fn main() {
    JunoCompiler::new()
        .file_in_src("median.jn")
        .unwrap()
        .schedule_in_src(if cfg!(feature = "cuda") {
            "gpu.sch"
        } else {
            "cpu.sch"
        })
        .unwrap()
        .build()
        .unwrap();
}
