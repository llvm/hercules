#![feature(concat_idents)]

use hercules_rt::runner;

juno_build::juno!("implicit_clone");

fn main() {
    async_std::task::block_on(async {
        let mut r = runner!(simple_implicit_clone);
        let output = r.run(3).await;
        assert_eq!(output, 11);

        let mut r = runner!(loop_implicit_clone);
        let output = r.run(100).await;
        assert_eq!(output, 7);

        let mut r = runner!(double_loop_implicit_clone);
        let output = r.run(3).await;
        assert_eq!(output, 42);

        let mut r = runner!(tricky_loop_implicit_clone);
        let output = r.run(2, 2).await;
        assert_eq!(output, 130);

        let mut r = runner!(tricky2_loop_implicit_clone);
        let output = r.run(2, 3).await;
        assert_eq!(output, 39);

        let mut r = runner!(tricky3_loop_implicit_clone);
        let output = r.run(5, 7).await;
        assert_eq!(output, 7);

        let mut r = runner!(no_implicit_clone);
        let output = r.run(4).await;
        assert_eq!(output, 13);

        let mut r = runner!(mirage_implicit_clone);
        let output = r.run(73).await;
        assert_eq!(output, 843);
    });
}

#[test]
fn implicit_clone_test() {
    main();
}
