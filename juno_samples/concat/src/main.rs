#![feature(concat_idents)]

use hercules_rt::runner;
#[cfg(feature = "cuda")]
use hercules_rt::CUDABox;
use hercules_rt::HerculesCPURef;

juno_build::juno!("concat");

fn main() {
    async_std::task::block_on(async {
        let mut r = runner!(concat_entry);
        let mut a_data = Box::new([7, 7, 0]);
        let mut b_data = Box::new([7, 7, 0, 0, 7, 7]);
        #[cfg(not(feature = "cuda"))]
        {
            let a = HerculesCPURef::from_slice(a_data.as_ref());
            let b = HerculesCPURef::from_slice(b_data.as_ref());
            let output = r.run(3, 6, a, b).await;
            assert_eq!(output, 42);

            const N: usize = 3;
            let arr: Box<[i32]> = (2..=4).collect();
            let arr = HerculesCPURef::from_slice(&arr);

            let mut r = runner!(concat_switch);
            let output = r.run(N as u64, 50, arr.clone()).await;
            let result = output.as_slice::<i32>();
            println!("{:?}", result);
            assert_eq!(result, [0, 1, 2, 3, 4]);

            let output = r.run(N as u64, 30, arr).await;
            let result = output.as_slice::<i32>();
            println!("{:?}", result);
            assert_eq!(result, [2, 3, 4, 0, 1]);
        }
        #[cfg(feature = "cuda")]
        {
            let a = CUDABox::from_cpu_ref(HerculesCPURef::from_slice(a_data.as_ref()));
            let b = CUDABox::from_cpu_ref(HerculesCPURef::from_slice(b_data.as_ref()));
            let output = r.run(3, 6, a.get_ref(), b.get_ref()).await;
            assert_eq!(output, 42);
        }
    });
}

#[test]
fn concat_test() {
    main();
}
