gvn(*);
phi-elim(*);
dce(*);

inline(*);
let out = auto-outline(*);
gpu(out.concat_entry);

ip-sroa(*);
sroa(*);
dce(*);
gvn(*);
phi-elim(*);
dce(*);

infer-schedules(*);

gcm(*);
float-collections(*);
dce(*);
gcm(*);
