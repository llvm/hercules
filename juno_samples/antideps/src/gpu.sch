gvn(*);
phi-elim(*);
dce(*);

let out = auto-outline(*);
gpu(out.simple_antideps, out.loop_antideps, out.complex_antideps1, out.complex_antideps2, out.very_complex_antideps, out.read_chains, out.array_of_structs, out.issue_21);

ip-sroa(*);
sroa(*);
dce(*);
gvn(*);
phi-elim(*);
dce(*);

infer-schedules(*);

gcm(*);
float-collections(*);
dce(*);
gcm(*);
