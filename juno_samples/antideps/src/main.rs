#![feature(concat_idents)]

use hercules_rt::runner;

juno_build::juno!("antideps");

fn main() {
    async_std::task::block_on(async {
        let mut r = runner!(simple_antideps);
        let output = r.run(1, 1).await;
        println!("{}", output);
        assert_eq!(output, 5);

        let mut r = runner!(loop_antideps);
        let output = r.run(11).await;
        println!("{}", output);
        assert_eq!(output, 5);

        let mut r = runner!(complex_antideps1);
        let output = r.run(9).await;
        println!("{}", output);
        assert_eq!(output, 20);

        let mut r = runner!(complex_antideps2);
        let output = r.run(44).await;
        println!("{}", output);
        assert_eq!(output, 226);

        let mut r = runner!(very_complex_antideps);
        let output = r.run(3).await;
        println!("{}", output);
        assert_eq!(output, 144);

        let mut r = runner!(read_chains);
        let output = r.run(2).await;
        println!("{}", output);
        assert_eq!(output, 14);

        let mut r = runner!(array_of_structs);
        let output = r.run(2).await;
        println!("{}", output);
        assert_eq!(output, 14);

        let mut r = runner!(issue_21);
        let output = r.run(42).await;
        println!("{}", output);
        assert_eq!(output, 42 * 2);
    });
}

#[test]
fn antideps_test() {
    main();
}
