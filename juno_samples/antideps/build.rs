use juno_build::JunoCompiler;

fn main() {
    #[cfg(not(feature = "cuda"))]
    {
        JunoCompiler::new()
            .file_in_src("antideps.jn")
            .unwrap()
            .build()
            .unwrap();
    }
    #[cfg(feature = "cuda")]
    {
        JunoCompiler::new()
            .file_in_src("antideps.jn")
            .unwrap()
            .schedule_in_src("gpu.sch")
            .unwrap()
            .build()
            .unwrap();
    }
}
