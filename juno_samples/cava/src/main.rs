use clap::Parser;

use juno_cava::{cava_harness, CavaInputs};

fn main() {
    let args = CavaInputs::parse();
    cava_harness(args);
}

#[test]
fn cava_test_small() {
    cava_harness(CavaInputs {
        input: "examples/raw_tulip-small.bin".to_string(),
        output: None,
        verify: true,
        output_verify: None,
        cam_model: "cam_models/NikonD7000".to_string(),
        crop_rows: Some(144),
        crop_cols: Some(192),
    });
}

#[test]
#[ignore]
fn cava_test_full() {
    cava_harness(CavaInputs {
        input: "examples/raw_tulips.bin".to_string(),
        output: None,
        verify: true,
        output_verify: None,
        cam_model: "cam_models/NikonD7000".to_string(),
        crop_rows: None,
        crop_cols: None,
    });
}
