#![feature(concat_idents)]

mod camera_model;
mod cava_rust;
mod image_proc;

pub use self::camera_model::*;
pub use self::cava_rust::CHAN;
pub use self::image_proc::*;

use hercules_rt::{runner, HerculesImmBox, HerculesImmBoxTo, HerculesMutBox};

use image::ImageError;

use clap::Parser;

juno_build::juno!("cava");

pub fn make_raw_image_and_cam_model(args: &CavaInputs) -> (RawImage, CamModel) {
    let raw_image =
        read_raw(&args.input, args.crop_rows, args.crop_cols).expect("Error loading image");
    let cam_model = load_cam_model(&args.cam_model, CHAN).expect("Error loading camera model");
    println!(
        "Running cava with {} rows, {} columns, and {} control points.",
        raw_image.rows, raw_image.cols, cam_model.num_ctrl_pts
    );
    (raw_image, cam_model)
}

pub fn prepare_hercules_inputs<'a, 'b>(
    raw_image: &'a RawImage,
    cam_model: &'b CamModel,
) -> (
    usize,
    usize,
    usize,
    HerculesImmBox<'a, u8>,
    HerculesImmBox<'b, f32>,
    HerculesImmBox<'b, f32>,
    HerculesImmBox<'b, f32>,
    HerculesImmBox<'b, f32>,
    HerculesImmBox<'b, f32>,
) {
    assert_eq!(
        raw_image.pixels.len(),
        CHAN * raw_image.rows * raw_image.cols
    );
    assert_eq!(cam_model.tstw.len(), CHAN * CHAN);
    assert_eq!(cam_model.ctrl_pts.len(), cam_model.num_ctrl_pts * CHAN);
    assert_eq!(cam_model.weights.len(), cam_model.num_ctrl_pts * CHAN);
    assert_eq!(cam_model.coefs.len(), 4 * CHAN);
    assert_eq!(cam_model.tonemap.len(), 256 * CHAN);

    let image = HerculesImmBox::from(&raw_image.pixels as &[u8]);
    let tstw = HerculesImmBox::from(&cam_model.tstw as &[f32]);
    let ctrl_pts = HerculesImmBox::from(&cam_model.ctrl_pts as &[f32]);
    let weights = HerculesImmBox::from(&cam_model.weights as &[f32]);
    let coefs = HerculesImmBox::from(&cam_model.coefs as &[f32]);
    let tonemap = HerculesImmBox::from(&cam_model.tonemap as &[f32]);

    (
        raw_image.rows,
        raw_image.cols,
        cam_model.num_ctrl_pts,
        image,
        tstw,
        ctrl_pts,
        weights,
        coefs,
        tonemap,
    )
}

enum Error {
    IOError(std::io::Error),
    ImageError(image::ImageError),
    ParseIntError(std::num::ParseIntError),
    ParseFloatError(std::num::ParseFloatError),
}

impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::IOError(err) => write!(f, "IO Error: {:?}", err),
            Error::ImageError(err) => write!(f, "Image Error: {:?}", err),
            Error::ParseIntError(err) => write!(f, "Parse Error: {:?}", err),
            Error::ParseFloatError(err) => write!(f, "Parse Error: {:?}", err),
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        Error::IOError(value)
    }
}

impl From<ImageError> for Error {
    fn from(value: ImageError) -> Self {
        Error::ImageError(value)
    }
}

impl From<std::num::ParseIntError> for Error {
    fn from(value: std::num::ParseIntError) -> Self {
        Error::ParseIntError(value)
    }
}

impl From<std::num::ParseFloatError> for Error {
    fn from(value: std::num::ParseFloatError) -> Self {
        Error::ParseFloatError(value)
    }
}

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
pub struct CavaInputs {
    pub input: String,
    #[clap(short, long, value_name = "PATH")]
    pub output: Option<String>,
    #[clap(short, long)]
    pub verify: bool,
    #[clap(long = "output-verify", value_name = "PATH")]
    pub output_verify: Option<String>,
    pub cam_model: String,
    #[clap(long)]
    pub crop_rows: Option<usize>,
    #[clap(long)]
    pub crop_cols: Option<usize>,
}

pub fn cava_harness(args: CavaInputs) {
    let (raw_image, cam_model) = make_raw_image_and_cam_model(&args);
    let (rows, cols, num_ctrl_pts, image, tstw, ctrl_pts, weights, coefs, tonemap) =
        prepare_hercules_inputs(&raw_image, &cam_model);
    let mut r = runner!(cava);

    let result = async_std::task::block_on(async {
        HerculesMutBox::from(
            r.run(
                rows as u64,
                cols as u64,
                num_ctrl_pts as u64,
                image.to(),
                tstw.to(),
                ctrl_pts.to(),
                weights.to(),
                coefs.to(),
                tonemap.to(),
            )
            .await,
        )
    })
    .as_slice()
    .to_vec()
    .into_boxed_slice();

    if let Some(output) = args.output {
        extern_image(rows, cols, &*result)
            .save(output)
            .expect("Error saving image");
    }

    if args.verify {
        let cpu_result = cava_rust::cava(
            rows,
            cols,
            num_ctrl_pts,
            &raw_image.pixels,
            &cam_model.tstw,
            &cam_model.ctrl_pts,
            &cam_model.weights,
            &cam_model.coefs,
            &cam_model.tonemap,
        );

        if let Some(output) = args.output_verify {
            extern_image(rows, cols, &cpu_result)
                .save(output)
                .expect("Error saving verification image");
        }

        let max_diff = result
            .iter()
            .zip(cpu_result.iter())
            .map(|(a, b)| (*a as i16 - *b as i16).abs())
            .max()
            .unwrap_or(0);

        assert!(
            max_diff <= 3,
            "Verification failed: maximum pixel difference of {} exceeds threshold of 3",
            max_diff
        );
        println!("Verified!");
    }
}
