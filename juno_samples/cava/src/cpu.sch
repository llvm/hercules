macro simpl!(X) {
  ccp(X);
  simplify-cfg(X);
  lift-dc-math(X);
  gvn(X);
  phi-elim(X);
  dce(X);
  infer-schedules(X);
}

simpl!(*);

let fuse1 = outline(cava@fuse1);
inline(fuse1);

let fuse2 = outline(cava@fuse2);
inline(fuse2);

let fuse3 = outline(cava@fuse3);
inline(fuse3);

let fuse4 = outline(cava@fuse4);
inline(fuse4);

let fuse5 = outline(cava@fuse5);
inline(fuse5);

ip-sroa(*);
sroa(*);
simpl!(*);

no-memset(fuse1@res1);
no-memset(fuse1@res2);
fixpoint {
  forkify(fuse1);
  fork-guard-elim(fuse1);
  fork-coalesce(fuse1);
}
simpl!(fuse1);
array-slf(fuse1);
loop-bound-canon(fuse1);
fixpoint {
  forkify(fuse1);
  fork-guard-elim(fuse1);
  fork-coalesce(fuse1);
}
predication(fuse1);
simpl!(fuse1);
write-predication(fuse1);
simpl!(fuse1);
parallel-reduce(fuse1@loop);
fork-extend[8](fuse1);

inline(fuse2);
no-memset(fuse2@res);
no-memset(fuse2@filter);
no-memset(fuse2@tmp);
fixpoint {
  forkify(fuse2);
  fork-guard-elim(fuse2);
  fork-coalesce(fuse2);
}
simpl!(fuse2);
predication(fuse2);
simpl!(fuse2);

let median = outline(fuse2@median);
fork-unroll(median@medianOuter);
simpl!(median);
fixpoint {
  forkify(median);
  fork-guard-elim(median);
}
simpl!(median);
fixpoint {
  fork-unroll(median);
}
ccp(median);
array-to-product(median);
sroa(median);
phi-elim(median);
predication(median);
simpl!(median);

inline(fuse2);
ip-sroa(*);
sroa(*);
array-slf(fuse2);
write-predication(fuse2);
simpl!(fuse2);

no-memset(fuse3@res);
fixpoint {
  forkify(fuse3);
  fork-guard-elim(fuse3);
  fork-coalesce(fuse3);
}
simpl!(fuse3);

no-memset(fuse4@res);
no-memset(fuse4@l2);
fixpoint {
  forkify(fuse4);
  fork-guard-elim(fuse4);
  fork-coalesce(fuse4);
}
simpl!(fuse4);

if !feature("dont_fuse_gamut") {
  fork-unroll(fuse4@channel_loop);
  simpl!(fuse4);
  fixpoint {
    fork-fusion(fuse4@channel_loop);
  }
  simpl!(fuse4);
  array-slf(fuse4);
  simpl!(fuse4);
}

if !feature("seq") {
  let par = fuse4@image_loop \ fuse4@channel_loop \ fuse4@cp_loop;
  fork-tile[4, 1, false, false](par);
  fork-tile[8, 0, false, false](par);
  fork-interchange[1, 2](par);
  let split = fork-split(par);
  let fuse4_body = outline(split.cava_3.fj2);
  fork-coalesce(fuse4, fuse4_body);
  simpl!(fuse4, fuse4_body);
  fuse4 = fuse4_body;
} else {
  fork-tile[6, 0, false, true](fuse4@channel_loop);
}

no-memset(fuse5@res1);
no-memset(fuse5@res2);
fixpoint {
  forkify(fuse5);
  fork-guard-elim(fuse5);
  fork-coalesce(fuse5);
}
simpl!(fuse5);
array-slf(fuse5);
simpl!(fuse5);

delete-uncalled(*);
simpl!(*);

fork-split(fuse1, fuse2, fuse3, fuse4, fuse5);
unforkify(fuse1, fuse2, fuse3, fuse4, fuse5);

simpl!(*);

delete-uncalled(*);
gcm(*);
