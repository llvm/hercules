#![feature(concat_idents)]

use criterion::{criterion_group, criterion_main, Criterion};

use hercules_rt::{runner, HerculesImmBoxTo};

use juno_cava::*;

juno_build::juno!("cava");

fn cava_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("cava bench");
    group.sample_size(10);

    let args = CavaInputs {
        input: "examples/raw_tulip-small.bin".to_string(),
        output: None,
        verify: false,
        output_verify: None,
        cam_model: "cam_models/NikonD7000".to_string(),
        crop_rows: Some(144),
        crop_cols: Some(192),
    };
    let (raw_image, cam_model) = make_raw_image_and_cam_model(&args);
    let (rows, cols, num_ctrl_pts, image, tstw, ctrl_pts, weights, coefs, tonemap) =
        prepare_hercules_inputs(&raw_image, &cam_model);
    let mut r = runner!(cava);
    let image = image.to();
    let tstw = tstw.to();
    let ctrl_pts = ctrl_pts.to();
    let weights = weights.to();
    let coefs = coefs.to();
    let tonemap = tonemap.to();

    group.bench_function("cava bench small", |b| {
        b.iter(|| {
            async_std::task::block_on(r.run(
                rows as u64,
                cols as u64,
                num_ctrl_pts as u64,
                image,
                tstw,
                ctrl_pts,
                weights,
                coefs,
                tonemap,
            ));
        })
    });

    let args = CavaInputs {
        input: "examples/raw_tulips.bin".to_string(),
        output: None,
        verify: true,
        output_verify: None,
        cam_model: "cam_models/NikonD7000".to_string(),
        crop_rows: None,
        crop_cols: None,
    };
    let (raw_image, cam_model) = make_raw_image_and_cam_model(&args);
    let (rows, cols, num_ctrl_pts, image, tstw, ctrl_pts, weights, coefs, tonemap) =
        prepare_hercules_inputs(&raw_image, &cam_model);
    let mut r = runner!(cava);
    let image = image.to();
    let tstw = tstw.to();
    let ctrl_pts = ctrl_pts.to();
    let weights = weights.to();
    let coefs = coefs.to();
    let tonemap = tonemap.to();

    group.bench_function("cava bench full", |b| {
        b.iter(|| {
            async_std::task::block_on(r.run(
                rows as u64,
                cols as u64,
                num_ctrl_pts as u64,
                image,
                tstw,
                ctrl_pts,
                weights,
                coefs,
                tonemap,
            ));
        })
    });
}

criterion_group!(benches, cava_bench);
criterion_main!(benches);
