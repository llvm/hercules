use juno_build::JunoCompiler;

fn main() {
    JunoCompiler::new()
        .file_in_src("matmul.jn")
        .unwrap()
        .schedule_in_src("matmul.sch")
        .unwrap()
        .build()
        .unwrap();
}
