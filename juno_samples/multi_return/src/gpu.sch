gvn(*);
phi-elim(*);
dce(*);

ip-sroa(*);
sroa(*);

ip-sroa[true](rolling_sum);
sroa[true](rolling_sum, rolling_sum_prod);

dce(*);

forkify(*);
fork-guard-elim(*);
gvn(*);
dce(*);

inline(*);
delete-uncalled(*);

let out = auto-outline(*);
gpu(out.rolling_sum_prod);

fork-fusion(out.rolling_sum_prod);
gvn(*);
dce(*);

float-collections(*);
unforkify(*);

gcm(*);
