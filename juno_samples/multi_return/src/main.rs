#![feature(concat_idents)]

juno_build::juno!("multi_return");

use hercules_rt::{runner, HerculesImmBox, HerculesImmBoxTo, HerculesMutBox};

fn main() {
    const N: usize = 32;
    let a: Box<[f32]> = (1..=N).map(|i| i as f32).collect();
    let arg = HerculesImmBox::from(a.as_ref());
    let mut r = runner!(rolling_sum_prod);
    let (sums, sum, prods, prod) =
        async_std::task::block_on(async { r.run(N as u64, arg.to()).await });

    let mut sums = HerculesMutBox::<f32>::from(sums);
    let mut prods = HerculesMutBox::<f32>::from(prods);

    let (expected_sums, expected_sum) = a.iter().fold((vec![0.0], 0.0), |(mut sums, sum), v| {
        let new_sum = sum + v;
        sums.push(new_sum);
        (sums, new_sum)
    });
    let (expected_prods, expected_prod) =
        a.iter().fold((vec![1.0], 1.0), |(mut prods, prod), v| {
            let new_prod = prod * v;
            prods.push(new_prod);
            (prods, new_prod)
        });

    assert_eq!(sum, expected_sum);
    assert_eq!(sums.as_slice(), expected_sums.as_slice());
    assert_eq!(prod, expected_prod);
    assert_eq!(prods.as_slice(), expected_prods.as_slice());
}

#[test]
fn test_multi_return() {
    main()
}
