gvn(*);
phi-elim(*);
dce(*);

let out = auto-outline(*);
gpu(out.matmul);

ip-sroa(*);
sroa(*);
dce(*);
float-collections(*);
gvn(*);
phi-elim(*);
dce(*);

infer-schedules(*);

gcm(*);
