gvn(*);
phi-elim(*);
dce(*);
ip-sroa(*);
sroa(*);
dce(*);
gvn(*);
dce(*);
phi-elim(*);
dce(*);
ccp(*);
dce(*);
simplify-cfg(*);
dce(*);
forkify(*);
fork-guard-elim(*);
fork-coalesce(*);
dce(*);

no-memset(multi_device_1@cons);
let l1 = outline(multi_device_1@loop1);
let l2 = outline(multi_device_1@loop2);
gpu(l1);
cpu(l2);
unforkify(l2);
ip-sroa(*);
sroa(*);
ccp(*);
gvn(*);
dce(*);

infer-schedules(*);

gcm(*);
