#![feature(concat_idents)]

#[cfg(feature = "cuda")]
use hercules_rt::runner;

#[cfg(feature = "cuda")]
juno_build::juno!("multi_device");

fn main() {
    #[cfg(feature = "cuda")]
    async_std::task::block_on(async {
        let mut r = runner!(multi_device_1);
        let out = r.run(42).await;
        assert_eq!(out, 42 * 16);
    });
}

#[test]
fn multi_device_test() {
    #[cfg(feature = "cuda")]
    main();
}
