use juno_build::JunoCompiler;

fn main() {
    #[cfg(feature = "cuda")]
    {
        JunoCompiler::new()
            .file_in_src("multi_device.jn")
            .unwrap()
            .schedule_in_src("multi_device.sch")
            .unwrap()
            .build()
            .unwrap();
    }
}
