fn gaussian_smoothing(
    rows: usize,
    cols: usize,
    gaussian_size: usize,
    input: &[f32],
    filter: &[f32],
) -> Vec<f32> {
    let mut result = vec![0.0; rows * cols];
    let gaussian_radius: i64 = (gaussian_size / 2) as i64;

    let rows = rows as i64;
    let cols = cols as i64;

    for gy in 0..rows {
        for gx in 0..cols {
            let gloc = gx + gy * cols;

            let mut smoothed_val = 0.0;

            for i in -gaussian_radius..=gaussian_radius {
                for j in -gaussian_radius..=gaussian_radius {
                    let mut load_offset = gloc + i * cols + j;

                    if gy + i < 0 {
                        // top contour
                        load_offset = gx + j;
                    } else if gy + i > rows - 1 {
                        // bottom contour
                        load_offset = (rows - 1) * cols + gx + j;
                    }

                    // Adjust so we are within image horizontally
                    if gx + j < 0 {
                        // left contour
                        load_offset -= gx + j;
                    } else if gx + j > cols - 1 {
                        // right contour
                        load_offset = load_offset - gx - j + cols - 1;
                    }

                    let gval = input[load_offset as usize];
                    let factor_offset =
                        (gaussian_radius + i) * (gaussian_size as i64) + gaussian_radius + j;
                    smoothed_val += gval * filter[factor_offset as usize];
                }
            }

            result[gloc as usize] = smoothed_val;
        }
    }

    result
}

const MIN_BR: f32 = 0.0;
const MAX_BR: f32 = 1.0;

fn laplacian_estimate(
    rows: usize,
    cols: usize,
    szb: usize,
    input: &[f32],
    structure: &[f32],
) -> Vec<f32> {
    assert!(szb == 3);

    let mut result = vec![0.0; rows * cols];

    for gy in 0..rows {
        for gx in 0..cols {
            let mut image_area = vec![0.0; szb * szb];

            // Copy for dilation filter
            image_area[1 * szb + 1] = input[gy * cols + gx];

            if gx == 0 {
                image_area[0 * szb + 0] = MIN_BR;
                image_area[1 * szb + 0] = MIN_BR;
                image_area[2 * szb + 0] = MIN_BR;
            } else {
                image_area[1 * szb + 0] = input[gy * cols + gx - 1];
                image_area[0 * szb + 0] = if gy > 0 {
                    input[(gy - 1) * cols + gx - 1]
                } else {
                    MIN_BR
                };
                image_area[2 * szb + 0] = if gy < rows - 1 {
                    input[(gy + 1) * cols + gx - 1]
                } else {
                    MIN_BR
                };
            }

            if gx == cols - 1 {
                image_area[0 * szb + 2] = MIN_BR;
                image_area[1 * szb + 2] = MIN_BR;
                image_area[2 * szb + 2] = MIN_BR;
            } else {
                image_area[1 * szb + 2] = input[gy * cols + gx + 1];
                image_area[0 * szb + 2] = if gy > 0 {
                    input[(gy - 1) * cols + gx + 1]
                } else {
                    MIN_BR
                };
                image_area[2 * szb + 2] = if gy < rows - 1 {
                    input[(gy + 1) * cols + gx + 1]
                } else {
                    MIN_BR
                };
            }

            image_area[0 * szb + 1] = if gy > 0 {
                input[(gy - 1) * cols + gx]
            } else {
                MIN_BR
            };
            image_area[2 * szb + 1] = if gy < rows - 1 {
                input[(gy + 1) * cols + gx]
            } else {
                MIN_BR
            };

            // Compute pixel of dilated image
            let mut dilated_pixel = MIN_BR;
            for i in 0..szb {
                for j in 0..szb {
                    dilated_pixel = f32::max(
                        dilated_pixel,
                        image_area[i * szb + j] * structure[i * szb + j],
                    );
                }
            }

            // Data copy for erosion filter - only change the boundary conditions
            if gx == 0 {
                image_area[0 * szb + 0] = MAX_BR;
                image_area[1 * szb + 0] = MAX_BR;
                image_area[2 * szb + 0] = MAX_BR;
            } else {
                if gy == 0 {
                    image_area[0 * szb + 0] = MAX_BR;
                }
                if gy == rows - 1 {
                    image_area[2 * szb + 0] = MAX_BR;
                }
            }

            if gx == cols - 1 {
                image_area[0 * szb + 2] = MAX_BR;
                image_area[1 * szb + 2] = MAX_BR;
                image_area[2 * szb + 2] = MAX_BR;
            } else {
                if gy == 0 {
                    image_area[0 * szb + 2] = MAX_BR;
                }
                if gy == rows - 1 {
                    image_area[2 * szb + 2] = MAX_BR;
                }
            }

            if gy == 0 {
                image_area[0 * szb + 1] = MAX_BR;
            }
            if gy == rows - 1 {
                image_area[2 * szb + 1] = MAX_BR;
            }

            // Compute pixel of eroded image
            let mut eroded_pixel = MAX_BR;
            for i in 0..szb {
                for j in 0..szb {
                    eroded_pixel = f32::min(
                        eroded_pixel,
                        image_area[i * szb + j] * structure[i * szb + j],
                    );
                }
            }

            let laplacian = dilated_pixel + eroded_pixel - 2.0 * image_area[1 * szb + 1];
            result[gy * cols + gx] = laplacian;
        }
    }

    result
}

fn compute_zero_crossings(
    rows: usize,
    cols: usize,
    szb: usize,
    input: &[f32],
    structure: &[f32],
) -> Vec<f32> {
    assert!(szb == 3);

    let mut result = vec![0.0; rows * cols];

    for gy in 0..rows {
        for gx in 0..cols {
            let mut image_area = vec![vec![0.0, 0.0, 0.0]; szb];

            // Copy for dilation filter
            image_area[1][1] = if input[gy * cols + gx] > MIN_BR {
                MAX_BR
            } else {
                MIN_BR
            };

            if gx == 0 {
                image_area[0][0] = MIN_BR;
                image_area[1][0] = MIN_BR;
                image_area[2][0] = MIN_BR;
            } else {
                image_area[1][0] = if input[gy * cols + gx - 1] > MIN_BR {
                    MAX_BR
                } else {
                    MIN_BR
                };
                image_area[0][0] = if gy > 0 {
                    if input[(gy - 1) * cols + gx - 1] > MIN_BR {
                        MAX_BR
                    } else {
                        MIN_BR
                    }
                } else {
                    MIN_BR
                };
                image_area[2][0] = if gy < rows - 1 {
                    if input[(gy + 1) * cols + gx - 1] > MIN_BR {
                        MAX_BR
                    } else {
                        MIN_BR
                    }
                } else {
                    MIN_BR
                };
            }

            if gx == cols - 1 {
                image_area[0][2] = MIN_BR;
                image_area[1][2] = MIN_BR;
                image_area[2][2] = MIN_BR;
            } else {
                image_area[1][2] = if input[gy * cols + gx + 1] > MIN_BR {
                    MAX_BR
                } else {
                    MIN_BR
                };
                image_area[0][2] = if gy > 0 {
                    if input[(gy - 1) * cols + gx + 1] > MIN_BR {
                        MAX_BR
                    } else {
                        MIN_BR
                    }
                } else {
                    MIN_BR
                };
                image_area[2][2] = if gy < rows - 1 {
                    if input[(gy + 1) * cols + gx + 1] > MIN_BR {
                        MAX_BR
                    } else {
                        MIN_BR
                    }
                } else {
                    MIN_BR
                };
            }

            image_area[0][1] = if gy > 0 {
                if input[(gy - 1) * cols + gx] > MIN_BR {
                    MAX_BR
                } else {
                    MIN_BR
                }
            } else {
                MIN_BR
            };
            image_area[2][1] = if gy < rows - 1 {
                if input[(gy + 1) * cols + gx] > MIN_BR {
                    MAX_BR
                } else {
                    MIN_BR
                }
            } else {
                MIN_BR
            };

            // Compute pixel of dilated image
            let mut dilated_pixel = MIN_BR;
            for i in 0..szb {
                for j in 0..szb {
                    dilated_pixel =
                        f32::max(dilated_pixel, image_area[i][j] * structure[i * szb + j]);
                }
            }

            // Data copy for erosion filter - only change the boundary conditions
            if gx == 0 {
                image_area[0][0] = MAX_BR;
                image_area[1][0] = MAX_BR;
                image_area[2][0] = MAX_BR;
            } else {
                if gy == 0 {
                    image_area[0][0] = MAX_BR;
                }
                if gy == rows - 1 {
                    image_area[2][0] = MAX_BR;
                }
            }

            if gx == cols - 1 {
                image_area[0][2] = MAX_BR;
                image_area[1][2] = MAX_BR;
                image_area[2][2] = MAX_BR;
            } else {
                if gy == 0 {
                    image_area[0][2] = MAX_BR;
                }
                if gy == rows - 1 {
                    image_area[2][2] = MAX_BR;
                }
            }

            if gy == 0 {
                image_area[0][1] = MAX_BR;
            }
            if gy == rows - 1 {
                image_area[2][1] = MAX_BR;
            }

            // Compute pixel of eroded image
            let mut eroded_pixel = MAX_BR;
            for i in 0..szb {
                for j in 0..szb {
                    eroded_pixel =
                        f32::min(eroded_pixel, image_area[i][j] * structure[i * szb + j]);
                }
            }

            let pixel_sign = dilated_pixel - eroded_pixel;
            result[gy * cols + gx] = pixel_sign;
        }
    }

    result
}

fn compute_gradient(
    rows: usize,
    cols: usize,
    sobel_size: usize,
    input: &[f32],
    sx: &[f32],
    sy: &[f32],
) -> Vec<f32> {
    assert!(sobel_size == 3);

    let mut result = vec![0.0; rows * cols];
    let sobel_radius = (sobel_size / 2) as i64;
    let rows = rows as i64;
    let cols = cols as i64;

    for gy in 0..rows {
        for gx in 0..cols {
            let gloc = gx + gy * cols;

            let mut gradient_x = 0.0;
            let mut gradient_y = 0.0;

            for i in -sobel_radius..=sobel_radius {
                for j in -sobel_radius..=sobel_radius {
                    let mut load_offset = gloc + i * cols + j;

                    if gy + i < 0 {
                        // top contour
                        load_offset = gx + j;
                    } else if gy + i > rows - 1 {
                        // bottom contour
                        load_offset = (rows - 1) * cols + gx + j;
                    }

                    // Adjust so we are within image horizontally
                    if gx + j < 0 {
                        // left contour
                        load_offset -= gx + j;
                    } else if gx + j > cols - 1 {
                        // right contour
                        load_offset = load_offset - gx - j + cols - 1;
                    }

                    let gval = input[load_offset as usize];
                    let s_offset = (sobel_radius + i) * (sobel_size as i64) + sobel_radius + j;
                    gradient_x += gval * sx[s_offset as usize];
                    gradient_y += gval * sy[s_offset as usize];
                }
            }

            result[gloc as usize] = f32::sqrt(gradient_x * gradient_x + gradient_y * gradient_y);
        }
    }

    result
}

fn compute_max_gradient(rows: usize, cols: usize, gradient: &[f32]) -> f32 {
    let mut max = gradient[0];

    for i in 1..rows * cols {
        if max < gradient[i] {
            max = gradient[i];
        }
    }

    max
}

fn reject_zero_crossings(
    rows: usize,
    cols: usize,
    zero_crossings: &[f32],
    gradient: &[f32],
    max_gradient: f32,
    theta: f32,
) -> Vec<f32> {
    let mut result = vec![0.0; rows * cols];

    for gy in 0..rows {
        for gx in 0..cols {
            result[gy * cols + gx] = if zero_crossings[gy * cols + gx] > 0.0
                && gradient[gy * cols + gx] > theta * max_gradient
            {
                1.0
            } else {
                0.0
            };
        }
    }

    result
}

pub fn edge_detection(
    rows: usize,
    cols: usize,
    gaussian_size: usize,
    szb: usize,
    soebel_size: usize,
    input: &[f32],
    gaussian_filter: &[f32],
    structure: &[f32],
    sx: &[f32],
    sy: &[f32],
    theta: f32,
) -> Vec<f32> {
    let smoothed = gaussian_smoothing(rows, cols, gaussian_size, input, gaussian_filter);
    let laplacian = laplacian_estimate(rows, cols, szb, &smoothed, structure);
    let zcs = compute_zero_crossings(rows, cols, szb, &laplacian, structure);
    let gradient = compute_gradient(rows, cols, soebel_size, &smoothed, sx, sy);
    let max_gradient = compute_max_gradient(rows, cols, &gradient);
    reject_zero_crossings(rows, cols, &zcs, &gradient, max_gradient, theta)
}
