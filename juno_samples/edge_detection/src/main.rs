use clap::Parser;

use juno_edge_detection::{edge_detection_harness, EdgeDetectionInputs};

fn main() {
    let args = EdgeDetectionInputs::parse();
    edge_detection_harness(args);
}

#[test]
fn edge_detection_test() {
    edge_detection_harness(EdgeDetectionInputs {
        input: "examples/formula1_scaled.mp4".to_string(),
        display: false,
        output: None,
        verify: true,
        display_verify: false,
        output_verify: None,
        // Limit frames to keep runtime low
        frames: Some(2),
    });
}
