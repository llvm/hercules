#![feature(concat_idents)]

#[cfg(feature = "cuda")]
use hercules_rt::CUDABox;
use hercules_rt::{runner, HerculesCPURef, HerculesCPURefMut};

juno_build::juno!("control");

fn main() {
    async_std::task::block_on(async {
        let a: Box<[f32]> = Box::new([17.0, 18.0, 19.0]);
        let mut b: Box<[i32]> = Box::new([12, 16, 4, 18, 23, 56, 93, 22, 14]);
        #[cfg(not(feature = "cuda"))]
        {
            let a = HerculesCPURef::from_slice(&a);
            let mut r = runner!(ccp_example);
            let output_example = r.run(a).await;
            assert_eq!(output_example, 1.0);

            let b = HerculesCPURefMut::from_slice(&mut b);
            let mut r = runner!(median_array);
            let output_median = r.run(9, b).await;
            assert_eq!(output_median, 18);
        }
        #[cfg(feature = "cuda")]
        {
            let mut a = CUDABox::from_cpu_ref(HerculesCPURef::from_slice(&a));
            let mut r = runner!(ccp_example);
            let output_example = r.run(a.get_ref_mut()).await;
            assert_eq!(output_example, 1.0);

            let mut b = CUDABox::from_cpu_ref(HerculesCPURef::from_slice(&b));
            let mut r = runner!(median_array);
            let output_median = r.run(9, b.get_ref_mut()).await;
            assert_eq!(output_median, 18);
        }

        let mut r = runner!(no_underflow);
        let out_no_underflow = r.run().await;
        assert_eq!(out_no_underflow, 7);

        let mut r = runner!(useless_control);
        let out_useless_control = r.run(-1.0).await;
        assert_eq!(out_useless_control, 13.0);
    });
}

#[test]
fn control_test() {
    main();
}
