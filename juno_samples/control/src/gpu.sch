gvn(*);
phi-elim(*);
dce(*);

let out = auto-outline(*);
gpu(out.ccp_example, out.median_array, out.no_underflow, out.useless_control);

ip-sroa(*);
sroa(*);
dce(*);
gvn(*);
dce(*);
phi-elim(*);
dce(*);
ccp(out.useless_control);
dce(out.useless_control);
simplify-cfg(out.useless_control);
dce(out.useless_control);
predication(out.useless_control);
dce(out.useless_control);
forkify(out.useless_control);
fork-guard-elim(out.useless_control);
fork-coalesce(out.useless_control);
dce(out.useless_control);
simplify-cfg(out.useless_control);
dce(out.useless_control);

infer-schedules(*);

gcm(*);
