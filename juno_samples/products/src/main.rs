#![feature(concat_idents)]

use hercules_rt::{runner, HerculesImmBox, HerculesImmBoxTo, HerculesMutBox};

juno_build::juno!("products");

fn main() {
    async_std::task::block_on(async {
        let input = vec![(0, 1), (2, 3)];
        let input: HerculesImmBox<(i32, i32)> = HerculesImmBox::from(input.as_slice());
        let mut r = runner!(product_read);
        let res: Vec<i32> = HerculesMutBox::from(r.run(input.to()).await)
            .as_slice()
            .to_vec();
        assert_eq!(res, vec![0, 1, 2, 3]);

        // Technically this returns a product of two i32s, but we can interpret that as an array
        let mut r = runner!(product_return);
        let res: Vec<i32> = HerculesMutBox::from(r.run(42, 17).await)
            .as_slice()
            .to_vec();
        assert_eq!(res, vec![42, 17]);
    });
}

#[test]
fn products_test() {
    main();
}
