gvn(*);
phi-elim(*);
dce(*);

let out = auto-outline(*);
gpu(out.product_read);

sroa(*);
reuse-products(*);
crc(*);
dce(*);
gvn(*);
phi-elim(*);
dce(*);

infer-schedules(*);

float-collections(*);
gcm(*);
