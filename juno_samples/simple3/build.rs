use juno_build::JunoCompiler;

fn main() {
    #[cfg(not(feature = "cuda"))]
    {
        JunoCompiler::new()
            .file_in_src("simple3.jn")
            .unwrap()
            .build()
            .unwrap();
    }
    #[cfg(feature = "cuda")]
    {
        JunoCompiler::new()
            .file_in_src("simple3.jn")
            .unwrap()
            .schedule_in_src("gpu.sch")
            .unwrap()
            .build()
            .unwrap();
    }
}
