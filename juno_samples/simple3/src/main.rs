#![feature(concat_idents)]

#[cfg(feature = "cuda")]
use hercules_rt::CUDABox;
use hercules_rt::{runner, HerculesCPURef};

juno_build::juno!("simple3");

fn main() {
    async_std::task::block_on(async {
        let a: Box<[u32]> = Box::new([1, 2, 3, 4, 5, 6, 7, 8]);
        let b: Box<[u32]> = Box::new([8, 7, 6, 5, 4, 3, 2, 1]);
        #[cfg(not(feature = "cuda"))]
        {
            let a = HerculesCPURef::from_slice(&a);
            let b = HerculesCPURef::from_slice(&b);
            let mut r = runner!(simple3);
            let c = r.run(8, a, b).await;
            assert_eq!(c, 120);
        }
        #[cfg(feature = "cuda")]
        {
            let a = CUDABox::from_cpu_ref(HerculesCPURef::from_slice(&a));
            let b = CUDABox::from_cpu_ref(HerculesCPURef::from_slice(&b));
            let mut r = runner!(simple3);
            let c = r.run(8, a.get_ref(), b.get_ref()).await;
            assert_eq!(c, 120);
        }
    });
}

#[test]
fn simple3_test() {
    main();
}
