#![feature(concat_idents)]

use rand::random;

#[cfg(feature = "cuda")]
use hercules_rt::CUDABox;
use hercules_rt::{runner, HerculesCPURef};

juno_build::juno!("code");

fn main() {
    async_std::task::block_on(async {
        const N: usize = 256;
        const M: usize = 64;
        const K: usize = 128;
        let a: Box<[i32]> = (0..N * M).map(|_| random::<i32>() % 100).collect();
        let b: Box<[i32]> = (0..M * K).map(|_| random::<i32>() % 100).collect();
        let c: Box<[i32]> = (0..K).map(|_| random::<i32>() % 100).collect();

        let mut correct_res: Box<[i32]> = (0..N).map(|_| 0).collect();
        for i in 0..N {
            for j in 0..K {
                let mut res = 0;
                for k in 0..M {
                    res += a[i * M + k] * b[k * K + j];
                }
                correct_res[i] += c[j] * res;
            }
        }

        #[cfg(not(feature = "cuda"))]
        {
            let a = HerculesCPURef::from_slice(&a);
            let b = HerculesCPURef::from_slice(&b);
            let c = HerculesCPURef::from_slice(&c);
            let mut r = runner!(test);
            let res = r.run(N as u64, M as u64, K as u64, a, b, c).await;
            assert_eq!(res.as_slice::<i32>(), &*correct_res);
        }
        #[cfg(feature = "cuda")]
        {
            let a = CUDABox::from_cpu_ref(HerculesCPURef::from_slice(&a));
            let b = CUDABox::from_cpu_ref(HerculesCPURef::from_slice(&b));
            let c = CUDABox::from_cpu_ref(HerculesCPURef::from_slice(&c));
            let mut r = runner!(test);
            let res = r
                .run(
                    N as u64,
                    M as u64,
                    K as u64,
                    a.get_ref(),
                    b.get_ref(),
                    c.get_ref(),
                )
                .await;
            let mut res_cpu: Box<[i32]> = vec![0; correct_res.len()].into_boxed_slice();
            res.to_cpu_ref(&mut res_cpu);
            assert_eq!(&*res_cpu, &*correct_res);
        }
    });
}

#[test]
fn schedule_test() {
    main();
}
