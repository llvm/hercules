macro juno-setup!(X) {
  //gvn(X);
  phi-elim(X);
  dce(X);
  lift-dc-math(X);
}
macro codegen-prep!(X) {
  infer-schedules(X);
  dce(X);
  gcm(X);
  dce(X);
  phi-elim(X);
  float-collections(X);
  gcm(X);
}


juno-setup!(*);

let first = outline(test@outer);
let second = outline(test@row);

// We can use the functions produced by outlining in our schedules
gvn(first, second, test);

ip-sroa(*);
sroa(*);

// We can evaluate expressions using labels and save them for later use
let inner = first@inner;

// A fixpoint can run a (series) of passes until no more changes are made
// (though some passes seem to make edits even if there are no real changes,
// so this is fragile).
// We could just let it run until it converges but can also tell it to panic
// if it hasn't converged after a number of iterations (like here) tell it to
// just stop after a certain number of iterations (stop after #) or to print
// the iteration number (print iter)
fixpoint panic after 2 {
  phi-elim(*);
}

host(*);
cpu(first, second);

codegen-prep!(*);
//xdot[true](*);
