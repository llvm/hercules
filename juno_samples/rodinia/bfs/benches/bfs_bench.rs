#![feature(concat_idents)]
use criterion::{criterion_group, criterion_main, Criterion};

use hercules_rt::{runner, HerculesImmBox, HerculesImmBoxTo};

juno_build::juno!("bfs");

use juno_bfs::graph_parser::*;

fn bfs_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("bfs bench");
    group.sample_size(10);

    let mut r = runner!(bfs);

    let mut bench = |name, input: &'_ str| {
        let (nodes, source, edges) = parse_graph(input.into()).expect("PANIC: Couldn't read input file for 64M benchmark. Currently, this benchmark uses a hard-coded path, so it can only be run on the lab machines.");
        let n = nodes.len() as u64;
        let m = edges.len() as u64;
        let nodes = HerculesImmBox::from(&nodes as &[Node]);
        let edges = HerculesImmBox::from(&edges as &[u32]);
        group.bench_function(name, |b| {
            b.iter(|| {
                async_std::task::block_on(async { r.run(n, m, nodes.to(), source, edges.to()).await });
            })
        });
    };

    bench("bfs bench 4096", "data/graph4096.txt");
    bench("bfs bench 65536", "data/graph65536.txt");
    bench("bfs bench 64M", "/scratch/aaronjc4/rodinia_3.1/data/bfs/graph64M.txt");
}

criterion_group!(benches, bfs_bench);
criterion_main!(benches);
