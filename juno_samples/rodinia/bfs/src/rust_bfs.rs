use crate::graph_parser::Node;

use std::collections::VecDeque;

pub fn bfs(graph_nodes: &[Node], source: u32, edges: &[u32]) -> Vec<i32> {
    let mut explored = vec![false; graph_nodes.len()];
    let mut costs = vec![-1; graph_nodes.len()];
    let mut worklist = VecDeque::new();

    let source = source as usize;

    explored[source] = true;
    costs[source] = 0;
    worklist.push_back(source);

    while let Some(node) = worklist.pop_front() {
        let edge_start = graph_nodes[node].edge_start;
        let num_edges = graph_nodes[node].num_edges;
        for edge in edge_start..edge_start + num_edges {
            let dst = edges[edge as usize] as usize;
            if !explored[dst] {
                explored[dst] = true;
                costs[dst] = costs[node] + 1;
                worklist.push_back(dst as usize);
            }
        }
    }

    costs
}
