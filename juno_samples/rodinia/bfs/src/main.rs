use clap::Parser;

use juno_bfs::{bfs_harness, BFSInputs};

fn main() {
    let args = BFSInputs::parse();
    bfs_harness(args);
}

#[test]
#[ignore]
fn bfs_test_4096() {
    bfs_harness(BFSInputs {
        input: "data/graph4096.txt".to_string(),
    });
}

#[test]
fn bfs_test_65536() {
    bfs_harness(BFSInputs {
        input: "data/graph65536.txt".to_string(),
    });
}
