macro simpl!(X) {
  ccp(X);
  simplify-cfg(X);
  lift-dc-math(X);
  gvn(X);
  phi-elim(X);
  crc(X);
  slf(X);
  dce(X);
  infer-schedules(X);
}

simpl!(*);
inline(compute_step_factor, compute_flux, compute_flux_contributions, compute_flux_contribution, time_step);
delete-uncalled(*);
simpl!(*);
ip-sroa[true](*);
sroa[true](*);
predication(*);
const-inline(*);
simpl!(*);
fixpoint {
  forkify(*);
  fork-guard-elim(*);
}
simpl!(*);
no-memset(compute_step_factor@res, compute_flux_contributions@res, compute_flux@res, copy_vars@res);
unforkify(compute_flux@inner_loop);

if !feature("seq") {
  fork-tile[32, 0, false, false](compute_step_factor);
  let split = fork-split(compute_step_factor);
  let compute_step_factor_body = outline(split._4_compute_step_factor.fj1);
  fork-coalesce(compute_step_factor, compute_step_factor_body);
  simpl!(compute_step_factor, compute_step_factor_body);
  compute_step_factor = compute_step_factor_body;

  fork-tile[32, 0, false, false](compute_flux_contributions);
  let split = fork-split(compute_flux_contributions);
  let compute_flux_contributions_body = outline(split._6_compute_flux_contributions.fj1);
  fork-coalesce(compute_flux_contributions, compute_flux_contributions_body);
  simpl!(compute_flux_contributions, compute_flux_contributions_body);
  compute_flux_contributions = compute_flux_contributions_body;

  fork-tile[32, 0, false, false](compute_flux);
  let split = fork-split(compute_flux);
  let compute_flux_body = outline(split._7_compute_flux.fj1);
  fork-coalesce(compute_flux, compute_flux_body);
  simpl!(compute_flux, compute_flux_body);
  compute_flux = compute_flux_body;

  fork-tile[32, 0, false, false](time_step);
  let split = fork-split(time_step);
  let time_step_body = outline(split._8_time_step.fj1);
  fork-coalesce(time_step, time_step_body);
  simpl!(time_step, time_step_body);
  time_step = time_step_body;

  fork-tile[32, 0, false, false](copy_vars);
  let split = fork-split(copy_vars);
  let copy_vars_body = outline(split._9_copy_vars.fj1);
  fork-coalesce(copy_vars, copy_vars_body);
  simpl!(copy_vars, copy_vars_body);
  copy_vars = copy_vars_body;
}

const-inline[false](*);
simpl!(*);
fork-split(compute_step_factor, compute_flux_contributions, compute_flux, time_step, copy_vars);
unforkify(compute_step_factor, compute_flux_contributions, compute_flux, time_step, copy_vars);
simpl!(*);

gcm(*);
