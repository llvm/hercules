use clap::Parser;

use juno_cfd::{cfd_harness, CFDInputs};

fn main() {
    let args = CFDInputs::parse();
    cfd_harness(args);
}

#[test]
fn test_euler() {
    cfd_harness(CFDInputs {
        data_file: "data/fvcorr.domn.097K".to_string(),
        iterations: 1,
        block_size: 16,
        pre_euler: false,
        verify: true,
    });
}

#[test]
fn test_pre_euler() {
    cfd_harness(CFDInputs {
        data_file: "data/fvcorr.domn.097K".to_string(),
        iterations: 1,
        block_size: 16,
        pre_euler: true,
        verify: true,
    });
}
