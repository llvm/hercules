use crate::setup::*;

pub fn compute_flux_contribution(
    density: f32,
    momentum: &Float3,
    density_energy: f32,
    pressure: f32,
    velocity: &Float3,
) -> (Float3, Float3, Float3, Float3) {
    let fc_momentum_x = Float3 {
        x: velocity.x * momentum.x + pressure,
        y: velocity.x * momentum.y,
        z: velocity.x * momentum.z,
    };
    let fc_momentum_y = Float3 {
        x: fc_momentum_x.y,
        y: velocity.y * momentum.y + pressure,
        z: velocity.y * momentum.z,
    };
    let fc_momentum_z = Float3 {
        x: fc_momentum_x.z,
        y: fc_momentum_y.z,
        z: velocity.z * momentum.z + pressure,
    };

    let de_p = density_energy + pressure;
    let fc_density_energy = Float3 {
        x: velocity.x * de_p,
        y: velocity.y * de_p,
        z: velocity.z * de_p,
    };

    (
        fc_momentum_x,
        fc_momentum_y,
        fc_momentum_z,
        fc_density_energy,
    )
}

fn compute_velocity(density: f32, momentum: &Float3) -> Float3 {
    Float3 {
        x: momentum.x / density,
        y: momentum.y / density,
        z: momentum.z / density,
    }
}

fn compute_speed_sqd(velocity: &Float3) -> f32 {
    velocity.x * velocity.x + velocity.y * velocity.y + velocity.z * velocity.z
}

fn compute_pressure(density: f32, density_energy: f32, speed_sqd: f32) -> f32 {
    (GAMMA - 1.0) * (density_energy - 0.5 * density * speed_sqd)
}

fn compute_speed_of_sound(density: f32, pressure: f32) -> f32 {
    (GAMMA * pressure / density).sqrt()
}

fn compute_step_factor(nelr: usize, variables: &Variables, areas: &[f32]) -> Vec<f32> {
    let mut step_factors = vec![0.0; nelr];

    for i in 0..nelr {
        let density = variables.density[i];

        let momentum = Float3 {
            x: variables.momentum.x[i],
            y: variables.momentum.y[i],
            z: variables.momentum.z[i],
        };

        let density_energy = variables.energy[i];

        let velocity = compute_velocity(density, &momentum);
        let speed_sqd = compute_speed_sqd(&velocity);
        let pressure = compute_pressure(density, density_energy, speed_sqd);
        let speed_of_sound = compute_speed_of_sound(density, pressure);

        step_factors[i] = 0.5 / (areas[i].sqrt() * (speed_sqd.sqrt() + speed_of_sound));
    }

    step_factors
}

fn compute_flux_euler(
    nelr: usize,
    variables: &Variables,
    elements_surrounding_elements: &[i32],
    normals: &Normals,
    ff_variable: &Variable,
    ff_fc_momentum_x: &Float3,
    ff_fc_momentum_y: &Float3,
    ff_fc_momentum_z: &Float3,
    ff_fc_density_energy: &Float3,
) -> Variables {
    let smoothing_coefficient: f32 = 0.2;

    let mut fluxes = Variables {
        density: AlignedSlice::of_len(nelr),
        momentum: Momentum {
            x: AlignedSlice::of_len(nelr),
            y: AlignedSlice::of_len(nelr),
            z: AlignedSlice::of_len(nelr),
        },
        energy: AlignedSlice::of_len(nelr),
    };

    for i in 0..nelr {
        let density_i = variables.density[i];
        let momentum_i = Float3 {
            x: variables.momentum.x[i],
            y: variables.momentum.y[i],
            z: variables.momentum.z[i],
        };

        let density_energy_i = variables.energy[i];

        let velocity_i = compute_velocity(density_i, &momentum_i);
        let speed_sqd_i = compute_speed_sqd(&velocity_i);
        let speed_i = speed_sqd_i.sqrt();
        let pressure_i = compute_pressure(density_i, density_energy_i, speed_sqd_i);
        let speed_of_sound_i = compute_speed_of_sound(density_i, pressure_i);

        let (fc_i_momentum_x, fc_i_momentum_y, fc_i_momentum_z, fc_i_density_energy) =
            compute_flux_contribution(
                density_i,
                &momentum_i,
                density_energy_i,
                pressure_i,
                &velocity_i,
            );

        let mut flux_i_density: f32 = 0.0;
        let mut flux_i_momentum = Float3 {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        };
        let mut flux_i_density_energy: f32 = 0.0;

        for j in 0..NNB {
            let nb = elements_surrounding_elements[i + j * nelr];
            let normal = Float3 {
                x: normals.x[i + j * nelr],
                y: normals.y[i + j * nelr],
                z: normals.z[i + j * nelr],
            };
            let normal_len =
                (normal.x * normal.x + normal.y * normal.y + normal.z * normal.z).sqrt();

            if nb >= 0 {
                // legitimate neighbor
                let nb = nb as usize;
                let density_nb = variables.density[nb];
                let momentum_nb = Float3 {
                    x: variables.momentum.x[nb],
                    y: variables.momentum.y[nb],
                    z: variables.momentum.z[nb],
                };
                let density_energy_nb = variables.energy[nb];
                let velocity_nb = compute_velocity(density_nb, &momentum_nb);
                let speed_sqd_nb = compute_speed_sqd(&velocity_nb);
                let pressure_nb = compute_pressure(density_nb, density_energy_nb, speed_sqd_nb);
                let speed_of_sound_nb = compute_speed_of_sound(density_nb, pressure_nb);

                let (fc_nb_momentum_x, fc_nb_momentum_y, fc_nb_momentum_z, fc_nb_density_energy) =
                    compute_flux_contribution(
                        density_nb,
                        &momentum_nb,
                        density_energy_nb,
                        pressure_nb,
                        &velocity_nb,
                    );

                // artificial viscosity
                let factor = -normal_len
                    * smoothing_coefficient
                    * 0.5
                    * (speed_i + speed_sqd_nb.sqrt() + speed_of_sound_i + speed_of_sound_nb);
                flux_i_density += factor * (density_i - density_nb);
                flux_i_density_energy += factor * (density_energy_i - density_energy_nb);
                flux_i_momentum.x += factor * (momentum_i.x - momentum_nb.x);
                flux_i_momentum.y += factor * (momentum_i.y - momentum_nb.y);
                flux_i_momentum.z += factor * (momentum_i.z - momentum_nb.z);

                // accumulate cell-centered fluxes
                let factor = 0.5 * normal.x;
                flux_i_density += factor * (momentum_nb.x + momentum_i.x);
                flux_i_density_energy += factor * (fc_nb_density_energy.x + fc_i_density_energy.x);
                flux_i_momentum.x += factor * (fc_nb_momentum_x.x + fc_i_momentum_x.x);
                flux_i_momentum.y += factor * (fc_nb_momentum_y.x + fc_i_momentum_y.x);
                flux_i_momentum.z += factor * (fc_nb_momentum_z.x + fc_i_momentum_z.x);

                let factor = 0.5 * normal.y;
                flux_i_density += factor * (momentum_nb.y + momentum_i.y);
                flux_i_density_energy += factor * (fc_nb_density_energy.y + fc_i_density_energy.y);
                flux_i_momentum.x += factor * (fc_nb_momentum_x.y + fc_i_momentum_x.y);
                flux_i_momentum.y += factor * (fc_nb_momentum_y.y + fc_i_momentum_y.y);
                flux_i_momentum.z += factor * (fc_nb_momentum_z.y + fc_i_momentum_z.y);

                let factor = 0.5 * normal.z;
                flux_i_density += factor * (momentum_nb.z + momentum_i.z);
                flux_i_density_energy += factor * (fc_nb_density_energy.z + fc_i_density_energy.z);
                flux_i_momentum.x += factor * (fc_nb_momentum_x.z + fc_i_momentum_x.z);
                flux_i_momentum.y += factor * (fc_nb_momentum_y.z + fc_i_momentum_y.z);
                flux_i_momentum.z += factor * (fc_nb_momentum_z.z + fc_i_momentum_z.z);
            } else if nb == -1 {
                // a wing boundary
                flux_i_momentum.x += normal.x * pressure_i;
                flux_i_momentum.y += normal.y * pressure_i;
                flux_i_momentum.z += normal.z * pressure_i;
            } else if nb == -2 {
                // a far field boundary
                let factor = 0.5 * normal.x;
                flux_i_density += factor * (ff_variable.momentum.x + momentum_i.x);
                flux_i_density_energy += factor * (ff_fc_density_energy.x + fc_i_density_energy.x);
                flux_i_momentum.x += factor * (ff_fc_momentum_x.x + fc_i_momentum_x.x);
                flux_i_momentum.y += factor * (ff_fc_momentum_y.x + fc_i_momentum_y.x);
                flux_i_momentum.z += factor * (ff_fc_momentum_z.x + fc_i_momentum_z.x);

                let factor = 0.5 * normal.y;
                flux_i_density += factor * (ff_variable.momentum.y + momentum_i.y);
                flux_i_density_energy += factor * (ff_fc_density_energy.y + fc_i_density_energy.y);
                flux_i_momentum.x += factor * (ff_fc_momentum_x.y + fc_i_momentum_x.y);
                flux_i_momentum.y += factor * (ff_fc_momentum_y.y + fc_i_momentum_y.y);
                flux_i_momentum.z += factor * (ff_fc_momentum_z.y + fc_i_momentum_z.y);

                let factor = 0.5 * normal.z;
                flux_i_density += factor * (ff_variable.momentum.z + momentum_i.z);
                flux_i_density_energy += factor * (ff_fc_density_energy.z + fc_i_density_energy.z);
                flux_i_momentum.x += factor * (ff_fc_momentum_x.z + fc_i_momentum_x.z);
                flux_i_momentum.y += factor * (ff_fc_momentum_y.z + fc_i_momentum_y.z);
                flux_i_momentum.z += factor * (ff_fc_momentum_z.z + fc_i_momentum_z.z);
            }
        }

        fluxes.density[i] = flux_i_density;
        fluxes.momentum.x[i] = flux_i_momentum.x;
        fluxes.momentum.y[i] = flux_i_momentum.y;
        fluxes.momentum.z[i] = flux_i_momentum.z;
        fluxes.energy[i] = flux_i_density_energy;
    }

    fluxes
}

fn compute_flux_contributions(
    nelr: usize,
    variables: &Variables,
) -> (Vec<f32>, Vec<f32>, Vec<f32>, Vec<f32>) {
    let mut fc_momentum_x = vec![0.0; nelr * NDIM];
    let mut fc_momentum_y = vec![0.0; nelr * NDIM];
    let mut fc_momentum_z = vec![0.0; nelr * NDIM];
    let mut fc_density_energy = vec![0.0; nelr * NDIM];

    for i in 0..nelr {
        let density_i = variables.density[i];
        let momentum_i = Float3 {
            x: variables.momentum.x[i],
            y: variables.momentum.y[i],
            z: variables.momentum.z[i],
        };

        let density_energy_i = variables.energy[i];

        let velocity_i = compute_velocity(density_i, &momentum_i);
        let speed_sqd_i = compute_speed_sqd(&velocity_i);
        let speed_i = speed_sqd_i.sqrt();
        let pressure_i = compute_pressure(density_i, density_energy_i, speed_sqd_i);
        let speed_of_sound_i = compute_speed_of_sound(density_i, pressure_i);

        let (fc_i_momentum_x, fc_i_momentum_y, fc_i_momentum_z, fc_i_density_energy) =
            compute_flux_contribution(
                density_i,
                &momentum_i,
                density_energy_i,
                pressure_i,
                &velocity_i,
            );

        fc_momentum_x[i + 0 * nelr] = fc_i_momentum_x.x;
        fc_momentum_x[i + 1 * nelr] = fc_i_momentum_x.y;
        fc_momentum_x[i + 2 * nelr] = fc_i_momentum_x.z;

        fc_momentum_y[i + 0 * nelr] = fc_i_momentum_y.x;
        fc_momentum_y[i + 1 * nelr] = fc_i_momentum_y.y;
        fc_momentum_y[i + 2 * nelr] = fc_i_momentum_y.z;

        fc_momentum_z[i + 0 * nelr] = fc_i_momentum_z.x;
        fc_momentum_z[i + 1 * nelr] = fc_i_momentum_z.y;
        fc_momentum_z[i + 2 * nelr] = fc_i_momentum_z.z;

        fc_density_energy[i + 0 * nelr] = fc_i_density_energy.x;
        fc_density_energy[i + 1 * nelr] = fc_i_density_energy.y;
        fc_density_energy[i + 2 * nelr] = fc_i_density_energy.z;
    }

    (
        fc_momentum_x,
        fc_momentum_y,
        fc_momentum_z,
        fc_density_energy,
    )
}

fn compute_flux_pre_euler(
    nelr: usize,
    variables: &Variables,
    elements_surrounding_elements: &[i32],
    normals: &Normals,
    fc_momentum_x: &[f32],
    fc_momentum_y: &[f32],
    fc_momentum_z: &[f32],
    fc_density_energy: &[f32],
    ff_variable: &Variable,
    ff_fc_momentum_x: &Float3,
    ff_fc_momentum_y: &Float3,
    ff_fc_momentum_z: &Float3,
    ff_fc_density_energy: &Float3,
) -> Variables {
    let smoothing_coefficient: f32 = 0.2;

    let mut fluxes = Variables {
        density: AlignedSlice::of_len(nelr),
        momentum: Momentum {
            x: AlignedSlice::of_len(nelr),
            y: AlignedSlice::of_len(nelr),
            z: AlignedSlice::of_len(nelr),
        },
        energy: AlignedSlice::of_len(nelr),
    };

    for i in 0..nelr {
        let density_i = variables.density[i];
        let momentum_i = Float3 {
            x: variables.momentum.x[i],
            y: variables.momentum.y[i],
            z: variables.momentum.z[i],
        };

        let density_energy_i = variables.energy[i];

        let velocity_i = compute_velocity(density_i, &momentum_i);
        let speed_sqd_i = compute_speed_sqd(&velocity_i);
        let speed_i = speed_sqd_i.sqrt();
        let pressure_i = compute_pressure(density_i, density_energy_i, speed_sqd_i);
        let speed_of_sound_i = compute_speed_of_sound(density_i, pressure_i);

        let fc_i_momentum_x = Float3 {
            x: fc_momentum_x[i + 0 * nelr],
            y: fc_momentum_x[i + 1 * nelr],
            z: fc_momentum_x[i + 2 * nelr],
        };
        let fc_i_momentum_y = Float3 {
            x: fc_momentum_y[i + 0 * nelr],
            y: fc_momentum_y[i + 1 * nelr],
            z: fc_momentum_y[i + 2 * nelr],
        };
        let fc_i_momentum_z = Float3 {
            x: fc_momentum_z[i + 0 * nelr],
            y: fc_momentum_z[i + 1 * nelr],
            z: fc_momentum_z[i + 2 * nelr],
        };
        let fc_i_density_energy = Float3 {
            x: fc_density_energy[i + 0 * nelr],
            y: fc_density_energy[i + 1 * nelr],
            z: fc_density_energy[i + 2 * nelr],
        };

        let mut flux_i_density: f32 = 0.0;
        let mut flux_i_momentum = Float3 {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        };
        let mut flux_i_density_energy: f32 = 0.0;

        for j in 0..NNB {
            let nb = elements_surrounding_elements[i + j * nelr];
            let normal = Float3 {
                x: normals.x[i + j * nelr],
                y: normals.y[i + j * nelr],
                z: normals.z[i + j * nelr],
            };
            let normal_len =
                (normal.x * normal.x + normal.y * normal.y + normal.z * normal.z).sqrt();

            if nb >= 0 {
                let nb = nb as usize;
                let density_nb = variables.density[nb];
                let momentum_nb = Float3 {
                    x: variables.momentum.x[nb],
                    y: variables.momentum.y[nb],
                    z: variables.momentum.z[nb],
                };
                let density_energy_nb = variables.energy[nb];
                let velocity_nb = compute_velocity(density_nb, &momentum_nb);
                let speed_sqd_nb = compute_speed_sqd(&velocity_nb);
                let pressure_nb = compute_pressure(density_nb, density_energy_nb, speed_sqd_nb);
                let speed_of_sound_nb = compute_speed_of_sound(density_nb, pressure_nb);

                let fc_nb_momentum_x = Float3 {
                    x: fc_momentum_x[nb + 0 * nelr],
                    y: fc_momentum_x[nb + 1 * nelr],
                    z: fc_momentum_x[nb + 2 * nelr],
                };
                let fc_nb_momentum_y = Float3 {
                    x: fc_momentum_y[nb + 0 * nelr],
                    y: fc_momentum_y[nb + 1 * nelr],
                    z: fc_momentum_y[nb + 2 * nelr],
                };
                let fc_nb_momentum_z = Float3 {
                    x: fc_momentum_z[nb + 0 * nelr],
                    y: fc_momentum_z[nb + 1 * nelr],
                    z: fc_momentum_z[nb + 2 * nelr],
                };
                let fc_nb_density_energy = Float3 {
                    x: fc_density_energy[nb + 0 * nelr],
                    y: fc_density_energy[nb + 1 * nelr],
                    z: fc_density_energy[nb + 2 * nelr],
                };

                // artificial viscosity
                let factor = -normal_len
                    * smoothing_coefficient
                    * 0.5
                    * (speed_i + speed_sqd_nb.sqrt() + speed_of_sound_i + speed_of_sound_nb);
                flux_i_density += factor * (density_i - density_nb);
                flux_i_density_energy += factor * (density_energy_i - density_energy_nb);
                flux_i_momentum.x += factor * (momentum_i.x - momentum_nb.x);
                flux_i_momentum.y += factor * (momentum_i.y - momentum_nb.y);
                flux_i_momentum.z += factor * (momentum_i.z - momentum_nb.z);

                // accumulate cell-centered fluxes
                let factor = 0.5 * normal.x;
                flux_i_density += factor * (momentum_nb.x + momentum_i.x);
                flux_i_density_energy += factor * (fc_nb_density_energy.x + fc_i_density_energy.x);
                flux_i_momentum.x += factor * (fc_nb_momentum_x.x + fc_i_momentum_x.x);
                flux_i_momentum.y += factor * (fc_nb_momentum_y.x + fc_i_momentum_y.x);
                flux_i_momentum.z += factor * (fc_nb_momentum_z.x + fc_i_momentum_z.x);

                let factor = 0.5 * normal.y;
                flux_i_density += factor * (momentum_nb.y + momentum_i.y);
                flux_i_density_energy += factor * (fc_nb_density_energy.y + fc_i_density_energy.y);
                flux_i_momentum.x += factor * (fc_nb_momentum_x.y + fc_i_momentum_x.y);
                flux_i_momentum.y += factor * (fc_nb_momentum_y.y + fc_i_momentum_y.y);
                flux_i_momentum.z += factor * (fc_nb_momentum_z.y + fc_i_momentum_z.y);

                let factor = 0.5 * normal.z;
                flux_i_density += factor * (momentum_nb.z + momentum_i.z);
                flux_i_density_energy += factor * (fc_nb_density_energy.z + fc_i_density_energy.z);
                flux_i_momentum.x += factor * (fc_nb_momentum_x.z + fc_i_momentum_x.z);
                flux_i_momentum.y += factor * (fc_nb_momentum_y.z + fc_i_momentum_y.z);
                flux_i_momentum.z += factor * (fc_nb_momentum_z.z + fc_i_momentum_z.z);
            } else if nb == -1 {
                flux_i_momentum.x += normal.x * pressure_i;
                flux_i_momentum.y += normal.y * pressure_i;
                flux_i_momentum.z += normal.z * pressure_i;
            } else if nb == -2 {
                let factor = 0.5 * normal.x;
                flux_i_density += factor * (ff_variable.momentum.x + momentum_i.x);
                flux_i_density_energy += factor * (ff_fc_density_energy.x + fc_i_density_energy.x);
                flux_i_momentum.x += factor * (ff_fc_momentum_x.x + fc_i_momentum_x.x);
                flux_i_momentum.y += factor * (ff_fc_momentum_y.x + fc_i_momentum_y.x);
                flux_i_momentum.z += factor * (ff_fc_momentum_z.x + fc_i_momentum_z.x);

                let factor = 0.5 * normal.y;
                flux_i_density += factor * (ff_variable.momentum.y + momentum_i.y);
                flux_i_density_energy += factor * (ff_fc_density_energy.y + fc_i_density_energy.y);
                flux_i_momentum.x += factor * (ff_fc_momentum_x.y + fc_i_momentum_x.y);
                flux_i_momentum.y += factor * (ff_fc_momentum_y.y + fc_i_momentum_y.y);
                flux_i_momentum.z += factor * (ff_fc_momentum_z.y + fc_i_momentum_z.y);

                let factor = 0.5 * normal.z;
                flux_i_density += factor * (ff_variable.momentum.z + momentum_i.z);
                flux_i_density_energy += factor * (ff_fc_density_energy.z + fc_i_density_energy.z);
                flux_i_momentum.x += factor * (ff_fc_momentum_x.z + fc_i_momentum_x.z);
                flux_i_momentum.y += factor * (ff_fc_momentum_y.z + fc_i_momentum_y.z);
                flux_i_momentum.z += factor * (ff_fc_momentum_z.z + fc_i_momentum_z.z);
            }
        }

        fluxes.density[i] = flux_i_density;
        fluxes.momentum.x[i] = flux_i_momentum.x;
        fluxes.momentum.y[i] = flux_i_momentum.y;
        fluxes.momentum.z[i] = flux_i_momentum.z;
        fluxes.energy[i] = flux_i_density_energy;
    }

    fluxes
}

fn time_step(
    j: usize,
    nelr: usize,
    old_variables: &Variables,
    variables: &mut Variables,
    step_factors: &[f32],
    fluxes: &Variables,
) {
    for i in 0..nelr {
        let factor = step_factors[i] / (RK + 1 - j) as f32;
        variables.density[i] = old_variables.density[i] + factor * fluxes.density[i];
        variables.momentum.x[i] = old_variables.momentum.x[i] + factor * fluxes.momentum.x[i];
        variables.momentum.y[i] = old_variables.momentum.y[i] + factor * fluxes.momentum.y[i];
        variables.momentum.z[i] = old_variables.momentum.z[i] + factor * fluxes.momentum.z[i];
        variables.energy[i] = old_variables.energy[i] + factor * fluxes.energy[i];
    }
}

pub fn euler(
    nelr: usize,
    iterations: usize,
    mut variables: Variables,
    areas: &[f32],
    elements_surrounding_elements: &[i32],
    normals: &Normals,
    ff_variable: &Variable,
    ff_fc_density_energy: &Float3,
    ff_fc_momentum_x: &Float3,
    ff_fc_momentum_y: &Float3,
    ff_fc_momentum_z: &Float3,
) -> Variables {
    for i in 0..iterations {
        let old_variables = variables.clone();
        let step_factors = compute_step_factor(nelr, &variables, areas);

        for j in 0..RK {
            let fluxes = compute_flux_euler(
                nelr,
                &variables,
                elements_surrounding_elements,
                normals,
                ff_variable,
                ff_fc_momentum_x,
                ff_fc_momentum_y,
                ff_fc_momentum_z,
                ff_fc_density_energy,
            );
            time_step(
                j,
                nelr,
                &old_variables,
                &mut variables,
                &step_factors,
                &fluxes,
            );
        }
    }

    variables
}

pub fn pre_euler(
    nelr: usize,
    iterations: usize,
    mut variables: Variables,
    areas: &[f32],
    elements_surrounding_elements: &[i32],
    normals: &Normals,
    ff_variable: &Variable,
    ff_fc_density_energy: &Float3,
    ff_fc_momentum_x: &Float3,
    ff_fc_momentum_y: &Float3,
    ff_fc_momentum_z: &Float3,
) -> Variables {
    for i in 0..iterations {
        let old_variables = variables.clone();
        let step_factors = compute_step_factor(nelr, &variables, areas);

        for j in 0..RK {
            let (fc_momentum_x, fc_momentum_y, fc_momentum_z, fc_density_energy) =
                compute_flux_contributions(nelr, &variables);
            let fluxes = compute_flux_pre_euler(
                nelr,
                &variables,
                elements_surrounding_elements,
                normals,
                &fc_momentum_x,
                &fc_momentum_y,
                &fc_momentum_z,
                &fc_density_energy,
                ff_variable,
                ff_fc_momentum_x,
                ff_fc_momentum_y,
                ff_fc_momentum_z,
                ff_fc_density_energy,
            );
            time_step(
                j,
                nelr,
                &old_variables,
                &mut variables,
                &step_factors,
                &fluxes,
            );
        }
    }

    variables
}
