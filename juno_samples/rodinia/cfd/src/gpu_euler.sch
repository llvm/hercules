macro simpl!(X) {
  ccp(X);
  simplify-cfg(X);
  lift-dc-math(X);
  gvn(X);
  phi-elim(X);
  crc(X);
  slf(X);
  dce(X);
  infer-schedules(X);
}

simpl!(*);
inline(compute_step_factor, compute_flux, compute_flux_contribution, time_step);
no-memset(compute_step_factor@res, compute_flux@res, copy_vars@res);
delete-uncalled(*);
gpu(copy_vars, compute_step_factor, compute_flux, time_step);

simpl!(*);
ip-sroa[true](*);
sroa[true](*);
predication(*);
const-inline(*);
simpl!(*);
fixpoint {
  forkify(*);
  fork-guard-elim(*);
}
simpl!(*);
unforkify(compute_flux@inner_loop);

fork-tile[32, 0, false, true](compute_step_factor);
fork-split(compute_step_factor);

fork-tile[32, 0, false, true](compute_flux);
fork-split(compute_flux);

fork-tile[32, 0, false, true](time_step);
fork-split(time_step);

fork-tile[32, 0, false, true](copy_vars);
fork-split(copy_vars);

gcm(*);
