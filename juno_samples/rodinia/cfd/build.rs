use juno_build::JunoCompiler;

fn main() {
    #[cfg(feature = "cuda")]
    JunoCompiler::new()
        .file_in_src("euler.jn")
        .unwrap()
        .schedule_in_src("gpu_euler.sch")
        .unwrap()
        .build()
        .unwrap();
    #[cfg(not(feature = "cuda"))]
    JunoCompiler::new()
        .file_in_src("euler.jn")
        .unwrap()
        .schedule_in_src("cpu_euler.sch")
        .unwrap()
        .build()
        .unwrap();
    #[cfg(feature = "cuda")]
    JunoCompiler::new()
        .file_in_src("pre_euler.jn")
        .unwrap()
        .schedule_in_src("gpu_pre_euler.sch")
        .unwrap()
        .build()
        .unwrap();
    #[cfg(not(feature = "cuda"))]
    JunoCompiler::new()
        .file_in_src("pre_euler.jn")
        .unwrap()
        .schedule_in_src("cpu_pre_euler.sch")
        .unwrap()
        .build()
        .unwrap();
}
