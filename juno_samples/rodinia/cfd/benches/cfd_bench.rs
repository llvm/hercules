#![feature(concat_idents)]
use criterion::{criterion_group, criterion_main, Criterion};

use hercules_rt::{runner, HerculesImmBox, HerculesImmBoxTo, HerculesMutBox, HerculesMutBoxTo};

juno_build::juno!("euler");
juno_build::juno!("pre_euler");

use juno_cfd::*;

fn cfd_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("cfd bench");
    group.sample_size(10);

    let mut euler_bench = |name, data_file, iterations| {
        let mut r = runner!(euler);
        let block_size = 16;
        let FarFieldConditions {
            ff_variable,
            ff_fc_momentum_x,
            ff_fc_momentum_y,
            ff_fc_momentum_z,
            ff_fc_density_energy,
        } = set_far_field_conditions();
        let GeometryData {
            nelr,
            areas,
            elements_surrounding_elements,
            normals,
        } = read_domain_geometry(data_file, block_size).expect("PANIC: Couldn't read input for CFD benchmark. Currently, the path for the largest CFD benchmark is hard-coded, so it can only be run on the lab machines.");
        let mut variables = initialize_variables(nelr, &ff_variable);

        let mut v_density = HerculesMutBox::from(variables.density.as_mut_slice());
        let mut v_momentum_x = HerculesMutBox::from(variables.momentum.x.as_mut_slice());
        let mut v_momentum_y = HerculesMutBox::from(variables.momentum.y.as_mut_slice());
        let mut v_momentum_z = HerculesMutBox::from(variables.momentum.z.as_mut_slice());
        let mut v_energy = HerculesMutBox::from(variables.energy.as_mut_slice());

        let areas = HerculesImmBox::from(areas.as_slice());
        let elements_surrounding_elements =
            HerculesImmBox::from(elements_surrounding_elements.as_slice());

        let normals_x = HerculesImmBox::from(normals.x.as_slice());
        let normals_y = HerculesImmBox::from(normals.y.as_slice());
        let normals_z = HerculesImmBox::from(normals.z.as_slice());

        group.bench_function(name, |b| {
            b.iter(|| {
                async_std::task::block_on(async {
                    r.run(
                        nelr as u64,
                        iterations as u64,
                        v_density.to(),
                        v_momentum_x.to(),
                        v_momentum_y.to(),
                        v_momentum_z.to(),
                        v_energy.to(),
                        areas.to(),
                        elements_surrounding_elements.to(),
                        normals_x.to(),
                        normals_y.to(),
                        normals_z.to(),
                        ff_variable.density,
                        ff_variable.momentum.x,
                        ff_variable.momentum.y,
                        ff_variable.momentum.z,
                        ff_variable.energy,
                        ff_fc_density_energy.x,
                        ff_fc_density_energy.y,
                        ff_fc_density_energy.z,
                        ff_fc_momentum_x.x,
                        ff_fc_momentum_x.y,
                        ff_fc_momentum_x.z,
                        ff_fc_momentum_y.x,
                        ff_fc_momentum_y.y,
                        ff_fc_momentum_y.z,
                        ff_fc_momentum_z.x,
                        ff_fc_momentum_z.y,
                        ff_fc_momentum_z.z,
                    )
                    .await
                });
            })
        });
    };
    euler_bench("cfd bench euler small", "data/fvcorr.domn.097K", 1);
    euler_bench(
        "cfd bench euler large",
        "/scratch/aaronjc4/rodinia_3.1/data/cfd/missile.domn.0.2M",
        150,
    );

    let mut pre_euler_bench = |name, data_file, iterations| {
        let mut r = runner!(pre_euler);
        let block_size = 16;
        let FarFieldConditions {
            ff_variable,
            ff_fc_momentum_x,
            ff_fc_momentum_y,
            ff_fc_momentum_z,
            ff_fc_density_energy,
        } = set_far_field_conditions();
        let GeometryData {
            nelr,
            areas,
            elements_surrounding_elements,
            normals,
        } = read_domain_geometry(data_file, block_size).expect("PANIC: Couldn't read input for CFD benchmark. Currently, the path for the largest CFD benchmark is hard-coded, so it can only be run on the lab machines.");
        let mut variables = initialize_variables(nelr, &ff_variable);

        let mut v_density = HerculesMutBox::from(variables.density.as_mut_slice());
        let mut v_momentum_x = HerculesMutBox::from(variables.momentum.x.as_mut_slice());
        let mut v_momentum_y = HerculesMutBox::from(variables.momentum.y.as_mut_slice());
        let mut v_momentum_z = HerculesMutBox::from(variables.momentum.z.as_mut_slice());
        let mut v_energy = HerculesMutBox::from(variables.energy.as_mut_slice());

        let areas = HerculesImmBox::from(areas.as_slice());
        let elements_surrounding_elements =
            HerculesImmBox::from(elements_surrounding_elements.as_slice());

        let normals_x = HerculesImmBox::from(normals.x.as_slice());
        let normals_y = HerculesImmBox::from(normals.y.as_slice());
        let normals_z = HerculesImmBox::from(normals.z.as_slice());

        group.bench_function(name, |b| {
            b.iter(|| {
                async_std::task::block_on(async {
                    r.run(
                        nelr as u64,
                        iterations as u64,
                        v_density.to(),
                        v_momentum_x.to(),
                        v_momentum_y.to(),
                        v_momentum_z.to(),
                        v_energy.to(),
                        areas.to(),
                        elements_surrounding_elements.to(),
                        normals_x.to(),
                        normals_y.to(),
                        normals_z.to(),
                        ff_variable.density,
                        ff_variable.momentum.x,
                        ff_variable.momentum.y,
                        ff_variable.momentum.z,
                        ff_variable.energy,
                        ff_fc_density_energy.x,
                        ff_fc_density_energy.y,
                        ff_fc_density_energy.z,
                        ff_fc_momentum_x.x,
                        ff_fc_momentum_x.y,
                        ff_fc_momentum_x.z,
                        ff_fc_momentum_y.x,
                        ff_fc_momentum_y.y,
                        ff_fc_momentum_y.z,
                        ff_fc_momentum_z.x,
                        ff_fc_momentum_z.y,
                        ff_fc_momentum_z.z,
                    )
                    .await
                });
            })
        });
    };
    pre_euler_bench("cfd bench pre-euler small", "data/fvcorr.domn.097K", 1);
    pre_euler_bench(
        "cfd bench pre-euler large",
        "/scratch/aaronjc4/rodinia_3.1/data/cfd/missile.domn.0.2M",
        150,
    );
}

criterion_group!(benches, cfd_bench);
criterion_main!(benches);
