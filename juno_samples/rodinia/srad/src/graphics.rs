use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;
use std::str::FromStr;

use nom::Parser;

pub struct Image {
    pub image: Vec<f32>,
    pub max: f32,
    pub rows: usize,
    pub cols: usize,
}

pub fn read_graphics<P>(path: P) -> Image
where
    P: AsRef<Path>,
{
    let mut file = File::open(path).expect("Error opening input file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Error reading input file");

    let mut parser = nom::combinator::all_consuming(image_parser);
    let (_, result) = parser.parse(&contents).expect("Parser error");

    result
}

pub fn write_graphics<P>(path: P, image: &[f32], rows: usize, cols: usize, max: f32)
where
    P: AsRef<Path>,
{
    let mut file = File::create(path).expect("Error opening output file");
    let f = &mut file;

    write!(f, "P2\n");
    write!(f, "{} {}\n", cols, rows);
    write!(f, "{}\n", max as i32);
    for i in 0..rows {
        for j in 0..cols {
            write!(f, "{} ", image[j * rows + i] as i32);
        }
        write!(f, "\n");
    }
}

pub fn resize(
    image: &[f32],
    rows_in: usize,
    cols_in: usize,
    rows_out: usize,
    cols_out: usize,
) -> Vec<f32> {
    let mut output = vec![0.0; rows_out * cols_out];

    let mut j2 = 0;
    for j in 0..cols_out {
        if j2 >= cols_in {
            j2 = j2 - cols_in;
        }
        let mut i2 = 0;
        for i in 0..rows_out {
            if i2 >= rows_in {
                i2 = i2 - rows_in;
            }
            output[j * rows_out + i] = image[j2 * rows_in + i2];
            i2 += 1;
        }
        j2 += 1;
    }

    output
}

fn image_parser<'a>(text: &'a str) -> nom::IResult<&'a str, Image> {
    // First is the magic number "P2"
    let text = nom::character::complete::char('P')(text)?.0;
    let text = nom::character::complete::char('2')(text)?.0;
    let text = nom::character::complete::multispace1(text)?.0;

    // Next, we find the number of columns and rows
    let (text, num_cols) = nom::character::complete::digit1(text)?;
    let text = nom::character::complete::multispace1(text)?.0;
    let (text, num_rows) = nom::character::complete::digit1(text)?;
    let text = nom::character::complete::multispace1(text)?.0;

    let cols = usize::from_str(num_cols).unwrap();
    let rows = usize::from_str(num_rows).unwrap();
    let mut image = vec![0.0; cols * rows];

    // Next, the maximum value
    let (text, num_max) = nom::character::complete::digit1(text)?;
    let text = nom::character::complete::multispace1(text)?.0;

    let max = f32::from_str(num_max).unwrap();

    // Finally, we read each pixel value (storing them in column major order)
    let mut text = text;
    for i in 0..rows {
        for j in 0..cols {
            let (txt, pixel_val) = nom::character::complete::digit1(text)?;
            text = nom::character::complete::multispace0(txt)?.0;

            let val = f32::from_str(pixel_val).unwrap();
            image[j * rows + i] = val;
        }
    }

    Ok((
        text,
        Image {
            image,
            max,
            rows,
            cols,
        },
    ))
}
