use clap::Parser;

use juno_srad::*;

fn main() {
    let args = SRADInputs::parse();
    srad_harness(args);
}

#[test]
fn srad_test() {
    srad_harness(SRADInputs {
        niter: 100,
        lambda: 0.5,
        nrows: 512,
        ncols: 512,
        image: "data/image.pgm".to_string(),
        output: None,
        verify: true,
        output_verify: None,
    });
}
