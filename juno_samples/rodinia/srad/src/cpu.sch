macro simpl!(X) {
  ccp(X);
  simplify-cfg(X);
  lift-dc-math(X);
  gvn(X);
  phi-elim(X);
  dce(X);
  infer-schedules(X);
}

no-memset(srad@scratch);
phi-elim(*);
let loop1 = outline(srad@loop1);
let loop2 = outline(srad@loop2);
let loop3 = outline(srad@loop3);
simpl!(*);
const-inline(*);
crc(*);
slf(*);
write-predication(*);
simpl!(*);
predication(*);
simpl!(*);
predication(*);
simpl!(*);
fixpoint {
  forkify(*);
  fork-guard-elim(*);
  fork-coalesce(*);
}
fork-dim-merge(loop1);
simpl!(*);
reduce-slf(*);
simpl!(*);
slf(*);
simpl!(*);

if !feature("seq") {
  fork-tile[32, 0, false, false](loop1);
  simpl!(loop1);
  let split = fork-split(loop1);
  simpl!(loop1);
  clean-monoid-reduces(loop1);
  let loop1_body = outline(split.srad_0.fj1);
  simpl!(loop1, loop1_body);
  unforkify(loop1_body);
  let fission = fork-fission[split.srad_0.fj0](loop1);
  simpl!(loop1, loop1_body);
  unforkify(fission.srad_0.fj_bottom);
  simpl!(loop1, loop1_body);
  loop1 = loop1_body;

  fork-tile[32, 0, false, false](loop2);
  let split = fork-split(loop2);
  let loop2_body = outline(split.srad_1.fj1);
  simpl!(loop2, loop2_body);
  loop2 = loop2_body;

  fork-tile[32, 0, false, false](loop3);
  let split = fork-split(loop3);
  let loop3_body = outline(split.srad_2.fj1);
  simpl!(loop3, loop3_body);
  loop3 = loop3_body;

  inline(srad@loop2, srad@loop3);
  delete-uncalled(*);
}

fork-split(extract, compress, loop1, loop2, loop3);
unforkify(extract, compress, loop1, loop2, loop3);

gcm(*);
