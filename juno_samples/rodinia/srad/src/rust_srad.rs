pub fn srad(nrows: usize, ncols: usize, niter: usize, image: &mut Vec<f32>, max: f32, lambda: f32) {
    let nelems = nrows * ncols;

    // EXTRACT
    for i in 0..nelems {
        image[i] = (image[i] / max).exp();
    }

    let mut dN = vec![0.0; nelems];
    let mut dS = vec![0.0; nelems];
    let mut dW = vec![0.0; nelems];
    let mut dE = vec![0.0; nelems];

    let mut c = vec![0.0; nelems];

    for _ in 0..niter {
        let mut sum = 0.0;
        let mut sum2 = 0.0;

        for i in 0..nrows {
            for j in 0..ncols {
                let tmp = image[i + nrows * j];
                sum += tmp;
                sum2 += tmp * tmp;
            }
        }

        let meanROI = sum / nelems as f32;
        let varROI = (sum2 / nelems as f32) - meanROI * meanROI;
        let q0sqr = varROI / (meanROI * meanROI);

        for j in 0..ncols {
            for i in 0..nrows {
                let k = i + nrows * j;
                let Jc = image[k];
                let iN = std::cmp::max(i, 1) - 1;
                let iS = std::cmp::min(i, nrows - 2) + 1;
                let jW = std::cmp::max(j, 1) - 1;
                let jE = std::cmp::min(j, ncols - 2) + 1;

                dN[k] = image[iN as usize + nrows * j] - Jc;
                dS[k] = image[iS as usize + nrows * j] - Jc;
                dW[k] = image[i + nrows * jW as usize] - Jc;
                dE[k] = image[i + nrows * jE as usize] - Jc;

                let G2 =
                    (dN[k] * dN[k] + dS[k] * dS[k] + dW[k] * dW[k] + dE[k] * dE[k]) / (Jc * Jc);
                let L = (dN[k] + dS[k] + dW[k] + dE[k]) / Jc;

                let num = (0.5 * G2) - ((1.0 / 16.0) * (L * L));
                let den = 1.0 + (0.25 * L);
                let qsqr = num / (den * den);

                let den = (qsqr - q0sqr) / (q0sqr * (1.0 + q0sqr));
                c[k] = 1.0 / (1.0 + den);

                if c[k] < 0.0 {
                    c[k] = 0.0;
                } else if c[k] > 1.0 {
                    c[k] = 1.0;
                }
            }
        }

        for j in 0..ncols {
            for i in 0..nrows {
                let k = i + nrows * j;
                let iS = std::cmp::min(i, nrows - 2) + 1;
                let jE = std::cmp::min(j, ncols - 2) + 1;

                let cN = c[k];
                let cS = c[iS as usize + nrows * j];
                let cW = c[k];
                let cE = c[i + nrows * jE as usize];

                let D = cN * dN[k] + cS * dS[k] + cW * dW[k] + cE * dE[k];

                image[k] = image[k] + 0.25 * lambda * D;
            }
        }
    }

    // COMPRESS
    for i in 0..nelems {
        image[i] = image[i].ln() * 255.0;
    }
}
