use juno_build::JunoCompiler;

fn main() {
    #[cfg(feature = "cuda")]
    JunoCompiler::new()
        .file_in_src("srad.jn")
        .unwrap()
        .schedule_in_src("gpu.sch")
        .unwrap()
        .build()
        .unwrap();
    #[cfg(not(feature = "cuda"))]
    JunoCompiler::new()
        .file_in_src("srad.jn")
        .unwrap()
        .schedule_in_src("cpu.sch")
        .unwrap()
        .build()
        .unwrap();
}
