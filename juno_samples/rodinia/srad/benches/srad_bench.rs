#![feature(concat_idents)]
use criterion::{criterion_group, criterion_main, Criterion};

use hercules_rt::{runner, HerculesMutBox, HerculesMutBoxTo};

juno_build::juno!("srad");

use juno_srad::*;

fn srad_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("srad bench");
    group.sample_size(10);

    let mut bench = |name, niter, nrows, ncols| {
        let mut r = runner!(srad);
        let lambda = 0.5;
        let image = "data/image.pgm".to_string();
        let Image {
            image: image_ori,
            max,
            rows: image_ori_rows,
            cols: image_ori_cols,
        } = read_graphics(image);
        let image = resize(&image_ori, image_ori_rows, image_ori_cols, nrows, ncols);
        let mut image_h = HerculesMutBox::from(image.clone());
        group.bench_function(name, |b| {
            b.iter(|| {
                async_std::task::block_on(async {
                    r.run(
                        nrows as u64,
                        ncols as u64,
                        niter as u64,
                        image_h.to(),
                        max,
                        lambda,
                    )
                    .await
                });
            })
        });
    };

    bench("srad bench small", 100, 512, 512);
    bench("srad bench large", 30, 2048, 2048);
}

criterion_group!(benches, srad_bench);
criterion_main!(benches);
