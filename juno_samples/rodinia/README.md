# Rodinia Benchmarks
This directory contains several of the benchmarks from the [Rodinia Benchmark Suite](http://www.cs.virginia.edu/rodinia/doku.php) ported into Juno.
The implementations are based on those provided with Rodinia version 3.1.
