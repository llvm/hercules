#![feature(concat_idents)]
use criterion::{criterion_group, criterion_main, Criterion};
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};

use hercules_rt::{runner, HerculesImmBox, HerculesImmBoxTo, HerculesMutBox, HerculesMutBoxTo};

juno_build::juno!("backprop");

// We need this even though we don't use anything from the library because of
// Rust build scripts only linking static libraries into the library, and not
// into the benchmark binary. Yuck!
#[allow(unused_imports)]
use juno_backprop::*;

fn backprop_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("backprop bench");
    group.sample_size(10);

    let mut rng = StdRng::seed_from_u64(7);

    let mut bench = |name, input_n: usize| {
        let hidden_n = 16;
        let output_n = 1;

        let mut input_vals = vec![0.0f32; input_n + 1];
        input_vals[0] = 1.0;

        // For some reason the bpnn_randomize_row function used on target just sets it to 0.1
        let target = vec![0.1f32; output_n + 1];

        let input_weights = (0..(input_n + 1) * (hidden_n + 1))
            .map(|_| rng.random::<f32>())
            .collect::<Vec<_>>();
        let hidden_weights = (0..(hidden_n + 1) * (output_n + 1))
            .map(|_| rng.random::<f32>())
            .collect::<Vec<_>>();

        let input_prev_weights = vec![0.0; (input_n + 1) * (hidden_n + 1)];
        let hidden_prev_weights = vec![0.0; (hidden_n + 1) * (output_n + 1)];

        let mut r = runner!(backprop);
        let input_vals = HerculesImmBox::from(&input_vals as &[f32]);
        let target = HerculesImmBox::from(&target as &[f32]);
        let mut input_weights = HerculesMutBox::from(input_weights.to_vec());
        let mut hidden_weights = HerculesMutBox::from(hidden_weights.to_vec());
        let mut input_prev_weights = HerculesMutBox::from(input_prev_weights.to_vec());
        let mut hidden_prev_weights = HerculesMutBox::from(hidden_prev_weights.to_vec());

        group.bench_function(name, |b| {
            b.iter(|| {
                async_std::task::block_on(async {
                    r.run(
                        input_n as u64,
                        hidden_n as u64,
                        output_n as u64,
                        input_vals.to(),
                        input_weights.to(),
                        hidden_weights.to(),
                        target.to(),
                        input_prev_weights.to(),
                        hidden_prev_weights.to(),
                    )
                    .await
                });
            })
        });
    };

    bench("backprop bench small", 65536);
    bench("backprop bench large", 33554432);
}

criterion_group!(benches, backprop_bench);
criterion_main!(benches);
