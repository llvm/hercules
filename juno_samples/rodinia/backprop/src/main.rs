use clap::Parser;

use juno_backprop::{backprop_harness, BackpropInputs};

fn main() {
    let args = BackpropInputs::parse();
    backprop_harness(args);
}

#[test]
fn backprop_test() {
    backprop_harness(BackpropInputs { layer_size: 65536 });
}
