fn layer_forward(n: usize, m: usize, vals: &[f32], weights: &[f32]) -> Vec<f32> {
    let mut result = vec![0.0; m + 1];
    result[0] = 1.0;

    for j in 1..=m {
        let mut sum = 0.0;
        for k in 0..=n {
            sum += weights[k * (m + 1) + j] * vals[k];
        }
        result[j] = 1.0 / (1.0 + (-sum).exp());
    }

    result
}

fn output_error(n: usize, target: &[f32], actual: &[f32]) -> (f32, Vec<f32>) {
    let mut result = vec![0.0; n + 1];
    let mut error = 0.0;

    for j in 1..=n {
        let o = actual[j];
        let t = target[j];
        result[j] = o * (1.0 - o) * (t - o);
        error += result[j].abs();
    }

    (error, result)
}

fn hidden_error(
    n: usize,
    m: usize,
    delta: &[f32],
    weights: &[f32],
    actual: &[f32],
) -> (f32, Vec<f32>) {
    let mut result = vec![0.0; n + 1];
    let mut error = 0.0;

    for j in 1..=n {
        let h = actual[j];
        let mut sum = 0.0;
        for k in 1..=m {
            sum += delta[k] * weights[j * (m + 1) + k];
        }
        result[j] = h * (1.0 - h) * sum;
        error += result[j].abs();
    }

    (error, result)
}

fn adjust_weights(
    n: usize,
    m: usize,
    delta: &[f32],
    vals: &[f32],
    mut weights: Vec<f32>,
    mut prev_weights: Vec<f32>,
) -> (Vec<f32>, Vec<f32>) {
    for j in 1..=m {
        for k in 0..=n {
            let new_dw = (0.3 * delta[j] * vals[k]) + (0.3 * prev_weights[k * (m + 1) + j]);
            weights[k * (m + 1) + j] += new_dw;
            prev_weights[k * (m + 1) + j] = new_dw;
        }
    }

    (weights, prev_weights)
}

pub fn backprop(
    input_n: usize,
    hidden_n: usize,
    output_n: usize,
    input_vals: &[f32],
    input_weights: Vec<f32>,
    hidden_weights: Vec<f32>,
    target: &[f32],
    input_prev_weights: Vec<f32>,
    hidden_prev_weights: Vec<f32>,
) -> (f32, f32, Vec<f32>, Vec<f32>, Vec<f32>, Vec<f32>) {
    let hidden_vals = layer_forward(input_n, hidden_n, input_vals, &input_weights);
    let output_vals = layer_forward(hidden_n, output_n, &hidden_vals, &hidden_weights);

    let (out_err, out_delta) = output_error(output_n, target, &output_vals);
    let (hid_err, hid_delta) = hidden_error(
        hidden_n,
        output_n,
        &out_delta,
        &hidden_weights,
        &hidden_vals,
    );

    let (hidden_weights, hidden_prev_weights) = adjust_weights(
        hidden_n,
        output_n,
        &out_delta,
        &hidden_vals,
        hidden_weights,
        hidden_prev_weights,
    );
    let (input_weights, input_prev_weights) = adjust_weights(
        input_n,
        hidden_n,
        &hid_delta,
        &input_vals,
        input_weights,
        input_prev_weights,
    );

    (
        out_err,
        hid_err,
        input_weights,
        hidden_weights,
        input_prev_weights,
        hidden_prev_weights,
    )
}
