#![feature(concat_idents)]

use hercules_rt::runner;

juno_build::juno!("patterns");

fn main() {
    async_std::task::block_on(async {
        let mut r = runner!(entry);
        let c = r.run(3, 8.0).await;
        println!("{}", c);
        assert_eq!(c, 14.0);
    });
}

#[test]
fn simple3_test() {
    main();
}
