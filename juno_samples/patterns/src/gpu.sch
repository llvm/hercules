gvn(*);
phi-elim(*);
dce(*);

let out = auto-outline(*);
gpu(out.entry);

ip-sroa(*);
sroa(*);
dce(*);
gvn(*);
phi-elim(*);
dce(*);

infer-schedules(*);

gcm(*);
