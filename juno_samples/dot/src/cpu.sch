phi-elim(dot);
ip-sroa(*);
sroa(dot);
dce(dot);

forkify(dot);
fork-guard-elim(dot);
dce(dot);

fork-tile[8, 0, false, true](dot);
fork-tile[8, 0, false, false](dot);
let split_out = fork-split(dot);
infer-schedules(*);
clean-monoid-reduces(*);
infer-schedules(*);
clean-monoid-reduces(*);

let out = outline(split_out.dot.fj1);
ip-sroa(*);
sroa(*);
gvn(*);
dce(*);

let fission_out = fork-fission[out@loop](dot);
simplify-cfg(dot);
dce(dot);
unforkify(fission_out.dot.fj_bottom);
ccp(dot);
simplify-cfg(dot);
gvn(dot);
dce(dot);
infer-schedules(dot);

unforkify(out);
ccp(out);
simplify-cfg(out);
gvn(out);
dce(out);
gcm(*);
