phi-elim(*);

forkify(*);
fork-guard-elim(*);
dce(*);

fork-tile[8, 0, false, true](*);
fork-split(*);

let out = auto-outline(*);
gpu(out.dot);
ip-sroa(*);
sroa(*);
dce(*);

unforkify(*);
gcm(*);

