#![feature(concat_idents)]
use std::iter::zip;

use rand::random;

use hercules_rt::{runner, HerculesImmBox, HerculesImmBoxTo};

juno_build::juno!("dot");

fn main() {
    async_std::task::block_on(async {
        const N: u64 = 1024 * 8;
        let a: Box<[i64]> = (0..N).map(|_| random::<i64>() % 100).collect();
        let b: Box<[i64]> = (0..N).map(|_| random::<i64>() % 100).collect();
        let a_herc = HerculesImmBox::from(&a as &[i64]);
        let b_herc = HerculesImmBox::from(&b as &[i64]);
        let mut r = runner!(dot);
        let output = r.run(N, a_herc.to(), b_herc.to()).await;
        let correct = zip(a, b).map(|(a, b)| a * b).sum();
        assert_eq!(output, correct);
    });
}

#[test]
fn dot_test() {
    main();
}
