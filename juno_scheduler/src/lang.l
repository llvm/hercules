%x comment
%%
/\*             <+comment>;

<comment>/\*    <+comment>;
<comment>\*+/   <-comment>;
<comment>\*+    ;
<comment>[\n\r] ;
<comment>.      ;

//[^\n\r]*      ;
[\t ]+          ;
[\n\r]          ;

,               ","
;               ";"
=               "="
@               "@"
\*              "*"
\.              "."

apply           "apply"
else            "else"
fixpoint        "fixpoint"
if              "if"
let             "let"
macro           "macro_keyword"
on              "on"
set             "set"
target          "target"
then            "then"

true            "true"
false           "false"

\(              "("
\)              ")"
\<              "<"
\>              ">"
\[              "["
\]              "]"
\{              "{"
\}              "}"

\\              "\\"
\|              "|"
&               "&"

!               "!"
\|\|            "||"
&&              "&&"

panic[\t \n\r]+after "panic_after"
print[\t \n\r]+iter  "print_iter"
stop[\t \n\r]+after  "stop_after"

[a-zA-Z_][a-zA-Z0-9_\-]*! "MACRO"
[a-zA-Z_][a-zA-Z0-9_\-]*  "ID"
[0-9]+                   "INT"
\"[a-zA-Z0-9_\-\s\.]*\"  "STRING"

.                     "UNMATCHED"
