use hercules_ir::ir::*;

use std::collections::HashMap;

#[derive(Debug, Copy, Clone)]
pub struct LabelInfo {
    pub func: FunctionID,
    pub label: LabelID,
}

#[derive(Debug, Copy, Clone)]
pub struct JunoFunctionID {
    pub idx: usize,
}

impl JunoFunctionID {
    pub fn new(idx: usize) -> Self {
        JunoFunctionID { idx }
    }
}

// From the Juno frontend we collect certain information we need for scheduling it, in particular a
// map from the function names to a "JunoFunctionID" which can be used to lookup the Hercules
// FunctionIDs that are definitions of that function (it may be multiple since the same Juno
// function may be instantiated with multiple difference type variables).
#[derive(Debug, Clone)]
pub struct JunoInfo {
    pub func_names: HashMap<String, JunoFunctionID>,
    pub func_info: JunoFunctions,
}

#[derive(Debug, Clone)]
pub struct JunoFunctions {
    pub func_ids: Vec<Vec<FunctionID>>,
}

impl JunoInfo {
    pub fn new<I>(funcs: I) -> Self
    where
        I: Iterator<Item = String>,
    {
        let mut func_names = HashMap::new();
        let mut func_ids = vec![];

        for (idx, name) in funcs.enumerate() {
            func_names.insert(name, JunoFunctionID::new(idx));
            func_ids.push(vec![]);
        }

        JunoInfo {
            func_names,
            func_info: JunoFunctions { func_ids },
        }
    }
}

impl JunoFunctions {
    pub fn get_function(&self, id: JunoFunctionID) -> &Vec<FunctionID> {
        &self.func_ids[id.idx]
    }
}
