use lrlex::lrlex_mod;
use lrpar::lrpar_mod;

lrlex_mod!("lang.l");
lrpar_mod!("lang.y");

pub use lang_y::*;
pub mod lexer {
    pub use super::lang_l::*;
}
