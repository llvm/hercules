use crate::ir::*;

#[macro_export]
macro_rules! pass {
    (DeleteUncalled) => {
        ScheduleStmt::Let {
            var: String::from("_"),
            exp: ScheduleExp::DeleteUncalled {
                on: Selector::Everything(),
            },
        }
    };
    ($p:ident) => {
        ScheduleStmt::Let {
            var: String::from("_"),
            exp: ScheduleExp::RunPass {
                pass: Pass::$p,
                args: vec![],
                on: Selector::Everything(),
            },
        }
    };
}

#[macro_export]
macro_rules! default_schedule {
    () => {
        ScheduleStmt::Block {
            body: vec![],
        }
    };
    ($($p:ident),+ $(,)?) => {
        ScheduleStmt::Block {
            body: vec![$(pass!($p)),+],
        }
    };
}

// Defualt schedule, which is used if no schedule is provided
pub fn default_schedule() -> ScheduleStmt {
    default_schedule![
        GVN,
        DCE,
        PhiElim,
        DCE,
        CRC,
        DCE,
        SLF,
        DCE,
        Inline,
        DeleteUncalled,
        SROA,
        PhiElim,
        DCE,
        ReuseProducts,
        DCE,
        CCP,
        SimplifyCFG,
        DCE,
        GVN,
        DCE,
        WritePredication,
        PhiElim,
        DCE,
        CRC,
        DCE,
        SLF,
        DCE,
        Predication,
        DCE,
        CCP,
        SimplifyCFG,
        DCE,
        GVN,
        DCE,
        LiftDCMath,
        DCE,
        GVN,
        DCE,
        Forkify,
        ForkGuardElim,
        ForkCoalesce,
        DCE,
        SimplifyCFG,
        DCE,
        ArraySLF,
        DCE,
        SLF,
        DCE,
        ForkSplit,
        Unforkify,
        CCP,
        SimplifyCFG,
        DCE,
        GVN,
        DCE,
        AutoOutline,
        SROA,
        ReuseProducts,
        SimplifyCFG,
        InferSchedules,
        DCE,
        GCM,
    ]
}
