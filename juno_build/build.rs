fn main() {
    #[cfg(feature = "cuda")]
    {
        println!("cargo::rustc-link-search=native=/usr/lib/x86_64-linux-gnu/");
        println!("cargo:rustc-link-search=native=/usr/local/cuda/lib64");
        println!("cargo::rustc-link-search=native=/opt/cuda/lib/");
        println!("cargo:rustc-link-lib=cudart");
    }
}
