use juno_compiler::*;

use std::env::{current_dir, var};
use std::fs::{create_dir_all, read_to_string};
use std::path::{Path, PathBuf};

// JunoCompiler is used to compile juno files into a library and manifest file appropriately to
// import the definitions into a rust project via the juno! macro defined below
// You can also specify a Hercules IR file instead of a Juno file and this will compile that IR
// file (though in that case a schedule is not supported)
pub struct JunoCompiler {
    ir_src_path: Option<PathBuf>,
    src_path: Option<PathBuf>,
    out_path: Option<PathBuf>,
    schedule: Option<String>,
}

impl JunoCompiler {
    pub fn new() -> Self {
        JunoCompiler {
            ir_src_path: None,
            src_path: None,
            out_path: None,
            schedule: None,
        }
    }

    // Sets the name of the Juno file, this file should be contained in the src/
    // file of the package that the JunoCompiler is used in the build script of
    pub fn file_in_src<P>(mut self, file: P) -> Result<Self, String>
    where
        P: AsRef<Path>,
    {
        self.src_path = Some(Self::make_src_path(&file)?);
        self.ir_src_path = None;
        self.out_path = Some(Self::get_out_path(&file)?);
        Self::print_cargo_info(file);

        Ok(self)
    }

    // Sets the name of the input Hercules IR file which is contained in the src/ directory of the
    // package that the JunoCompiler is used in the build script of
    pub fn ir_in_src<P>(mut self, file: P) -> Result<Self, String>
    where
        P: AsRef<Path>,
    {
        self.ir_src_path = Some(Self::make_src_path(&file)?);
        self.src_path = None;
        self.out_path = Some(Self::get_out_path(&file)?);
        Self::print_cargo_info(file);

        Ok(self)
    }

    // Construct the path to a file in the source directory
    fn make_src_path<P>(file: P) -> Result<PathBuf, String>
    where
        P: AsRef<Path>,
    {
        // Since the file will be in the src directory, the path must be relative
        if !file.as_ref().is_relative() {
            return Err(format!(
                "Source path '{}' must be a relative path.",
                file.as_ref().to_str().unwrap_or("<invalid UTF-8>")
            ));
        }

        let Ok(mut path) = current_dir() else {
            return Err("Failed to retrieve current directory.".to_string());
        };
        path.push("src");
        path.push(file.as_ref());

        Ok(path)
    }

    // Construct the path to the output directory, under the OUT_DIR cargo provides
    fn get_out_path<P>(file: P) -> Result<PathBuf, String>
    where
        P: AsRef<Path>,
    {
        let mut out = PathBuf::new();
        out.push(var("OUT_DIR").unwrap());
        out.push(file.as_ref().parent().unwrap().to_str().unwrap());
        let Ok(()) = create_dir_all(&out) else {
            return Err("Failed to create output directory.".to_string());
        };
        Ok(out)
    }

    // Prints information to tell cargo the following
    // - Rerun if the source file changes
    // - Add the output directory to its linker search
    // - Link the compiled library file
    // Tell cargo to rerun if the Juno file changes
    fn print_cargo_info<P>(src_file: P)
    where
        P: AsRef<Path>,
    {
        println!(
            "cargo::rerun-if-changed=src/{}",
            src_file.as_ref().to_str().unwrap()
        );
        println!(
            "cargo::rustc-link-search=native={}",
            var("OUT_DIR").unwrap()
        );
        println!(
            "cargo::rustc-link-lib=static={}",
            src_file.as_ref().file_stem().unwrap().to_str().unwrap()
        );
    }

    // Set the schedule as a schedule file in the src directory
    pub fn schedule_in_src<P>(mut self, file: P) -> Result<Self, String>
    where
        P: AsRef<Path>,
    {
        // Since the file will be in the src directory, the path must be relative
        if !file.as_ref().is_relative() {
            return Err(format!(
                "Schedule path '{}' must be a relative path.",
                file.as_ref().to_str().unwrap_or("<invalid UTF-8>")
            ));
        }

        let Ok(mut path) = current_dir() else {
            return Err("Failed to retrieve current directory.".to_string());
        };
        path.push("src");
        path.push(file.as_ref());
        self.schedule = Some(path.to_str().unwrap().to_string());

        // Tell cargo to rerun if the schedule changes
        println!(
            "cargo::rerun-if-changed=src/{}",
            file.as_ref().to_str().unwrap()
        );

        Ok(self)
    }

    // Builds the juno file into a libary and a manifest file.
    pub fn build(self) -> Result<(), String> {
        let JunoCompiler {
            ir_src_path,
            src_path,
            out_path: Some(out_path),
            schedule,
        } = self
        else {
            return Err("No source file specified.".to_string());
        };

        let out_dir = out_path.to_str().unwrap().to_string();

        if let Some(src_path) = src_path {
            let src_file = src_path.to_str().unwrap().to_string();

            match compile(src_file, schedule, out_dir) {
                Ok(()) => Ok(()),
                Err(errs) => Err(format!("{}", errs)),
            }
        } else {
            let Some(ir_src_path) = ir_src_path else {
                return Err("No source file specified.".to_string());
            };

            let ir_src_file = ir_src_path.to_str().unwrap().to_string();
            let ir_src_path = Path::new(&ir_src_file);
            let module_name = String::from(ir_src_path.file_stem().unwrap().to_str().unwrap());

            let Ok(contents) = read_to_string(&ir_src_path) else {
                return Err("Unable to open and read input file.".to_string());
            };
            let Ok(ir_mod) = hercules_ir::parse::parse(&contents) else {
                return Err("Unable to parse Hercules IR file.".to_string());
            };

            match compile_ir(ir_mod, schedule, out_dir, module_name) {
                Ok(()) => Ok(()),
                Err(errs) => Err(format!("{}", errs)),
            }
        }
    }
}

// The juno!(filename) macro impots the definitions from the manifest which is
// compiled using the JunoCompiler object (in a build.rs file)
#[macro_export]
macro_rules! juno {
    ($path:expr) => {
        with_builtin_macros::with_builtin!(
            let $hrt = concat!(env!("OUT_DIR"), "/rt_", $path, ".hrt") in {
            include!($hrt);
        });
    };
}
