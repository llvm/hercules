use core::panic;
use std::collections::HashSet;

use bitvec::prelude::*;
use nestify::nest;

use hercules_ir::*;

use crate::*;

#[derive(Debug)]
pub struct LoopVarianceInfo {
    pub loop_header: NodeID,
    pub map: DenseNodeMap<LoopVariance>,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LoopVariance {
    Unknown,
    Invariant,
    Variant,
}

type NodeVec = BitVec<u8, Lsb0>;

#[derive(Clone, Debug)]
pub struct Loop {
    pub header: NodeID,
    pub control: NodeVec, //
}

impl Loop {
    pub fn get_all_nodes(&self) -> NodeVec {
        let mut all_loop_nodes = self.control.clone();
        all_loop_nodes.set(self.header.idx(), true);
        all_loop_nodes
    }
}

nest! {
    #[derive(Clone, Copy, Debug, PartialEq)]*
    pub enum InductionVariable {
        pub Basic {
            node: NodeID,
            initializer: NodeID,
            update_expression: NodeID,
            update_value: NodeID,
            final_value: Option<NodeID>,
        },
        SCEV(NodeID), // TODO @(xrouth)
    }
}

impl InductionVariable {
    pub fn phi(&self) -> NodeID {
        match self {
            InductionVariable::Basic {
                node,
                initializer: _,
                update_expression: _,
                update_value: _,
                final_value: _,
            } => *node,
            InductionVariable::SCEV(_) => todo!(),
        }
    }
}

// TODO: Optimize.
pub fn calculate_loop_nodes(editor: &FunctionEditor, natural_loop: &Loop) -> HashSet<NodeID> {
    // Stop on PHIs / reduces outside of loop.
    let stop_on: HashSet<NodeID> = editor
        .node_ids()
        .filter(|node| {
            let data = &editor.func().nodes[node.idx()];

            // External Phi
            if let Node::Phi { control, data: _ } = data {
                match natural_loop.control.get(control.idx()) {
                    Some(v) => {
                        if !*v {
                            return true;
                        }
                    }
                    None => {
                        panic!(
                            "unexpceted index: {:?} for loop {:?}",
                            control, natural_loop.header
                        );
                    }
                }
            }
            // External Reduce
            if let Node::Reduce {
                control,
                init: _,
                reduct: _,
            } = data
            {
                match natural_loop.control.get(control.idx()) {
                    Some(v) => {
                        if !*v {
                            return true;
                        }
                    }
                    None => {
                        panic!(
                            "unexpceted index: {:?} for loop {:?}",
                            control, natural_loop.header
                        );
                    }
                }
            }

            // External Control
            if data.is_control() {
                match natural_loop.control.get(node.idx()) {
                    Some(v) => {
                        if !*v {
                            return true;
                        }
                    }
                    None => {
                        panic!(
                            "unexpceted index: {:?} for loop {:?}",
                            node, natural_loop.header
                        );
                    }
                }
            }

            return false;
        })
        .collect();

    let phis: Vec<_> = editor
        .node_ids()
        .filter(|node| {
            let Node::Phi { control, data: _ } = editor.func().nodes[node.idx()] else {
                return false;
            };
            natural_loop.control[control.idx()]
        })
        .collect();

    let all_users: HashSet<NodeID> = phis
        .clone()
        .iter()
        .flat_map(|phi| walk_all_users_stop_on(*phi, editor, stop_on.clone()))
        .chain(phis.clone())
        .collect();

    let all_uses: HashSet<_> = phis
        .clone()
        .iter()
        .flat_map(|phi| walk_all_uses_stop_on(*phi, editor, stop_on.clone()))
        .chain(phis.clone())
        .filter(|node| {
            // Get rid of nodes in stop_on
            !stop_on.contains(node)
        })
        .collect();

    all_users
        .intersection(&all_uses)
        .chain(phis.iter())
        .cloned()
        .collect()
}

/** returns PHIs that are on any regions inside the loop. */
pub fn get_all_loop_phis<'a>(
    function: &'a Function,
    l: &'a Loop,
) -> impl Iterator<Item = NodeID> + 'a {
    function
        .nodes
        .iter()
        .enumerate()
        .filter_map(move |(node_id, node)| {
            if let Some((control, _)) = node.try_phi() {
                if l.control[control.idx()] {
                    Some(NodeID::new(node_id))
                } else {
                    None
                }
            } else {
                None
            }
        })
}

// FIXME: Need a trait that Editor and Function both implement, that gives us UseDefInfo

/** Given a loop determine for each data node if the value might change upon each iteration of the loop */
pub fn compute_loop_variance(editor: &FunctionEditor, l: &Loop) -> LoopVarianceInfo {
    // Gather all Phi nodes that are controlled by this loop.
    let mut loop_vars: Vec<NodeID> = vec![];

    for node_id in editor.get_users(l.header) {
        let node = &editor.func().nodes[node_id.idx()];
        if let Some((control, _)) = node.try_phi() {
            if l.control[control.idx()] {
                loop_vars.push(node_id);
            }
        }
    }

    let len = editor.func().nodes.len();

    let mut all_loop_nodes = l.control.clone();

    all_loop_nodes.set(l.header.idx(), true);

    let mut variance_map: DenseNodeMap<LoopVariance> = vec![LoopVariance::Unknown; len];

    fn recurse(
        function: &Function,
        node: NodeID,
        all_loop_nodes: &BitVec<u8, Lsb0>,
        variance_map: &mut DenseNodeMap<LoopVariance>,
        visited: &mut DenseNodeMap<bool>,
    ) -> LoopVariance {
        if visited[node.idx()] {
            return variance_map[node.idx()];
        }

        visited[node.idx()] = true;

        let node_variance = match variance_map[node.idx()] {
            LoopVariance::Invariant => LoopVariance::Invariant,
            LoopVariance::Variant => LoopVariance::Variant,
            LoopVariance::Unknown => {
                let mut node_variance = LoopVariance::Invariant;

                // Two conditions cause something to be loop variant:
                for node_use in get_uses(&function.nodes[node.idx()]).as_ref() {
                    // 1) The use is a PHI *controlled* by the loop
                    if let Some((control, _)) = function.nodes[node_use.idx()].try_phi() {
                        if *all_loop_nodes.get(control.idx()).unwrap() {
                            node_variance = LoopVariance::Variant;
                            break;
                        }
                    }

                    // 2) Any of the nodes uses are loop variant
                    if recurse(function, *node_use, all_loop_nodes, variance_map, visited)
                        == LoopVariance::Variant
                    {
                        node_variance = LoopVariance::Variant;
                        break;
                    }
                }

                variance_map[node.idx()] = node_variance;

                node_variance
            }
        };

        return node_variance;
    }

    let mut visited: DenseNodeMap<bool> = vec![false; len];

    for node in (0..len).map(NodeID::new) {
        recurse(
            editor.func(),
            node,
            &all_loop_nodes,
            &mut variance_map,
            &mut visited,
        );
    }

    return LoopVarianceInfo {
        loop_header: l.header,
        map: variance_map,
    };
}

nest! {
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LoopExit {
    Conditional {
        if_node: NodeID,
        condition_node: NodeID,
    },
    Unconditional(NodeID)
}
}

pub fn get_loop_exit_conditions(
    function: &Function,
    l: &Loop,
    control_subgraph: &Subgraph,
) -> Option<LoopExit> {
    // impl IntoIterator<Item = LoopExit>
    // DFS Traversal on loop control subgraph until we find a node that is outside the loop, find the last IF on this path.
    let mut last_if_on_path: DenseNodeMap<Option<NodeID>> = vec![None; function.nodes.len()];

    // FIXME: (@xrouth) THIS IS MOST CERTAINLY BUGGED
    // this might be bugged... i.e might need to udpate `last if` even if already defined.
    // needs to be `saturating` kinda, more iterative. May need to visit nodes more than once?

    // FIXME: (@xrouth) Right now we assume only one exit from the loop, later: check for multiple exits on the loop,
    // either as an assertion here or some other part of forkify or analysis.
    let mut bag_of_control_nodes = vec![l.header];
    let mut visited: DenseNodeMap<bool> = vec![false; function.nodes.len()];

    let mut final_if: Option<NodeID> = None;

    // do WFS
    while !bag_of_control_nodes.is_empty() {
        let node = bag_of_control_nodes.pop().unwrap();
        if visited[node.idx()] {
            continue;
        }
        visited[node.idx()] = true;

        final_if = if function.nodes[node.idx()].is_if() {
            Some(node)
        } else {
            last_if_on_path[node.idx()]
        };

        if !l.control[node.idx()] {
            break;
        }

        for succ in control_subgraph.succs(node) {
            last_if_on_path[succ.idx()] = final_if;
            bag_of_control_nodes.push(succ.clone());
        }
    }

    final_if.map(|v| LoopExit::Conditional {
        if_node: v,
        condition_node: if let Node::If { control: _, cond } = function.nodes[v.idx()] {
            cond
        } else {
            unreachable!()
        },
    })
}

pub fn has_const_fields(editor: &FunctionEditor, ivar: InductionVariable) -> bool {
    match ivar {
        InductionVariable::Basic {
            node: _,
            initializer,
            final_value,
            update_expression,
            update_value,
        } => {
            if final_value.is_none() {
                return false;
            }
            [initializer, update_value]
                .iter()
                .any(|node| !editor.node(node).is_constant())
        }
        InductionVariable::SCEV(_) => false,
    }
}

/* Loop has any IV from range 0....N, N can be dynconst iterates +1 per iteration */
// IVs need to be bounded...
pub fn has_canonical_iv<'a>(
    editor: &FunctionEditor,
    _l: &Loop,
    ivs: &'a [InductionVariable],
) -> Option<&'a InductionVariable> {
    ivs.iter().find(|iv| match iv {
        InductionVariable::Basic {
            node: _,
            initializer,
            final_value,
            update_expression,
            update_value,
        } => {
            (editor
                .node(initializer)
                .is_zero_constant(&editor.get_constants())
                || editor
                    .node(initializer)
                    .is_zero_dc(&editor.get_dynamic_constants()))
                && (editor
                    .node(update_value)
                    .is_one_constant(&editor.get_constants())
                    || editor
                        .node(update_value)
                        .is_one_dc(&editor.get_dynamic_constants()))
                && (final_value
                    .map(|val| {
                        editor.node(val).is_constant() || editor.node(val).is_dynamic_constant()
                    })
                    .is_some())
        }
        InductionVariable::SCEV(_) => false,
    })
}

// Need a transformation that forces all IVs to be SCEVs of an IV from range 0...N, +1, else places them in a separate loop?
pub fn compute_induction_vars(
    function: &Function,
    l: &Loop,
    _loop_variance: &LoopVarianceInfo,
) -> Vec<InductionVariable> {
    // 1) Gather PHIs contained in the loop.
    // FIXME: (@xrouth) Should this just be PHIs controlled by the header?
    let mut loop_vars: Vec<NodeID> = vec![];

    for (node_id, node) in function.nodes.iter().enumerate() {
        if let Some((control, _)) = node.try_phi() {
            if l.control[control.idx()] {
                loop_vars.push(NodeID::new(node_id));
            }
        }
    }

    // FIXME: (@xrouth) For now, only compute variables that have one assignment,
    // (look into this:) possibly treat multiple assignment as separate induction variables.
    let mut induction_variables: Vec<InductionVariable> = vec![];

    /* For each PHI controlled by the loop, check how it is modified */

    // It's initializer needs to be loop invariant, it's update needs to be loop variant.
    for phi_id in loop_vars {
        let phi_node = &function.nodes[phi_id.idx()];
        let (region, data) = phi_node.try_phi().unwrap();
        let region_node = &function.nodes[region.idx()];
        let Node::Region {
            preds: region_inputs,
        } = region_node
        else {
            continue;
        };

        // The initializer index is the first index of the inputs to the region node of that isn't in the loop. (what is loop_header, wtf...)
        // FIXME (@xrouth): If there is control flow in the loop, we won't find ... WHAT
        let Some(initializer_idx) = region_inputs
            .iter()
            .position(|&node_id| !l.control[node_id.idx()])
        else {
            continue;
        };

        let initializer_id = data[initializer_idx];

        // Check dynamic constancy:
        let initializer = &function.nodes[initializer_id.idx()];

        // In the case of a non 0 starting value:
        // - a new dynamic constant or constant may need to be created that is the difference between the initiailizer and the loop bounds.
        // Initializer does not necessarily have to be constant, but this is fine for now.
        if !(initializer.is_dynamic_constant() || initializer.is_constant()) {
            continue;
        }

        // Check all data inputs to this phi, that aren't the initializer (i.e the value the comes from control outside of the loop)
        // For now we expect only one initializer.
        let data_inputs = data
            .iter()
            .filter(|data_id| NodeID::new(initializer_idx) != **data_id);

        for data_id in data_inputs {
            let node = &function.nodes[data_id.idx()];
            for bop in [BinaryOperator::Add] {
                //, BinaryOperator::Mul, BinaryOperator::Sub] {
                if let Some((a, b)) = node.try_binary(bop) {
                    let iv = [(a, b), (b, a)]
                        .iter()
                        .find_map(|(pattern_phi, pattern_const)| {
                            if *pattern_phi == phi_id
                                && function.nodes[pattern_const.idx()].is_constant()
                                || function.nodes[pattern_const.idx()].is_dynamic_constant()
                            {
                                return Some(InductionVariable::Basic {
                                    node: phi_id,
                                    initializer: initializer_id,
                                    final_value: None,
                                    update_expression: *data_id,
                                    update_value: b,
                                });
                            } else {
                                None
                            }
                        });
                    if let Some(iv) = iv {
                        induction_variables.push(iv);
                    }
                }
            }
        }
    }

    induction_variables
}

pub fn get_loop_condition_ivs(
    editor: &FunctionEditor,
    l: &Loop,
    induction_vars: &Vec<InductionVariable>,
    loop_condition: &LoopExit,
) -> HashSet<NodeID> {
    let condition_node = match loop_condition {
        LoopExit::Conditional {
            if_node: _,
            condition_node,
        } => condition_node,
        LoopExit::Unconditional(_) => todo!(),
    };

    // Find IVs used by the loop condition, not across loop iterations.
    // without leaving the loop.
    let stop_on: HashSet<_> = editor
        .node_ids()
        .filter(|node_id| {
            if let Node::Phi { control, data: _ } = editor.node(node_id) {
                *control == l.header
            } else {
                false
            }
        })
        .collect();

    // Bound IVs used in loop bound.
    let loop_bound_uses: HashSet<_> =
        walk_all_uses_stop_on(*condition_node, editor, stop_on).collect();

    HashSet::from_iter(induction_vars.iter().filter_map(|iv| {
        if loop_bound_uses.contains(&iv.phi()) {
            Some(iv.phi())
        } else {
            None
        }
    }))
}

// Find loop iterations
pub fn compute_iv_ranges(
    editor: &FunctionEditor,
    l: &Loop,
    induction_vars: Vec<InductionVariable>,
    loop_condition: &LoopExit,
) -> Vec<InductionVariable> {
    let condition_node = match loop_condition.clone() {
        LoopExit::Conditional {
            if_node: _,
            condition_node,
        } => condition_node,
        LoopExit::Unconditional(_) => todo!(),
    };

    let loop_bound_iv_phis = get_loop_condition_ivs(editor, l, &induction_vars, loop_condition);

    let (loop_bound_ivs, other_ivs): (Vec<InductionVariable>, Vec<InductionVariable>) =
        induction_vars
            .into_iter()
            .partition(|f| loop_bound_iv_phis.contains(&f.phi()));

    // Assume there is only one loop bound iv.
    let Some(iv) = loop_bound_ivs.first() else {
        return other_ivs;
    };

    if loop_bound_ivs.len() > 1 {
        return loop_bound_ivs.into_iter().chain(other_ivs).collect();
    }

    // Bound IVs used in the loop condition.

    // FIXME: DO linear algerbra to solve for loop bounds with multiple variables involved.
    let final_value = match &editor.func().nodes[condition_node.idx()] {
        Node::Phi {
            control: _,
            data: _,
        } => None,
        Node::Reduce {
            control: _,
            init: _,
            reduct: _,
        } => None,
        Node::Parameter { index: _ } => None,
        Node::Constant { id: _ } => None,
        Node::Unary { input: _, op: _ } => None,
        Node::Ternary {
            first: _,
            second: _,
            third: _,
            op: _,
        } => None,
        Node::Binary { left, right, op } => {
            match op {
                BinaryOperator::LT => {
                    // Check for a loop guard condition.
                    // left < right
                    if *left == iv.phi()
                        && (editor.func().nodes[right.idx()].is_constant()
                            || editor.func().nodes[right.idx()].is_dynamic_constant())
                    {
                        Some(*right)
                    }
                    // left + const < right,
                    else if let Node::Binary {
                        left: inner_left,
                        right: inner_right,
                        op: _,
                    } = editor.node(left)
                    {
                        let pattern = [(inner_left, inner_right), (inner_right, inner_left)]
                            .iter()
                            .find_map(|(pattern_iv, pattern_constant)| {
                                if iv.phi() == **pattern_iv
                                    && (editor.node(*pattern_constant).is_constant()
                                        || editor.node(*pattern_constant).is_dynamic_constant())
                                {
                                    // FIXME: pattern_constant can be anything >= loop_update expression,
                                    let update = match iv {
                                        InductionVariable::Basic {
                                            node: _,
                                            initializer: _,
                                            final_value: _,
                                            update_expression: _,
                                            update_value: update,
                                        } => update,
                                        InductionVariable::SCEV(_) => todo!(),
                                    };
                                    if *pattern_constant == update {
                                        Some(*right)
                                    } else {
                                        None
                                    }
                                } else {
                                    None
                                }
                            });
                        pattern.iter().cloned().next()
                    } else {
                        None
                    }
                }
                BinaryOperator::LTE => None,
                BinaryOperator::GT => None,
                BinaryOperator::GTE => None,
                BinaryOperator::EQ => None,
                BinaryOperator::NE => None,
                _ => None,
            }
        }
        _ => None,
    };

    let basic = match iv {
        InductionVariable::Basic {
            node,
            initializer,
            final_value: _,
            update_expression,
            update_value,
        } => InductionVariable::Basic {
            node: *node,
            initializer: *initializer,
            update_expression: *update_expression,
            update_value: *update_value,
            final_value,
        },
        InductionVariable::SCEV(_) => todo!(),
    };

    // Propagate bounds to other IVs.
    vec![basic].into_iter().chain(other_ivs).collect()
}
