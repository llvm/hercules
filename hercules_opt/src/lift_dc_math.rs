use hercules_ir::ir::*;

use crate::*;

/*
 * Lift math in IR nodes into dynamic constants.
 */
pub fn lift_dc_math(editor: &mut FunctionEditor) {
    // Create worklist (starts as all nodes).
    let mut worklist: Vec<NodeID> = editor.node_ids().collect();
    while let Some(work) = worklist.pop() {
        // Look for single nodes that can be converted to dynamic constants.
        let users: Vec<_> = editor.get_users(work).collect();
        let nodes = &editor.func().nodes;
        let dc = match nodes[work.idx()] {
            Node::Constant { id } => {
                // Why do we need this weird crap? This is due to a limitation
                // in Rust's lifetime rules w/ let guards.
                let cons = if let Constant::UnsignedInteger64(cons) = *editor.get_constant(id) {
                    cons
                } else {
                    continue;
                };
                DynamicConstant::Constant(cons as usize)
            }
            Node::Binary { op, left, right } => {
                let (left, right) = if let (
                    Node::DynamicConstant { id: left },
                    Node::DynamicConstant { id: right },
                ) = (&nodes[left.idx()], &nodes[right.idx()])
                {
                    (*left, *right)
                } else {
                    continue;
                };
                match op {
                    BinaryOperator::Add => DynamicConstant::add(left, right),
                    BinaryOperator::Sub => DynamicConstant::sub(left, right),
                    BinaryOperator::Mul => DynamicConstant::mul(left, right),
                    BinaryOperator::Div => DynamicConstant::div(left, right),
                    BinaryOperator::Rem => DynamicConstant::rem(left, right),
                    _ => {
                        continue;
                    }
                }
            }
            Node::IntrinsicCall {
                intrinsic,
                ref args,
            } => {
                let (left, right) = if args.len() == 2
                    && let (Node::DynamicConstant { id: left }, Node::DynamicConstant { id: right }) =
                        (&nodes[args[0].idx()], &nodes[args[1].idx()])
                {
                    (*left, *right)
                } else {
                    continue;
                };
                match intrinsic {
                    Intrinsic::Min => DynamicConstant::min(left, right),
                    Intrinsic::Max => DynamicConstant::max(left, right),
                    _ => {
                        continue;
                    }
                }
            }
            _ => {
                continue;
            }
        };

        // Replace the node with the computed dynamic constant.
        let success = editor.edit(|mut edit| {
            let dc = edit.add_dynamic_constant(dc);
            let node = edit.add_node(Node::DynamicConstant { id: dc });
            edit = edit.replace_all_uses(work, node)?;
            edit.delete_node(work)
        });
        if success {
            worklist.extend(users);
        }
    }
}
