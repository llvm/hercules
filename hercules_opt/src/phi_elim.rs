use std::collections::VecDeque;
use std::iter::FromIterator;

use bitvec::prelude::*;

use hercules_ir::ir::*;

use crate::*;

/*
 * This is a Hercules IR transformation that:
 * - Eliminates phi nodes where all inputs are the same (here this means the
 *   same node in IR, GVN and related are separate).
 * - Eliminate regions with only a single predecessor.
 *
 * The first of these optimizations is inspired by the description of the SSA
 * construction algorithm in Braun et al., CC 2013 used by the front-end. This
 * optimization performs the phi removal suggested by that paper, as in our
 * construction algorithm performing it during IR code generation is difficult.
 */

/*
 * Top level function to run phi elimination, as described above. Deletes nodes
 * by setting nodes to gravestones. Works with a function already containing
 * gravestones.
 */
pub fn phi_elim(editor: &mut FunctionEditor) {
    // Determine region nodes that can't be removed, because they have a call
    // user.
    let mut has_call_user = bitvec![u8, Lsb0; 0; editor.func().nodes.len()];
    for idx in 0..editor.func().nodes.len() {
        if let Node::Call {
            control,
            function: _,
            dynamic_constants: _,
            args: _,
        } = editor.func().nodes[idx]
        {
            assert!(
                !has_call_user[control.idx()],
                "PANIC: Found region node with two call users ({:?}).",
                control
            );
            has_call_user.set(control.idx(), true);
        }
    }

    // Iterate over nodes in the function using a worklist. When a phi or region
    // is removed, it's users need to be re-added to the worklist.
    let mut worklist = VecDeque::from_iter(editor.node_ids());
    while let Some(id) = worklist.pop_front() {
        let node = &editor.func().nodes[id.idx()];
        if let Node::Phi { control: _, data } = node {
            // Determine if this phi can be removed.
            let mut unique = Some(data[0]);
            for use_id in &data[1..] {
                if unique == Some(id) {
                    // Ignore self-loops.
                    unique = Some(*use_id);
                } else if *use_id != id && Some(*use_id) != unique {
                    // If there are two uses that are not equal and are not
                    // self-loops, then there's not a unique predecessor.
                    unique = None;
                    break;
                }
            }

            // If there's a unique data use, remove the phi and replace uses of
            // the phi with uses of the unique data use.
            if let Some(unique_use_id) = unique {
                let success = editor.edit(|mut edit| {
                    edit = edit.replace_all_uses(id, unique_use_id)?;
                    edit.delete_node(id)
                });
                if success {
                    worklist.extend(editor.get_users(unique_use_id));
                }
            }
        } else if let Node::Region { preds } = node {
            // Regions can be removed if they have one predecessor and aren't a
            // call region.
            if preds.len() == 1 && !has_call_user[id.idx()] {
                let pred = preds[0];
                let success = editor.edit(|mut edit| {
                    edit = edit.replace_all_uses(id, pred)?;
                    edit.delete_node(id)
                });
                if success {
                    worklist.extend(editor.get_users(pred));
                }
            }
        }
    }
}
