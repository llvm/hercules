use hercules_ir::*;

use crate::*;

/*
 * Top level function to collapse read chains in a function.
 */
pub fn crc(editor: &mut FunctionEditor) {
    let mut changed = true;
    while changed {
        changed = false;
        for id in editor.node_ids() {
            if let Node::Read {
                collect: lower_collect,
                indices: ref lower_indices,
            } = editor.func().nodes[id.idx()]
                && let Node::Read {
                    collect: upper_collect,
                    indices: ref upper_indices,
                } = editor.func().nodes[lower_collect.idx()]
            {
                let collapsed_read = Node::Read {
                    collect: upper_collect,
                    indices: upper_indices
                        .iter()
                        .chain(lower_indices.iter())
                        .map(|idx| idx.clone())
                        .collect(),
                };
                let success = editor.edit(|mut edit| {
                    let new_id = edit.add_node(collapsed_read);
                    let edit = edit.replace_all_uses(id, new_id)?;
                    edit.delete_node(id)
                });
                changed = changed || success;
            }
        }
    }
}
