use std::collections::{HashMap, HashSet};
use std::iter::zip;

use crate::*;

/*
 * Math expressions are stored as a simple tree.
 */
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ForkDimension(pub usize, pub DynamicConstantID);

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum MathExpr {
    // Zero constant of a specific type.
    Zero(TypeID),
    // One constant of a specific type.
    One(TypeID),
    // Opaque value corresponding to a particular node in the original IR.
    OpaqueNode(NodeID),

    // Thread ID from the fork corresponding to the reduce being expressed.
    // Thread IDs from outside forks are considered OpaqueNodes.
    ThreadID(ForkDimension),

    // Sum reduction over a dimension of a fork.
    SumReduction(MathID, Box<[ForkDimension]>),
    // Comprehend a scalar expression into an array over fork dimensions.
    Comprehension(MathID, Box<[ForkDimension]>),

    // Read from an array.
    Read(MathID, Box<[MathID]>),

    // Math ops.
    Unary(UnaryOperator, MathID),
    Binary(BinaryOperator, MathID, MathID),
    Ternary(TernaryOperator, MathID, MathID, MathID),
    IntrinsicFunc(Intrinsic, Box<[MathID]>),
}

pub type MathEnv = Vec<MathExpr>;

define_id_type!(MathID);

/*
 * Top level function to run "einsum" analysis on fork-joins. This is a terrible
 * name for this analysis, since it's actually more general than identifying
 * einsums, but einsum syntax has a passing resemblance to the idea of this
 * analysis and it's what we keep calling it, so we're doomed to this bad name.
 * The idea of this analysis is to convert some fork-joins into pure math
 * expressions that we can rewrite into intrinsic functions for higher level
 * operators like matmul. Specifically, this function returns a map from each
 * reduce node to a math expression.
 */
pub fn einsum(
    function: &Function,
    types: &Vec<Type>,
    constants: &Vec<Constant>,
    def_use: &ImmutableDefUseMap,
    typing: &Vec<TypeID>,
    fork_join_map: &HashMap<NodeID, NodeID>,
    fork_join_nest: &HashMap<NodeID, Vec<NodeID>>,
    nodes_in_fork_joins: &HashMap<NodeID, HashSet<NodeID>>,
) -> (MathEnv, HashMap<NodeID, MathID>) {
    let mut env = vec![];
    let mut rev_env = HashMap::new();
    let mut result = HashMap::new();

    // Iterate fork-joins bottom-up, since we need to compute the math
    // expressions of inner reduces before getting to outer reduces. Since fork-
    // joins are strictly nested, we can literally iterate entries of
    // `fork_join_nest` in decreasing order of nesting size to accomplish this.
    let mut nests: Vec<_> = fork_join_nest
        .into_iter()
        .filter(|(id, _)| function.nodes[id.idx()].is_fork())
        .collect();
    nests.sort_by(|a, b| b.1.len().cmp(&a.1.len()));
    for fork in nests.into_iter().map(|(id, _)| *id) {
        let Node::Fork {
            control: _,
            ref factors,
        } = function.nodes[fork.idx()]
        else {
            panic!()
        };
        let join = fork_join_map[&fork];
        let thread_ids: Vec<_> = def_use
            .get_users(fork)
            .into_iter()
            .filter_map(|id| {
                function.nodes[id.idx()]
                    .try_thread_id()
                    .map(|(_, dim)| (*id, dim))
            })
            .collect();
        let reduces = def_use
            .get_users(join)
            .into_iter()
            .filter_map(|id| function.nodes[id.idx()].try_reduce().map(|v| (*id, v)));
        let mut ctx = EinsumContext {
            function,
            typing,
            constants,
            nodes_in_fork_joins,
            fork,
            factors,
            thread_ids: &thread_ids,
            so_far: &mut result,
            env: &mut env,
            rev_env: &mut rev_env,
        };

        // Compute a math expression for each reduce node in the fork-join with
        // appropriate schedules.
        for (reduce, (_, init, reduct)) in reduces {
            // The reduce defines an array where each fork dimension corresponds
            // to one array dimension.
            if function.schedules[reduce.idx()].contains(&Schedule::ParallelReduce)
                && let Node::Write {
                    collect,
                    data,
                    ref indices,
                } = function.nodes[reduct.idx()]
                && collect == reduce
                && indices.len() == 1
                && let Some(indices) = indices[0].try_position()
                && let Some(dimension_bounds) = indices
                    .into_iter()
                    .map(|id| {
                        function.nodes[id.idx()]
                            .try_thread_id()
                            .filter(|(tid_fork, _)| *tid_fork == fork)
                            .map(|(_, dim)| dim)
                    })
                    .collect::<Option<Vec<usize>>>()
                && let Type::Array(_, ref array_bounds) = types[typing[reduce.idx()].idx()]
                && zip(array_bounds.into_iter(), dimension_bounds.iter())
                    .all(|(array, fork)| *array == factors[*fork])
            {
                let data_expr = ctx.compute_math_expr(data);
                let reduce_expr = MathExpr::Comprehension(
                    data_expr,
                    dimension_bounds
                        .into_iter()
                        .map(|dim| ForkDimension(dim, factors[dim]))
                        .collect(),
                );
                // We don't need to consider the initializer, since the writes
                // cover the whole array.
                let total_id = ctx.intern_math_expr(reduce_expr);
                ctx.result_insert(reduce, total_id);
            }
            // The reduce defines a sum reduction over a set of fork dimensions.
            else if let Node::Binary {
                op: BinaryOperator::Add,
                left,
                right,
            } = function.nodes[reduct.idx()]
                && ((left == reduce) ^ (right == reduce))
            {
                let data_expr = ctx.compute_math_expr(if left == reduce { right } else { left });
                let reduce_expr = MathExpr::SumReduction(
                    data_expr,
                    factors
                        .into_iter()
                        .enumerate()
                        .map(|(dim, factor)| ForkDimension(dim, *factor))
                        .collect(),
                );
                // Add the initializer.
                let reduce_expr_id = ctx.intern_math_expr(reduce_expr);
                let init_expr_id = ctx.compute_math_expr(init);
                let add_expr = MathExpr::Binary(BinaryOperator::Add, init_expr_id, reduce_expr_id);
                let total_id = ctx.intern_math_expr(add_expr);
                ctx.result_insert(reduce, total_id);
            }
        }
    }

    (env, result)
}

struct EinsumContext<'a> {
    function: &'a Function,
    typing: &'a Vec<TypeID>,
    constants: &'a Vec<Constant>,
    nodes_in_fork_joins: &'a HashMap<NodeID, HashSet<NodeID>>,
    fork: NodeID,
    factors: &'a [DynamicConstantID],
    thread_ids: &'a Vec<(NodeID, usize)>,
    so_far: &'a mut HashMap<NodeID, MathID>,
    env: &'a mut MathEnv,
    rev_env: &'a mut HashMap<MathExpr, MathID>,
}

impl<'a> EinsumContext<'a> {
    fn compute_math_expr(&mut self, id: NodeID) -> MathID {
        let math_expr = match self.function.nodes[id.idx()] {
            Node::Constant { id: cons_id } if self.constants[cons_id.idx()].is_zero() => {
                MathExpr::Zero(self.typing[id.idx()])
            }
            Node::Constant { id: cons_id } if self.constants[cons_id.idx()].is_one() => {
                MathExpr::One(self.typing[id.idx()])
            }
            Node::ThreadID { control, dimension } if control == self.fork => {
                MathExpr::ThreadID(ForkDimension(dimension, self.factors[dimension]))
            }
            Node::Unary { op, input } => {
                let input = self.compute_math_expr(input);
                MathExpr::Unary(op, input)
            }
            Node::Binary { op, left, right } => {
                let left = self.compute_math_expr(left);
                let right = self.compute_math_expr(right);
                MathExpr::Binary(op, left, right)
            }
            Node::Ternary {
                op,
                first,
                second,
                third,
            } => {
                let first = self.compute_math_expr(first);
                let second = self.compute_math_expr(second);
                let third = self.compute_math_expr(third);
                MathExpr::Ternary(op, first, second, third)
            }
            Node::IntrinsicCall {
                intrinsic,
                ref args,
            } => {
                let args = args
                    .into_iter()
                    .map(|id| self.compute_math_expr(*id))
                    .collect();
                MathExpr::IntrinsicFunc(intrinsic, args)
            }
            Node::Read {
                collect,
                ref indices,
            } if indices.len() == 1
                && let Some(indices) = indices[0].try_position() =>
            {
                let collect = self.compute_math_expr(collect);
                let indices = indices
                    .into_iter()
                    .map(|id| self.compute_math_expr(*id))
                    .collect();
                MathExpr::Read(collect, indices)
            }
            Node::Reduce {
                control: _,
                init: _,
                reduct: _,
            } if let Some(reduce) = self.so_far.get(&id) => {
                // Substitute opaque uses of thread ID nodes in inner expression
                // with thread ID math expression, and increment inner-fork
                // dimensions (alpha renaming).
                return self.substitute_new_dims(*reduce);
            }
            _ => MathExpr::OpaqueNode(id),
        };
        self.intern_math_expr(math_expr)
    }

    fn intern_math_expr(&mut self, expr: MathExpr) -> MathID {
        if let Some(id) = self.rev_env.get(&expr) {
            *id
        } else {
            let id = MathID::new(self.env.len());
            self.env.push(expr.clone());
            self.rev_env.insert(expr, id);
            id
        }
    }

    fn result_insert(&mut self, node: NodeID, math: MathID) {
        self.so_far.insert(node, math);
    }

    fn substitute_new_dims(&mut self, id: MathID) -> MathID {
        match self.env[id.idx()] {
            MathExpr::OpaqueNode(opaque)
                if let Some((_, dim)) = self
                    .thread_ids
                    .into_iter()
                    .filter(|(node, _)| *node == opaque)
                    .next() =>
            {
                self.intern_math_expr(MathExpr::ThreadID(ForkDimension(*dim, self.factors[*dim])))
            }
            MathExpr::ThreadID(dim) => self.intern_math_expr(MathExpr::ThreadID(ForkDimension(
                dim.0 + self.factors.len(),
                dim.1,
            ))),
            MathExpr::SumReduction(id, ref dims) => {
                let dims = dims
                    .into_iter()
                    .map(|dim| ForkDimension(dim.0 + self.factors.len(), dim.1))
                    .collect();
                let id = self.substitute_new_dims(id);
                self.intern_math_expr(MathExpr::SumReduction(id, dims))
            }
            MathExpr::Comprehension(id, ref dims) => {
                let dims = dims
                    .into_iter()
                    .map(|dim| ForkDimension(dim.0 + self.factors.len(), dim.1))
                    .collect();
                let id = self.substitute_new_dims(id);
                self.intern_math_expr(MathExpr::Comprehension(id, dims))
            }
            MathExpr::Read(array, ref indices) => {
                let indices = indices
                    .clone()
                    .iter()
                    .map(|id| self.substitute_new_dims(*id))
                    .collect();
                let array = self.substitute_new_dims(array);
                self.intern_math_expr(MathExpr::Read(array, indices))
            }
            MathExpr::Unary(op, input) => {
                let input = self.substitute_new_dims(input);
                self.intern_math_expr(MathExpr::Unary(op, input))
            }
            MathExpr::Binary(op, left, right) => {
                let left = self.substitute_new_dims(left);
                let right = self.substitute_new_dims(right);
                self.intern_math_expr(MathExpr::Binary(op, left, right))
            }
            MathExpr::Ternary(op, first, second, third) => {
                let first = self.substitute_new_dims(first);
                let second = self.substitute_new_dims(second);
                let third = self.substitute_new_dims(third);
                self.intern_math_expr(MathExpr::Ternary(op, first, second, third))
            }
            MathExpr::IntrinsicFunc(intrinsic, ref args) => {
                let args = args
                    .clone()
                    .iter()
                    .map(|id| self.substitute_new_dims(*id))
                    .collect();
                self.intern_math_expr(MathExpr::IntrinsicFunc(intrinsic, args))
            }
            _ => id,
        }
    }
}

pub fn opaque_nodes_in_expr(env: &MathEnv, id: MathID) -> HashSet<NodeID> {
    let mut set = HashSet::new();
    let mut stack = vec![id];
    while let Some(id) = stack.pop() {
        match env[id.idx()] {
            MathExpr::Zero(_) | MathExpr::One(_) | MathExpr::ThreadID(_) => {}
            MathExpr::OpaqueNode(id) => {
                set.insert(id);
            }
            MathExpr::SumReduction(id, _) | MathExpr::Comprehension(id, _) => {
                stack.push(id);
            }
            MathExpr::Read(id, ref ids) => {
                stack.push(id);
                stack.extend(ids);
            }
            MathExpr::Unary(_, input) => {
                stack.push(input);
            }
            MathExpr::Binary(_, left, right) => {
                stack.push(left);
                stack.push(right);
            }
            MathExpr::Ternary(_, first, second, third) => {
                stack.push(first);
                stack.push(second);
                stack.push(third);
            }
            MathExpr::IntrinsicFunc(_, ref args) => {
                stack.extend(args);
            }
        }
    }
    set
}

pub fn debug_print_math_expr(id: MathID, env: &MathEnv) {
    match env[id.idx()] {
        MathExpr::Zero(_) => print!("0"),
        MathExpr::One(_) => print!("1"),
        MathExpr::OpaqueNode(id) => print!("{:?}", id),
        MathExpr::ThreadID(dim) => print!("#{}", dim.0),
        MathExpr::SumReduction(id, ref dims) => {
            print!("Sum (");
            for dim in dims {
                print!("#{}/{:?},", dim.0, dim.1);
            }
            print!(") ");
            debug_print_math_expr(id, env);
        }
        MathExpr::Comprehension(id, ref dims) => {
            print!("[");
            for dim in dims {
                print!("#{}/{:?},", dim.0, dim.1);
            }
            print!("] ");
            debug_print_math_expr(id, env);
        }
        MathExpr::Read(id, ref pos) => {
            print!("read(");
            debug_print_math_expr(id, env);
            for pos in pos {
                print!(", ");
                debug_print_math_expr(*pos, env);
            }
            print!(")");
        }
        MathExpr::Unary(op, input) => {
            print!("{}(", op.lower_case_name());
            debug_print_math_expr(input, env);
            print!(")");
        }
        MathExpr::Binary(op, left, right) => {
            print!("{}(", op.lower_case_name());
            debug_print_math_expr(left, env);
            print!(", ");
            debug_print_math_expr(right, env);
            print!(")");
        }
        MathExpr::Ternary(op, first, second, third) => {
            print!("{}(", op.lower_case_name());
            debug_print_math_expr(first, env);
            print!(", ");
            debug_print_math_expr(second, env);
            print!(", ");
            debug_print_math_expr(third, env);
            print!(")");
        }
        MathExpr::IntrinsicFunc(intrinsic, ref args) => {
            print!("{}(", intrinsic.lower_case_name());
            for arg in args {
                debug_print_math_expr(*arg, env);
                print!(", ");
            }
            print!(")");
        }
    }
}
