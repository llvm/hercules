use crate::*;

use std::cmp::{max, min};
use std::collections::BTreeSet;
use std::ops::Deref;

use either::Either;

pub trait DynamicConstantView {
    fn get_dynconst(&self, id: DynamicConstantID) -> impl Deref<Target = DynamicConstant> + '_;
    fn add_dynconst(&mut self, dc: DynamicConstant) -> DynamicConstantID;

    fn dc_const(&mut self, val: usize) -> DynamicConstantID {
        self.add_dynconst(DynamicConstant::Constant(val))
    }

    fn dc_param(&mut self, index: usize) -> DynamicConstantID {
        self.add_dynconst(DynamicConstant::Parameter(index))
    }

    fn dc_add(&mut self, dcs: Vec<DynamicConstantID>) -> DynamicConstantID {
        let mut constant_val = 0;
        let mut fields = vec![];

        for dc in dcs {
            match self.get_dynconst(dc).deref() {
                DynamicConstant::Constant(x) => constant_val += x,
                DynamicConstant::Add(xs) => fields.extend_from_slice(xs),
                _ => fields.push(dc),
            }
        }

        // If either there are no fields or the constant is non-zero, add it
        if constant_val != 0 || fields.len() == 0 {
            fields.push(self.add_dynconst(DynamicConstant::Constant(constant_val)));
        }

        if fields.len() <= 1 {
            // If there is only one term to add, just return it
            fields[0]
        } else {
            fields.sort();
            self.add_dynconst(DynamicConstant::Add(fields))
        }
    }

    fn dc_mul(&mut self, dcs: Vec<DynamicConstantID>) -> DynamicConstantID {
        let mut constant_val = 1;
        let mut fields = vec![];

        for dc in dcs {
            match self.get_dynconst(dc).deref() {
                DynamicConstant::Constant(x) => constant_val *= x,
                DynamicConstant::Mul(xs) => fields.extend_from_slice(xs),
                _ => fields.push(dc),
            }
        }

        if constant_val == 0 {
            return self.add_dynconst(DynamicConstant::Constant(0));
        }

        if constant_val != 1 || fields.len() == 0 {
            fields.push(self.add_dynconst(DynamicConstant::Constant(constant_val)));
        }

        if fields.len() <= 1 {
            fields[0]
        } else {
            fields.sort();
            self.add_dynconst(DynamicConstant::Mul(fields))
        }
    }

    fn dc_min(&mut self, dcs: Vec<DynamicConstantID>) -> DynamicConstantID {
        let mut constant_val: Option<usize> = None;
        // For min and max we track the fields via a set during normalization as this removes
        // duplicates (and we use a BTreeSet as it can produce its elements in sorted order)
        let mut fields = BTreeSet::new();

        for dc in dcs {
            match self.get_dynconst(dc).deref() {
                DynamicConstant::Constant(x) => {
                    if let Some(cur_min) = constant_val {
                        constant_val = Some(min(cur_min, *x));
                    } else {
                        constant_val = Some(*x);
                    }
                }
                DynamicConstant::Min(xs) => fields.extend(xs),
                _ => {
                    fields.insert(dc);
                }
            }
        }

        if let Some(const_val) = constant_val {
            // Since dynamic constants are non-negative, ignore the constant if it is 0
            if const_val != 0 {
                fields.insert(self.add_dynconst(DynamicConstant::Constant(const_val)));
            }
        }

        if fields.len() == 0 {
            // The minimum of 0 dynamic constants is 0 since dynamic constants are non-negative
            self.add_dynconst(DynamicConstant::Constant(0))
        } else if fields.len() <= 1 {
            *fields.first().unwrap()
        } else {
            self.add_dynconst(DynamicConstant::Min(fields.into_iter().collect()))
        }
    }

    fn dc_max(&mut self, dcs: Vec<DynamicConstantID>) -> DynamicConstantID {
        let mut constant_val: Option<usize> = None;
        let mut fields = BTreeSet::new();

        for dc in dcs {
            match self.get_dynconst(dc).deref() {
                DynamicConstant::Constant(x) => {
                    if let Some(cur_max) = constant_val {
                        constant_val = Some(max(cur_max, *x));
                    } else {
                        constant_val = Some(*x);
                    }
                }
                DynamicConstant::Max(xs) => fields.extend(xs),
                _ => {
                    fields.insert(dc);
                }
            }
        }

        if let Some(const_val) = constant_val {
            fields.insert(self.add_dynconst(DynamicConstant::Constant(const_val)));
        }

        assert!(
            fields.len() > 0,
            "Max of 0 dynamic constant expressions is undefined"
        );

        if fields.len() <= 1 {
            *fields.first().unwrap()
        } else {
            self.add_dynconst(DynamicConstant::Max(fields.into_iter().collect()))
        }
    }

    fn dc_sub(&mut self, x: DynamicConstantID, y: DynamicConstantID) -> DynamicConstantID {
        let dc = match (self.get_dynconst(x).deref(), self.get_dynconst(y).deref()) {
            (DynamicConstant::Constant(x), DynamicConstant::Constant(y)) => {
                Either::Left(DynamicConstant::Constant(x - y))
            }
            (_, DynamicConstant::Constant(0)) => Either::Right(x),
            _ => Either::Left(DynamicConstant::Sub(x, y)),
        };

        match dc {
            Either::Left(dc) => self.add_dynconst(dc),
            Either::Right(id) => id,
        }
    }

    fn dc_div(&mut self, x: DynamicConstantID, y: DynamicConstantID) -> DynamicConstantID {
        let dc = match (self.get_dynconst(x).deref(), self.get_dynconst(y).deref()) {
            (DynamicConstant::Constant(x), DynamicConstant::Constant(y)) => {
                Either::Left(DynamicConstant::Constant(x / y))
            }
            (_, DynamicConstant::Constant(1)) => Either::Right(x),
            _ => Either::Left(DynamicConstant::Div(x, y)),
        };

        match dc {
            Either::Left(dc) => self.add_dynconst(dc),
            Either::Right(id) => id,
        }
    }

    fn dc_rem(&mut self, x: DynamicConstantID, y: DynamicConstantID) -> DynamicConstantID {
        let dc = match (self.get_dynconst(x).deref(), self.get_dynconst(y).deref()) {
            (DynamicConstant::Constant(x), DynamicConstant::Constant(y)) => {
                Either::Left(DynamicConstant::Constant(x % y))
            }
            _ => Either::Left(DynamicConstant::Rem(x, y)),
        };

        match dc {
            Either::Left(dc) => self.add_dynconst(dc),
            Either::Right(id) => id,
        }
    }

    fn dc_normalize(&mut self, dc: DynamicConstant) -> DynamicConstantID {
        match dc {
            DynamicConstant::Add(xs) => self.dc_add(xs),
            DynamicConstant::Mul(xs) => self.dc_mul(xs),
            DynamicConstant::Min(xs) => self.dc_min(xs),
            DynamicConstant::Max(xs) => self.dc_max(xs),
            DynamicConstant::Sub(x, y) => self.dc_sub(x, y),
            DynamicConstant::Div(x, y) => self.dc_div(x, y),
            DynamicConstant::Rem(x, y) => self.dc_rem(x, y),
            _ => self.add_dynconst(dc),
        }
    }
}
