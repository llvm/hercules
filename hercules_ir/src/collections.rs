use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::iter::{once, repeat, zip};

use either::Either;

use crate::*;

/*
 * Analysis result that finds "collection objects" in Hercules IR. This analysis
 * is inter-procedural, since collection objects can be passed / returned to /
 * from called functions. This analysis also tracks which collection objects are
 * mutated "in" a function - a collection object is mutated in a function if
 * that function contains a write node that may write to that collection object
 * or if that function contains a call node that may take that collection object
 * as an argument and that collection parameter of that function is mutated.
 * Collection objects are numbered locally - the following nodes may originate a
 * collection object:
 *
 * - Parameter: each parameter index gets assigned a single collection object,
 *   each parameter node gets assigned the object of its index.
 * - Constant: each collection constant node gets assigned a single collection
 *   object.
 * - Call: each function is analyzed to determine which collection objects (of
 *   its parameters or an object it originates) may be returned; a call node
 *   originates a new collection object if it may return an object originated
 *   inside the callee.
 * - Undef: each undef node with a non-primitive type gets assigned a single
 *   collection object.
 *
 * The analysis contains the following information:
 *
 * - For each node in each function, which collection objects may be on the
 *   output of the node?
 * - For each function, which collection objects may be mutated inside that
 *   function, and by what nodes?
 * - For each function, which collection objects may be returned?
 * - For each collection object, how was it originated?
 */
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum CollectionObjectOrigin {
    Parameter(usize),
    Constant(NodeID),
    DataProjection(NodeID),
    Undef(NodeID),
}

define_id_type!(CollectionObjectID);

#[derive(Debug, Clone)]
pub struct FunctionCollectionObjects {
    objects_per_node: Vec<Vec<CollectionObjectID>>,
    mutated: Vec<Vec<NodeID>>,
    returned: Vec<Vec<CollectionObjectID>>,
    origins: Vec<CollectionObjectOrigin>,
}

pub type CollectionObjects = BTreeMap<FunctionID, FunctionCollectionObjects>;

impl CollectionObjectOrigin {
    pub fn is_parameter(&self) -> bool {
        self.try_parameter().is_some()
    }

    pub fn try_parameter(&self) -> Option<usize> {
        match self {
            CollectionObjectOrigin::Parameter(index) => Some(*index),
            _ => None,
        }
    }

    pub fn try_constant(&self) -> Option<NodeID> {
        match self {
            CollectionObjectOrigin::Constant(id) => Some(*id),
            _ => None,
        }
    }
}

impl FunctionCollectionObjects {
    pub fn objects(&self, id: NodeID) -> &Vec<CollectionObjectID> {
        &self.objects_per_node[id.idx()]
    }

    pub fn origin(&self, object: CollectionObjectID) -> CollectionObjectOrigin {
        self.origins[object.idx()]
    }

    pub fn param_to_object(&self, index: usize) -> Option<CollectionObjectID> {
        self.origins
            .iter()
            .position(|origin| *origin == CollectionObjectOrigin::Parameter(index))
            .map(CollectionObjectID::new)
    }

    pub fn returned_objects(&self, selection: usize) -> &Vec<CollectionObjectID> {
        &self.returned[selection]
    }

    pub fn all_returned_objects(&self) -> impl Iterator<Item = CollectionObjectID> + '_ {
        self.returned
            .iter()
            .flat_map(|colls| colls.iter().map(|c| *c))
    }

    pub fn is_mutated(&self, object: CollectionObjectID) -> bool {
        !self.mutators(object).is_empty()
    }

    pub fn mutators(&self, object: CollectionObjectID) -> &Vec<NodeID> {
        &self.mutated[object.idx()]
    }

    pub fn num_objects(&self) -> usize {
        self.origins.len()
    }

    pub fn iter_objects(&self) -> impl Iterator<Item = CollectionObjectID> {
        (0..self.num_objects()).map(CollectionObjectID::new)
    }
}

/*
 * Each node is assigned a set of collection objects output-ed from the node.
 * This is just a set of collection object IDs (usize).
 */
#[derive(PartialEq, Eq, Clone, Debug)]
struct CollectionObjectLattice {
    objs: BTreeSet<CollectionObjectID>,
}

impl Semilattice for CollectionObjectLattice {
    fn meet(a: &Self, b: &Self) -> Self {
        CollectionObjectLattice {
            objs: a.objs.union(&b.objs).map(|x| *x).collect(),
        }
    }

    fn top() -> Self {
        CollectionObjectLattice {
            objs: BTreeSet::new(),
        }
    }

    fn bottom() -> Self {
        // Technically, this lattice is unbounded - technically technically, the
        // lattice is bounded by the number of collection objects in a given
        // function, but incorporating this information is not possible in our
        // Semilattice inferface. Luckily bottom() isn't necessary if we never
        // call it, which we don't for this analysis.
        panic!()
    }
}

/*
 * Top level function to analyze collection objects in a Hercules module.
 */
pub fn collection_objects(
    functions: &Vec<Function>,
    types: &Vec<Type>,
    reverse_postorders: &Vec<Vec<NodeID>>,
    typing: &ModuleTyping,
    callgraph: &CallGraph,
) -> CollectionObjects {
    // Analyze functions in reverse topological order, since the analysis of a
    // function depends on all functions it calls.
    let mut collection_objects: CollectionObjects = BTreeMap::new();
    let topo = callgraph.topo();

    for func_id in topo {
        let func = &functions[func_id.idx()];
        let typing = &typing[func_id.idx()];
        let reverse_postorder = &reverse_postorders[func_id.idx()];

        // Find collection objects originating at parameters, constants,
        // data projections (of calls), or undefs.
        // Each of these nodes may *originate* one collection object.
        let param_origins = func
            .param_types
            .iter()
            .enumerate()
            .filter(|(_, ty_id)| !types[ty_id.idx()].is_primitive())
            .map(|(idx, _)| CollectionObjectOrigin::Parameter(idx));
        let other_origins = func
            .nodes
            .iter()
            .enumerate()
            .filter_map(|(idx, node)| match node {
                Node::Constant { id: _ } if !types[typing[idx].idx()].is_primitive() => {
                    Some(CollectionObjectOrigin::Constant(NodeID::new(idx)))
                }
                Node::DataProjection { data, selection } => {
                    let Node::Call {
                        control: _,
                        function: callee,
                        dynamic_constants: _,
                        args: _,
                    } = func.nodes[data.idx()]
                    else {
                        panic!("Data-projection's data is not a call node");
                    };

                    let fco = &collection_objects[&callee];
                    if fco.returned[*selection]
                        .iter()
                        .any(|returned| fco.origins[returned.idx()].try_parameter().is_none())
                    {
                        // If the callee may return a new collection object, then
                        // this data projection node originates a single collection object. The
                        // node may output multiple collection objects, say if the
                        // callee may return an object passed in as a parameter -
                        // this is determined later.
                        Some(CollectionObjectOrigin::DataProjection(NodeID::new(idx)))
                    } else {
                        None
                    }
                }
                Node::Undef { ty: _ } if !types[typing[idx].idx()].is_primitive() => {
                    Some(CollectionObjectOrigin::Undef(NodeID::new(idx)))
                }
                _ => None,
            });
        let origins: Vec<_> = param_origins.chain(other_origins).collect();

        // Run dataflow analysis to figure out which collection objects each
        // data node may output. Note that there's a strict subset of data nodes
        // that can output collection objects:
        //
        // - Phi: selects between objects in SSA form, may be assigned multiple
        //   possible objects.
        // - Reduce: reduces over an object, similar to phis.
        // - Parameter: may originate an object.
        // - Constant: may originate an object.
        // - DataProjection: may originate an object and may return an object
        //   passed in to its associated call as a parameter.
        // - LibraryCall: may return an object passed in as a parameter, but may
        //   not originate an object.
        // - Read: may extract a smaller object from the input - this is
        //   considered to be the same object as the input, as no copy takes
        //   place.
        // - Write: updates an object - this is considered to be the same object
        //   as the input object, as the write gets lowered to an in-place
        //   mutation.
        // - Undef: may originate a dummy object.
        // - Ternary (select): selects between two objects, may output either.
        let lattice = dataflow_global(func, reverse_postorder, |global_input, id| {
            let inputs = get_uses(&func.nodes[id.idx()])
                .as_ref()
                .iter()
                .map(|id| &global_input[id.idx()])
                .collect::<Vec<_>>();

            match func.nodes[id.idx()] {
                Node::Phi {
                    control: _,
                    data: _,
                }
                | Node::Reduce {
                    control: _,
                    init: _,
                    reduct: _,
                }
                | Node::Ternary {
                    op: TernaryOperator::Select,
                    first: _,
                    second: _,
                    third: _,
                } => inputs
                    .into_iter()
                    .fold(CollectionObjectLattice::top(), |acc, input| {
                        CollectionObjectLattice::meet(&acc, input)
                    }),
                Node::Parameter { index } => {
                    let obj = origins
                        .iter()
                        .position(|origin| *origin == CollectionObjectOrigin::Parameter(index))
                        .map(CollectionObjectID::new);
                    CollectionObjectLattice {
                        objs: obj.into_iter().collect(),
                    }
                }
                Node::Constant { id: _ } => {
                    let obj = origins
                        .iter()
                        .position(|origin| *origin == CollectionObjectOrigin::Constant(id))
                        .map(CollectionObjectID::new);
                    CollectionObjectLattice {
                        objs: obj.into_iter().collect(),
                    }
                }
                Node::DataProjection { data, selection }
                    if !types[typing[id.idx()].idx()].is_primitive() =>
                {
                    let Node::Call {
                        control: _,
                        function: callee,
                        dynamic_constants: _,
                        ref args,
                    } = func.nodes[data.idx()]
                    else {
                        panic!();
                    };

                    let new_obj = origins
                        .iter()
                        .position(|origin| *origin == CollectionObjectOrigin::DataProjection(id))
                        .map(CollectionObjectID::new);
                    let fco = &collection_objects[&callee];
                    let param_objs = fco.returned[selection]
                        .iter()
                        .filter_map(|returned| fco.origins[returned.idx()].try_parameter())
                        .map(|param_index| &global_input[args[param_index].idx()]);

                    let mut objs: BTreeSet<_> = new_obj.into_iter().collect();
                    for param_objs in param_objs {
                        objs.extend(&param_objs.objs);
                    }
                    CollectionObjectLattice { objs }
                }
                Node::LibraryCall {
                    library_function,
                    args: _,
                    ty: _,
                    device: _,
                } => match library_function {
                    LibraryFunction::GEMM => inputs[0].clone(),
                },
                Node::Undef { ty: _ } => {
                    let obj = origins
                        .iter()
                        .position(|origin| *origin == CollectionObjectOrigin::Undef(id))
                        .map(CollectionObjectID::new);
                    CollectionObjectLattice {
                        objs: obj.into_iter().collect(),
                    }
                }
                Node::Read {
                    collect: _,
                    indices: _,
                } if !types[typing[id.idx()].idx()].is_primitive() => inputs[0].clone(),
                Node::Write {
                    collect: _,
                    data: _,
                    indices: _,
                } => inputs[0].clone(),
                _ => CollectionObjectLattice::top(),
            }
        });
        let objects_per_node: Vec<Vec<_>> = lattice
            .into_iter()
            .map(|l| l.objs.into_iter().collect())
            .collect();

        // Look at the collection objects that each return value may take as input.
        let mut returned: Vec<BTreeSet<CollectionObjectID>> =
            vec![BTreeSet::new(); func.return_types.len()];
        for node in func.nodes.iter() {
            if let Node::Return { control: _, data } = node {
                for (idx, node) in data.iter().enumerate() {
                    returned[idx].extend(&objects_per_node[node.idx()]);
                }
            }
        }
        let returned = returned
            .into_iter()
            .map(|set| set.into_iter().collect())
            .collect();

        // Determine which objects are potentially mutated.
        let mut mutated = vec![vec![]; origins.len()];
        for (idx, node) in func.nodes.iter().enumerate() {
            if node.is_write() {
                // Every object that the write itself corresponds to is mutable
                // in this function.
                for object in objects_per_node[idx].iter() {
                    mutated[object.idx()].push(NodeID::new(idx));
                }
            } else if let Node::Call {
                control: _,
                function: callee,
                dynamic_constants: _,
                args,
            } = node
            {
                let fco = &collection_objects[&callee];
                for (param_idx, arg) in args.into_iter().enumerate() {
                    // If this parameter corresponds to an object and it's
                    // mutable in the callee...
                    if let Some(param_callee_object) = fco.param_to_object(param_idx)
                        && fco.is_mutated(param_callee_object)
                    {
                        // Then every object corresponding to the argument node
                        // in this function is mutable.
                        for object in objects_per_node[arg.idx()].iter() {
                            mutated[object.idx()].push(NodeID::new(idx));
                        }
                    }
                }
            } else if let Node::LibraryCall {
                library_function,
                args,
                ty: _,
                device: _,
            } = node
            {
                match library_function {
                    LibraryFunction::GEMM => {
                        for object in objects_per_node[args[0].idx()].iter() {
                            mutated[object.idx()].push(NodeID::new(idx));
                        }
                    }
                }
            }
        }

        assert_eq!(objects_per_node.len(), func.nodes.len());
        let fco = FunctionCollectionObjects {
            objects_per_node,
            mutated,
            returned,
            origins,
        };
        collection_objects.insert(func_id, fco);
    }

    collection_objects
}

/*
 * The zero lattice determine what indices a collection has been written to so
 * far. Reads from collection constants not at an index that's been written to
 * return zero - if any such read exists for a collection constant, then that
 * collection constant must be memset to zero explicitly. This is a similar
 * analysis to store-to-load forwarding, but we are looking for cases where the
 * store to forward might be a zero coming from a zero memset.
 */
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct ZeroLattice {
    written_to: BTreeSet<Box<[Index]>>,
}

impl Semilattice for ZeroLattice {
    fn meet(a: &Self, b: &Self) -> Self {
        // Merge the two sets. Find equal indices sets between `a` and `b`.
        let mut ret = BTreeSet::new();
        for indices in a.written_to.iter() {
            if b.written_to.contains(indices) {
                // If both sets have the same indices, add it to the meet value.
                ret.insert(indices.clone());
            }
        }
        ZeroLattice { written_to: ret }
    }

    fn top() -> Self {
        ZeroLattice {
            written_to: once(Box::new([]) as Box<[Index]>).collect(),
        }
    }

    fn bottom() -> Self {
        ZeroLattice {
            written_to: BTreeSet::new(),
        }
    }
}

/*
 * Analysis that determines what collection constants in a function don't need
 * to be lowered to memsets. If a collection constant is only read at indices
 * that it has first been written to, then the backing memory for the constant
 * doesn't have to be set to zero, since those zeros are never read.
 */
pub fn no_reset_constant_collections(
    function: &Function,
    types: &Vec<Type>,
    reverse_postorder: &Vec<NodeID>,
    typing: &Vec<TypeID>,
    objects: &FunctionCollectionObjects,
    reduce_einsum: &(MathEnv, HashMap<NodeID, MathID>),
) -> BTreeSet<NodeID> {
    // First, run a dataflow analysis that determines at each collection node
    // what indices have definitely been written to.
    let lattice = forward_dataflow(function, reverse_postorder, |inputs, id| {
        match function.nodes[id.idx()] {
            Node::Phi {
                control: _,
                data: _,
            }
            | Node::Ternary {
                op: TernaryOperator::Select,
                first: _,
                second: _,
                third: _,
            } => inputs.into_iter().fold(ZeroLattice::top(), |acc, input| {
                ZeroLattice::meet(&acc, input)
            }),
            Node::Reduce {
                control: _,
                init: _,
                reduct: _,
            } => {
                // Otherwise, meet the `init` and `reduct` inputs.
                ZeroLattice::meet(&inputs[0], &inputs[1])
            }
            Node::Write {
                collect: _,
                data: _,
                ref indices,
            } => {
                let mut value = inputs[0].clone();
                value.written_to.insert(indices.clone());
                value
            }
            _ => ZeroLattice::bottom(),
        }
    });

    // Second, collect triples from reads in the program containing:
    // 1. What indices are read.
    // 2. What node is read.
    // 3. What indices were written into the node already.
    let full_indices: Box<[Index]> = Box::new([]);
    let triples = (0..function.nodes.len())
        .map(|idx| {
            // Items #1 and #2.
            let id = NodeID::new(idx);
            let indices_and_nodes = match function.nodes[id.idx()] {
                Node::Read {
                    collect,
                    ref indices,
                } => Either::Left(zip(once(indices), once(collect))),
                Node::Write {
                    collect: _,
                    data,
                    indices: _,
                } => Either::Left(zip(once(&full_indices), once(data))),
                Node::Return {
                    control: _,
                    ref data,
                }
                | Node::Call {
                    control: _,
                    function: _,
                    dynamic_constants: _,
                    args: ref data,
                } => Either::Right(zip(repeat(&full_indices), data.into_iter().map(|id| *id))),
                _ => return None,
            };

            // Item #3.
            Some(
                indices_and_nodes
                    .map(|(indices, read)| (indices, read, &lattice[read.idx()].written_to)),
            )
        })
        .flatten()
        .flatten();

    // Third, look at each triple and check if there is a read that reads at
    // indices not contained by any of the written to indices. If so, then any
    // constant collection that may originate the object at that place has to be
    // explicitly memset to zero. The result starts as every constant collection
    // in the function, and is progressively shaved of constants that need to be
    // reset explicitly.
    let mut result: BTreeSet<_> = (0..function.nodes.len())
        .filter(|idx| {
            function.nodes[*idx].is_constant() && !types[typing[*idx].idx()].is_primitive()
        })
        .map(NodeID::new)
        .collect();
    for (indices, node, written_to) in triples {
        if !written_to
            .into_iter()
            .any(|written_to| indices_contain_other_indices(written_to, indices))
        {
            for constant in objects
                .objects(node)
                .into_iter()
                .filter_map(|obj| objects.origin(*obj).try_constant())
            {
                result.remove(&constant);
            }
        }
    }

    result
}
