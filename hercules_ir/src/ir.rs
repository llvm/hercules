use std::collections::HashSet;
use std::fmt::Write;
use std::iter::zip;
use std::ops::Coroutine;
use std::ops::CoroutineState;
use std::pin::Pin;

use bitvec::prelude::*;
use ordered_float::OrderedFloat;
use serde::Deserialize;
use serde::Serialize;

use crate::*;

/*
 * A module is a list of functions. Functions contain types, constants, and
 * dynamic constants, which are interned at the module level. Thus, if one
 * wants to run an intraprocedural pass in parallel, it is advised to first
 * destruct the module, then reconstruct it once finished.
 */
#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Module {
    pub functions: Vec<Function>,
    pub types: Vec<Type>,
    pub constants: Vec<Constant>,
    pub dynamic_constants: Vec<DynamicConstant>,
    pub labels: Vec<String>,
}

/*
 * A function has a name, a list of types for its parameters, a single return
 * type, a list of nodes in its sea-of-nodes style IR, and a number of dynamic
 * constants. When calling a function, arguments matching the parameter types
 * are required, as well as the correct number of dynamic constants. All dynamic
 * constants are 64-bit unsigned integers (usize / u64), so it is sufficient to
 * just store how many of them the function takes as arguments.
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Function {
    pub name: String,
    pub param_types: Vec<TypeID>,
    pub return_types: Vec<TypeID>,
    pub num_dynamic_constants: u32,
    pub entry: bool,

    pub nodes: Vec<Node>,

    pub schedules: FunctionSchedules,
    pub labels: FunctionLabels,
    pub device: Option<Device>,
}

/*
 * Hercules IR has a fairly standard type system, with the exception of the
 * control type. Hercules IR is based off of the sea-of-nodes IR, the main
 * feature of which being a merged control and data flow graph. Thus, control
 * is a type of value, just like any other type. However, the type system is
 * very restrictive over what can be done with control values. Summation types
 * are an IR equivalent of Rust's enum types.
 */
#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Type {
    Control,
    Boolean,
    Integer8,
    Integer16,
    Integer32,
    Integer64,
    UnsignedInteger8,
    UnsignedInteger16,
    UnsignedInteger32,
    UnsignedInteger64,
    Float8,
    BFloat16,
    Float32,
    Float64,
    Product(Box<[TypeID]>),
    Summation(Box<[TypeID]>),
    Array(TypeID, Box<[DynamicConstantID]>),
    MultiReturn(Box<[TypeID]>),
}

/*
 * Constants are pretty standard in Hercules IR. Float constants used the
 * ordered_float crate so that constants can be keys in maps (used for
 * interning constants during IR construction). Product, summation, and array
 * constants all contain their own type.
 */
#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Constant {
    Boolean(bool),
    Integer8(i8),
    Integer16(i16),
    Integer32(i32),
    Integer64(i64),
    UnsignedInteger8(u8),
    UnsignedInteger16(u16),
    UnsignedInteger32(u32),
    UnsignedInteger64(u64),
    Float32(OrderedFloat<f32>),
    Float64(OrderedFloat<f64>),
    Product(TypeID, Box<[ConstantID]>),
    Summation(TypeID, u32, ConstantID),
    // Array constants are always zero.
    Array(TypeID),
}

/*
 * Dynamic constants are unsigned 64-bit integers passed to a Hercules function
 * at runtime using the Hercules conductor API. They cannot be the result of
 * computations in Hercules IR. For a single execution of a Hercules function,
 * dynamic constants are constant throughout execution. This provides a
 * mechanism by which Hercules functions can operate on arrays with variable
 * length, while not needing Hercules functions to perform dynamic memory
 * allocation - by providing dynamic constants to the conductor API, the
 * conductor can allocate memory as necessary.
 */
#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum DynamicConstant {
    // The usize here is a constant value.
    Constant(usize),
    // The usize here is an index (which dynamic constant parameter of a
    // function is this).
    Parameter(usize),
    // Supported integer operations on dynamic constants.
    Add(Vec<DynamicConstantID>),
    Mul(Vec<DynamicConstantID>),
    Min(Vec<DynamicConstantID>),
    Max(Vec<DynamicConstantID>),

    Sub(DynamicConstantID, DynamicConstantID),
    Div(DynamicConstantID, DynamicConstantID),
    Rem(DynamicConstantID, DynamicConstantID),
}

/*
 * Hercules has a single node for reading, Read, and a single node for writing,
 * Write, that are both used for modifying product, sum, and array structures.
 * However, each of these types are indexed differently. Thus, these two nodes
 * operate on an index list, composing indices at different levels in a type
 * tree. Each type that can be indexed has a unique variant in the index enum.
 */
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Index {
    Field(usize),
    Variant(usize),
    Position(Box<[NodeID]>),
}

/*
 * Hercules IR is a single flow graph per function - this flow graph mixes data
 * and control flow.
 */
#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Node {
    Start,
    Region {
        preds: Box<[NodeID]>,
    },
    If {
        control: NodeID,
        cond: NodeID,
    },
    Match {
        control: NodeID,
        sum: NodeID,
    },
    Fork {
        control: NodeID,
        factors: Box<[DynamicConstantID]>,
    },
    Join {
        control: NodeID,
    },
    Phi {
        control: NodeID,
        data: Box<[NodeID]>,
    },
    ThreadID {
        control: NodeID,
        dimension: usize,
    },
    Reduce {
        control: NodeID,
        init: NodeID,
        reduct: NodeID,
    },
    Return {
        control: NodeID,
        data: Box<[NodeID]>,
    },
    Parameter {
        index: usize,
    },
    Constant {
        id: ConstantID,
    },
    DynamicConstant {
        id: DynamicConstantID,
    },
    Unary {
        input: NodeID,
        op: UnaryOperator,
    },
    Binary {
        left: NodeID,
        right: NodeID,
        op: BinaryOperator,
    },
    Ternary {
        first: NodeID,
        second: NodeID,
        third: NodeID,
        op: TernaryOperator,
    },
    Call {
        control: NodeID,
        function: FunctionID,
        dynamic_constants: Box<[DynamicConstantID]>,
        args: Box<[NodeID]>,
    },
    IntrinsicCall {
        intrinsic: Intrinsic,
        args: Box<[NodeID]>,
    },
    LibraryCall {
        library_function: LibraryFunction,
        args: Box<[NodeID]>,
        ty: TypeID,
        device: Device,
    },
    Read {
        collect: NodeID,
        indices: Box<[Index]>,
    },
    Write {
        collect: NodeID,
        data: NodeID,
        indices: Box<[Index]>,
    },
    ControlProjection {
        control: NodeID,
        selection: usize,
    },
    DataProjection {
        data: NodeID,
        selection: usize,
    },
    Undef {
        ty: TypeID,
    },
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum UnaryOperator {
    Not,
    Neg,
    Cast(TypeID),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum BinaryOperator {
    Add,
    Sub,
    Mul,
    Div,
    Rem,
    LT,
    LTE,
    GT,
    GTE,
    EQ,
    NE,
    Or,
    And,
    Xor,
    LSh,
    RSh,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum TernaryOperator {
    Select,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Intrinsic {
    Abs,
    ACos,
    ACosh,
    ASin,
    ASinh,
    ATan,
    ATan2,
    ATanh,
    Cbrt,
    Ceil,
    Cos,
    Cosh,
    Exp,
    Exp2,
    ExpM1,
    Floor,
    Ln,
    Ln1P,
    Log,
    Log10,
    Log2,
    Max,
    Min,
    Pow,
    Powf,
    Powi,
    Round,
    Sin,
    Sinh,
    Sqrt,
    Tan,
    Tanh,
}

/*
 * An individual schedule is a single "directive" for the compiler to take into
 * consideration at some point during the compilation pipeline. Each schedule is
 * associated with a single node.
 */
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum Schedule {
    // This fork can be run in parallel. Any notion of tiling is handled by the
    // fork tiling passes - the backend will lower a parallel fork with
    // dimension N into a N-way thread launch.
    ParallelFork,
    // This reduce can be "run in parallel" - conceptually, the `reduct`
    // backedge can be removed, and the reduce code can be merged into the
    // parallel code.
    ParallelReduce,
    // This reduce can be re-associated. This may lower a sequential dependency
    // chain into a reduction tree.
    MonoidReduce,
    // This constant node doesn't need to be memset to zero.
    NoResetConstant,
    // This call should be called in a spawned future.
    AsyncCall,
}

/*
 * The authoritative enumeration of supported backends. Multiple backends may
 * correspond to the same kind of hardware.
 */
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Device {
    LLVM,
    CUDA,
    // Entry functions are lowered to async Rust code that calls device
    // functions (leaf nodes in the call graph), possibly concurrently.
    AsyncRust,
}

/*
 * The authoritative enumeration of supported library calls.
 */
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum LibraryFunction {
    GEMM,
}

/*
 * A single node may have multiple schedules.
 */
pub type FunctionSchedules = Vec<Vec<Schedule>>;

/*
 * A single node may have multiple labels.
 */
pub type FunctionLabels = Vec<HashSet<LabelID>>;

/*
 * Basic block info consists of two things:
 *
 * 1. A map from node to block (named by control nodes).
 * 2. For each node, which nodes are in its own block.
 *
 * Note that for #2, the structure is Vec<NodeID>, meaning the nodes are ordered
 * inside the block. This order corresponds to the traversal order of the nodes
 * in the block needed by the backend code generators.
 */
pub type BasicBlocks = (Vec<NodeID>, Vec<Vec<NodeID>>);

impl Module {
    /*
     * Printing out types, constants, and dynamic constants fully requires a
     * reference to the module, since references to other types, constants, and
     * dynamic constants are done using IDs.
     */
    pub fn write_type<W: Write>(&self, ty_id: TypeID, w: &mut W) -> std::fmt::Result {
        match &self.types[ty_id.idx()] {
            Type::Control => write!(w, "Control"),
            Type::Boolean => write!(w, "Boolean"),
            Type::Integer8 => write!(w, "Integer8"),
            Type::Integer16 => write!(w, "Integer16"),
            Type::Integer32 => write!(w, "Integer32"),
            Type::Integer64 => write!(w, "Integer64"),
            Type::UnsignedInteger8 => write!(w, "UnsignedInteger8"),
            Type::UnsignedInteger16 => write!(w, "UnsignedInteger16"),
            Type::UnsignedInteger32 => write!(w, "UnsignedInteger32"),
            Type::UnsignedInteger64 => write!(w, "UnsignedInteger64"),
            Type::Float8 => write!(w, "Float8"),
            Type::BFloat16 => write!(w, "BFloat16"),
            Type::Float32 => write!(w, "Float32"),
            Type::Float64 => write!(w, "Float64"),
            Type::Product(fields) => {
                write!(w, "Product(")?;
                for idx in 0..fields.len() {
                    let field_ty_id = fields[idx];
                    self.write_type(field_ty_id, w)?;
                    if idx + 1 < fields.len() {
                        write!(w, ", ")?;
                    }
                }
                write!(w, ")")
            }
            Type::Summation(fields) => {
                write!(w, "Summation(")?;
                for idx in 0..fields.len() {
                    let field_ty_id = fields[idx];
                    self.write_type(field_ty_id, w)?;
                    if idx + 1 < fields.len() {
                        write!(w, ", ")?;
                    }
                }
                write!(w, ")")
            }
            Type::Array(elem, extents) => {
                write!(w, "Array(")?;
                self.write_type(*elem, w)?;
                for extent in extents.iter() {
                    write!(w, ", ")?;
                    self.write_dynamic_constant(*extent, w)?;
                }
                write!(w, ")")
            }
            Type::MultiReturn(fields) => {
                write!(w, "MultiReturn(")?;
                for idx in 0..fields.len() {
                    let field_ty_id = fields[idx];
                    self.write_type(field_ty_id, w)?;
                    if idx + 1 < fields.len() {
                        write!(w, ", ")?;
                    }
                }
                write!(w, ")")
            }
        }?;

        Ok(())
    }

    pub fn write_constant<W: Write>(&self, cons_id: ConstantID, w: &mut W) -> std::fmt::Result {
        match &self.constants[cons_id.idx()] {
            Constant::Boolean(val) => write!(w, "{}", val),
            Constant::Integer8(val) => write!(w, "{}", val),
            Constant::Integer16(val) => write!(w, "{}", val),
            Constant::Integer32(val) => write!(w, "{}", val),
            Constant::Integer64(val) => write!(w, "{}", val),
            Constant::UnsignedInteger8(val) => write!(w, "{}", val),
            Constant::UnsignedInteger16(val) => write!(w, "{}", val),
            Constant::UnsignedInteger32(val) => write!(w, "{}", val),
            Constant::UnsignedInteger64(val) => write!(w, "{}", val),
            Constant::Float32(val) => write!(w, "{}", val),
            Constant::Float64(val) => write!(w, "{}", val),
            Constant::Product(_, fields) => {
                write!(w, "(")?;
                for idx in 0..fields.len() {
                    let field_cons_id = fields[idx];
                    self.write_constant(field_cons_id, w)?;
                    if idx + 1 < fields.len() {
                        write!(w, ", ")?;
                    }
                }
                write!(w, ")")
            }
            Constant::Summation(_, variant, field) => {
                write!(w, "%{}(", variant)?;
                self.write_constant(*field, w)?;
                write!(w, ")")
            }
            Constant::Array(_) => {
                write!(w, "[]")
            }
        }?;

        Ok(())
    }

    pub fn write_dynamic_constant<W: Write>(
        &self,
        dc_id: DynamicConstantID,
        w: &mut W,
    ) -> std::fmt::Result {
        match &self.dynamic_constants[dc_id.idx()] {
            DynamicConstant::Constant(cons) => write!(w, "{}", cons),
            DynamicConstant::Parameter(param) => write!(w, "#{}", param),
            DynamicConstant::Add(xs)
            | DynamicConstant::Mul(xs)
            | DynamicConstant::Min(xs)
            | DynamicConstant::Max(xs) => {
                match &self.dynamic_constants[dc_id.idx()] {
                    DynamicConstant::Add(_) => write!(w, "+")?,
                    DynamicConstant::Mul(_) => write!(w, "*")?,
                    DynamicConstant::Min(_) => write!(w, "min")?,
                    DynamicConstant::Max(_) => write!(w, "max")?,
                    _ => (),
                }
                write!(w, "(")?;
                for arg in xs {
                    self.write_dynamic_constant(*arg, w)?;
                    write!(w, ",")?;
                }
                write!(w, ")")
            }
            DynamicConstant::Sub(x, y)
            | DynamicConstant::Div(x, y)
            | DynamicConstant::Rem(x, y) => {
                match &self.dynamic_constants[dc_id.idx()] {
                    DynamicConstant::Sub(_, _) => write!(w, "-")?,
                    DynamicConstant::Div(_, _) => write!(w, "/")?,
                    DynamicConstant::Rem(_, _) => write!(w, "%")?,
                    _ => (),
                }
                write!(w, "(")?;
                self.write_dynamic_constant(*x, w)?;
                write!(w, ", ")?;
                self.write_dynamic_constant(*y, w)?;
                write!(w, ")")
            }
        }?;

        Ok(())
    }

    pub fn map<F, O>(&self, f: F) -> Vec<O>
    where
        F: Fn(&Function) -> O,
    {
        self.functions.iter().map(|func| f(func)).collect()
    }
}

/*
 * Create an iterator that traverses all the types in the module bottom up.
 * This uses a coroutine to make iteratively traversing the type DAGs
 * easier.
 */
pub fn types_bottom_up(types: &Vec<Type>) -> impl Iterator<Item = TypeID> + '_ {
    let mut visited = bitvec![u8, Lsb0; 0; types.len()];
    let mut stack = (0..types.len()).map(TypeID::new).collect::<Vec<TypeID>>();
    let coroutine = #[coroutine]
    move || {
        // Since this is a coroutine, handle recursion manually.
        while let Some(id) = stack.pop() {
            if visited[id.idx()] {
                continue;
            }
            match &types[id.idx()] {
                Type::Product(children) | Type::Summation(children) => {
                    // We have to yield the children of this node before
                    // this node itself. We keep track of which nodes have
                    // yielded using visited.
                    let can_yield = children.iter().all(|x| visited[x.idx()]);
                    if can_yield {
                        visited.set(id.idx(), true);
                        yield id;
                    } else {
                        // Push ourselves, then children, so that children
                        // get popped first.
                        stack.push(id);
                        for id in children.iter() {
                            stack.push(*id);
                        }
                    }
                }
                Type::Array(child, _) => {
                    // Same idea as product / summation, but there's only
                    // one child.
                    let can_yield = visited[child.idx()];
                    if can_yield {
                        visited.set(id.idx(), true);
                        yield id;
                    } else {
                        stack.push(id);
                        stack.push(*child);
                    }
                }
                _ => {
                    visited.set(id.idx(), true);
                    yield id;
                }
            }
        }
    };
    CoroutineIterator {
        coroutine: Box::new(coroutine),
    }
}

/*
 * Create an iterator that traverses all the constants in the module bottom up.
 * This uses a coroutine to make iteratively traversing the constant DAGs
 * easier.
 */
pub fn constants_bottom_up(constants: &Vec<Constant>) -> impl Iterator<Item = ConstantID> + '_ {
    let mut visited = bitvec![u8, Lsb0; 0; constants.len()];
    let mut stack = (0..constants.len())
        .map(ConstantID::new)
        .collect::<Vec<ConstantID>>();
    let coroutine = #[coroutine]
    move || {
        // Since this is a coroutine, handle recursion manually.
        while let Some(id) = stack.pop() {
            if visited[id.idx()] {
                continue;
            }
            match &constants[id.idx()] {
                Constant::Product(_, children) => {
                    // We have to yield the children of this node before
                    // this node itself. We keep track of which nodes have
                    // yielded using visited.
                    let can_yield = children.iter().all(|x| visited[x.idx()]);
                    if can_yield {
                        visited.set(id.idx(), true);
                        yield id;
                    } else {
                        // Push ourselves, then children, so that children
                        // get popped first.
                        stack.push(id);
                        for id in children.iter() {
                            stack.push(*id);
                        }
                    }
                }
                Constant::Summation(_, _, child) => {
                    // Same idea as product / summation, but there's only
                    // one child.
                    let can_yield = visited[child.idx()];
                    if can_yield {
                        visited.set(id.idx(), true);
                        yield id;
                    } else {
                        stack.push(id);
                        stack.push(*child);
                    }
                }
                _ => {
                    visited.set(id.idx(), true);
                    yield id;
                }
            }
        }
    };
    CoroutineIterator {
        coroutine: Box::new(coroutine),
    }
}

/*
 * Create an iterator that traverses all the dynamic constants in the module
 * bottom up. This uses a coroutine to make iteratively traversing the dynamic
 * constant DAGs easier. This bottom-up visitor will ignore dynamic constants
 * that reference non-sensical IDs. These are artifacts left over by the process
 * of subsituting dynamic constants during some transformation passes, and
 * shouldn't be used by any nodes.
 */
pub fn dynamic_constants_bottom_up(
    dynamic_constants: &Vec<DynamicConstant>,
) -> impl Iterator<Item = DynamicConstantID> + '_ {
    let mut visited = bitvec![u8, Lsb0; 0; dynamic_constants.len()];
    let mut invalid = bitvec![u8, Lsb0; 0; dynamic_constants.len()];
    let mut stack = (0..dynamic_constants.len())
        .map(DynamicConstantID::new)
        .collect::<Vec<DynamicConstantID>>();
    let coroutine = #[coroutine]
    move || {
        // Since this is a coroutine, handle recursion manually.
        while let Some(id) = stack.pop() {
            if visited[id.idx()] {
                continue;
            }
            match &dynamic_constants[id.idx()] {
                DynamicConstant::Add(args)
                | DynamicConstant::Mul(args)
                | DynamicConstant::Min(args)
                | DynamicConstant::Max(args) => {
                    // We have to yield the children of this node before
                    // this node itself. We keep track of which nodes have
                    // yielded using visited.
                    if args
                        .iter()
                        .any(|i| i.idx() >= visited.len() || invalid[i.idx()])
                    {
                        // This is an invalid dynamic constant and should be skipped
                        invalid.set(id.idx(), true);
                        continue;
                    }

                    if args.iter().all(|i| visited[i.idx()]) {
                        // Since all children have been yielded, we yield ourself
                        visited.set(id.idx(), true);
                        yield id;
                    } else {
                        // Otherwise push self onto stack so that the children will get popped
                        // first
                        stack.push(id);
                        stack.extend(args.clone());
                    }
                }
                DynamicConstant::Sub(left, right)
                | DynamicConstant::Div(left, right)
                | DynamicConstant::Rem(left, right) => {
                    if left.idx() >= visited.len()
                        || right.idx() >= visited.len()
                        || invalid[left.idx()]
                        || invalid[right.idx()]
                    {
                        // This is an invalid dynamic constant and should be
                        // skipped.
                        invalid.set(id.idx(), true);
                        continue;
                    } else if visited[left.idx()] && visited[right.idx()] {
                        visited.set(id.idx(), true);
                        yield id;
                    } else {
                        // Push ourselves, then children, so that children
                        // get popped first.
                        stack.push(id);
                        stack.push(*left);
                        stack.push(*right);
                    }
                }
                _ => {
                    visited.set(id.idx(), true);
                    yield id;
                }
            }
        }
    };
    CoroutineIterator {
        coroutine: Box::new(coroutine),
    }
}

struct CoroutineIterator<G, I>
where
    G: Coroutine<Yield = I, Return = ()> + Unpin,
{
    coroutine: G,
}

impl<G, I> Iterator for CoroutineIterator<G, I>
where
    G: Coroutine<Yield = I, Return = ()> + Unpin,
{
    type Item = I;

    fn next(&mut self) -> Option<Self::Item> {
        // Iterator corresponds to yields from coroutine.
        match Pin::new(&mut self.coroutine).resume(()) {
            CoroutineState::Yielded(item) => Some(item),
            CoroutineState::Complete(_) => None,
        }
    }
}

impl Function {
    /*
     * Many transformations will delete nodes. There isn't strictly a gravestone
     * node value, so use the start node as a gravestone value (for IDs other
     * than 0). This function cleans up gravestoned nodes. This function returns
     * a map from old IDs to the new IDs, so that other data structures can be
     * updated.
     */
    pub fn delete_gravestones(&mut self) -> Vec<NodeID> {
        // Step 1: figure out which nodes are gravestones.
        let mut gravestones = (0..self.nodes.len())
            .filter(|x| *x != 0 && self.nodes[*x].is_start())
            .map(|x| NodeID::new(x));

        // Step 2: figure out the mapping between old node IDs and new node IDs.
        let mut node_mapping = Vec::with_capacity(self.nodes.len());
        let mut next_gravestone = gravestones.next();
        let mut num_gravestones_passed = 0;
        for idx in 0..self.nodes.len() {
            if Some(NodeID::new(idx)) == next_gravestone {
                node_mapping.push(NodeID::new(0));
                num_gravestones_passed += 1;
                next_gravestone = gravestones.next();
            } else {
                node_mapping.push(NodeID::new(idx - num_gravestones_passed));
            }
        }

        // Step 3: create new nodes vector. Along the way, update all uses.
        let mut old_nodes = vec![];
        std::mem::swap(&mut old_nodes, &mut self.nodes);

        let mut new_nodes = Vec::with_capacity(old_nodes.len() - num_gravestones_passed);
        for (idx, mut node) in old_nodes.into_iter().enumerate() {
            // Skip node if it's dead.
            if idx != 0 && node.is_start() {
                continue;
            }

            // Update uses.
            for u in get_uses_mut(&mut node).as_mut() {
                let old_id = **u;
                let new_id = node_mapping[old_id.idx()];
                if new_id == NodeID::new(0) && old_id != NodeID::new(0) {
                    panic!("While deleting gravestones, came across a use of a gravestoned node. The user has ID {} and was using ID {}. Here's the user: {:?}", idx, old_id.idx(), node);
                }
                **u = new_id;
            }

            // Add to new_nodes.
            new_nodes.push(node);
        }
        std::mem::swap(&mut new_nodes, &mut self.nodes);

        // Step 4: update the schedules.
        self.schedules.fix_gravestones(&node_mapping);
        self.labels.fix_gravestones(&node_mapping);

        node_mapping
    }
}

/*
 * Some analysis results can be updated after gravestone deletions.
 */
pub trait GraveUpdatable {
    fn fix_gravestones(&mut self, grave_mapping: &Vec<NodeID>);
}

impl<T: Clone> GraveUpdatable for Vec<T> {
    fn fix_gravestones(&mut self, grave_mapping: &Vec<NodeID>) {
        let mut new_self = vec![];
        for (data, (idx, mapping)) in
            std::iter::zip(self.into_iter(), grave_mapping.iter().enumerate())
        {
            if idx == 0 || mapping.idx() != 0 {
                assert_eq!(new_self.len(), mapping.idx());
                new_self.push(data.clone());
            }
        }
        *self = new_self;
    }
}

impl Type {
    pub fn is_control(&self) -> bool {
        if let Type::Control = self {
            true
        } else {
            false
        }
    }

    pub fn is_multireturn(&self) -> bool {
        if let Type::MultiReturn(_) = self {
            true
        } else {
            false
        }
    }

    pub fn is_bool(&self) -> bool {
        self == &Type::Boolean
    }

    pub fn is_unsigned(&self) -> bool {
        match self {
            Type::UnsignedInteger8 => true,
            Type::UnsignedInteger16 => true,
            Type::UnsignedInteger32 => true,
            Type::UnsignedInteger64 => true,
            _ => false,
        }
    }

    pub fn is_signed(&self) -> bool {
        match self {
            Type::Integer8 => true,
            Type::Integer16 => true,
            Type::Integer32 => true,
            Type::Integer64 => true,
            _ => false,
        }
    }

    pub fn is_fixed(&self) -> bool {
        match self {
            Type::Integer8 => true,
            Type::Integer16 => true,
            Type::Integer32 => true,
            Type::Integer64 => true,
            Type::UnsignedInteger8 => true,
            Type::UnsignedInteger16 => true,
            Type::UnsignedInteger32 => true,
            Type::UnsignedInteger64 => true,
            _ => false,
        }
    }

    pub fn is_integer(&self) -> bool {
        self.is_fixed() || self.is_bool()
    }

    pub fn is_float(&self) -> bool {
        match self {
            Type::Float8 => true,
            Type::BFloat16 => true,
            Type::Float32 => true,
            Type::Float64 => true,
            _ => false,
        }
    }

    pub fn is_arithmetic(&self) -> bool {
        self.is_fixed() || self.is_float()
    }

    pub fn is_primitive(&self) -> bool {
        self.is_bool() || self.is_fixed() || self.is_float()
    }

    pub fn is_product(&self) -> bool {
        if let Type::Product(_) = self {
            true
        } else {
            false
        }
    }

    pub fn is_summation(&self) -> bool {
        if let Type::Summation(_) = self {
            true
        } else {
            false
        }
    }

    pub fn is_array(&self) -> bool {
        if let Type::Array(_, _) = self {
            true
        } else {
            false
        }
    }

    pub fn try_element_type(&self) -> Option<TypeID> {
        if let Type::Array(elem, _) = self {
            Some(*elem)
        } else {
            None
        }
    }

    pub fn try_extents(&self) -> Option<&[DynamicConstantID]> {
        if let Type::Array(_, extents) = self {
            Some(extents)
        } else {
            None
        }
    }

    pub fn try_product(&self) -> Option<&[TypeID]> {
        if let Type::Product(ts) = self {
            Some(ts)
        } else {
            None
        }
    }

    pub fn try_multi_return(&self) -> Option<&[TypeID]> {
        if let Type::MultiReturn(ts) = self {
            Some(ts)
        } else {
            None
        }
    }

    pub fn num_bits(&self) -> u8 {
        match self {
            Type::Boolean => 1,
            Type::Integer8 | Type::UnsignedInteger8 => 8,
            Type::Integer16 | Type::UnsignedInteger16 => 16,
            Type::Integer32 | Type::UnsignedInteger32 | Type::Float32 => 32,
            Type::Integer64 | Type::UnsignedInteger64 | Type::Float64 => 64,
            _ => panic!(),
        }
    }
}

impl Constant {
    pub fn is_array(&self) -> bool {
        if let Constant::Array(_) = self {
            true
        } else {
            false
        }
    }

    // A zero constant may need to return constants that don't exist yet, so we
    // need mutable access to the constants array.
    pub fn try_product_fields(&self) -> Option<Vec<ConstantID>> {
        match self {
            Constant::Product(_, fields) => Some(fields.iter().map(|x| *x).collect()),
            _ => None,
        }
    }

    pub fn try_array_type(&self) -> Option<TypeID> {
        match self {
            Constant::Array(ty) => Some(*ty),
            _ => None,
        }
    }

    pub fn try_product_type(&self) -> Option<TypeID> {
        match self {
            Constant::Product(ty, _) => Some(*ty),
            _ => None,
        }
    }

    pub fn is_scalar(&self) -> bool {
        match self {
            Constant::Boolean(_) => true,
            Constant::Integer8(_) => true,
            Constant::Integer16(_) => true,
            Constant::Integer32(_) => true,
            Constant::Integer64(_) => true,
            Constant::UnsignedInteger8(_) => true,
            Constant::UnsignedInteger16(_) => true,
            Constant::UnsignedInteger32(_) => true,
            Constant::UnsignedInteger64(_) => true,
            Constant::Float32(_) => true,
            Constant::Float64(_) => true,
            _ => false,
        }
    }

    pub fn is_false(&self) -> bool {
        match self {
            Constant::Boolean(false) => true,
            _ => false,
        }
    }

    pub fn is_true(&self) -> bool {
        match self {
            Constant::Boolean(true) => true,
            _ => false,
        }
    }

    pub fn is_zero(&self) -> bool {
        match self {
            Constant::Integer8(0) => true,
            Constant::Integer16(0) => true,
            Constant::Integer32(0) => true,
            Constant::Integer64(0) => true,
            Constant::UnsignedInteger8(0) => true,
            Constant::UnsignedInteger16(0) => true,
            Constant::UnsignedInteger32(0) => true,
            Constant::UnsignedInteger64(0) => true,
            Constant::Float32(ord) => *ord == OrderedFloat::<f32>(0.0),
            Constant::Float64(ord) => *ord == OrderedFloat::<f64>(0.0),
            _ => false,
        }
    }

    pub fn is_one(&self) -> bool {
        match self {
            Constant::Integer8(1) => true,
            Constant::Integer16(1) => true,
            Constant::Integer32(1) => true,
            Constant::Integer64(1) => true,
            Constant::UnsignedInteger8(1) => true,
            Constant::UnsignedInteger16(1) => true,
            Constant::UnsignedInteger32(1) => true,
            Constant::UnsignedInteger64(1) => true,
            Constant::Float32(ord) => *ord == OrderedFloat::<f32>(1.0),
            Constant::Float64(ord) => *ord == OrderedFloat::<f64>(1.0),
            _ => false,
        }
    }

    pub fn is_largest(&self) -> bool {
        match self {
            Constant::Integer8(i8::MAX) => true,
            Constant::Integer16(i16::MAX) => true,
            Constant::Integer32(i32::MAX) => true,
            Constant::Integer64(i64::MAX) => true,
            Constant::UnsignedInteger8(u8::MAX) => true,
            Constant::UnsignedInteger16(u16::MAX) => true,
            Constant::UnsignedInteger32(u32::MAX) => true,
            Constant::UnsignedInteger64(u64::MAX) => true,
            Constant::Float32(ord) => *ord == OrderedFloat::<f32>(f32::INFINITY),
            Constant::Float64(ord) => *ord == OrderedFloat::<f64>(f64::INFINITY),
            _ => false,
        }
    }

    pub fn is_smallest(&self) -> bool {
        match self {
            Constant::Integer8(i8::MIN) => true,
            Constant::Integer16(i16::MIN) => true,
            Constant::Integer32(i32::MIN) => true,
            Constant::Integer64(i64::MIN) => true,
            Constant::UnsignedInteger8(u8::MIN) => true,
            Constant::UnsignedInteger16(u16::MIN) => true,
            Constant::UnsignedInteger32(u32::MIN) => true,
            Constant::UnsignedInteger64(u64::MIN) => true,
            Constant::Float32(ord) => *ord == OrderedFloat::<f32>(f32::NEG_INFINITY),
            Constant::Float64(ord) => *ord == OrderedFloat::<f64>(f64::NEG_INFINITY),
            _ => false,
        }
    }
}

impl DynamicConstant {
    pub fn add(x: DynamicConstantID, y: DynamicConstantID) -> Self {
        Self::Add(vec![x, y])
    }

    pub fn sub(x: DynamicConstantID, y: DynamicConstantID) -> Self {
        Self::Sub(x, y)
    }

    pub fn mul(x: DynamicConstantID, y: DynamicConstantID) -> Self {
        Self::Mul(vec![x, y])
    }

    pub fn div(x: DynamicConstantID, y: DynamicConstantID) -> Self {
        Self::Div(x, y)
    }

    pub fn rem(x: DynamicConstantID, y: DynamicConstantID) -> Self {
        Self::Rem(x, y)
    }

    pub fn min(x: DynamicConstantID, y: DynamicConstantID) -> Self {
        Self::Min(vec![x, y])
    }

    pub fn max(x: DynamicConstantID, y: DynamicConstantID) -> Self {
        Self::Max(vec![x, y])
    }

    pub fn is_parameter(&self) -> bool {
        if let DynamicConstant::Parameter(_) = self {
            true
        } else {
            false
        }
    }

    pub fn is_constant(&self) -> bool {
        if let DynamicConstant::Constant(_) = self {
            true
        } else {
            false
        }
    }

    pub fn is_zero(&self) -> bool {
        *self == DynamicConstant::Constant(0)
    }

    pub fn is_one(&self) -> bool {
        *self == DynamicConstant::Constant(1)
    }

    pub fn is_largest(&self) -> bool {
        *self == DynamicConstant::Constant(usize::MAX)
    }

    pub fn is_smallest(&self) -> bool {
        *self == DynamicConstant::Constant(usize::MIN)
    }

    pub fn try_parameter(&self) -> Option<usize> {
        if let DynamicConstant::Parameter(v) = self {
            Some(*v)
        } else {
            None
        }
    }

    pub fn try_constant(&self) -> Option<usize> {
        if let DynamicConstant::Constant(v) = self {
            Some(*v)
        } else {
            None
        }
    }
}

pub fn evaluate_dynamic_constant(
    cons: DynamicConstantID,
    dcs: &Vec<DynamicConstant>,
) -> Option<usize> {
    // Because of normalization, if a dynamic constant can be expressed as a constant it must be a
    // constant
    let DynamicConstant::Constant(cons) = dcs[cons.idx()] else {
        return None;
    };
    Some(cons)
}

/*
 * Simple predicate functions on nodes take a lot of space, so use a macro.
 */
macro_rules! define_pattern_predicate {
    ($x: ident, $y: pat) => {
        pub fn $x(&self) -> bool {
            if let $y = self {
                true
            } else {
                false
            }
        }
    };
}

impl Index {
    define_pattern_predicate!(is_field, Index::Field(_));
    define_pattern_predicate!(is_position, Index::Position(_));

    pub fn try_field(&self) -> Option<usize> {
        if let Index::Field(field) = self {
            Some(*field)
        } else {
            None
        }
    }

    pub fn try_position(&self) -> Option<&[NodeID]> {
        if let Index::Position(indices) = self {
            Some(&indices)
        } else {
            None
        }
    }

    pub fn lower_case_name(&self) -> String {
        match self {
            Index::Field(idx) => format!("field {}", idx),
            Index::Variant(idx) => format!("variant {}", idx),
            Index::Position(idxs) => format!("position {:?}", &*idxs),
        }
    }
}

impl Node {
    define_pattern_predicate!(is_start, Node::Start);
    define_pattern_predicate!(is_region, Node::Region { preds: _ });
    define_pattern_predicate!(
        is_if,
        Node::If {
            control: _,
            cond: _,
        }
    );
    define_pattern_predicate!(
        is_fork,
        Node::Fork {
            control: _,
            factors: _,
        }
    );
    define_pattern_predicate!(is_join, Node::Join { control: _ });
    define_pattern_predicate!(
        is_phi,
        Node::Phi {
            control: _,
            data: _,
        }
    );
    define_pattern_predicate!(
        is_thread_id,
        Node::ThreadID {
            control: _,
            dimension: _
        }
    );
    define_pattern_predicate!(
        is_reduce,
        Node::Reduce {
            control: _,
            init: _,
            reduct: _,
        }
    );
    define_pattern_predicate!(
        is_return,
        Node::Return {
            control: _,
            data: _,
        }
    );
    define_pattern_predicate!(is_parameter, Node::Parameter { index: _ });
    define_pattern_predicate!(is_constant, Node::Constant { id: _ });
    define_pattern_predicate!(is_dynamic_constant, Node::DynamicConstant { id: _ });
    define_pattern_predicate!(
        is_call,
        Node::Call {
            control: _,
            function: _,
            dynamic_constants: _,
            args: _
        }
    );
    define_pattern_predicate!(
        is_read,
        Node::Read {
            collect: _,
            indices: _
        }
    );
    define_pattern_predicate!(
        is_write,
        Node::Write {
            collect: _,
            indices: _,
            data: _
        }
    );
    define_pattern_predicate!(is_match, Node::Match { control: _, sum: _ });
    define_pattern_predicate!(
        is_control_projection,
        Node::ControlProjection {
            control: _,
            selection: _
        }
    );
    define_pattern_predicate!(
        is_data_projection,
        Node::DataProjection {
            data: _,
            selection: _
        }
    );

    define_pattern_predicate!(is_undef, Node::Undef { ty: _ });

    pub fn try_region(&self) -> Option<&[NodeID]> {
        if let Node::Region { preds } = self {
            Some(preds)
        } else {
            None
        }
    }

    pub fn try_if(&self) -> Option<(NodeID, NodeID)> {
        if let Node::If { control, cond } = self {
            Some((*control, *cond))
        } else {
            None
        }
    }

    pub fn try_control_proj(&self) -> Option<(NodeID, usize)> {
        if let Node::ControlProjection { control, selection } = self {
            Some((*control, *selection))
        } else {
            None
        }
    }

    pub fn try_data_proj(&self) -> Option<(NodeID, usize)> {
        if let Node::DataProjection { data, selection } = self {
            Some((*data, *selection))
        } else {
            None
        }
    }

    pub fn try_phi(&self) -> Option<(NodeID, &[NodeID])> {
        if let Node::Phi { control, data } = self {
            Some((*control, data))
        } else {
            None
        }
    }

    pub fn try_return(&self) -> Option<(NodeID, &[NodeID])> {
        if let Node::Return { control, data } = self {
            Some((*control, data))
        } else {
            None
        }
    }

    pub fn try_fork(&self) -> Option<(NodeID, &[DynamicConstantID])> {
        if let Node::Fork { control, factors } = self {
            Some((*control, factors))
        } else {
            None
        }
    }

    pub fn try_thread_id(&self) -> Option<(NodeID, usize)> {
        if let Node::ThreadID { control, dimension } = self {
            Some((*control, *dimension))
        } else {
            None
        }
    }

    pub fn try_join(&self) -> Option<NodeID> {
        if let Node::Join { control } = self {
            Some(*control)
        } else {
            None
        }
    }

    pub fn try_reduce(&self) -> Option<(NodeID, NodeID, NodeID)> {
        if let Node::Reduce {
            control,
            init,
            reduct,
        } = self
        {
            Some((*control, *init, *reduct))
        } else {
            None
        }
    }

    pub fn try_parameter(&self) -> Option<usize> {
        if let Node::Parameter { index } = self {
            Some(*index)
        } else {
            None
        }
    }

    pub fn try_constant(&self) -> Option<ConstantID> {
        if let Node::Constant { id } = self {
            Some(*id)
        } else {
            None
        }
    }

    pub fn try_call(
        &self,
    ) -> Option<(
        NodeID,
        FunctionID,
        &Box<[DynamicConstantID]>,
        &Box<[NodeID]>,
    )> {
        if let Node::Call {
            control,
            function,
            dynamic_constants,
            args,
        } = self
        {
            Some((*control, *function, dynamic_constants, args))
        } else {
            None
        }
    }

    pub fn try_dynamic_constant(&self) -> Option<DynamicConstantID> {
        if let Node::DynamicConstant { id } = self {
            Some(*id)
        } else {
            None
        }
    }

    pub fn try_binary(&self, bop: BinaryOperator) -> Option<(NodeID, NodeID)> {
        if let Node::Binary { left, right, op } = self
            && *op == bop
        {
            Some((*left, *right))
        } else {
            None
        }
    }

    pub fn try_ternary(&self, bop: TernaryOperator) -> Option<(NodeID, NodeID, NodeID)> {
        if let Node::Ternary {
            first,
            second,
            third,
            op,
        } = self
            && *op == bop
        {
            Some((*first, *second, *third))
        } else {
            None
        }
    }

    pub fn try_read(&self) -> Option<(NodeID, &[Index])> {
        if let Node::Read { collect, indices } = self {
            Some((*collect, indices))
        } else {
            None
        }
    }

    pub fn try_write(&self) -> Option<(NodeID, NodeID, &[Index])> {
        if let Node::Write {
            collect,
            data,
            indices,
        } = self
        {
            Some((*collect, *data, indices))
        } else {
            None
        }
    }

    pub fn is_zero_constant(&self, constants: &Vec<Constant>) -> bool {
        if let Node::Constant { id } = self
            && constants[id.idx()].is_zero()
        {
            true
        } else {
            false
        }
    }

    pub fn is_zero_dc(&self, dynamic_constants: &Vec<DynamicConstant>) -> bool {
        if let Node::DynamicConstant { id } = self
            && dynamic_constants[id.idx()].try_constant() == Some(0)
        {
            true
        } else {
            false
        }
    }

    pub fn is_one_dc(&self, dynamic_constants: &Vec<DynamicConstant>) -> bool {
        if let Node::DynamicConstant { id } = self
            && dynamic_constants[id.idx()].try_constant() == Some(1)
        {
            true
        } else {
            false
        }
    }

    pub fn is_one_constant(&self, constants: &Vec<Constant>) -> bool {
        if let Node::Constant { id } = self
            && constants[id.idx()].is_one()
        {
            true
        } else {
            false
        }
    }

    pub fn try_control_projection(&self, branch: usize) -> Option<NodeID> {
        if let Node::ControlProjection { control, selection } = self
            && branch == *selection
        {
            Some(*control)
        } else {
            None
        }
    }

    pub fn upper_case_name(&self) -> &'static str {
        match self {
            Node::Start => "Start",
            Node::Region { preds: _ } => "Region",
            Node::If {
                control: _,
                cond: _,
            } => "If",
            Node::Match { control: _, sum: _ } => "Match",
            Node::Fork {
                control: _,
                factors: _,
            } => "Fork",
            Node::Join { control: _ } => "Join",
            Node::Phi {
                control: _,
                data: _,
            } => "Phi",
            Node::ThreadID {
                control: _,
                dimension: _,
            } => "ThreadID",
            Node::Reduce {
                control: _,
                init: _,
                reduct: _,
            } => "Reduce",
            Node::Return {
                control: _,
                data: _,
            } => "Return",
            Node::Parameter { index: _ } => "Parameter",
            Node::DynamicConstant { id: _ } => "DynamicConstant",
            Node::Constant { id: _ } => "Constant",
            Node::Unary { input: _, op } => op.upper_case_name(),
            Node::Binary {
                left: _,
                right: _,
                op,
            } => op.upper_case_name(),
            Node::Ternary {
                first: _,
                second: _,
                third: _,
                op,
            } => op.upper_case_name(),
            Node::Call {
                control: _,
                function: _,
                dynamic_constants: _,
                args: _,
            } => "Call",
            Node::IntrinsicCall {
                intrinsic: _,
                args: _,
            } => "Intrinsic",
            Node::LibraryCall {
                library_function: _,
                args: _,
                ty: _,
                device: _,
            } => "Library",
            Node::Read {
                collect: _,
                indices: _,
            } => "Read",
            Node::Write {
                collect: _,
                data: _,
                indices: _,
            } => "Write",
            Node::ControlProjection {
                control: _,
                selection: _,
            } => "ControlProjection",
            Node::DataProjection {
                data: _,
                selection: _,
            } => "DataProjection",
            Node::Undef { ty: _ } => "Undef",
        }
    }

    pub fn lower_case_name(&self) -> &'static str {
        match self {
            Node::Start => "start",
            Node::Region { preds: _ } => "region",
            Node::If {
                control: _,
                cond: _,
            } => "if",
            Node::Match { control: _, sum: _ } => "match",
            Node::Fork {
                control: _,
                factors: _,
            } => "fork",
            Node::Join { control: _ } => "join",
            Node::Phi {
                control: _,
                data: _,
            } => "phi",
            Node::ThreadID {
                control: _,
                dimension: _,
            } => "thread_id",
            Node::Reduce {
                control: _,
                init: _,
                reduct: _,
            } => "reduce",
            Node::Return {
                control: _,
                data: _,
            } => "return",
            Node::Parameter { index: _ } => "parameter",
            Node::DynamicConstant { id: _ } => "dynamic_constant",
            Node::Constant { id: _ } => "constant",
            Node::Unary { input: _, op } => op.lower_case_name(),
            Node::Binary {
                left: _,
                right: _,
                op,
            } => op.lower_case_name(),
            Node::Ternary {
                first: _,
                second: _,
                third: _,
                op,
            } => op.lower_case_name(),
            Node::Call {
                control: _,
                function: _,
                dynamic_constants: _,
                args: _,
            } => "call",
            Node::IntrinsicCall {
                intrinsic: _,
                args: _,
            } => "intrinsic",
            Node::LibraryCall {
                library_function: _,
                args: _,
                ty: _,
                device: _,
            } => "library",
            Node::Read {
                collect: _,
                indices: _,
            } => "read",
            Node::Write {
                collect: _,
                data: _,
                indices: _,
            } => "write",
            Node::ControlProjection {
                control: _,
                selection: _,
            } => "control_projection",
            Node::DataProjection {
                data: _,
                selection: _,
            } => "data_projection",
            Node::Undef { ty: _ } => "undef",
        }
    }

    pub fn is_control(&self) -> bool {
        self.is_start()
            || self.is_region()
            || self.is_if()
            || self.is_match()
            || self.is_fork()
            || self.is_join()
            || self.is_return()
            || self.is_control_projection()
    }
}

impl UnaryOperator {
    pub fn upper_case_name(&self) -> &'static str {
        match self {
            UnaryOperator::Not => "Not",
            UnaryOperator::Neg => "Neg",
            UnaryOperator::Cast(_) => "Cast",
        }
    }

    pub fn lower_case_name(&self) -> &'static str {
        match self {
            UnaryOperator::Not => "not",
            UnaryOperator::Neg => "neg",
            UnaryOperator::Cast(_) => "cast",
        }
    }
}

impl BinaryOperator {
    pub fn upper_case_name(&self) -> &'static str {
        match self {
            BinaryOperator::Add => "Add",
            BinaryOperator::Sub => "Sub",
            BinaryOperator::Mul => "Mul",
            BinaryOperator::Div => "Div",
            BinaryOperator::Rem => "Rem",
            BinaryOperator::LT => "LT",
            BinaryOperator::LTE => "LTE",
            BinaryOperator::GT => "GT",
            BinaryOperator::GTE => "GTE",
            BinaryOperator::EQ => "EQ",
            BinaryOperator::NE => "NE",
            BinaryOperator::Or => "Or",
            BinaryOperator::And => "And",
            BinaryOperator::Xor => "Xor",
            BinaryOperator::LSh => "LSh",
            BinaryOperator::RSh => "RSh",
        }
    }

    pub fn lower_case_name(&self) -> &'static str {
        match self {
            BinaryOperator::Add => "add",
            BinaryOperator::Sub => "sub",
            BinaryOperator::Mul => "mul",
            BinaryOperator::Div => "div",
            BinaryOperator::Rem => "rem",
            BinaryOperator::LT => "lt",
            BinaryOperator::LTE => "lte",
            BinaryOperator::GT => "gt",
            BinaryOperator::GTE => "gte",
            BinaryOperator::EQ => "eq",
            BinaryOperator::NE => "ne",
            BinaryOperator::Or => "or",
            BinaryOperator::And => "and",
            BinaryOperator::Xor => "xor",
            BinaryOperator::LSh => "lsh",
            BinaryOperator::RSh => "rsh",
        }
    }
}

impl TernaryOperator {
    pub fn upper_case_name(&self) -> &'static str {
        match self {
            TernaryOperator::Select => "Select",
        }
    }

    pub fn lower_case_name(&self) -> &'static str {
        match self {
            TernaryOperator::Select => "select",
        }
    }
}

impl Intrinsic {
    pub fn parse<'a>(name: &'a str) -> Option<Self> {
        match name {
            "abs" => Some(Intrinsic::Abs),
            "acos" => Some(Intrinsic::ACos),
            "acosh" => Some(Intrinsic::ACosh),
            "asin" => Some(Intrinsic::ASin),
            "asinh" => Some(Intrinsic::ASinh),
            "atan" => Some(Intrinsic::ATan),
            "atan2" => Some(Intrinsic::ATan2),
            "atanh" => Some(Intrinsic::ATanh),
            "cbrt" => Some(Intrinsic::Cbrt),
            "ceil" => Some(Intrinsic::Ceil),
            "cos" => Some(Intrinsic::Cos),
            "cosh" => Some(Intrinsic::Cosh),
            "exp" => Some(Intrinsic::Exp),
            "exp2" => Some(Intrinsic::Exp2),
            "exp_m1" => Some(Intrinsic::ExpM1),
            "floor" => Some(Intrinsic::Floor),
            "ln" => Some(Intrinsic::Ln),
            "ln_1p" => Some(Intrinsic::Ln1P),
            "log" => Some(Intrinsic::Log),
            "log10" => Some(Intrinsic::Log10),
            "log2" => Some(Intrinsic::Log2),
            "max" => Some(Intrinsic::Max),
            "min" => Some(Intrinsic::Min),
            "pow" => Some(Intrinsic::Pow),
            "powf" => Some(Intrinsic::Powf),
            "powi" => Some(Intrinsic::Powi),
            "round" => Some(Intrinsic::Round),
            "sin" => Some(Intrinsic::Sin),
            "sinh" => Some(Intrinsic::Sinh),
            "sqrt" => Some(Intrinsic::Sqrt),
            "tan" => Some(Intrinsic::Tan),
            "tanh" => Some(Intrinsic::Tanh),
            _ => None,
        }
    }

    pub fn upper_case_name(&self) -> &'static str {
        match self {
            Intrinsic::Abs => "Abs",
            Intrinsic::ACos => "Acos",
            Intrinsic::ACosh => "Acosh",
            Intrinsic::ASin => "Asin",
            Intrinsic::ASinh => "Asinh",
            Intrinsic::ATan => "Atan",
            Intrinsic::ATan2 => "Atan2",
            Intrinsic::ATanh => "Atanh",
            Intrinsic::Cbrt => "Cbrt",
            Intrinsic::Ceil => "Ceil",
            Intrinsic::Cos => "Cos",
            Intrinsic::Cosh => "Cosh",
            Intrinsic::Exp => "Exp",
            Intrinsic::Exp2 => "Exp2",
            Intrinsic::ExpM1 => "Exp_m1",
            Intrinsic::Floor => "Floor",
            Intrinsic::Ln => "Ln",
            Intrinsic::Ln1P => "Ln_1p",
            Intrinsic::Log => "Log",
            Intrinsic::Log10 => "Log10",
            Intrinsic::Log2 => "Log2",
            Intrinsic::Max => "Max",
            Intrinsic::Min => "Min",
            Intrinsic::Pow => "Pow",
            Intrinsic::Powf => "Powf",
            Intrinsic::Powi => "Powi",
            Intrinsic::Round => "Round",
            Intrinsic::Sin => "Sin",
            Intrinsic::Sinh => "Sinh",
            Intrinsic::Sqrt => "Sqrt",
            Intrinsic::Tan => "Tan",
            Intrinsic::Tanh => "Tanh",
        }
    }

    pub fn lower_case_name(&self) -> &'static str {
        match self {
            Intrinsic::Abs => "abs",
            Intrinsic::ACos => "acos",
            Intrinsic::ACosh => "acosh",
            Intrinsic::ASin => "asin",
            Intrinsic::ASinh => "asinh",
            Intrinsic::ATan => "atan",
            Intrinsic::ATan2 => "atan2",
            Intrinsic::ATanh => "atanh",
            Intrinsic::Cbrt => "cbrt",
            Intrinsic::Ceil => "ceil",
            Intrinsic::Cos => "cos",
            Intrinsic::Cosh => "cosh",
            Intrinsic::Exp => "exp",
            Intrinsic::Exp2 => "exp2",
            Intrinsic::ExpM1 => "exp_m1",
            Intrinsic::Floor => "floor",
            Intrinsic::Ln => "ln",
            Intrinsic::Ln1P => "ln_1p",
            Intrinsic::Log => "log",
            Intrinsic::Log10 => "log10",
            Intrinsic::Log2 => "log2",
            Intrinsic::Max => "max",
            Intrinsic::Min => "min",
            Intrinsic::Pow => "pow",
            Intrinsic::Powf => "powf",
            Intrinsic::Powi => "powi",
            Intrinsic::Round => "round",
            Intrinsic::Sin => "sin",
            Intrinsic::Sinh => "sinh",
            Intrinsic::Sqrt => "sqrt",
            Intrinsic::Tan => "tan",
            Intrinsic::Tanh => "tanh",
        }
    }
}

impl Device {
    pub fn name(&self) -> &'static str {
        match self {
            Device::LLVM => "cpu",
            Device::CUDA => "cuda",
            Device::AsyncRust => "rt",
        }
    }
}

/*
 * Helper function to tell if two lists of indices have the same structure.
 */
pub fn indices_structurally_equivalent(indices1: &[Index], indices2: &[Index]) -> bool {
    if indices1.len() == indices2.len() {
        let mut equiv = true;
        for pair in zip(indices1, indices2) {
            equiv = equiv
                && match pair {
                    (Index::Field(idx1), Index::Field(idx2)) => idx1 == idx2,
                    (Index::Variant(idx1), Index::Variant(idx2)) => idx1 == idx2,
                    (Index::Position(ref pos1), Index::Position(ref pos2)) => {
                        assert_eq!(pos1.len(), pos2.len());
                        true
                    }
                    _ => false,
                };
        }
        equiv
    } else {
        false
    }
}

/*
 * Helper function to determine if two lists of indices may overlap.
 */
pub fn indices_may_overlap(indices1: &[Index], indices2: &[Index]) -> bool {
    for pair in zip(indices1, indices2) {
        match pair {
            // Check that the field numbers are the same.
            (Index::Field(idx1), Index::Field(idx2)) => {
                if idx1 != idx2 {
                    return false;
                }
            }
            // Variant indices always may overlap, since it's the same
            // underlying memory. Position indices always may overlap, since the
            // indexing nodes may be the same at runtime.
            (Index::Variant(_), Index::Variant(_)) | (Index::Position(_), Index::Position(_)) => {}
            _ => panic!(),
        }
    }
    // `zip` will exit as soon as either iterator is done - two sets of indices
    // may overlap when one indexes a larger sub-value than the other.
    true
}

/*
 * Helper function to determine if a list of indices A definitely contains a
 * list of indices B.
 */
pub fn indices_contain_other_indices(indices_a: &[Index], indices_b: &[Index]) -> bool {
    if indices_a.len() > indices_b.len() {
        return false;
    }

    for (idx1, idx2) in zip(indices_a, indices_b) {
        if idx1 != idx2 {
            return false;
        }
    }

    true
}

/*
 * Rust things to make newtyped IDs usable.
 */

pub trait ID: Clone + Eq + Ord + std::hash::Hash + Copy {
    fn new(x: usize) -> Self;
    fn idx(&self) -> usize;
}

#[macro_export]
macro_rules! define_id_type {
    ($x: ident) => {
        #[derive(
            Debug,
            Default,
            Clone,
            Copy,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            serde::Serialize,
            serde::Deserialize,
        )]
        pub struct $x(u32);

        impl ID for $x {
            fn new(x: usize) -> Self {
                $x(x as u32)
            }

            fn idx(&self) -> usize {
                self.0 as usize
            }
        }
    };
}

#[macro_export]
macro_rules! define_dual_id_type {
    ($x: ident) => {
        #[derive(
            Debug,
            Default,
            Clone,
            Copy,
            PartialEq,
            Eq,
            Hash,
            PartialOrd,
            Ord,
            serde::Serialize,
            serde::Deserialize,
        )]
        pub struct $x(u32, u32);

        impl $x {
            pub fn new(x: usize, y: usize) -> Self {
                $x(x as u32, y as u32)
            }

            pub fn idx_0(&self) -> usize {
                self.0 as usize
            }

            pub fn idx_1(&self) -> usize {
                self.1 as usize
            }
        }
    };
}

define_id_type!(FunctionID);
define_id_type!(NodeID);
define_id_type!(TypeID);
define_id_type!(ConstantID);
define_id_type!(DynamicConstantID);
define_id_type!(LabelID);
