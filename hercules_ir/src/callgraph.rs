use crate::*;

/*
 * Custom type for an immutable call graph.
 */
#[derive(Debug, Clone)]
pub struct CallGraph {
    first_callees: Vec<u32>,
    callees: Vec<FunctionID>,
    first_callers: Vec<u32>,
    callers: Vec<FunctionID>,
}

impl CallGraph {
    pub fn num_callees(&self, id: FunctionID) -> usize {
        if id.idx() + 1 < self.first_callees.len() {
            self.first_callees[id.idx() + 1] as usize - self.first_callees[id.idx()] as usize
        } else {
            self.callees.len() - self.first_callees[id.idx()] as usize
        }
    }

    pub fn get_callees(&self, id: FunctionID) -> &[FunctionID] {
        let first_callee = self.first_callees[id.idx()] as usize;
        let num_callees = self.num_callees(id) as usize;
        &self.callees[first_callee..first_callee + num_callees]
    }

    pub fn num_callers(&self, id: FunctionID) -> usize {
        if id.idx() + 1 < self.first_callers.len() {
            self.first_callers[id.idx() + 1] as usize - self.first_callers[id.idx()] as usize
        } else {
            self.callers.len() - self.first_callers[id.idx()] as usize
        }
    }

    pub fn get_callers(&self, id: FunctionID) -> &[FunctionID] {
        let first_caller = self.first_callers[id.idx()] as usize;
        let num_callers = self.num_callers(id) as usize;
        &self.callers[first_caller..first_caller + num_callers]
    }

    pub fn num_functions(&self) -> usize {
        self.first_callees.len()
    }

    pub fn topo(&self) -> Vec<FunctionID> {
        let mut num_calls: Vec<usize> = (0..self.num_functions())
            .map(|idx| self.num_callees(FunctionID::new(idx)))
            .collect();
        let mut no_calls_stack: Vec<FunctionID> = num_calls
            .iter()
            .enumerate()
            .filter(|(_, num)| **num == 0)
            .map(|(idx, _)| FunctionID::new(idx))
            .collect();
        let mut topo = vec![];
        while let Some(no_call_func) = no_calls_stack.pop() {
            topo.push(no_call_func);
            for caller in self.get_callers(no_call_func) {
                num_calls[caller.idx()] -= 1;
                if num_calls[caller.idx()] == 0 {
                    no_calls_stack.push(*caller);
                }
            }
        }

        // Mutual recursion is not currently supported, so assert that a
        // topological sort exists.
        assert_eq!(
            topo.len(),
            self.num_functions(),
            "PANIC: Found mutual recursion in Hercules IR."
        );
        topo
    }
}

/*
 * Top level function to calculate the call graph of a Hercules module.
 */
pub fn callgraph(functions: &Vec<Function>) -> CallGraph {
    // Step 1: collect the functions called in each function.
    let callee_functions: Vec<Vec<FunctionID>> = functions
        .iter()
        .map(|func| {
            let mut called: Vec<_> = func
                .nodes
                .iter()
                .filter_map(|node| {
                    if let Node::Call {
                        control: _,
                        function,
                        dynamic_constants: _,
                        args: _,
                    } = node
                    {
                        Some(*function)
                    } else {
                        None
                    }
                })
                .collect();
            called.sort_unstable();
            called.dedup();
            called
        })
        .collect();

    // Step 2: collect the functions calling each function.
    let mut caller_functions = vec![vec![]; callee_functions.len()];
    for (caller_idx, callees) in callee_functions.iter().enumerate() {
        let caller_id = FunctionID::new(caller_idx);
        for callee in callees {
            caller_functions[callee.idx()].push(caller_id);
        }
    }

    // Step 3: pack callee/caller info into CallGraph structure.
    let mut callgraph = CallGraph {
        first_callees: vec![],
        callees: vec![],
        first_callers: vec![],
        callers: vec![],
    };
    for callees in callee_functions {
        callgraph.first_callees.push(callgraph.callees.len() as u32);
        callgraph.callees.extend(callees);
    }
    for callers in caller_functions {
        callgraph.first_callers.push(callgraph.callers.len() as u32);
        callgraph.callers.extend(callers);
    }

    callgraph
}
