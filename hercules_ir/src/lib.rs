#![feature(
    coroutines,
    coroutine_trait,
    let_chains,
    if_let_guard,
    stmt_expr_attributes,
    iter_intersperse
)]

pub mod build;
pub mod callgraph;
pub mod collections;
pub mod dataflow;
pub mod dc_normalization;
pub mod def_use;
pub mod device;
pub mod dom;
pub mod dot;
pub mod einsum;
pub mod fork_join_analysis;
pub mod ir;
pub mod loops;
pub mod parse;
pub mod subgraph;
pub mod typecheck;
pub mod verify;

pub use crate::build::*;
pub use crate::callgraph::*;
pub use crate::collections::*;
pub use crate::dataflow::*;
pub use crate::dc_normalization::*;
pub use crate::def_use::*;
pub use crate::device::*;
pub use crate::dom::*;
pub use crate::dot::*;
pub use crate::einsum::*;
pub use crate::fork_join_analysis::*;
pub use crate::ir::*;
pub use crate::loops::*;
pub use crate::parse::*;
pub use crate::subgraph::*;
pub use crate::typecheck::*;
pub use crate::verify::*;
