use std::collections::HashMap;
use std::iter::zip;
use std::ops::Deref;

use crate::*;

use self::TypeSemilattice::*;

/*
 * Enum for type semilattice.
 */
#[derive(Eq, Clone, Debug)]
enum TypeSemilattice {
    Unconstrained,
    Concrete(TypeID),
    Error(String),
}

impl TypeSemilattice {
    fn is_error(&self) -> bool {
        if let Error(_) = self {
            true
        } else {
            false
        }
    }
}

/* Define custom PartialEq, so that dataflow will terminate right away if there
 * are errors.
 */
impl PartialEq for TypeSemilattice {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Unconstrained, Unconstrained) => true,
            (Concrete(id1), Concrete(id2)) => id1 == id2,
            (Error(_), Error(_)) => true,
            _ => false,
        }
    }
}

impl Semilattice for TypeSemilattice {
    fn meet(a: &Self, b: &Self) -> Self {
        match (a, b) {
            (Unconstrained, b) => b.clone(),
            (a, Unconstrained) => a.clone(),
            (Concrete(id1), Concrete(id2)) => {
                if id1 == id2 {
                    Concrete(*id1)
                } else {
                    // Error will only allocate when a type error has occurred.
                    // In that case, we're less concerned about speed of the
                    // compiler, and more allocations are acceptable.
                    Error(format!(
                        "Couldn't reconcile two different concrete types, with IDs {} and {}.",
                        id1.idx(),
                        id2.idx()
                    ))
                }
            }
            (Error(msg), _) => Error(msg.clone()),
            (_, Error(msg)) => Error(msg.clone()),
        }
    }

    fn bottom() -> Self {
        // Strings don't allocate unless they actually contain characters, so
        // this is cheap.
        Error(String::new())
    }

    fn top() -> Self {
        Unconstrained
    }
}

pub type ModuleTyping = Vec<Vec<TypeID>>;

/*
 * Top level typecheck function. Typechecking is a module-wide analysis.
 * Returns a type for every node in every function.
 */
pub fn typecheck(
    functions: &Vec<Function>,
    types: &mut Vec<Type>,
    constants: &Vec<Constant>,
    dynamic_constants: &mut Vec<DynamicConstant>,
    reverse_postorders: &Vec<Vec<NodeID>>,
) -> Result<ModuleTyping, String> {
    // Step 1: assemble a reverse type map. This is needed to get or create the
    // ID of potentially new types. Break down module into references to
    // individual elements at this point, so that borrows don't overlap each
    // other.
    let mut reverse_type_map: HashMap<Type, TypeID> = types
        .iter()
        .enumerate()
        .map(|(idx, ty)| (ty.clone(), TypeID::new(idx)))
        .collect();
    let mut reverse_dynamic_constant_map: HashMap<DynamicConstant, DynamicConstantID> =
        dynamic_constants
            .iter()
            .enumerate()
            .map(|(idx, ty)| (ty.clone(), DynamicConstantID::new(idx)))
            .collect();

    // Step 2: run dataflow. This is an occurrence of dataflow where the flow
    // function performs a non-associative operation on the predecessor "out"
    // values.
    let results: Vec<Vec<TypeSemilattice>> = zip(functions, reverse_postorders)
        .map(|(function, reverse_postorder)| {
            forward_dataflow(function, reverse_postorder, |inputs, id| {
                typeflow(
                    inputs,
                    id,
                    function,
                    functions,
                    types,
                    constants,
                    dynamic_constants,
                    &mut reverse_type_map,
                    &mut reverse_dynamic_constant_map,
                )
            })
        })
        .collect();

    // Step 3: convert the individual type lattice values into lists of
    // concrete type values, or a single error.
    results
        .into_iter()
        // For each type list, we want to convert its element TypeSemilattices
        // into Result<TypeID, String>.
        .map(|result| {
            result
                .into_iter()
                // For each TypeSemilattice, convert into Result<TypeID, String>.
                .map(|x| match x {
                    Unconstrained => Err(String::from("Found unconstrained type in program.")),
                    Concrete(id) => Ok(id),
                    Error(msg) => Err(msg.clone()),
                })
                .collect()
        })
        .collect()
}

/*
 * Flow function for typechecking.
 */
fn typeflow(
    inputs: &[&TypeSemilattice],
    node_id: NodeID,
    function: &Function,
    functions: &Vec<Function>,
    types: &mut Vec<Type>,
    constants: &Vec<Constant>,
    dynamic_constants: &mut Vec<DynamicConstant>,
    reverse_type_map: &mut HashMap<Type, TypeID>,
    reverse_dynamic_constant_map: &mut HashMap<DynamicConstant, DynamicConstantID>,
) -> TypeSemilattice {
    // Whenever we want to reference a specific type (for example, for the
    // start node), we need to get its type ID. This helper function gets the
    // ID if it already exists. If the type doesn't already exist, the helper
    // adds it to the type intern list.
    let get_type_id =
        |ty: Type, types: &mut Vec<Type>, reverse_type_map: &mut HashMap<Type, TypeID>| -> TypeID {
            if let Some(id) = reverse_type_map.get(&ty) {
                *id
            } else {
                let id = TypeID::new(reverse_type_map.len());
                reverse_type_map.insert(ty.clone(), id);
                types.push(ty);
                id
            }
        };

    // We need to make sure dynamic constant parameters reference valid dynamic
    // constant parameters of the current function. This involves traversing a
    // given dynamic constant expression to determine that all referenced
    // parameter dynamic constants are valid.
    fn check_dynamic_constants(
        root: DynamicConstantID,
        dynamic_constants: &Vec<DynamicConstant>,
        num_parameters: u32,
    ) -> bool {
        match &dynamic_constants[root.idx()] {
            DynamicConstant::Constant(_) => true,
            DynamicConstant::Parameter(idx) => *idx < num_parameters as usize,
            DynamicConstant::Add(xs)
            | DynamicConstant::Mul(xs)
            | DynamicConstant::Min(xs)
            | DynamicConstant::Max(xs) => xs
                .iter()
                .all(|dc| check_dynamic_constants(*dc, dynamic_constants, num_parameters)),
            DynamicConstant::Sub(x, y)
            | DynamicConstant::Div(x, y)
            | DynamicConstant::Rem(x, y) => {
                check_dynamic_constants(*x, dynamic_constants, num_parameters)
                    && check_dynamic_constants(*y, dynamic_constants, num_parameters)
            }
        }
    }

    // Each node requires different type logic. This unfortunately results in a
    // large match statement. Oh well. Each arm returns the lattice value for
    // the "out" type of the node.
    match &function.nodes[node_id.idx()] {
        Node::Start => {
            if inputs.len() != 0 {
                return Error(String::from("Start node must have zero inputs."));
            }

            // The start node is the producer of the control token.
            Concrete(get_type_id(Type::Control, types, reverse_type_map))
        }
        Node::Region { preds: _ } => {
            if inputs.len() == 0 {
                return Error(String::from("Region node must have at least one input."));
            }

            let mut meet = inputs[0].clone();
            for l in inputs[1..].iter() {
                meet = TypeSemilattice::meet(&meet, l);
            }

            // Only special case is if concrete type is non-control. In
            // this case, we override that concrete type with an error,
            // since the input types must all be control types. Any other
            // lattice value can be returned as-is.
            if let Concrete(id) = meet {
                if !types[id.idx()].is_control() {
                    return Error(String::from(
                        "Region node's input type cannot be non-control.",
                    ));
                }
            }

            meet
        }
        Node::If {
            control: _,
            cond: _,
        } => {
            if inputs.len() != 2 {
                return Error(String::from("If node must have exactly two inputs."));
            }

            // Check type of data input first, since we may return while
            // checking control input.
            if let Concrete(id) = inputs[1] {
                if !types[id.idx()].is_bool() {
                    return Error(String::from(
                        "If node's condition input cannot have non-boolean type.",
                    ));
                }
            } else if inputs[1].is_error() {
                // If an input has an error lattice value, it must be
                // propagated.
                return inputs[1].clone();
            }

            if let Concrete(id) = inputs[0] {
                if !types[id.idx()].is_control() {
                    return Error(String::from(
                        "If node's control input cannot have non-control type.",
                    ));
                }
            }

            inputs[0].clone()
        }
        Node::Fork {
            control: _,
            factors,
        } => {
            if inputs.len() != 1 {
                return Error(String::from("Fork node must have exactly one input."));
            }

            for factor in factors.iter() {
                if !check_dynamic_constants(
                    *factor,
                    dynamic_constants,
                    function.num_dynamic_constants,
                ) {
                    return Error(String::from("Referenced parameter dynamic constant is not a valid dynamic constant parameter for the current function."));
                }
            }

            if let Concrete(id) = inputs[0] {
                if let Type::Control = &types[id.idx()] {
                    // Out type is control type.
                    return Concrete(*id);
                } else {
                    return Error(String::from(
                        "Fork node's input cannot have non-control type.",
                    ));
                }
            }

            inputs[0].clone()
        }
        Node::Join { control: _ } => {
            if inputs.len() != 1 {
                return Error(String::from("Join node must have exactly two inputs."));
            }

            // If the control input isn't concrete yet, we can't assemble a
            // concrete output type, so just return the control input non-
            // concrete type.
            if let Concrete(control_id) = inputs[0] {
                if let Type::Control = &types[control_id.idx()] {
                    return Concrete(*control_id);
                } else {
                    return Error(String::from(
                        "Join node's first input cannot have non-control type.",
                    ));
                }
            }

            inputs[0].clone()
        }
        Node::Phi {
            control: _,
            data: _,
        } => {
            if inputs.len() < 2 {
                return Error(String::from("Phi node must have at least two inputs."));
            }

            // Check type of control input first, since this may produce an
            // error.
            if let Concrete(id) = inputs[inputs.len() - 1] {
                if !types[id.idx()].is_control() {
                    return Error(String::from(
                        "Phi node's control input cannot have non-control type.",
                    ));
                }
            } else if inputs[inputs.len() - 1].is_error() {
                // If an input has an error lattice value, it must be
                // propagated.
                return inputs[inputs.len() - 1].clone();
            }

            // Output type of phi node is same type as every data input.
            let mut meet = inputs[0].clone();
            for l in inputs[1..inputs.len() - 1].iter() {
                if let Concrete(id) = l {
                    if types[id.idx()].is_control() {
                        return Error(String::from(
                            "Phi node's data inputs cannot have control type.",
                        ));
                    }
                }
                meet = TypeSemilattice::meet(&meet, l);
            }

            meet
        }
        Node::ThreadID {
            control: _,
            dimension: _,
        } => {
            if inputs.len() != 1 {
                return Error(String::from("ThreadID node must have exactly one input."));
            }

            // If type of control input is an error, we must propagate it.
            if inputs[0].is_error() {
                return inputs[0].clone();
            }

            // Type of thread ID is always u64.
            Concrete(get_type_id(
                Type::UnsignedInteger64,
                types,
                reverse_type_map,
            ))
        }
        Node::Reduce {
            control: _,
            init: _,
            reduct: _,
        } => {
            if inputs.len() != 3 {
                return Error(String::from("Reduce node must have exactly two inputs."));
            }

            if let (Concrete(control_id), Concrete(init_id), Concrete(reduct_id)) =
                (inputs[0], inputs[1], inputs[2])
            {
                // Check control input is control.
                if let Type::Control = types[control_id.idx()] {
                } else {
                    return Error(String::from(
                        "Reduce node's control input must have control type.",
                    ));
                }

                // Check init input isn't control.
                if let Type::Control = types[init_id.idx()] {
                    return Error(String::from(
                        "Reduce node's initialization input must not have control type.",
                    ));
                }

                // Check reduct input isn't control.
                if let Type::Control = types[reduct_id.idx()] {
                    return Error(String::from(
                        "Reduce node's reduction input must not have control type.",
                    ));
                }

                TypeSemilattice::meet(inputs[1], inputs[2])
            } else if inputs[0].is_error() {
                inputs[0].clone()
            } else {
                TypeSemilattice::meet(inputs[1], inputs[2])
            }
        }
        Node::Return {
            control: _,
            data: _,
        } => {
            if inputs.len() < 1 {
                return Error(String::from("Return node must have at least one input."));
            }

            // Check type of control input first, since this may produce an
            // error.
            if let Concrete(id) = inputs[0] {
                if !types[id.idx()].is_control() {
                    return Error(String::from(
                        "Return node's control input cannot have non-control type.",
                    ));
                }
            } else if inputs[0].is_error() {
                // If an input has an error lattice value, it must be
                // propagated.
                return inputs[0].clone();
            }

            for (idx, (input, return_type)) in inputs[1..]
                .iter()
                .zip(function.return_types.iter())
                .enumerate()
            {
                if let Concrete(id) = input {
                    if *id != *return_type {
                        return Error(format!("Return node's data input at index {} does not match function's return type.", idx));
                    }
                } else if input.is_error() {
                    return (*input).clone();
                }
            }

            Concrete(get_type_id(
                Type::Product(Box::new([])),
                types,
                reverse_type_map,
            ))
        }
        Node::Parameter { index } => {
            if inputs.len() != 1 {
                return Error(String::from("Parameter node must have one input."));
            }

            if *index >= function.param_types.len() {
                return Error(String::from("Parameter node must reference an index corresponding to an existing function argument."));
            }

            // Type of parameter is stored directly in function.
            let param_id = function.param_types[*index];

            Concrete(param_id)
        }
        Node::Constant { id } => {
            if inputs.len() != 1 {
                return Error(String::from("Constant node must have one input."));
            }

            // Most constants' type are obvious.
            match constants[id.idx()] {
                Constant::Boolean(_) => {
                    Concrete(get_type_id(Type::Boolean, types, reverse_type_map))
                }
                Constant::Integer8(_) => {
                    Concrete(get_type_id(Type::Integer8, types, reverse_type_map))
                }
                Constant::Integer16(_) => {
                    Concrete(get_type_id(Type::Integer16, types, reverse_type_map))
                }
                Constant::Integer32(_) => {
                    Concrete(get_type_id(Type::Integer32, types, reverse_type_map))
                }
                Constant::Integer64(_) => {
                    Concrete(get_type_id(Type::Integer64, types, reverse_type_map))
                }
                Constant::UnsignedInteger8(_) => {
                    Concrete(get_type_id(Type::UnsignedInteger8, types, reverse_type_map))
                }
                Constant::UnsignedInteger16(_) => Concrete(get_type_id(
                    Type::UnsignedInteger16,
                    types,
                    reverse_type_map,
                )),
                Constant::UnsignedInteger32(_) => Concrete(get_type_id(
                    Type::UnsignedInteger32,
                    types,
                    reverse_type_map,
                )),
                Constant::UnsignedInteger64(_) => Concrete(get_type_id(
                    Type::UnsignedInteger64,
                    types,
                    reverse_type_map,
                )),
                Constant::Float32(_) => {
                    Concrete(get_type_id(Type::Float32, types, reverse_type_map))
                }
                Constant::Float64(_) => {
                    Concrete(get_type_id(Type::Float64, types, reverse_type_map))
                }
                // Product, summation, and array constants are exceptions. they
                // all explicitly store their type.
                Constant::Product(id, _) => {
                    if let Type::Product(_) = types[id.idx()] {
                        Concrete(id)
                    } else {
                        Error(String::from(
                            "Product constant must store an explicit product type.",
                        ))
                    }
                }
                Constant::Summation(id, _, _) => {
                    if let Type::Summation(_) = types[id.idx()] {
                        Concrete(id)
                    } else {
                        Error(String::from(
                            "Summation constant must store an explicit summation type.",
                        ))
                    }
                }
                Constant::Array(id) => {
                    if let Type::Array(_, _) = &types[id.idx()] {
                        Concrete(id)
                    } else {
                        Error(String::from(
                            "Array constant must store an explicit array type.",
                        ))
                    }
                }
            }
        }
        Node::DynamicConstant { id } => {
            if inputs.len() != 1 {
                return Error(String::from("DynamicConstant node must have one input."));
            }

            if !check_dynamic_constants(*id, dynamic_constants, function.num_dynamic_constants) {
                return Error(String::from("Referenced parameter dynamic constant is not a valid dynamic constant parameter for the current function."));
            }

            // Dynamic constants are always u64.
            Concrete(get_type_id(
                Type::UnsignedInteger64,
                types,
                reverse_type_map,
            ))
        }
        Node::Unary { input: _, op } => {
            if inputs.len() != 1 {
                return Error(String::from("Unary node must have exactly one input."));
            }

            if let Concrete(id) = inputs[0] {
                match op {
                    UnaryOperator::Not => {
                        if !types[id.idx()].is_bool() && !types[id.idx()].is_fixed() {
                            return Error(String::from(
                                "Not unary node input cannot have non-bool and non-fixed type.",
                            ));
                        }
                    }
                    UnaryOperator::Neg => {
                        if types[id.idx()].is_unsigned() {
                            return Error(String::from(
                                "Neg unary node input cannot have unsigned type.",
                            ));
                        }
                        if !types[id.idx()].is_arithmetic() {
                            return Error(String::from(
                                "Neg unary node input cannot have non-arithmetic type.",
                            ));
                        }
                    }
                    UnaryOperator::Cast(dst_id) => {
                        let src_ty = &types[id.idx()];
                        let dst_ty = &types[dst_id.idx()];
                        if cast_compatible(src_ty, dst_ty) {
                            return Concrete(*dst_id);
                        } else {
                            return Error(String::from(
                                "Cast unary node has incompatible input and output types.",
                            ));
                        }
                    }
                }
            }

            inputs[0].clone()
        }
        Node::Binary {
            left: _,
            right: _,
            op,
        } => {
            if inputs.len() != 2 {
                return Error(String::from("Binary node must have exactly two inputs."));
            }

            let input_ty = TypeSemilattice::meet(inputs[0], inputs[1]);

            if let Concrete(id) = input_ty {
                match op {
                    BinaryOperator::Add
                    | BinaryOperator::Sub
                    | BinaryOperator::Mul
                    | BinaryOperator::Div
                    | BinaryOperator::Rem => {
                        if !types[id.idx()].is_arithmetic() {
                            return Error(format!(
                                "{:?} binary node input cannot have non-arithmetic type.",
                                op,
                            ));
                        }
                    }
                    BinaryOperator::LT
                    | BinaryOperator::LTE
                    | BinaryOperator::GT
                    | BinaryOperator::GTE => {
                        if !types[id.idx()].is_arithmetic() {
                            return Error(format!(
                                "{:?} binary node input cannot have non-arithmetic type.",
                                op,
                            ));
                        }

                        // Comparison operators change the input type.
                        return Concrete(get_type_id(Type::Boolean, types, reverse_type_map));
                    }
                    BinaryOperator::EQ | BinaryOperator::NE => {
                        if types[id.idx()].is_control() {
                            return Error(format!(
                                "{:?} binary node input cannot have control type.",
                                op,
                            ));
                        }

                        // Equality operators potentially change the input type.
                        return Concrete(get_type_id(Type::Boolean, types, reverse_type_map));
                    }
                    BinaryOperator::Or | BinaryOperator::And | BinaryOperator::Xor => {
                        if !types[id.idx()].is_fixed() && !types[id.idx()].is_bool() {
                            return Error(format!(
                                "{:?} binary node input cannot have non-fixed type and non-boolean type.",
                                op,
                            ));
                        }
                    }
                    BinaryOperator::LSh | BinaryOperator::RSh => {
                        if !types[id.idx()].is_fixed() {
                            return Error(format!(
                                "{:?} binary node input cannot have non-fixed type.",
                                op,
                            ));
                        }
                    }
                }
            }

            input_ty.clone()
        }
        Node::Ternary {
            first: _,
            second: _,
            third: _,
            op,
        } => {
            if inputs.len() != 3 {
                return Error(String::from("Ternary node must have exactly three inputs."));
            }

            match op {
                TernaryOperator::Select => {
                    if let Concrete(id) = inputs[0] {
                        if !types[id.idx()].is_bool() {
                            return Error(String::from(
                                "Select ternary node input cannot have non-bool condition input.",
                            ));
                        }
                    }

                    TypeSemilattice::meet(inputs[1], inputs[2])
                }
            }
        }
        Node::Call {
            control: _,
            function: callee_id,
            dynamic_constants: dc_args,
            args: _,
        } => {
            let callee = &functions[callee_id.idx()];

            // Check number of run-time arguments.
            if inputs.len() - 1 != callee.param_types.len() {
                return Error(format!(
                    "Call node in {} has {} inputs, but calls a function ({}) with {} parameters.",
                    function.name,
                    inputs.len() - 1,
                    callee.name,
                    callee.param_types.len(),
                ));
            }

            // Check number of dynamic constant arguments.
            if dc_args.len() != callee.num_dynamic_constants as usize {
                return Error(format!(
                    "Call node in {} references {} dynamic constants, but calls a function ({}) expecting {} dynamic constants.",
                    function.name,
                    dc_args.len(),
                    callee.name,
                    callee.num_dynamic_constants
                ));
            }

            for dc_id in dc_args.iter() {
                if !check_dynamic_constants(
                    *dc_id,
                    dynamic_constants,
                    function.num_dynamic_constants,
                ) {
                    return Error(String::from("Referenced parameter dynamic constant is not a valid dynamic constant parameter for the current function."));
                }
            }

            // Construct the substitution object
            let mut subst = DCSubst::new(
                types,
                reverse_type_map,
                dynamic_constants,
                reverse_dynamic_constant_map,
                dc_args,
            );

            // Check argument types.
            for (input, param_ty) in zip(inputs.iter().skip(1), callee.param_types.iter()) {
                let param_ty = subst.type_subst(*param_ty);
                if let Concrete(input_id) = input {
                    if param_ty != *input_id {
                        return Error(String::from(
                            "Call node mismatches argument types with callee function.",
                        ));
                    }
                } else if input.is_error() {
                    // If an input type is an error, we must propagate it.
                    return (*input).clone();
                }
            }

            Concrete(subst.build_return_type(&callee.return_types))
        }
        Node::IntrinsicCall { intrinsic, args: _ } => {
            let num_params = match intrinsic {
                Intrinsic::Abs
                | Intrinsic::ACos
                | Intrinsic::ACosh
                | Intrinsic::ASin
                | Intrinsic::ASinh
                | Intrinsic::ATan
                | Intrinsic::ATanh
                | Intrinsic::Cbrt
                | Intrinsic::Ceil
                | Intrinsic::Cos
                | Intrinsic::Cosh
                | Intrinsic::Exp
                | Intrinsic::Exp2
                | Intrinsic::ExpM1
                | Intrinsic::Floor
                | Intrinsic::Ln
                | Intrinsic::Ln1P
                | Intrinsic::Log10
                | Intrinsic::Log2
                | Intrinsic::Round
                | Intrinsic::Sin
                | Intrinsic::Sinh
                | Intrinsic::Sqrt
                | Intrinsic::Tan
                | Intrinsic::Tanh => 1,
                Intrinsic::ATan2
                | Intrinsic::Log
                | Intrinsic::Max
                | Intrinsic::Min
                | Intrinsic::Pow
                | Intrinsic::Powf
                | Intrinsic::Powi => 2,
            };

            // Check number of run-time arguments
            if inputs.len() != num_params {
                return Error(format!(
                    "Intrinsic {} has {} inputs, but calls an intrinsic with {} parameters.",
                    intrinsic.lower_case_name(),
                    inputs.len(),
                    num_params,
                ));
            }

            // Check argument types. This process depends on the intrinsic
            // since intrinsics can be polymorphic
            // We also return the return type from here
            match intrinsic {
                // Intrinsics that take any numeric type and return the same
                Intrinsic::Abs | Intrinsic::Max | Intrinsic::Min => {
                    if let Concrete(id) = inputs[0] {
                        if types[id.idx()].is_arithmetic() {
                            Concrete(*id)
                        } else {
                            Error(format!(
                                "{} intrinsic cannot have non-numeric input type.",
                                intrinsic.lower_case_name()
                            ))
                        }
                    } else {
                        // Otherwise, propogate errors and unconstrained types
                        (*inputs[0]).clone()
                    }
                }
                // Intrinsics that take any float type and return the same
                Intrinsic::ACos
                | Intrinsic::ACosh
                | Intrinsic::ASin
                | Intrinsic::ASinh
                | Intrinsic::ATan
                | Intrinsic::ATanh
                | Intrinsic::Cbrt
                | Intrinsic::Ceil
                | Intrinsic::Cos
                | Intrinsic::Cosh
                | Intrinsic::Exp
                | Intrinsic::Exp2
                | Intrinsic::ExpM1
                | Intrinsic::Floor
                | Intrinsic::Ln
                | Intrinsic::Ln1P
                | Intrinsic::Log10
                | Intrinsic::Log2
                | Intrinsic::Round
                | Intrinsic::Sin
                | Intrinsic::Sinh
                | Intrinsic::Sqrt
                | Intrinsic::Tan
                | Intrinsic::Tanh => {
                    if let Concrete(id) = inputs[0] {
                        if types[id.idx()].is_float() {
                            Concrete(*id)
                        } else {
                            Error(format!(
                                "{} intrinsic cannot have non-float input type.",
                                intrinsic.lower_case_name()
                            ))
                        }
                    } else {
                        // Otherwise, propogate errors and unconstrained types
                        (*inputs[0]).clone()
                    }
                }
                // Intrinsics that take any two values of the same float type
                // and return the same
                Intrinsic::ATan2 | Intrinsic::Log | Intrinsic::Powf => {
                    let input_ty = TypeSemilattice::meet(inputs[0], inputs[1]);

                    if let Concrete(id) = input_ty {
                        if types[id.idx()].is_float() {
                            Concrete(id)
                        } else {
                            Error(format!(
                                "{} intrinsic cannot have non-float input types.",
                                intrinsic.lower_case_name()
                            ))
                        }
                    } else {
                        // Otherwise, propogate errors and unconstrained types
                        (*inputs[0]).clone()
                    }
                }
                Intrinsic::Pow => {
                    if let Concrete(id) = inputs[0] {
                        if types[id.idx()].is_fixed() {
                            if let Concrete(id) = inputs[1] {
                                if types[id.idx()] != Type::UnsignedInteger32 {
                                    return Error(format!(
                                        "{} intrinsic expects u32 as second argument.",
                                        intrinsic.lower_case_name()
                                    ));
                                }
                            }
                            Concrete(*id)
                        } else {
                            Error(format!(
                                "{} intrinsic cannot have non-integer first argument.",
                                intrinsic.lower_case_name()
                            ))
                        }
                    } else {
                        // Otherwise, propagate errors and unconstrained types
                        (*inputs[0]).clone()
                    }
                }
                Intrinsic::Powi => {
                    if let Concrete(id) = inputs[0] {
                        if types[id.idx()].is_float() {
                            if let Concrete(id) = inputs[1] {
                                if types[id.idx()] != Type::Integer32 {
                                    return Error(format!(
                                        "{} intrinsic expects i32 as second argument.",
                                        intrinsic.lower_case_name()
                                    ));
                                }
                            }
                            Concrete(*id)
                        } else {
                            Error(format!(
                                "{} intrinsic cannot have non-float first argument.",
                                intrinsic.lower_case_name()
                            ))
                        }
                    } else {
                        // Otherwise, propagate errors and unconstrained types
                        (*inputs[0]).clone()
                    }
                }
            }
        }
        Node::LibraryCall {
            library_function: _,
            args: _,
            ty,
            device: _,
        } => Concrete(*ty),
        Node::Read {
            collect: _,
            indices,
        } => {
            if indices.len() == 0 {
                return Error(String::from("Read node must have at least one index."));
            }

            // Traverse the collect input's type tree downwards.
            if let Concrete(mut collect_id) = inputs[0] {
                for index in indices.iter() {
                    match (&types[collect_id.idx()], index) {
                        (Type::Product(fields), Index::Field(field)) => {
                            if *field >= fields.len() {
                                return Error(String::from("Read node's field index must be in the range of the product type being indexed."));
                            }
                            collect_id = fields[*field];
                        }
                        (Type::Summation(variants), Index::Variant(variant)) => {
                            if *variant >= variants.len() {
                                return Error(String::from("Read node's variant index must be in the range of the variant type being indexed."));
                            }
                            collect_id = variants[*variant];
                        }
                        (Type::Array(elem_ty_id, dim_sizes), Index::Position(indices)) => {
                            if dim_sizes.len() != indices.len() {
                                return Error(String::from("Read node's position index must have the same number of dimensions as the array type being indexed."));
                            }
                            collect_id = *elem_ty_id;
                        }
                        _ => {
                            return Error(String::from(
                                "Read node has mismatched input type and indices.",
                            ));
                        }
                    }
                }

                // If successfully traversed, the leaf type is the result.
                return Concrete(collect_id);
            }

            inputs[0].clone()
        }
        Node::Write {
            collect: _,
            data: _,
            indices,
        } => {
            // Traverse the collect input's type tree downwards.
            if let (Concrete(mut collect_id), Concrete(data_id)) = (inputs[0], inputs[1]) {
                for index in indices.iter() {
                    match (&types[collect_id.idx()], index) {
                        (Type::Product(fields), Index::Field(field)) => {
                            if *field >= fields.len() {
                                return Error(String::from("Write node's field index must be in the range of the product type being indexed."));
                            }
                            collect_id = fields[*field];
                        }
                        (Type::Summation(variants), Index::Variant(variant)) => {
                            if *variant >= variants.len() {
                                return Error(String::from("Write node's variant index must be in the range of the variant type being indexed."));
                            }
                            collect_id = variants[*variant];
                        }
                        (Type::Array(elem_ty_id, dim_sizes), Index::Position(indices)) => {
                            if dim_sizes.len() != indices.len() {
                                return Error(String::from("Write node's position index must have the same number of dimensions as the array type being indexed."));
                            }
                            collect_id = *elem_ty_id;
                        }
                        _ => {
                            return Error(String::from(
                                "Write node has mismatched input type and indices.",
                            ));
                        }
                    }
                }

                // The leaf type being indexed must be what's being written.
                if *data_id != collect_id {
                    return Error(String::from(
                        "Write node has mismatched data type and indexed type.",
                    ));
                }
            }

            // No matter what, the type is the type of the collect input.
            inputs[0].clone()
        }
        Node::Match { control: _, sum: _ } => {
            if inputs.len() != 2 {
                return Error(String::from("Match node must have exactly two inputs."));
            }

            // Check sum and control inputs simultaneously, since both need to
            // be concrete to determine a concrete type for a match node.
            if let (Concrete(control_id), Concrete(sum_id)) = (inputs[0], inputs[1]) {
                if let Type::Summation(_) = &types[sum_id.idx()] {
                    if !types[control_id.idx()].is_control() {
                        return Error(String::from(
                            "Match node's control input cannot have non-control type.",
                        ));
                    } else {
                        return inputs[0].clone();
                    }
                } else {
                    return Error(String::from(
                        "Match node's condition input cannot have non-sum type.",
                    ));
                }
            }

            // Otherwise, currently unconstrained, or an error.
            match TypeSemilattice::meet(inputs[0], inputs[1]) {
                TypeSemilattice::Unconstrained => TypeSemilattice::Unconstrained,
                TypeSemilattice::Concrete(_) => TypeSemilattice::Unconstrained,
                TypeSemilattice::Error(msg) => TypeSemilattice::Error(msg),
            }
        }
        Node::ControlProjection {
            control: _,
            selection: _,
        } => {
            // Type is the type of the _if node
            inputs[0].clone()
        }
        Node::DataProjection { data: _, selection } => {
            if let Concrete(type_id) = inputs[0] {
                match &types[type_id.idx()] {
                    Type::MultiReturn(types) => {
                        if *selection >= types.len() {
                            return Error(String::from("Data projection's selection must be in range of the multi-return being indexed"));
                        }
                        return Concrete(types[*selection]);
                    }
                    _ => {
                        return Error(String::from(
                            "Data projection node must read from multi-return value.",
                        ));
                    }
                }
            }

            inputs[0].clone()
        }
        Node::Undef { ty } => TypeSemilattice::Concrete(*ty),
    }
}

/*
 * Determine if a given cast conversion is valid.
 */
pub fn cast_compatible(src_ty: &Type, dst_ty: &Type) -> bool {
    // Can convert between any pair of primitive types, as long as the cast is
    // not from a floating point type to a boolean type.
    src_ty.is_primitive() && dst_ty.is_primitive() && !(src_ty.is_float() && dst_ty.is_bool())
}

/*
 * Data structures and methods for substituting given dynamic constant arguments into the provided
 * types and dynamic constants
 */
struct DCSubst<'a> {
    types: &'a mut Vec<Type>,
    reverse_type_map: &'a mut HashMap<Type, TypeID>,
    dynamic_constants: &'a mut Vec<DynamicConstant>,
    reverse_dynamic_constant_map: &'a mut HashMap<DynamicConstant, DynamicConstantID>,
    dc_args: &'a [DynamicConstantID],
}

impl<'a> DynamicConstantView for DCSubst<'a> {
    fn get_dynconst(&self, id: DynamicConstantID) -> impl Deref<Target = DynamicConstant> + '_ {
        &self.dynamic_constants[id.idx()]
    }

    fn add_dynconst(&mut self, dc: DynamicConstant) -> DynamicConstantID {
        if let Some(id) = self.reverse_dynamic_constant_map.get(&dc) {
            *id
        } else {
            let id = DynamicConstantID::new(self.dynamic_constants.len());
            self.reverse_dynamic_constant_map.insert(dc.clone(), id);
            self.dynamic_constants.push(dc);
            id
        }
    }
}

impl<'a> DCSubst<'a> {
    fn new(
        types: &'a mut Vec<Type>,
        reverse_type_map: &'a mut HashMap<Type, TypeID>,
        dynamic_constants: &'a mut Vec<DynamicConstant>,
        reverse_dynamic_constant_map: &'a mut HashMap<DynamicConstant, DynamicConstantID>,
        dc_args: &'a [DynamicConstantID],
    ) -> Self {
        Self {
            types,
            reverse_type_map,
            dynamic_constants,
            reverse_dynamic_constant_map,
            dc_args,
        }
    }

    fn intern_type(&mut self, ty: Type) -> TypeID {
        if let Some(id) = self.reverse_type_map.get(&ty) {
            *id
        } else {
            let id = TypeID::new(self.types.len());
            self.reverse_type_map.insert(ty.clone(), id);
            self.types.push(ty);
            id
        }
    }

    fn build_return_type(&mut self, tys: &[TypeID]) -> TypeID {
        let tys = tys.iter().map(|t| self.type_subst(*t)).collect();
        self.intern_type(Type::MultiReturn(tys))
    }

    fn type_subst(&mut self, typ: TypeID) -> TypeID {
        match &self.types[typ.idx()] {
            Type::Control
            | Type::Boolean
            | Type::Integer8
            | Type::Integer16
            | Type::Integer32
            | Type::Integer64
            | Type::UnsignedInteger8
            | Type::UnsignedInteger16
            | Type::UnsignedInteger32
            | Type::UnsignedInteger64
            | Type::Float8
            | Type::BFloat16
            | Type::Float32
            | Type::Float64 => typ,
            Type::Product(ts) => {
                let new_ts = ts.clone().iter().map(|t| self.type_subst(*t)).collect();
                self.intern_type(Type::Product(new_ts))
            }
            Type::Summation(ts) => {
                let new_ts = ts.clone().iter().map(|t| self.type_subst(*t)).collect();
                self.intern_type(Type::Summation(new_ts))
            }
            Type::Array(elem, dims) => {
                let elem = *elem;
                let new_dims = dims
                    .clone()
                    .iter()
                    .map(|d| self.dyn_const_subst(*d))
                    .collect();
                let new_elem = self.type_subst(elem);
                self.intern_type(Type::Array(new_elem, new_dims))
            }
            Type::MultiReturn(..) => panic!("A multi-return type should never be substituted"),
        }
    }

    fn dyn_const_subst(&mut self, dyn_const: DynamicConstantID) -> DynamicConstantID {
        match &self.dynamic_constants[dyn_const.idx()] {
            DynamicConstant::Constant(_) => dyn_const,
            DynamicConstant::Parameter(i) => self.dc_args[*i],
            DynamicConstant::Add(xs) => {
                let sxs = xs
                    .clone()
                    .into_iter()
                    .map(|dc| self.dyn_const_subst(dc))
                    .collect();
                self.dc_add(sxs)
            }
            DynamicConstant::Sub(l, r) => {
                let x = *l;
                let y = *r;
                let sx = self.dyn_const_subst(x);
                let sy = self.dyn_const_subst(y);
                self.dc_sub(sx, sy)
            }
            DynamicConstant::Mul(xs) => {
                let sxs = xs
                    .clone()
                    .into_iter()
                    .map(|dc| self.dyn_const_subst(dc))
                    .collect();
                self.dc_mul(sxs)
            }
            DynamicConstant::Div(l, r) => {
                let x = *l;
                let y = *r;
                let sx = self.dyn_const_subst(x);
                let sy = self.dyn_const_subst(y);
                self.dc_div(sx, sy)
            }
            DynamicConstant::Rem(l, r) => {
                let x = *l;
                let y = *r;
                let sx = self.dyn_const_subst(x);
                let sy = self.dyn_const_subst(y);
                self.dc_rem(sx, sy)
            }
            DynamicConstant::Min(xs) => {
                let sxs = xs
                    .clone()
                    .into_iter()
                    .map(|dc| self.dyn_const_subst(dc))
                    .collect();
                self.dc_min(sxs)
            }
            DynamicConstant::Max(xs) => {
                let sxs = xs
                    .clone()
                    .into_iter()
                    .map(|dc| self.dyn_const_subst(dc))
                    .collect();
                self.dc_max(sxs)
            }
        }
    }
}
