use std::collections::BTreeSet;
use std::mem::take;

use crate::*;

/*
 * Top level function to definitively place functions onto devices. A function
 * may store a device placement, but only optionally - this function assigns
 * devices to the rest of the functions.
 */
pub fn device_placement(functions: &Vec<Function>, callgraph: &CallGraph) -> Vec<Device> {
    let mut devices = vec![];

    for (idx, function) in functions.into_iter().enumerate() {
        if let Some(device) = function.device {
            devices.push(device);
        } else if function.entry || callgraph.num_callees(FunctionID::new(idx)) != 0 {
            devices.push(Device::AsyncRust);
        } else {
            devices.push(Device::LLVM);
        }
    }

    devices
}
